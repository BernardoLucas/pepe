package com.mangyBear.Pepe.service.interfaces;

import com.sun.jna.Native;
import com.sun.jna.WString;
import com.sun.jna.platform.win32.Tlhelp32.PROCESSENTRY32.ByReference;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.StdCallLibrary;

public interface Kernel32 extends StdCallLibrary {
	public static int PROCESS_QUERY_INFORMATION = 0x0400;
    public static int PROCESS_VM_READ = 0x0010;
	
	Kernel32 INSTANCE = (Kernel32) Native.loadLibrary("Kernel32", Kernel32.class);
	
	HANDLE OpenEventW(int access, boolean inheritHandle, WString name);
	HANDLE GetCurrentProcess();
	HANDLE CreateToolhelp32Snapshot(WinDef.DWORD tlHelp, WinDef.DWORD word);
	
	HANDLE OpenProcess(int dwDesiredAccess, boolean bInheritHandle, int pointer);
	
	int WaitForSingleObject(HANDLE hHandle, int dwMilliseconds);
	boolean Process32Next(HANDLE snapshot, ByReference processEntry);
	boolean CloseHandle(HANDLE hObject);
	
	int GetLastError();
}
