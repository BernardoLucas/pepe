package com.mangyBear.Pepe.service;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class VariableService extends BasePersistence<Variable> {

    @Override
    public void store(Variable variable) throws Exception {
    		super.store(variable);
    }

    public void update(Variable variable) throws Exception {
        super.update(variable);
    }

    @Override
    public void remove(Variable variable) throws Exception {
        super.remove(variable);
    }

	public Variable findByName(String name) throws Exception {
		try {
			return super.findEquals("variableByName", name);
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<Variable> findLikeName(String name) throws Exception {
		return super.findLike("variableLikeName", name);
	}

	public List<Variable> findLikeNameAndGroupType(String name, VariableGroupType type) throws Exception {
		return super.findLikeAndGroupType("variableLikeNameAndGroupType", name, type);
	}

	public List<Variable> findByIdFlow(Long id) {
		TypedQuery<Variable> query = em.createNamedQuery("variableByFlow", Variable.class);
		 query.setParameter("idFlowDad", id);
		 return query.getResultList();
	}
}
