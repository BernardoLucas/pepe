package com.mangyBear.Pepe.service;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.jacob.com.LibraryLoader;
import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.types.desktopParams.DesktopType;
import com.mangyBear.Pepe.domain.types.frame.ListDragSource;
import com.mangyBear.Pepe.domain.types.graph.GraphGridStyleType;
import com.mangyBear.Pepe.service.Finder.Window;
import com.mangyBear.Pepe.ui.component.GetNetworkAddress;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mangyBear.Pepe.ui.frame.FrameMain;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.util.mxGraphTransferable;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class MyConstants {

	public static final String FINDER_TYPE = "FinderType";
	public static final String ACTION_GROUP_TYPE = "ActionGroupType";
	public static final String ACTION_TYPE = "ActionType";

	public static final String USER_HOME = System.getProperty("user.home");
	public static final String SYSTEM_FILE_SEPARATOR = System.getProperty("file.separator");
	public static final String SYSTEM_LINE_SEPARATOR = System.getProperty("line.separator");
	public static final String OS_NAME = System.getProperty("os.name");
	public static final String JVM_BIT_VERSION = System.getProperty("sun.arch.data.model");
	public static final String IP_ADDRESS = (new GetNetworkAddress()).GetAddress("ip");
	public static final String MAC_ADDRESS = (new GetNetworkAddress()).GetAddress("mac");
	public static boolean stopWaitingColor;
	private static int coutToDarthVader;
	private static final Map<Integer, String> darthVaderQuotes = new HashMap<>();
	//Pepe Settings
	private static GraphGridStyleType gridStyle;
	private static int mouseDelay;
	private static String docxTmplPath;
	private static String docxExportPath;
	private static String logExportPath;
	private static String prntScrExportPath;
	
	private static Flow executingFlow;
	
	public static String getAppHome() {
		try {
			return new File(".").getCanonicalPath();
		} catch (IOException e) {
			return USER_HOME;
		}
	}
	
	public static Dimension getScreenSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}

	public static int getCoutToDarthVader() {
		return coutToDarthVader;
	}

	public static void setCoutToDarthVader(int coutToDarthVader) {
		MyConstants.coutToDarthVader = coutToDarthVader <= 20 ? coutToDarthVader : 1;
	}

	public static Map<Integer, String> getDarthVaderQuotes() {
		return darthVaderQuotes;
	}

	public static void populateDarthVaderQuotes() {
		getDarthVaderQuotes().put(4, "Give yourself to the Dark Side.");
		getDarthVaderQuotes().put(8, "I find your lack of faith disturbing.");
		getDarthVaderQuotes().put(12, "Impressive. Most impressive. You have controlled your fear. Now, release your anger.");
		getDarthVaderQuotes().put(16, "The Force is strong with this one.");
		getDarthVaderQuotes().put(20, "I am your father!");
	}

    public static void easterEggDarthVader(FrameMain frameMain) {
        int currentDarthCount = getCoutToDarthVader() + 1;
        for (Map.Entry<Integer, String> entrySet : getDarthVaderQuotes().entrySet()) {
            Integer key = entrySet.getKey();
            if (currentDarthCount == key) {
                JOptionPane.showMessageDialog(frameMain, entrySet.getValue(), "Darth Vader", JOptionPane.PLAIN_MESSAGE, MyConstants.setIconSize("images/Ask Frame/DarthVader.png", 32, 32));
            }
        }
        setCoutToDarthVader(currentDarthCount);
    }

	public static GraphGridStyleType getGridStyle() {
		return gridStyle;
	}

	public static void setGridStyle(GraphGridStyleType gridStyle) {
		MyConstants.gridStyle = gridStyle;
	}

	public static int getMouseDelay() {
		return mouseDelay;
	}

	public static void setMouseDelay(int mouseDelay) {
		MyConstants.mouseDelay = mouseDelay;
	}

	public static String getDocxTmplPath() {
		return docxTmplPath;
	}

	public static void setDocxTmplPath(String docxTmplPath) {
		MyConstants.docxTmplPath = docxTmplPath;
	}
	
	public static String getDocxExportPath() {
		return docxExportPath;
	}

	public static void setDocxExportPath(String docxExportPath) {
		MyConstants.docxExportPath = docxExportPath;
	}

	public static String getLogExportPath() {
		return logExportPath;
	}

	public static void setLogExportPath(String logExportPath) {
		MyConstants.logExportPath = logExportPath;
	}

	public static String getPrntScrExportPath() {
		return prntScrExportPath;
	}

	public static void setPrntScrExportPath(String prntScrExportPath) {
		MyConstants.prntScrExportPath = prntScrExportPath;
	}

	public static Flow getExecutingFlow() {
		return executingFlow;
	}

	public static void setExecutingFlow(Flow executingFlow) {
		MyConstants.executingFlow = executingFlow;
	}

	public static String getOSBitVersion() {
		boolean is64bit = false;
		if (OS_NAME.contains("Windows")) {
		    is64bit = (System.getenv("ProgramFiles(x86)") != null);
		} else {
		    is64bit = (System.getProperty("os.arch").indexOf("64") != -1);
		}
		return is64bit ? "64" : "32";
	}

	public static ImageIcon setIconSize(String iconPath, int width, int height) {
		Image img = new ImageIcon(iconPath).getImage();
		Image newimg = img.getScaledInstance(width, height,	Image.SCALE_SMOOTH);
		return new ImageIcon(newimg);
	}

	public static Point getScreenCenterToFrame(JFrame frame) {
		// Determine the new location of the window
		int frameWidth = frame.getSize().width;
		int frameHeight = frame.getSize().height;
		int x = (getScreenSize().width - frameWidth) / 2;
		int y = (getScreenSize().height - frameHeight) / 2;

		return new Point(x, y);
	}

	public static Point setPanelAtCenterFrame(JFrame frame, JPanel panel) {
		int panelWidth = panel.getSize().width;
		int panelHeight = panel.getSize().height;
		int x = (frame.getWidth() - panelWidth) / 2;
		int y = (frame.getHeight() - panelHeight) / 2;

		return new Point(x, y);
	}
	
	public static String setAsString(Object obj) {
		if (obj != null) {
			if (obj instanceof DesktopType) {
				return ((DesktopType) obj).getValue().toString();
			}
			return obj.toString();
		}
		return "";
	}

	public static String getAutoItX3() {
		String osBitVersion = getOSBitVersion();
		String regPath = "cd \"%systemroot%\\";
		String dllPath = new File("lib", "AutoItX3_x" + osBitVersion + ".dll").getAbsolutePath();
		if (!osBitVersion.equals("32")) {
			regPath += "System32\\";
		} else {
			regPath += "SysWoW64\\";
		}
		return "/c " + regPath + "\" && regsvr32 /s " + dllPath;
	}

	public void setJACOB_DLL_PATH() {
		System.setProperty(LibraryLoader.JACOB_DLL_PATH, new File("lib", "jacob-1.18-x" + getOSBitVersion() + ".dll").getAbsolutePath());
	}

	public static String getImageSearchDLL() {
		String imageSearchDll = "ImageSearchDLL_x" + getOSBitVersion();
		System.load(new File("lib", imageSearchDll + ".dll").getAbsolutePath());
		return imageSearchDll;
	}

	public static Rectangle validateScreenRectangle(Rectangle screenRegion) {
		if (screenRegion == null || screenRegion.isEmpty()) {
			return new Rectangle(getScreenSize());
		}

		if (screenRegion.getWidth() > getScreenSize().getWidth()
				|| (screenRegion.x + screenRegion.getWidth()) > getScreenSize().getWidth()) {
			screenRegion.width = (int) getScreenSize().getWidth();
		}

		if (screenRegion.getHeight() > getScreenSize().getHeight()
				|| (screenRegion.y + screenRegion.getHeight()) > getScreenSize()
						.getHeight()) {
			screenRegion.height = (int) getScreenSize().getHeight();
		}

		return screenRegion;
	}
	
    public static String getValidTitle(Window window) {
    	if (window.getHexStringHwnd() == null) {
    		return window.getText();
    	}
    	return window.getHexStringHwnd();
    }
    
    public static List<CellValue> getCellValuesToDrop(DropTargetDropEvent dtde) throws UnsupportedFlavorException, IOException {
    	// Only accept a flavor that returns a CellValue
    	List<CellValue> cellValueList = new ArrayList<>();
    	DataFlavor[] fl = dtde.getCurrentDataFlavors();
    	for (int i = 0; i < fl.length; i++) {
    		Object transferData = dtde.getTransferable().getTransferData(fl[i]);
    		//Check if it is a mxGraphTransferable
    		if (transferData instanceof mxGraphTransferable) {
    			mxGraphTransferable transferable = (mxGraphTransferable) transferData;
    			//Get all being transfer
    			for (Object objectCell : transferable.getCells()) {
    				//Check if it is a mxCell
    				if (objectCell instanceof mxCell) {
    					Object objectValue = ((mxCell) objectCell).getValue();
    					//Check if it is a CellValue
    					if (objectValue instanceof CellValue) {
    						//Add to return list
    						cellValueList.add((CellValue) objectValue);
    					}
    				}
    			}
    		} else if (transferData instanceof List<?>) {
    			for (Object object : (List<?>) transferData) {
    				//Check if it is a SuggestLRItem
    				if (object instanceof SuggestDoubleColumn) {
    					//Add to return list
    					cellValueList.add(((SuggestDoubleColumn) object).getLeftText());
    				}
    			}
    		}
    	}
    	
    	return cellValueList;
    }

	public static ListDragSource getDragSource(DropTargetDropEvent dtde) throws UnsupportedFlavorException, IOException {
		// Only accept a flavor that returns a CellValue
		DataFlavor[] fl = dtde.getCurrentDataFlavors();
		for (int i = 0; i < fl.length; i++) {
			Object transferData = dtde.getTransferable().getTransferData(fl[i]);
			//Check if it is a mxGraphTransferable
			if (transferData instanceof List<?>) {
				for (Object object : (List<?>) transferData) {
					//Check if it is a SuggestLRItem
					if (object instanceof SuggestDoubleColumn) {
						//Add to return list
						return ((SuggestDoubleColumn) object).getDragSource();
					}
				}
			}
		}
		
		return null;
	}
    
    public static Dimension parseDimension(List<String> listDimension) {
    	if (listDimension.size() < 2) {
    		return new Dimension(0, 0);
    	}
    	
    	return new Dimension(parseInt(listDimension.get(0)), parseInt(listDimension.get(1)));
    }
    
    public static Point parsePoint(List<String> listPoint) {
    	if (listPoint.size() < 2) {
    		return new Point(0, 0);
    	}
    	
    	return new Point(parseInt(listPoint.get(0)), parseInt(listPoint.get(1)));
    }
    
    public static Rectangle parseRectangle(List<String> listRectangle) {
    	if (listRectangle.size() < 2) {
    		return new Rectangle(0, 0, 0, 0);
    	}
    	if (listRectangle.size() >= 2 && listRectangle.size() < 4) {
    		return new Rectangle(parseInt(listRectangle.get(0)), parseInt(listRectangle.get(1)), 0, 0);
    	}
    	
    	return new Rectangle(parseInt(listRectangle.get(0)), parseInt(listRectangle.get(1)), parseInt(listRectangle.get(2)), parseInt(listRectangle.get(3)));
    }
    
    public static int parseInt(String string) {
    	return (string == null || string.isEmpty()) ? 0 : Integer.parseInt(string);
    }
    
    public static double parseDouble(String string) {
    	return (string == null || string.isEmpty()) ? 0 : Double.valueOf(string);
    }
    
    public static long parseLong(String string) {
    	return (string == null || string.isEmpty()) ? 0 : Long.valueOf(string);
    }
    
    public static boolean getBoolean(String string) {
    	return (string == null || string.isEmpty()) ? false : Boolean.getBoolean(string);
    }
}
