package com.mangyBear.Pepe.service;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class ActionService extends BasePersistence<Action> {

    @Override
    public void store(Action action) throws Exception {
        super.store(action);
    }

    public void update(Action action) throws Exception {
        super.update(action);
    }

    @Override
    public void remove(Action action) throws Exception {
        super.remove(action);
    }
}
