package com.mangyBear.Pepe.service;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.persistence.config.BasePersistence;
import com.mangyBear.Pepe.ui.component.readWriteFile.ExportLog;

public class StepService extends BasePersistence<Step> {

    @Override
    public void store(Step step) throws Exception {
        super.store(step);
    }

    public void update(Step step) throws Exception {
        super.update(step);
    }

    @Override
    public void remove(Step step) throws Exception {
        super.remove(step);
    }
    
    public Step findByName(String name) throws Exception {
    	return super.findEquals("stepByName", name);
    }

	public List<Step> findLikeName(String name) throws Exception {
		return super.findLike("stepLikeName", name);
	}
	
	public List<Step> findLikeNameLimited(String name, int limit) throws Exception {
		return super.findLikeLimited("stepLikeName", name, limit);
	}

	public void execute(ExportLog log, Step step) {
		(new ExecuteStepService()).execute(log, step);
	}
}
