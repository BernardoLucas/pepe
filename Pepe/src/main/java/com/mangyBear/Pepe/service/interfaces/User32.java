package com.mangyBear.Pepe.service.interfaces;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.RECT;
import com.sun.jna.platform.win32.WinUser.WNDENUMPROC;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

public interface User32 extends StdCallLibrary {
	User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class, W32APIOptions.DEFAULT_OPTIONS);

	boolean EnumWindows(WNDENUMPROC lpEnumFunc, Pointer arg);
	boolean EnumChildWindows(HWND hWnd, WNDENUMPROC lpEnumFunc, Pointer arg);
	
	boolean IsWindowVisible(HWND hWnd);

	int GetClassName(HWND hWnd, char[] lpClassName, int nMaxCount);
	int GetWindowTextA(HWND hWnd, byte[] lpString, int nMaxCount);
	int GetWindowText(HWND hWnd, char[] lpString, int nMaxCount);
	int GetWindowTextLength(HWND hWnd);
	int GetWindowRect(HWND hWnd, RECT rectangle);
	int GetWindowModuleFileName(HWND focusedWindow, char[] nameName, int i);
	HWND GetTopWindow(HWND wnd);
	HWND GetWindow(HWND wnd, int flag);
	
    long GetWindowThreadProcessId(HWND hWnd, PointerByReference pointer);
	long GetWindowThreadProcessId(HWND wnd, IntByReference ibr);

	final int GW_HWNDNEXT = 2;
}
