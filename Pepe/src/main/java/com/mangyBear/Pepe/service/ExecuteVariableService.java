package com.mangyBear.Pepe.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.mangyBear.Pepe.domain.entity.VarObject;
import com.mangyBear.Pepe.domain.entity.VarObjectParameter;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.entity.VariableParameter;
import com.mangyBear.Pepe.domain.entityRun.VariableRun;
import com.mangyBear.Pepe.domain.types.action.ReflectionType;
import com.mangyBear.Pepe.domain.types.variable.VariableType;

public class ExecuteVariableService {

	private static Map<String, VariableRun> varMap = new HashMap<>();
	
	protected Map<String, VariableRun> getVarMap() {
		return varMap;
	}
	
	protected void variableListToMap(List<Variable> variableList) {
		for (Variable variable : variableList) {
			getVarMap().put(variable.getName(), new VariableRun(variable));
		}
	}
	
	protected void setVariableValue(String varName, Object value) {
		VariableRun variable = getVarByName(varName);
		
		if (variable != null && VariableType.VARIABLE_OBJECT_CHANGEABLE.equals(variable.getVariableType())) {
			variable.setValue(value);
		}
		
		if (variable == null && (varName.startsWith("[") && varName.endsWith("]"))) {
			getVarMap().put(varName, new VariableRun(varName, value, VariableType.VARIABLE_OBJECT_CHANGEABLE));
		}
	}
	
	private VariableRun getVarByName(String varName) {
		return getVarMap().get(varName);
	}
	
	public Object getVariable(Object paramValue) {
		if (paramValue == null) {
			return null;
		}
		boolean isList = paramValue.toString().contains("]-");
		
		if (isList) {
			return getListVariable(paramValue);
		}

		return getBasicVariable(paramValue);
		
	}
	
	private Object getListVariable(Object paramValue) {
		String[] params = paramValue.toString().split("-", 2);
		Object paramList = getBasicVariable(params[0]);
		Object paramListPos = getBasicVariable(params[1]);
		if (paramList instanceof List && paramListPos instanceof String) {
			return ((List<?>) paramList).get(MyConstants.parseInt(paramListPos.toString()));
		}
		
		return paramValue;
	}
	
	private Object getBasicVariable(Object paramValue) {
		if (isVariable(paramValue)) {
			paramValue = getVariable(getVariableValue(paramValue.toString()));
		}
		
		return paramValue;
	}
	
	protected boolean isVariable(Object varName) {
		if (varName == null || !(varName instanceof String)) {
			return false;
		}
		
		String varNameAsString = varName.toString();
		if (varNameAsString.isEmpty()) {
			return false;
		}
		

		int openCount = StringUtils.countMatches(varNameAsString, "[");
		int closeCount = StringUtils.countMatches(varNameAsString, "]");
		if (openCount != 1 || closeCount != 1) {
			return false;
		}
		
		if (!varNameAsString.startsWith("[") || !varNameAsString.endsWith("]")) {
			return false;
		}

		return true;
	}
    
	protected Object getVariableValue(String varName) {
		VariableRun variable = getVarByName(varName);
		if (variable != null) {
			switch (variable.getVariableType()) {
				case VARIABLE_WEBDRIVER_CHROME:
				case VARIABLE_WEBDRIVER_FIREFOX:
				case VARIABLE_WEBDRIVER_IE:
					if (variable.getValue() instanceof String) {
						variable.setValue(executeVariable(variable));
					}
					return variable.getValue();
				case VARIABLE_FILE_CSV:
				case VARIABLE_FILE_PROPERTY:
				case VARIABLE_FILE_XML:
				case VARIABLE_SQL_QUERY:
					return executeVariable(variable);
				default:
					return variable.getValue();
			}
		}
		return removeBrackets(varName);
	}
	
	private String removeBrackets(String varName) {
		varName = varName.startsWith("[") ? varName.substring(1) : varName;
		return varName.endsWith("[") ? varName.substring(0, varName.length() + 1) : varName;
	}

	public Object executeVariable(VariableRun variable) {
		ReflectionType reflectionType = variable.getVariableType().getReflectionType();
		Class<?> cls = reflectionType.getReflectionClass();
		Class<?>[] paramInt = (Class<?>[]) reflectionType.getReflectionParams().toArray();
		String nameMethod = reflectionType.getReflectionMethod();
		
		Object[] paramValue = buildExecuteVariableParams(variable);
		return executeMethod(cls, paramInt, paramValue, nameMethod);
	}
	
	private Object[] buildExecuteVariableParams(VariableRun variable) {
		List<Object> paramValue = executeVarObject(variable.getObject());
		
		VariableType variableType = variable.getVariableType();
		List<String> params = variableType.getFrameAddType().getLabels();
		for (String nameParam : params) {
			for (VariableParameter param : variable.getParameters()) {
				if (nameParam.equalsIgnoreCase(param.getName())) {
					paramValue.add(getVariable(param.getValue()));
				}
			}
		}
		return paramValue.toArray();
	}

	private List<Object> executeVarObject(VarObject object) {
		List<Object> paramValue = buildExecuteVarObjectParams(object);
		if (object.getType().getReflectionType() != null) {
			ReflectionType reflectionType = object.getType().getReflectionType();
			Class<?> cls = reflectionType.getReflectionClass();
			Class<?>[] paramInt = (Class<?>[]) reflectionType.getReflectionParams().toArray();
			String nameMethod = reflectionType.getReflectionMethod();
			
			return new ArrayList<Object>(Arrays.asList(executeMethod(cls, paramInt, paramValue.toArray(), nameMethod)));
		}
		
		return paramValue;
	}

	private List<Object> buildExecuteVarObjectParams(VarObject object) {
		List<String> params = object.getType().getFrameAddType().getLabels();
		List<Object> paramValue = new ArrayList<>();
		for (String nameParam : params) {
			for (VarObjectParameter param : object.getParameters()) {
				if (nameParam.equalsIgnoreCase(param.getName())) {
					paramValue.add(getVariable(param.getValue()));
				}
			}
		}
		return paramValue;
	}

	private Object executeMethod(Class<?> cls, Class<?>[] paramInt, Object[] paramValue, String nameMethod) {
		try {
			Object obj = cls.newInstance();
			Method method = cls.getDeclaredMethod(nameMethod, paramInt);
			return method.invoke(obj, paramValue);
		} catch (InstantiationException | IllegalAccessException
				| NoSuchMethodException | SecurityException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}
}
