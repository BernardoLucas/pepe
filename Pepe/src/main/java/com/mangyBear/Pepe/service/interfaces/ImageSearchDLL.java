package com.mangyBear.Pepe.service.interfaces;

import com.mangyBear.Pepe.service.MyConstants;
import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 *
 * @author Lucas Silva Bernardo
 */
public interface ImageSearchDLL extends Library {

    ImageSearchDLL INSTANCE = (ImageSearchDLL) Native.loadLibrary(MyConstants.getImageSearchDLL(), ImageSearchDLL.class);

    String ImageSearch(int x1, int y1, int right, int bottom, String findImage);

}
