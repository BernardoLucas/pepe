package com.mangyBear.Pepe.service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import com.mangyBear.Pepe.domain.entity.FinderParameter;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class FinderParameterService extends BasePersistence<FinderParameter> {

    @Override
    public void store(FinderParameter finderParam) throws Exception {
        super.store(finderParam);
    }

    public void update(FinderParameter finderParam) throws Exception {
        super.update(finderParam);
    }

    @Override
    public void remove(FinderParameter finderParam) throws Exception {
        super.remove(finderParam);
    }

	public List<FinderParameter> findParamLikeValue(String value) throws Exception {
		return new ArrayList<>(new LinkedHashSet<>(super.findLike("finderParamLikeValue", value)));
	}
}
