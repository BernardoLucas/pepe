package com.mangyBear.Pepe.service;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class LogicService extends BasePersistence<Logic> {

    @Override
    public void store(Logic logic) throws Exception {
        super.store(logic);
    }

    public void update(Logic logic) throws Exception {
        super.update(logic);
    }

    @Override
    public void remove(Logic logic) throws Exception {
        super.remove(logic);
    }
    
    public Logic findByName(String name) throws Exception {
    	return super.findEquals("logicByName", name);
    }

	public List<Logic> findLikeName(String name) throws Exception {
		return super.findLike("logicLikeName", name);
	}

	public List<Logic> findLikeNameLimited(String name, int limit) throws Exception {
		return super.findLikeLimited("logicLikeName", name, limit);
	}
}
