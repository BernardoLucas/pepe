package com.mangyBear.Pepe.service;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.VarObject;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class VarObjectService extends BasePersistence<VarObject> {

    @Override
    public void store(VarObject varObject) throws Exception {
        super.store(varObject);
    }

    public void update(VarObject varObject) throws Exception {
        super.update(varObject);
    }

    @Override
    public void remove(VarObject varObject) throws Exception {
        super.remove(varObject);
    }
    
    public VarObject findByName(String name) throws Exception {
    	return super.findEquals("varObjectByName", name);
    }

	public List<VarObject> findLikeName(String name) throws Exception {
		return super.findLike("varObjectLikeName", name);
	}
}
