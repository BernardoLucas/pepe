package com.mangyBear.Pepe.service;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Favorite;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class FavoriteService extends BasePersistence<Favorite> {

	@Override
	public void store(Favorite favorite) throws Exception {
		super.store(favorite);
	}

	public void update(Favorite favorite) throws Exception {
		super.update(favorite);
	}

	@Override
	public void remove(Favorite favorite) throws Exception {
		super.remove(favorite);
	}
	
	public List<Favorite> findByMacLimited(int limit) throws Exception {
		return super.findByMacLimited("findByMac", limit);
	}

	public List<Favorite> findByMacLikeName(String nmFavorite) throws Exception {
		return super.findByStringAndMac("findByMacLikeName", nmFavorite);
	}
}
