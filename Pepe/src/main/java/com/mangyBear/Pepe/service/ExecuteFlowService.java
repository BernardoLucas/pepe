package com.mangyBear.Pepe.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.mangyBear.Pepe.domain.entity.EndFlow;
import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.FlowItem;
import com.mangyBear.Pepe.domain.entity.FlowItemConnection;
import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.entityRun.LogicResultRun;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.domain.types.run.StatusType;
import com.mangyBear.Pepe.ui.component.readWriteFile.ExportDocx;
import com.mangyBear.Pepe.ui.component.readWriteFile.ExportLog;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mangyBear.Pepe.ui.frame.FrameAskPause;
import com.mangyBear.Pepe.ui.listener.PauseListener;

public class ExecuteFlowService extends ExecuteVariableService {

	private final FlowItemService flowItemService;
	private final VariableService variableService;
	private final PauseListener pauseListener;
	private final ExportLog exportLog;

	private static int execStatus;// 0=Normal, 1=Pause, 2=Restart, 3=Stop
	private static EndFlow exportEnd;
	
	public ExecuteFlowService() {
		this.flowItemService = new FlowItemService();
		this.variableService = new VariableService();
		this.pauseListener = new PauseListener();
		this.exportLog = new ExportLog();
	}

	public void execute(CellValue beginCell) {
		MyConstants.setExecutingFlow((Flow) beginCell.getFullItem());//show message exporting
		variableListToMap(variableService.findByIdFlow(beginCell.getId()));

		Map<GraphType, Map<String, Object>> resultMap = new LinkedHashMap<>();
		executeFlowItem(resultMap, flowItemService.findFirstByIdFlow(beginCell.getId()));
		
		getVarMap().clear();
		if (getExecStatus() == 2) {
			setExecStatus(0);
			execute(beginCell);
		}
		
		if (getExportEnd() != null) {
			if (getExportEnd().getExportDocx()) {
				(new ExportDocx()).run(MyConstants.getExecutingFlow().getNmFlow(), resultMap);
			}
			if (getExportEnd().getExportLog()) {
				exportLog.export(MyConstants.getExecutingFlow().getNmFlow());
			}
		}
		pauseListener.clear();
	}
	
	protected void showAskDialog() {
		if (getExecStatus() == 1) {
			(new FrameAskPause()).setVisible(true);
		}
	}

	private Map<GraphType, Map<String, Object>> executeChildFlow(Long id) {
		variableListToMap(variableService.findByIdFlow(id));
		
		Map<GraphType, Map<String, Object>> childResultMap = new LinkedHashMap<>();
		executeFlowItem(childResultMap, flowItemService.findFirstByIdFlow(id));

		return childResultMap;
	}

	private void executeFlowItem(Map<GraphType, Map<String, Object>> resultMap, FlowItem item) {
		String logicReturn = "";

		showAskDialog();
		if (getExecStatus() == 2 || getExecStatus() == 3) {
			return;
		}

		if (item.getFlow() != null) {
			addExecReturnToMap(resultMap, GraphType.CELL_FLOW, item.getFlow().getNmFlow(), executeChildFlow(item.getFlow().getId()));
		} else if (item.getLogic() != null) {
			logicReturn = executeLogic(item.getLogic());
			LogicResultRun lResult = buidLogicResult(item.getLogic(), logicReturn);
			exportLog.write(GraphType.CELL_LOGIC, lResult);
			addExecReturnToMap(resultMap, GraphType.CELL_LOGIC, item.getLogic().getNmLogic(), lResult);
		} else if (item.getStep() != null) {
			addExecReturnToMap(resultMap, GraphType.CELL_STEP, item.getStep().getNmStep(), (new ExecuteStepService()).execute(exportLog, item.getStep()));
		} else if (item.getEnd() != null) {
			setExecStatus(3);
			setExportEnd(item.getEnd());
			return;
		}
		
		showAskDialog();
		if (getExecStatus() == 2 || getExecStatus() == 3) {
			return;
		}

		List<FlowItemConnection> nextItems = item.getNextFlowItems();
		if (nextItems != null && !nextItems.isEmpty()) {
			for (FlowItemConnection nextItem : nextItems) {
				String condition = nextItem.getCondition();
				if (condition == null || condition.equalsIgnoreCase(logicReturn)) {
					executeFlowItem(resultMap, nextItem.getCurrentItem());
				}
			}
		}
	}

	private String executeLogic(Logic logic) {
		Object inputValue = getVariableValue(logic.getInput());
		Object expected = getVariableValue(logic.getExpected());
		return String.valueOf(compare(inputValue, logic.getDenial(), expected));
	}

	private boolean compare(Object inputValue, Boolean denial, Object expected) {
		if (inputValue == null || denial == null || expected == null) {
			return false;
		}

		boolean comparison;
		Class<?> expectedClass = expected.getClass();
		if (expectedClass.isInstance(inputValue)) {
			comparison = inputValue.equals(expected);
		} else {
			comparison = inputValue.toString().equals(expectedAsString(expected));
		}

		return denial ? !comparison : comparison;
	}
	
	private String expectedAsString(Object expected) {
		return "null".equalsIgnoreCase(expected.toString()) ? null : expected.toString();
	}
	
	private LogicResultRun buidLogicResult(Logic logic, String logicReturn) {
		LogicResultRun logicResult = new LogicResultRun();
		logicResult.setDate(new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]").format(new Date()));
		logicResult.setLogic(logic);
		logicResult.setLogicReturn(logicReturn);
		logicResult.setLogicStatus(logicReturn == null ? StatusType.NOK : StatusType.OK);
		
		return logicResult;
	}
	
	private void addExecReturnToMap(Map<GraphType, Map<String, Object>> resultMap, GraphType type, String itemName, Object result) {
		Map<String, Object> tempMap = new LinkedHashMap<>();
		tempMap.put(itemName, result);
		resultMap.put(type, tempMap);
	}

	public static int getExecStatus() {
		return execStatus;
	}

	public static void setExecStatus(int execStatus) {
		ExecuteFlowService.execStatus = execStatus;
	}

	public static EndFlow getExportEnd() {
		return exportEnd;
	}

	public static void setExportEnd(EndFlow exportEnd) {
		ExecuteFlowService.exportEnd = exportEnd;
	}
}
