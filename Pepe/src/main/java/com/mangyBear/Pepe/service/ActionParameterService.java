package com.mangyBear.Pepe.service;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.ActionParameter;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class ActionParameterService extends BasePersistence<ActionParameter> {

    @Override
    public void store(ActionParameter actionParam) throws Exception {
        super.store(actionParam);
    }

    public void update(ActionParameter actionParam) throws Exception {
        super.update(actionParam);
    }

    @Override
    public void remove(ActionParameter actionParam) throws Exception {
        super.remove(actionParam);
    }

	public List<ActionParameter> findParamLikeValue(String value) throws Exception {
		return super.findLike("actionParamLikeValue", value);
	}
}
