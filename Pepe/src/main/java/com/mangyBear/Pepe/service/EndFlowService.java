package com.mangyBear.Pepe.service;

import com.mangyBear.Pepe.domain.entity.EndFlow;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class EndFlowService extends BasePersistence<EndFlow> {

    @Override
    public void store(EndFlow endFlow) throws Exception {
        super.store(endFlow);
    }

    public void update(EndFlow endFlow) throws Exception {
        super.update(endFlow);
    }

    @Override
    public void remove(EndFlow endFlow) throws Exception {
        super.remove(endFlow);
    }
}
