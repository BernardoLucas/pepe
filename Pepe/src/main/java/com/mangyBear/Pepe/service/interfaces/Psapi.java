package com.mangyBear.Pepe.service.interfaces;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.StdCallLibrary;

public interface Psapi extends StdCallLibrary {
	Psapi INSTANCE = (Psapi) Native.loadLibrary("psapi", Psapi.class);
    int GetModuleFileNameExW(HANDLE process, Pointer hModule, byte[] lpString, int nMaxCount);
}
