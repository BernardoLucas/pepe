package com.mangyBear.Pepe.service.Finder;

import org.apache.commons.lang3.StringUtils;

public class Window {

	private String title;
	private String text;
	private String handle;
	private Integer pid;
	
	public Window(String title, String text, String handle, Integer pid) {
		setTitle(title);
		setText(text);
		setHandle(handle);
		setPid(pid);
	}
	
	@Override
	public String toString() {
		return getTitle();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getHandle() {
		return handle;
	}

	public String getHexStringHwnd() {
		String hwndAsString = (handle == null || handle.isEmpty()) ? "0" : handle.toString().substring(2);
		return hwndAsString.equals("0") ? null : "[HANDLE:0x" + StringUtils.leftPad(hwndAsString, 16, "0") + "]";
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}
}
