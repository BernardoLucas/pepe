package com.mangyBear.Pepe.service;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.FlowItem;
import com.mangyBear.Pepe.persistence.config.BasePersistence;
import com.mangyBear.Pepe.ui.domain.CellValue;

public class FlowService extends BasePersistence<Flow> {
	FlowItemService flowItemService = new FlowItemService();

	@Override
	public void store(Flow flow) throws Exception {
		super.store(flow);
	}

	public void update(Flow flow) throws Exception {
		super.update(flow);
	}

	@Override
	public void remove(Flow flow) throws Exception {
		flow = findById(flow.getId());
		for (FlowItem item : flow.getFlowItems()) {
			flowItemService.remove(item,false);
		}
		super.remove(flow);
	}

	public Flow findByName(String name) throws Exception {
		return super.findEquals("flowByName", name);
	}

	public List<Flow> findLikeName(String name) throws Exception {
		return super.findLike("flowLikeName", name);
	}

	public List<Flow> findLikeNameLimited(String name, int limit) throws Exception {
		return super.findLikeLimited("flowLikeName", name, limit);
	}

	public void execute(CellValue beginCell) {
		(new ExecuteFlowService()).execute(beginCell);
	}
}
