package com.mangyBear.Pepe.service;

import java.util.List;

import javax.persistence.TypedQuery;

import com.mangyBear.Pepe.domain.entity.FlowItem;
import com.mangyBear.Pepe.domain.entity.FlowItemConnection;
import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class FlowItemService extends BasePersistence<FlowItem> {
	FlowItemConnectionService flowItemConnectionService = new FlowItemConnectionService();

	 protected FlowItem findFirstByIdFlow(Long id) {
		 TypedQuery<FlowItem> query = em.createNamedQuery("flowItemByFlowAndFirstConnection", FlowItem.class);
		 query.setParameter("idFlowDad", id);
		 return query.getSingleResult();
	 }
	 
	public void remove(FlowItem object, Boolean removeAll) throws Exception {
		object = findById(object.getId());
		
		
		flowItemConnectionService.removeByFlowItem(object.getId());
		
		if (object.getStep() != null) {
			Step step = object.getStep();
			
			if (removeAll && findFlowItemsByStep(step.getId()).isEmpty()) {
				StepService eser = new StepService();
				eser.remove(step);
			}
		}
		
		if (object.getLogic() != null) {
			Logic logic = object.getLogic();
			
			if (removeAll && findFlowItemsByLogic(logic.getId()).isEmpty()) {
				LogicService eser = new LogicService();
				eser.remove(logic);
			}
		}
		
		super.remove(object);
//		if (object.getFlow() != null) {
//			super.remove(object);
//		}
	}

	protected List<FlowItem> findFlowItemsByStep(Long id) {
		 TypedQuery<FlowItem> query = em.createNamedQuery("findByStep", FlowItem.class);
		 query.setParameter("idStep", id);
		 return query.getResultList();
	}
	
	private List<FlowItem> findFlowItemsByLogic(Long id) {
		TypedQuery<FlowItem> query = em.createNamedQuery("findByLogic", FlowItem.class);
		query.setParameter("idLogic", id);
		return query.getResultList();
	}
}
