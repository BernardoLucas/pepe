package com.mangyBear.Pepe.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.domain.entity.ActionParameter;
import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.entity.FinderParameter;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.domain.entityRun.ActionResultRun;
import com.mangyBear.Pepe.domain.types.action.FinderType;
import com.mangyBear.Pepe.domain.types.action.ReflectionType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.domain.types.run.StatusType;
import com.mangyBear.Pepe.ui.component.readWriteFile.ExportLog;

public class ExecuteStepService extends ExecuteFlowService{
	
	private String variableName;
	private ActionResultRun actionResult;
	private List<ActionResultRun> actionResultList = new ArrayList<>();
	
	public List<ActionResultRun> execute(ExportLog exportLog, Step step) {
		for (Action action : step.getActionsByOrder()) {
			actionResult = new ActionResultRun();
			Object actionReturn = null;
			
			Finder finder = action.getFinder();
			
			
			Object finderReturn = executeFinder(finder);
			
			addFinderToActionResult(finder, finderReturn);
			exportLog.write(GraphType.CELL_STEP, actionResult);
			if (getExecStatus() == 2 || getExecStatus() == 3) {
				return actionResultList;
			}
			
			if (finder == null || finderReturn != null) {
				actionReturn = executeAction(action, finderReturn);
				if (getExecStatus() == 2 || getExecStatus() == 3) {
					return actionResultList;
				}
				if (variableName != null && (actionReturn != null && !(actionReturn instanceof Exception))) {
					setVariableValue(variableName, actionReturn);
					variableName = null;
				}
			}
			addActionToActionResult(action, actionReturn);
			exportLog.write(GraphType.CELL_STEP, actionResult);
		}
		return actionResultList;
	}
	
	private void addFinderToActionResult(Finder finder, Object finderReturn) {
		actionResult.setFinderDate(new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]").format(new Date()));
		actionResult.setFinder(finder);
		actionResult.setFinderReturn(finderReturn);
		actionResult.setFinderStatus(finder == null ? StatusType.OK : (finderReturn == null ? StatusType.NOK : StatusType.OK));
	}
	
	private void addActionToActionResult(Action action, Object actionReturn) {
		actionResult.setActionDate(new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]").format(new Date()));
		actionResult.setAction(action);
		actionResult.setActionReturn(actionReturn);
		actionResult.setActionStatus((actionReturn != null && actionReturn instanceof Exception) ? StatusType.NOK : StatusType.OK);
		actionResultList.add(actionResult);
	}

	private Object executeAction(Action action, Object finderResult) {
		ReflectionType reflectionType = action.getActionType().getReflectionType();
		Class<?> cls = reflectionType.getReflectionClass();
		Class<?>[] paramInt = (Class<?>[]) reflectionType.getReflectionParams().toArray();
		boolean hasReturn = reflectionType.getReflectionReturn() != null;
		String nameMethod = reflectionType.getReflectionMethod();
		
		Object[] paramValue = buildActionParams(action, finderResult, paramInt, hasReturn);
		return executeMethod(cls, paramInt, paramValue, nameMethod);
	}
	
	private Object[] buildActionParams(Action action, Object finderResult, Class<?>[] paramInt, boolean hasReturn) {
		List<Object> paramValueList = new ArrayList<>();
		if (finderResult != null) {
			paramValueList.add(finderResult);
		}
		paramValueList.addAll(paramsOrdering(action));
		
		setVarNameRemoveReturn(paramValueList, hasReturn);
		
		return changeValue(paramValueList).toArray();
	}
	
	private List<Object> paramsOrdering(Action action) {
		List<Object> paramValue = new ArrayList<>();
		List<String> params = action.getActionType().getFrameAddType().getLabels();
		for (String nameParam : params) {
			for (ActionParameter param : action.getParameters()) {
				if (nameParam.equalsIgnoreCase(param.getName())) {
					paramValue.add(param.getValue());
				}
			}
		}
		return paramValue;
	}
	
	private void setVarNameRemoveReturn(List<Object> paramValue, boolean hasReturn) {
		Object paramReturnVar = paramValue.get(paramValue.size() - 1);
		if (hasReturn && paramReturnVar != null) {
			variableName = isVariable(paramReturnVar) ? paramReturnVar.toString() : null;
			paramValue.remove(paramValue.size() - 1);
		}
	}
	
	private List<Object> changeValue(List<Object> paramValueList) {
		List<Object> paramValueListReturn = new ArrayList<>();
		for (Object paramValue : paramValueList) {
			paramValueListReturn.add(getVariable(paramValue));
		}
		
		return paramValueListReturn;
	}

	private Object executeFinder(Finder finder) {
		if (finder != null) {
			ReflectionType reflectionType = finder.getFinderType().getReflectionType();
			Class<?> cls = reflectionType.getReflectionClass();
			Class<?>[] paramInt = (Class<?>[]) reflectionType.getReflectionParams().toArray();
			String nameMethod = reflectionType.getReflectionMethod();
			
			Object[] paramValue = buildFinderParams(finder, paramInt);
			return executeMethod(cls, paramInt, paramValue, nameMethod);
		}
		return null;
	}

	private Object[] buildFinderParams(Finder finder, Class<?>[] paramInt) {
		FinderType finderType = finder.getFinderType();
		List<String> params = new ArrayList<>();
		if (FinderType.POSITION.equals(finderType)) {
			params.addAll(Arrays.asList("Width", "Height"));
		}
		params.addAll(finderType.getFrameAddType().getLabels());
		List<Object> paramValue = new ArrayList<>();
		for (String nameParam : params) {
			for (FinderParameter param : finder.getParameters()) {
				if (nameParam.equalsIgnoreCase(param.getName())) {
					paramValue.add(getVariable(param.getValue()));
				}
			}
		}
		return paramValue.toArray();
	}

	private Object executeMethod(Class<?> cls, Class<?>[] paramInt, Object[] paramValue, String nameMethod) {
		try {
			Object obj = cls.newInstance();
			Method method = cls.getDeclaredMethod(nameMethod, paramInt);
			
			showAskDialog();
			if (getExecStatus() == 2 || getExecStatus() == 3) {
				return null;
			}
			
			return method.invoke(obj, paramValue);
		} catch (InstantiationException | IllegalAccessException
				| NoSuchMethodException | SecurityException
				| InvocationTargetException e) {
			return e;
		}
	}
}
