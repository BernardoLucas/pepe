package com.mangyBear.Pepe.service.Finder;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mangyBear.Pepe.domain.types.action.ByType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.interfaces.ImageSearchDLL;
import com.mangyBear.Pepe.ui.component.PositionCompansation;

import autoitx4java.AutoItX;

/**
 * Copyright (c) 2011 Richard Kanavati
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 *
 * @author Lucas Silva Bernardo
 */
public class Locator {

    private final AutoItX autoitX;
    
    public Locator() {
    	(new MyConstants()).setJACOB_DLL_PATH();
    	this.autoitX = new AutoItX();
    }

    public Control locateControl(Object titleWindow, String text, String controlId) {
    	Window window = null;
    	if (titleWindow instanceof String) {
    		window = locateWindow((String) titleWindow, text);
    		
    	}
    	if (titleWindow instanceof Window) {
    		window = (Window) titleWindow;
    	}
    	
    	Control control = new Control();
		control.setWindow(window);
		control.setControl(controlId);
		control.setHandle(autoitX.controlGetHandle(MyConstants.getValidTitle(window), text, controlId));
		return control;
    }

    public File locateFile(String path) {
        File file = new File(completePath(path));
        return file.exists() ? file : null;
    }
    
    private String completePath(String path) {
    	if (path.endsWith(".exe") && !(path.contains("/") || path.contains("\\"))) {
    		path = System.getenv("WINDIR") + path;
    	}
    	return path;
    }

    public String locateImage(String imagePath, int similarity, Rectangle screenRegion) {
        Rectangle newSR = MyConstants.validateScreenRectangle(screenRegion);
        String similarityAndPath = "*" + String.valueOf(similarity) + " " + imagePath;

        return ImageSearchDLL.INSTANCE.ImageSearch(newSR.x, newSR.y, newSR.width, newSR.height, similarityAndPath);
        //0->found, 1->x, 2->y, 3->image width, 4->image height;
    }

    public List<String> locatePosition(String width, String height, String x, String y) {
    	PositionCompansation compansation = new PositionCompansation();
    	return compansation.getValidPosition(MyConstants.parseDouble(width), MyConstants.parseDouble(height), MyConstants.parseDouble(x), MyConstants.parseDouble(y));
    }

    public String locateProcess(String name) {
        return autoitX.processExists(name) == 1 ? name : null;
    }

    public String locateRegistry(String name) {
    	try {
    		if (LocateRegistry.getRegistry().lookup(name) != null) {
    			return name;
    		}
		} catch (RemoteException | NotBoundException e) {
		}
    	return null;
    }

    public List<String> locateScreenRegion(String x, String y, String width, String height) {
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        if (screenDimension.getWidth() >= (MyConstants.parseInt(x) + MyConstants.parseInt(width))
        		&& screenDimension.getHeight() >= (MyConstants.parseInt(y) + MyConstants.parseInt(height))) {
        	
        	return Arrays.asList((x == null || x.isEmpty()) ? "0" : x, (y == null || y.isEmpty()) ? "0" : y,
        			(width == null || width.isEmpty()) ? "0" : width, (height == null || height.isEmpty()) ? "0" : height);
        }
        return null;
    }

    public Alert locateWebAlert(WebDriver driver, String waitTime) {
    	int timeout = MyConstants.parseInt(waitTime);
        return (new WebDriverWait(driver, timeout > 0 ? timeout : 0)).until(ExpectedConditions.alertIsPresent());
    }

    public WebElement locateWebElement(WebDriver driver, String locator, String byElement, String waitTime) {
    	int timeout = MyConstants.parseInt(waitTime);
        return (new WebDriverWait(driver, timeout > 0 ? timeout : 0)).until(ExpectedConditions.visibilityOfElementLocated(ByType.getLocator(locator, byElement)));
    }

    public Window locateWindow(Object titleWindow, String text) {
    	if (titleWindow instanceof Window) {
    		return (Window) titleWindow;
    	}
    	
    	String title = (String) titleWindow;
    	return new Window(title, text, autoitX.winGetHandle(title, text), MyConstants.parseInt(autoitX.winGetProcess(title, text)));
    }
}
