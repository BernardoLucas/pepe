package com.mangyBear.Pepe.service;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Schedule;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class ScheduleService extends BasePersistence<Schedule> {

	@Override
	public void store(Schedule schedule) throws Exception {
		super.store(schedule);
	}

	public void update(Schedule schedule) throws Exception {
		super.update(schedule);
	}

	@Override
	public void remove(Schedule schedule) throws Exception {
		super.remove(schedule);
	}

	public Schedule findByName(String name) throws Exception {
		return super.findEquals("scheduleByName", name);
	}

	public List<Schedule> findLikeName(String name) throws Exception {
		return super.findLike("scheduleLikeName", name);
	}

	public List<Schedule> findLikeNameAndMac(String name) throws Exception {
		return super.findByStringAndMac("scheduleLikeNameAndMac", name);
	}

	public List<Schedule> findLikeNameMacAndFlow(String name, Long flowId) throws Exception {
		return super.findByStringMacAndLong("scheduleLikeNameMacAndFlow", name, flowId);
	}

	public List<Schedule> findActiveMacSchedulesByDay(String scheduleDay) throws Exception {
		return super.findActiveMacSchedulesByDay("SELECT entity from Schedule entity where exec_" + scheduleDay
				+ " is :state AND (public = :mac OR public = NULL) AND active is TRUE");
	}
}
