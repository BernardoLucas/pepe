package com.mangyBear.Pepe.service;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.mangyBear.Pepe.domain.entity.FlowItemConnection;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class FlowItemConnectionService extends BasePersistence<FlowItemConnection> {
	
    @Override
    public void remove(FlowItemConnection connection) throws Exception {
    	super.remove(findById(connection.getId()));
    }
    
    public void removeByFlowItem(Long idFlowItem) throws Exception {
    	Query query = em.createNamedQuery("removeByFlowItem");
		query.setParameter("idFlowItem", idFlowItem);
		query.executeUpdate();
    }
	
	public Boolean existsConnection(Long previousId, Long currentId) {
		TypedQuery<Integer> query = em.createNamedQuery("existsConnection", Integer.class);
		 query.setParameter("idPrevious", previousId);
		 query.setParameter("idCurrent", currentId);
		 try {
			 query.getSingleResult();
			 return Boolean.TRUE;
		 } catch (NoResultException e) {
			 return Boolean.FALSE;
		 }
	}
}
