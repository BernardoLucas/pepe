package com.mangyBear.Pepe.service;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.persistence.config.BasePersistence;

public class FinderService extends BasePersistence<Finder> {

    @Override
    public void store(Finder finder) throws Exception {
        super.store(finder);
    }

    public void update(Finder finder) throws Exception {
        super.update(finder);
    }

    @Override
    public void remove(Finder finder) throws Exception {
        super.remove(finder);
    }
    
    public Finder findByName(String name) throws Exception {
    	return super.findEquals("finderByName", name);
    }

	public List<Finder> findLikeName(String name) throws Exception {
		return super.findLike("finderLikeName", name);
	}
}
