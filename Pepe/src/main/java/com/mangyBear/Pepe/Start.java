package com.mangyBear.Pepe;

import java.io.File;
import java.util.Locale;

import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.mangyBear.Pepe.domain.types.graph.GraphGridStyleType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.readWriteFile.PropertyReader;
import com.mangyBear.Pepe.ui.component.schedule.SchedulerFlow;
import com.mangyBear.Pepe.ui.component.schedule.SchedulerFlowExecutor;
import com.mangyBear.Pepe.ui.frame.FrameMain;

public class Start {

	public static void main(String args[]) {
		try {
			setPepeSettings();
			scheduleFlowsExecutions();
			(new SchedulerFlowExecutor()).schedule(0, 0, 0);
			
			Locale.setDefault(Locale.ENGLISH);
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			
//	        new RunAsAdmin(System.getenv("WINDIR") + "\\system32\\cmd.exe", MyConstants.getAutoItX3());

	        MyConstants.populateDarthVaderQuotes();
	        
	        (new FrameMain()).setVisible(true);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
    }
	
	private static void setPepeSettings() {
		PropertyReader propertyReader = new PropertyReader();
		String style = propertyReader.getKeyValue(new File(MyConstants.getAppHome() + "/pepe.config"), "[Grid Style]", "style", GraphGridStyleType.DOT.toString());
		String delay = propertyReader.getKeyValue(new File(MyConstants.getAppHome() + "/pepe.config"), "[Mouse Delay]", "delay", "0");
		String docxTmpl = propertyReader.getKeyValue(new File(MyConstants.getAppHome() + "/pepe.config"),"[Docx Tmpl]", "path", "");
		String docxExport = propertyReader.getKeyValue(new File(MyConstants.getAppHome() + "/pepe.config"),"[Docx Export]", "path", "");
		String logExport = propertyReader.getKeyValue(new File(MyConstants.getAppHome() + "/pepe.config"),"[Log Export]", "path", "");
		String prntScrExport = propertyReader.getKeyValue(new File(MyConstants.getAppHome() + "/pepe.config"), "[PrntScr Export]", "path",  "");
		MyConstants.setGridStyle(GraphGridStyleType.getGridStylesByName(style));
		MyConstants.setMouseDelay(Integer.parseInt(delay));
		MyConstants.setDocxTmplPath(docxTmpl);
		MyConstants.setDocxExportPath(docxExport);
		MyConstants.setLogExportPath(logExport);
		MyConstants.setPrntScrExportPath(prntScrExport);
	}
	
	private static void scheduleFlowsExecutions() {
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				(new SchedulerFlow()).schedule();
				return null;
			}
		}.execute();
	}
}
