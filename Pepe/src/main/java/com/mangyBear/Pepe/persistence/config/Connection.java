package com.mangyBear.Pepe.persistence.config;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Connection {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Persistence1");
	
	private static EntityManager em = emf.createEntityManager();
	private static EntityTransaction et = em.getTransaction();
	
	public static EntityManager getEm() {
		return em;
	}
	
	public static EntityTransaction getEt() {
		return et;
	}

	

}
