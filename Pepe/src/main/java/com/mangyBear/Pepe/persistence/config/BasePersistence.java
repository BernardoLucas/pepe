package com.mangyBear.Pepe.persistence.config;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.service.MyConstants;

public abstract class BasePersistence<T> {

	protected EntityManager em = Connection.getEm();
	protected EntityTransaction et = Connection.getEt();

	public void store(T object) throws Exception {
		et.begin();
		em.persist(object);
		et.commit();
	}

	public void update(T object) throws Exception {
		et.begin();
		em.merge(object);
		et.commit();
	}

	public void remove(T object) throws Exception {
		et.begin();
		try {
			em.remove(object);
			et.commit();
		}catch (Exception e) {
			et.rollback();
			throw e;
		}
		
	}

	protected void remove(Long id) throws Exception {
		et.begin();
		try {
			em.remove(this.findById(id));
			et.commit();
		}catch (Exception e) {
			et.rollback();
			throw e;
		}
	}

	public T findById(Long id) throws Exception {
		return (T) em.find(getGenerics(), id);
	}

	protected T findEquals(String namedQuery, String name) throws Exception {
		TypedQuery<T> query = em.createNamedQuery(namedQuery, getGenerics());
		query.setParameter("name", name.toLowerCase());
		return query.getSingleResult();
	}
	
	protected List<T> findLike(String namedQuery, String name) throws Exception {
		TypedQuery<T> query = em.createNamedQuery(namedQuery, getGenerics());
		query.setParameter("name", "%" + name.toLowerCase() + "%");
		return query.getResultList();
	}

	protected List<T> findLikeAndGroupType(String namedQuery, String name, VariableGroupType type) throws Exception {
		TypedQuery<T> query = em.createNamedQuery(namedQuery, getGenerics());
		query.setParameter("name", "%" + name.toLowerCase() + "%");
		query.setParameter("type", type);
		return query.getResultList();
	}

	protected List<T> findLikeLimited(String namedQuery, String name, int limit) throws Exception {
		TypedQuery<T> query = em.createNamedQuery(namedQuery, getGenerics());
		query.setParameter("name", "%" + name.toLowerCase() + "%");
		query.setMaxResults(limit);
		return query.getResultList();
	}
	
	protected List<T> findByMacLimited(String namedQuery, int limit) throws Exception {
		TypedQuery<T> query = em.createNamedQuery(namedQuery, getGenerics());
		query.setParameter("mac", MyConstants.MAC_ADDRESS);
		query.setMaxResults(limit);
		return query.getResultList();
	}

	protected List<T> findByStringAndMac(String namedQuery, String name) throws Exception {
		TypedQuery<T> query = em.createNamedQuery(namedQuery, getGenerics());
		query.setParameter("name", "%" + name.toLowerCase() + "%");
		query.setParameter("mac", MyConstants.MAC_ADDRESS);
		return query.getResultList();
	}

	protected List<T> findByStringMacAndLong(String namedQuery, String name, Long id) throws Exception {
		TypedQuery<T> query = em.createNamedQuery(namedQuery, getGenerics());
		query.setParameter("name", "%" + name.toLowerCase() + "%");
		query.setParameter("mac", MyConstants.MAC_ADDRESS);
		query.setParameter("id", id);
		return query.getResultList();
	}

	protected List<T> findActiveMacSchedulesByDay(String queryToExecute) throws Exception {
		TypedQuery<T> query = em.createQuery(queryToExecute, getGenerics());
		query.setParameter("state", true);
		query.setParameter("mac", MyConstants.MAC_ADDRESS);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	private Class<T> getGenerics() {
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
}
