package com.mangyBear.Pepe.domain.types.frame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.ImageIcon;

import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.MyConstants;

public enum FramesHelpType {
	
	FRAME_ASK_FAVORITES("Frame Options",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Ask Frame/Import.png", 24, 24),
					MyConstants.setIconSize("images/Ask Frame/Clone.png", 24, 24),
					MyConstants.setIconSize("images/Ask Frame/RoundDelete.png", 24, 24)),
			"The frame where you can choose what to do with an already created item.",
			Arrays.asList("Import", "Clone", "Delete", "Clone Logic Items", "Clone Step Items"),
			Arrays.asList("Imports the item to the working flow.",
					"Creates a new item based on the selected and imports to the working flow.",
					"Deletes the selected favorite from the favorites list.",
					"When cloning a flow you can choose to clone all logic cells inside it.",
					"When cloning a flow you can choose to clone all step cells inside it.")),
	FRAME_ASK_NEW_CELL("Frame New Cell",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Ask Frame/Hexagon.png", 24, 24),
					MyConstants.setIconSize("images/Ask Frame/Rhombus.png", 24, 24),
					MyConstants.setIconSize("images/Ask Frame/Circle.png", 24, 24)),
			"The frame where you can choose what cell you will create.",
			Arrays.asList("Step", "Logic", "End"),
			Arrays.asList("It is the cell that makes one or more actions during the flow execution.",
					"It is the cell that treats conditionals, where you can create bifurcations within the flow.",
					"It is the cell that ends an execution with exportation of results, if enabled.")),
	FRAME_ASK_PAUSE("Execution Paused",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Ask Frame/Play.png", 24, 24),
					MyConstants.setIconSize("images/Ask Frame/Restart.png", 24, 24),
					MyConstants.setIconSize("images/Ask Frame/Stop.png", 24, 24)),
			"The frame where you can choose what to do with the current execution.",
			Arrays.asList("Play", "Restart", "Stop"),
			Arrays.asList("To continue execution from where was paused.",
					"To restart the execution from beginning.",
					"To stop execution.")),
	FRAME_ASK_SEARCH("Frame Options",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Ask Frame/Import.png", 24, 24),
					MyConstants.setIconSize("images/Ask Frame/Clone.png", 24, 24)),
			"The frame where you can choose what to do with an already created item.",
			Arrays.asList("Import", "Clone", "Clone Logic Items", "Clone Step Items"),
			Arrays.asList("Imports the item to the working flow.",
					"Creates a new item based on the selected and imports to the working flow.",
					"When cloning a flow you can choose to clone all logic cells inside it.",
					"When cloning a flow you can choose to clone all step cells inside it.")),
	FRAME_END("Frame End",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18)),
			"The frame where you can export the results of an executed flow.",
			Arrays.asList("Docx", "Log"),
			Arrays.asList("A Microsoft Word Open XML Format Document file with evidences of a flow run.",
					"A file that records events that occur in a flow run.")),
	FRAME_FLOW("Frame Flow",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/RoundComment.png", 28, 16)),
			"The frame where you can edit the name of the flow, objects and variables.",
			Arrays.asList("Name", "Files", "SQLs", "WebDrivers", "Objects", "Tip"),
			Arrays.asList("The name of the flow.",
			"Files are variables that get value from files.",
			"SQLs are variables that get value after a query execution.",
			"WebDrivers are variables that run a web driver.",
			"Objects are variables that can store any type of value.",
			"Fields with blue border are searchable.\n(Type Enter to search)")),
	FRAME_FLOW_FILE("Files",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Flow/FileTab.png", 30, 30),
					MyConstants.setIconSize("images/Frame Flow/RoundAdd.png", 24, 24),
					MyConstants.setIconSize("images/Frame Flow/RoundDelete.png", 24, 24),
					MyConstants.setIconSize("images/Frame Flow/RoundSave.png", 24, 24),
					MyConstants.setIconSize("images/Frame Flow/RoundCancel.png", 24, 24)),
			"The tab where you can create, edit or delete the File variables of a flow.",
			Arrays.asList("Name", "Path", "Parameters"),
			Arrays.asList("The name of the file variable.",
					"The path to the file./n(.csv, .prop or .xml)", "The parameters to get the value according to the file type.")),
	FRAME_FLOW_OBJECT("Objects",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Flow/ObjectTab.png", 30, 30),
					MyConstants.setIconSize("images/Frame Flow/RoundAdd.png", 24, 24),
					MyConstants.setIconSize("images/Frame Flow/RoundDelete.png", 24, 24)),
			"The tab where you can create, edit or delete the Object variables of a flow.",
			Arrays.asList("Name", "Type", "Value"),
			Arrays.asList("The name of the variable.",
					"The type of the variable.",
					"The value of the variable.")),
	FRAME_FLOW_SQL("SQLs",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Flow/SQLTab.png", 30, 30),
					MyConstants.setIconSize("images/Frame Flow/RoundAdd.png", 24, 24),
					MyConstants.setIconSize("images/Frame Flow/RoundDelete.png", 24, 24)),
			"The tab where you can create, edit or delete the SQL variables of a flow.",
			Arrays.asList("Name", "DataBase", "Query"),
			Arrays.asList("The name of the SQL variable.",
					"The data base where the query will be executed.",
					"The query to be executed.")),
	FRAME_FLOW_WEBDRIVER("WebDrivers",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Flow/WebDriverTab.png", 30, 30),
					MyConstants.setIconSize("images/Frame Flow/RoundAdd.png", 24, 24),
					MyConstants.setIconSize("images/Frame Flow/RoundDelete.png", 24, 24)),
			"The tab where you can create, edit or delete the WebDriver variables of a flow.",
			Arrays.asList("Name", "Path", "Type", "Configurations"),
			Arrays.asList("The name of the WebDriver variable.",
					"The path to the file./n(Firefox does not need a web driver)",
					"The type of the variable./n(Chrome, Firefox or Internet Explorer)",
					"The configurations to run the web driver.")),
	FRAME_LOGIC("Frame Logic",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18)),
			"The frame where you can create or edit the logic steps of a flow.",
			Arrays.asList("Name", "Input", "Deny", "Expected", "Tip"),
			Arrays.asList("The name of the current logic.",
					"From where this logic will get the information to compare.",
					"Check to deny the comparsion.",
					"What value to be expected of the variable \"Input\".",
					"Fields with blue border are searchable.\n(Type Enter to search)")),
	FRAME_MAIN("Pépe Automation Center",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Main Frame/LogoPepe.png", 200, 200),
					MyConstants.setIconSize("images/Main Frame/pepeTaskBar.png", 16, 16),
					MyConstants.setIconSize("images/Main Frame/pepeTaskBar.png", 32, 32),
					MyConstants.setIconSize("images/Main Frame/pepeTaskBar.png", 48, 48),
					MyConstants.setIconSize("images/Main Frame/RoundSettings.png", 25, 25),
					MyConstants.setIconSize("images/Main Frame/RoundHelp.png", 25, 25),
					MyConstants.setIconSize("images/Main Frame/RoundExecution.png", 25, 25),
					MyConstants.setIconSize("images/Main Frame/RoundSchedule.png", 25, 25),
					MyConstants.setIconSize("images/Main Frame/RoundRecord.png", 24, 24),
					MyConstants.setIconSize("images/Main Frame/RoundNewFlow.png", 25, 25),
					MyConstants.setIconSize("images/Main Frame/RoundNewCell.png", 25, 25),
					MyConstants.setIconSize("images/Main Frame/RoundSave.png", 25, 25),
					MyConstants.setIconSize("images/Main Frame/RoundTrash.png", 25, 25)),
			"The main frame of the system.\nWhere you can create your best test automations!",
			Arrays.asList("Settings", "Help", "Run", "Schedule", "Record", "New Flow", "New Cell", "Save", "Trash"),
			Arrays.asList("Opens a frame with system settings.", "Opens a frame with all informations.",
					"Runs the selected flow.", "Schedules a run to the selected flow.", "Records user actions that can be turned into an automation flow.",
					"Opens a popup to create a new flow.", "Opens a frame to create step, logic or end cells.", "Saves the selected flow.",
					"Deletes the selected cells or connections from the selected flow.\n(If the \"begin\" cell or nothing is selected, the whole flow is deleted)")),
	FRAME_RECORDER("Frame Recorder",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Recorder/StartRecord.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/StopRecord.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/OpenBrowser.png", 22, 22),
					MyConstants.setIconSize("images/Frame Recorder/CloseBrowser.png", 22, 22)),
			"The frame where you can easily create your test automation flow.",
			Arrays.asList("Web Driver", "Open URL", "Record", "Browser", "Tip"),
			Arrays.asList("The name of the current step.",
					"Finders are the way how the system will locate the object to interact with.",
					"Actions are the interaction with the located object. If more than one, it will execute following the list order.",
					"Fields with blue border are searchable.\n(Type Enter to search)")),
	FRAME_RECORDER_ACTION("Actions",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Recorder/ActionTab.png", 20, 20),
					MyConstants.setIconSize("images/Frame Recorder/RoundSave.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/RoundDelete.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/RoundCancel.png", 24, 24)),
			"The tab where you can create, edit or delete the actions of a step.",
			Arrays.asList("Finder", "Action", "Value", "Step", "Delete", "Parameters", "Tip"),
			Arrays.asList("The finder that the action depends on.",
					"The group where the action belongs.",
					"The action type.",
					"To select the action you want to delete.",
					"The parameters that the action needs.",
					"Two clicks to edit a row.")),
	FRAME_RECORDER_FINDER("Finders",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Recorder/FinderTab.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/RoundSave.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/RoundDelete.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/RoundCancel.png", 24, 24)),
			"The tab where you can create, edit or delete the finders of a step.",
			Arrays.asList("Name", "Type", "Element", "Delete", "Parameters", "Tip"),
			Arrays.asList("The name of the finder.",
					"The type of the finder.",
					"To select the finders you want to delete.",
					"The parameters that the finder needs.",
					"Two clicks to edit a row.\nPress \"F2\" or two clicks to edit the finder name.")),
	FRAME_RECORDER_STEP("Steps",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Recorder/StepTab.png", 20, 20),
					MyConstants.setIconSize("images/Frame Recorder/RoundAdd.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/RoundSave.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/RoundDelete.png", 24, 24),
					MyConstants.setIconSize("images/Frame Recorder/RoundCancel.png", 24, 24)),
			"The tab where you can create, edit or delete the finders of a step.",
			Arrays.asList("Name", "Finder", "Delete", "Parameters", "Tip"),
			Arrays.asList("The name of the finder.",
					"The type of the finder.",
					"To select the finders you want to delete.",
					"The parameters that the finder needs.",
					"Two clicks to edit a row.\nPress \"F2\" or two clicks to edit the finder name.")),
	FRAME_SCHEDULER("Frame Scheduler",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18)),
			"The frame where you can create or edit the schedules of a flow.",
			Arrays.asList("Name", "Flow", "Days", "Time", "Public", "Active/Inactive", "Tip"),
			Arrays.asList("The name of the current schedule. (Searching with Flow field filled will only return schedules with same flow)",
					"The name of the flow to be schedule.",
					"The days when the schedule will be executed.",
					"The time when the schedule will be executed.",
					"If the schedule can be seen and executed for all instances of the software that have access to the same database.",
					"Whether the schedule is active or not.",
					"Fields with blue border are searchable.\n(Type Enter to search)")),
	FRAME_SETTINGS("Frame Settings",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18)),
			"The frame where you can create or edit the logic steps of a flow.",
			Arrays.asList("About Pépe", "Grid Style", "Mouse Delay", "Docx Tmpl", "Docx Export", "Log Export", "PrntScr Export"),
			Arrays.asList("Opens another frame with informations about the system.",
					"How the graph grid will be shown.",
					"The speed to move the mouse.\n(If zero, mouse will move instantly)",
					"The path to a .docx template file that Pépe will use to construct a table of evidence after a flow run.\n(If blank, it will construct a table of evidence in a blank .docx)",
					"The path where Pépe will export the evidences after a flow run.\n(If blank and checked to export in " + GraphType.CELL_END + " cell, it will export to desktop)",
					"The path where Pépe will export a log file after a flow run.\n(If blank and checked to export in " + GraphType.CELL_END + " cell, it will export to desktop)",
					"The path where Pépe will export all screen shots during a flow run.\n(If blank, the screen shots will only be attached to the .docx)")),
	FRAME_STEP("Frame Step",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18)),
			"The frame where you can create or edit the steps of a flow.",
			Arrays.asList("Name", "Finders", "Actions", "Tip"),
			Arrays.asList("The name of the current step.",
					"Finders are the way how the system will locate the object to interact with.",
					"Actions are the interaction with the located object. If more than one, it will execute following the list order.",
					"Fields with blue border are searchable.\n(Type Enter to search)")),
	FRAME_STEP_FINDER("Finders",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Step/FinderTab.png", 24, 24),
					MyConstants.setIconSize("images/Frame Step/RoundAdd.png", 24, 24),
					MyConstants.setIconSize("images/Frame Step/RoundDelete.png", 24, 24)),
			"The tab where you can create, edit or delete the finders of a step.",
			Arrays.asList("Name", "Finder", "Delete", "Parameters", "Tip"),
			Arrays.asList("The name of the finder.",
					"The type of the finder.",
					"To select the finders you want to delete.",
					"The parameters that the finder needs.",
					"Two clicks to edit a row.\nPress \"F2\" or two clicks to edit the finder name.")),
	FRAME_STEP_ACTION("Actions",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18),
					MyConstants.setIconSize("images/Frame Step/ActionTab.png", 20, 20),
					MyConstants.setIconSize("images/Frame Step/RoundAdd.png", 24, 24),
					MyConstants.setIconSize("images/Frame Step/RoundDelete.png", 24, 24)),
			"The tab where you can create, edit or delete the actions of a step.",
			Arrays.asList("Finder", "Group", "Type", "Delete", "Parameters", "Tip"),
			Arrays.asList("The finder that the action depends on.",
					"The group where the action belongs.",
					"The action type.",
					"To select the action you want to delete.",
					"The parameters that the action needs.",
					"Two clicks to edit a row.")),
	PANEL_SEARCH("Work Area",
			Arrays.asList(MyConstants.setIconSize("images/RoundInformation.png", 18, 18)),
			"The area where you create, edit or delete test automations",
			Arrays.asList("Favorites", "Search Results", "Graph", "Tip"),
			Arrays.asList("Everything you find most important.",
					"Where you can search steps, logics and flows.\n(Minimum three characters and press enter to search)",
					"A tab to create/edit a test automation.",
					"You can drag and drop cells between both lists and graphs.\n(To add a favorite, drop cells inside the list. (Except end cells) To remove, drop cells outside the list)"));
	
	private final String name;
	private final List<ImageIcon> icons;
	private final String description;
	private final List<String> labels;
	private final List<String> infos;

	private FramesHelpType(String name, List<ImageIcon> icons, String description, List<String> labels, List<String> infos) {
		this.name = name;
		this.icons = icons;
		this.description = description;
		this.labels = labels;
		this.infos = infos;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public String getName() {
		return name;
	}

	public List<ImageIcon> getIcons() {
		return icons;
	}

	public String getDescription() {
		return description;
	}

	public List<String> getLabels() {
		return labels;
	}

	public List<String> getInfos() {
		return infos;
	}

    public static List<FramesHelpType> getAllFrameHelpTypes() {
        return Arrays.asList(FramesHelpType.values());
    }
    
    public static List<FramesHelpType> getFrameHelpTypesLike(String name, boolean byName) {
    	List<FramesHelpType> typeList = new ArrayList<>();
    	for (FramesHelpType frameHelp : FramesHelpType.values()) {
    		if (frameHelp.toString().toLowerCase().contains(name.toLowerCase())) {
    			typeList.add(frameHelp);
    		}
    	}
    	return byName ? sortFrameHelpTypeByName(typeList) : typeList;
    }

    private static List<FramesHelpType> sortFrameHelpTypeByName(List<FramesHelpType> listToSort) {
    	Collections.sort(listToSort, new Comparator<FramesHelpType>() {
            @Override
            public int compare(FramesHelpType o1, FramesHelpType o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        return listToSort;
    }
}
