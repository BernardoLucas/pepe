package com.mangyBear.Pepe.domain.entityRun;

import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.types.run.StatusType;

public class LogicResultRun {

	private String date;
	private Logic logic;
	private Object logicReturn;
	private StatusType logicStatus;

	@Override
	public String toString() {
		return this.getLogic().getNmLogic();
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Logic getLogic() {
		return logic;
	}

	public void setLogic(Logic logic) {
		this.logic = logic;
	}

	public Object getLogicReturn() {
		return logicReturn;
	}

	public void setLogicReturn(Object logicReturn) {
		this.logicReturn = logicReturn;
	}

	public StatusType getLogicStatus() {
		return logicStatus;
	}

	public void setLogicStatus(StatusType logicStatus) {
		this.logicStatus = logicStatus;
	}
}