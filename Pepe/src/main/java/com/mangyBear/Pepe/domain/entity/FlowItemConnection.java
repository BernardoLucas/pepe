package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="flow_item_connection")
@NamedQueries({
	@NamedQuery(name = "existsConnection", query = "SELECT 1 from FlowItemConnection entity where entity.previousItem.id = :idPrevious and entity.currentItem.id = :idCurrent"),
	@NamedQuery(name = "removeByFlowItem", query = "delete  from FlowItemConnection entity where entity.previousItem.id = :idFlowItem or entity.currentItem.id = :idFlowItem")
	})
public class FlowItemConnection implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_flow_item_connection", nullable=false, unique=true)
	private Long id;
	
	@Column(name="condition")
    private String condition;

	@ManyToOne
	@JoinColumn(name="id_previous_item")
	private FlowItem previousItem;
	
	@ManyToOne
	@JoinColumn(name="id_current_item")
	private FlowItem currentItem;
	
	@Override
	public String toString() {
		return this.getCondition();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public FlowItem getPreviousItem() {
		return previousItem;
	}

	public void setPreviousItem(FlowItem previousItem) {
		this.previousItem = previousItem;
	}

	public FlowItem getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(FlowItem currentItem) {
		this.currentItem = currentItem;
	}
	
	public FlowItemConnection clone() {
        try {
        	FlowItemConnection clone = (FlowItemConnection) super.clone();
        	clone.setId(null);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in FlowItemConnection. " );
            return this;
        }
    }
}
