package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mangyBear.Pepe.domain.types.action.FinderType;

@Entity
@Table(name="finder")
@NamedQueries({
	@NamedQuery(name = "finderByName", query = "SELECT entity from Finder entity where lower(nmFinder) = :name"),
	@NamedQuery(name = "finderLikeName", query = "SELECT entity from Finder entity where lower(nmFinder) LIKE :name")})
public class Finder implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_finder", nullable=false, unique=true)
	private Long id;
	
	@Column(name="nm_finder", nullable=false, unique=true)
	private String nmFinder;
    
	@Column(name="tp_finder", nullable=false)
	@Enumerated(EnumType.STRING)
	private FinderType finderType;
	
	@ManyToOne
	@JoinColumn(name="id_step")
	private Step step;
    
	@OneToMany(mappedBy="finder", cascade = CascadeType.ALL)
    private List<FinderParameter> parameters;

	@Override
	public String toString() {
		return this.getNmFinder();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmFinder() {
		return nmFinder;
	}

	public void setNmFinder(String nmFinder) {
		this.nmFinder = nmFinder;
	}

	public FinderType getFinderType() {
		return finderType;
	}

	public void setFinderType(FinderType finderType) {
		this.finderType = finderType;
	}
    
	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public List<FinderParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<FinderParameter> parameters) {
		this.parameters = parameters;
	}
	
	public Finder clone() {
        try {
            Finder clone = (Finder) super.clone();
            clone.setId(null);
            clone.setNmFinder(clone.getNmFinder() + "-CLONE-");
            setParametersClones(clone);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in Finder. " );
            return this;
        }
    }
	
	private void setParametersClones(Finder clone) {
		if (clone.getParameters() == null || clone.getParameters().isEmpty()) {
			return;
		}
		
		clone.setParameters(new ArrayList<FinderParameter>());
		for (FinderParameter parameter : parameters) {
			FinderParameter parameterClone = parameter.clone();
			parameterClone.setFinder(clone);
			clone.getParameters().add(parameterClone);
		}
	}
}
