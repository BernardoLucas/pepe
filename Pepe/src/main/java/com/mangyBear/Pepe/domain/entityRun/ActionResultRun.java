package com.mangyBear.Pepe.domain.entityRun;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.types.run.StatusType;

public class ActionResultRun {

	private String finderDate;
	private Finder finder;
	private Object finderReturn;
	private StatusType finderStatus;
	private String actionDate;
	private Action action;
	private Object actionReturn;
	private StatusType actionStatus;

	@Override
	public String toString() {
		return this.getAction().getActionType().toString();
	}

	public String getFinderDate() {
		return finderDate;
	}

	public void setFinderDate(String finderDate) {
		this.finderDate = finderDate;
	}

	public Finder getFinder() {
		return finder;
	}

	public void setFinder(Finder finder) {
		this.finder = finder;
	}

	public Object getFinderReturn() {
		return finderReturn;
	}

	public void setFinderReturn(Object finderReturn) {
		this.finderReturn = finderReturn;
	}

	public StatusType getFinderStatus() {
		return finderStatus;
	}

	public void setFinderStatus(StatusType finderStatus) {
		this.finderStatus = finderStatus;
	}

	public String getActionDate() {
		return actionDate;
	}

	public void setActionDate(String actionDate) {
		this.actionDate = actionDate;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Object getActionReturn() {
		return actionReturn;
	}

	public void setActionReturn(Object actionReturn) {
		this.actionReturn = actionReturn;
	}

	public StatusType getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(StatusType actionStatus) {
		this.actionStatus = actionStatus;
	}
}