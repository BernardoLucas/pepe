package com.mangyBear.Pepe.domain.types.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum ActionGroupType {

    CONTROL_ACTION_GROUP("Control Action", FinderType.CONTROL, true),
    CONTROL_CHECKBOX_GROUP("CheckBox", FinderType.CONTROL, true),
    CONTROL_COMBOBOX_GROUP("ComboBox", FinderType.CONTROL, true),
    CONTROL_COMMAND_GROUP("Command", FinderType.CONTROL, true),
    CONTROL_LIST_VIEW_GROUP("List View", FinderType.CONTROL, true),
    CONTROL_TREE_VIEW_GROUP("Tree View", FinderType.CONTROL, true),
    
    FILE_GROUP("File", FinderType.FILE, true),
    
    LIST_GROUP("List", FinderType.NO_FINDER, true),
    
    MATH_GROUP("Math", FinderType.NO_FINDER, true),
    
    NOFINDER_DESKTOP_GROUP("Desktop", FinderType.NO_FINDER, true),
    NOFINDER_MOUSE_GROUP("Mouse", FinderType.NO_FINDER, true),
    
    POSITION_GROUP("Position", FinderType.POSITION, true),
    
    PROCESS_GROUP("Process", FinderType.PROCESS, true),
    
    REGISTRY_GROUP("Registry", FinderType.REGISTRY, true),
    
    SCREENREGION_IMAGE_GROUP("Image", FinderType.SCREEN_REGION, true),
    SCREENREGION_PIXEL_GROUP("Pixel", FinderType.SCREEN_REGION, true),
    
    STRING_GROUP("String", FinderType.NO_FINDER, true),
    
    WEB_ALERT_GROUP("Web Alert", FinderType.WEB_ALERT, true),
    WEB_BASIC_GROUP("Web", FinderType.WEB_NO_FINDER, true),
    WEB_ELEMENT_GROUP("Web Element", FinderType.WEB_ELEMENT, true),
    WEB_JAVA_SCRIPT_GROUP("JavaScript", FinderType.WEB_NO_FINDER, true),
    WEB_GROUP("Web Manage", FinderType.WEB_NO_FINDER, true),
    WEB_IME_GROUP("Web IME", FinderType.WEB_NO_FINDER, true),
    WEB_LOGS_GROUP("Web Logs", FinderType.WEB_NO_FINDER, true),
    WEB_TIMEOUTS_GROUP("Web TimeOuts", FinderType.WEB_NO_FINDER, true),
    WEB_WINDOW_GROUP("Web Window", FinderType.WEB_NO_FINDER, true),
    WEB_NAVIGATE_GROUP("Web Navigate", FinderType.WEB_NO_FINDER, true),
    WEB_SWITCH_TO_GROUP("Web Switch To", FinderType.WEB_NO_FINDER, true),
    
    WINDOW_GROUP("Window", FinderType.WINDOW, true);

    private final String name;
    private final FinderType finderType;
    private final boolean visible;

    private ActionGroupType(String name, FinderType finderType, boolean visible) {
        this.name = name;
        this.finderType = finderType;
        this.visible = visible;
    }

    @Override
    public String toString() {
        return getName();
    }

    public String getName() {
        return name;
    }

    public FinderType getFinderType() {
        return finderType;
    }

    public boolean isVisible() {
        return visible;
    }

    public static List<ActionGroupType> getGroupsWithNoFinder(boolean byName) {
        List<ActionGroupType> groupList = new ArrayList<>();
        for (ActionGroupType group : ActionGroupType.values()) {
            if (group.getFinderType().equals(FinderType.WEB_NO_FINDER)
                    || group.getFinderType().equals(FinderType.NO_FINDER)) {
                groupList.add(group);
            }
        }
        return byName ? sortActionGroupType(groupList) : groupList;
    }

    public static List<ActionGroupType> getGroupsByFinder(FinderType finder, boolean byName) {
        List<ActionGroupType> groupList = new ArrayList<>();
        for (ActionGroupType group : ActionGroupType.values()) {
            if (group.getFinderType().equals(finder) && group.isVisible()) {
                groupList.add(group);
            }
        }
        return byName ? sortActionGroupType(groupList) : groupList;
    }
	
	private static List<ActionGroupType> sortActionGroupType(List<ActionGroupType> listToSort) {
		Collections.sort(listToSort, new Comparator<ActionGroupType>() {
			@Override
			public int compare(ActionGroupType o1, ActionGroupType o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
		return listToSort;
	}
}
