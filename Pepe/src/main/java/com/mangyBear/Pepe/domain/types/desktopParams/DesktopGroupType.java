package com.mangyBear.Pepe.domain.types.desktopParams;

public enum DesktopGroupType {
	
	DRIVE_OPTION("Drive Option"),
	PROCESS_PRIORITY("Process Priority"),
	RUN_STATE("Run State"),
	SHUTDOWN("Shutdown"),
	WINDOW_STATE("Window State");

	private final String name;

    private DesktopGroupType(String name) {
        this.name = name;
    }

    @Override
	public String toString() {
		return getName();
	}

	public String getName() {
		return name;
	}
}
