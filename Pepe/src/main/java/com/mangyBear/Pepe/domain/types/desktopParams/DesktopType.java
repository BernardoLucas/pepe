package com.mangyBear.Pepe.domain.types.desktopParams;

import java.util.ArrayList;
import java.util.List;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public enum DesktopType {

//	SW_HIDE("Hide",
//			AutoItX.SW_HIDE,
//			DesktopGroupType.WINDOW_STATE),
//	SW_MAXIMIZE("Maximize",
//			AutoItX.SW_MAXIMIZE,
//			DesktopGroupType.WINDOW_STATE),
//	SW_RESTORE("Restore",
//			AutoItX.SW_RESTORE,
//			DesktopGroupType.WINDOW_STATE),
//	SW_SHOW("Show",
//			AutoItX.SW_SHOW,
//			DesktopGroupType.WINDOW_STATE),
	SW_SHOW_DEFAULT("Show Default",
			AutoItX.SW_SHOWDEFAULT,
			DesktopGroupType.WINDOW_STATE),
	SW_SHOW_MAXIMIZED("Show Maximized",
			AutoItX.SW_SHOWMAXIMIZED,
			DesktopGroupType.WINDOW_STATE),
	SW_SHOW_MINIMIZED("Show Minimized",
			AutoItX.SW_SHOWMINIMIZED,
			DesktopGroupType.WINDOW_STATE),
	SW_SHOW_MINNOACTIVE("Show Minimized No Active",
			AutoItX.SW_SHOWMINNOACTIVE,
			DesktopGroupType.WINDOW_STATE),
	SW_SHOW_NA("Show No Active",
			AutoItX.SW_SHOWNA,
			DesktopGroupType.WINDOW_STATE),
	SW_SHOW_NOACTIVATE("Show No Activate",
			AutoItX.SW_SHOWNOACTIVATE,
			DesktopGroupType.WINDOW_STATE),
	SW_SHOW_NORMAL("Show Normal",
			AutoItX.SW_SHOWNORMAL,
			DesktopGroupType.WINDOW_STATE),

	PP_IDLE_LOW("Idle/Low",
			0,
			DesktopGroupType.PROCESS_PRIORITY),
	PP_BELOW_NORMAL("Below Normal",
			1,
			DesktopGroupType.PROCESS_PRIORITY),
	PP_NORMAL("Normal",
			2,
			DesktopGroupType.PROCESS_PRIORITY),
	PP_ABOVE_NORMAL("Above Normal",
			3,
			DesktopGroupType.PROCESS_PRIORITY),
	PP_HIGH("High",
			4,
			DesktopGroupType.PROCESS_PRIORITY),
	PP_REALTIME("Realtime",
			5,
			DesktopGroupType.PROCESS_PRIORITY),

	// DT_DRIVETYPE("DRIVE TYPE",
	// 1,
	// "PROCESS PRIORITY"),
	// DT_SSDSTATUS("SSD STATUS",
	// 2,
	// "PROCESS PRIORITY"),
	// DT_BUSTYPE("BUS TYPE",
	// 3,
	// "PROCESS PRIORITY"),

	SD_LOGOFF("LOGOFF",
			0,
			DesktopGroupType.SHUTDOWN),
	SD_SHUTDOWN("SHUTDOWN",
			1,
			DesktopGroupType.SHUTDOWN),
	SD_REBOOT("REBOOT",
			2,
			DesktopGroupType.SHUTDOWN),
	SD_FORCE("FORCE",
			4,
			DesktopGroupType.SHUTDOWN),
	SD_POWERDOWN("POWERDOWN",
			8,
			DesktopGroupType.SHUTDOWN),
	SD_FORCEHUNG("FORCEHUNG",
			16,
			DesktopGroupType.SHUTDOWN),
	SD_STANDBY("STANDBY",
			32,
			DesktopGroupType.SHUTDOWN),
	SD_HIBERNATE("HIBERNATE",
			64,
			DesktopGroupType.SHUTDOWN),

	ON_NO_PROFILE("Logon No Profile",
			0,
			DesktopGroupType.RUN_STATE),
	ON_PROFILE("Logon Profile",
			1,
			DesktopGroupType.RUN_STATE),
	WORK_CREDENTIALS("Network Credentials",
			2,
			DesktopGroupType.RUN_STATE),
	ERIT_PROCESS_ENVIRONMENT("Inherit Process's Environment",
			4,
			DesktopGroupType.RUN_STATE),

	DMA_DEFAULT("Default",
			0,
			DesktopGroupType.DRIVE_OPTION),
	DMA_PERSISTENT_MAPPING("Persistent Mapping",
			1,
			DesktopGroupType.DRIVE_OPTION),
	DMA_AUTHENTICATION_DIALOG("Authentication Dialog",
			8,
			DesktopGroupType.DRIVE_OPTION);

	private final String name;
	private final Integer value;
	private final DesktopGroupType group;

	private DesktopType(String name, Integer value, DesktopGroupType group) {
		this.name = name;
		this.value = value;
		this.group = group;
	}

	@Override
	public String toString() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public Integer getValue() {
		return value;
	}

	public DesktopGroupType getGroup() {
		return group;
	}
	
	public static List<DesktopType> getDesktopTypeByGroup(DesktopGroupType group) {
		List<DesktopType> desktopTypeList = new ArrayList<>();
		for (DesktopType desktopType : DesktopType.values()) {
			if (desktopType.getGroup().equals(group)) {
				desktopTypeList.add(desktopType);
			}
		}
		return desktopTypeList;
	}

	public static int getDesktopTypeValueByName(String name) {
		for (DesktopType desktopType : DesktopType.values()) {
			if (desktopType.getName().equalsIgnoreCase(name)) {
				return desktopType.getValue();
			}
		}
		return 0;
	}
}
