package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "finder_parameter")
@NamedQuery(name = "finderParamLikeValue", query = "SELECT entity from FinderParameter entity where lower(value) LIKE :name")
public class FinderParameter implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_finder_parameter", nullable = false, unique = true)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "value")
	private String value;

	@ManyToOne
	@JoinColumn(name = "id_finder")
	private Finder finder;

	@Override
	public String toString() {
		return this.getName() + "=" + this.getValue();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Finder getFinder() {
		return finder;
	}

	public void setFinder(Finder finder) {
		this.finder = finder;
	}
	
	@Override
	public int hashCode() {
        return getValue().hashCode();
    }
	
	@Override
	public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FinderParameter)) {
            return false;
        }
        FinderParameter other = (FinderParameter) obj;
        return this.getValue().equals(other.getValue());
    }
	
	public FinderParameter clone() {
        try {
        	FinderParameter clone = (FinderParameter) super.clone();
            clone.setId(null);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in FinderParameter. " );
            return this;
        }
    }
}
