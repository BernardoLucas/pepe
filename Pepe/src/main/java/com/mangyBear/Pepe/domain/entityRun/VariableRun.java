package com.mangyBear.Pepe.domain.entityRun;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.VarObject;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.entity.VariableParameter;
import com.mangyBear.Pepe.domain.types.variable.VariableType;

public class VariableRun {

	private Long id;
	private Flow flowDad;
	private VarObject object;
	private String name;
	private VariableType variableType;
	private List<VariableParameter> parameters;
	private Object value;

	public VariableRun(Variable variable) {
		setId(variable.getId());
		setFlowDad(variable.getFlowDad());
		setObject(variable.getVarObject());
		setName(variable.getName());
		setVariableType(variable.getVariableType());
		setParameters(variable.getParameters());
		setValue(getVariableValue(variable));
	}

	public VariableRun(String name, Object value, VariableType type) {
		setName(name);
		setVariableType(type);
		setValue(value);
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idValue) {
		this.id = idValue;
	}

	public Flow getFlowDad() {
		return flowDad;
	}

	public void setFlowDad(Flow flowDad) {
		this.flowDad = flowDad;
	}

	public VarObject getObject() {
		return object;
	}

	public void setObject(VarObject object) {
		this.object = object;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public VariableType getVariableType() {
		return variableType;
	}

	public void setVariableType(VariableType variableType) {
		this.variableType = variableType;
	}

	public List<VariableParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<VariableParameter> parameters) {
		this.parameters = parameters;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	private Object getVariableValue(Variable variable) {
		if (VariableType.VARIABLE_OBJECT_CHANGEABLE.equals(variable.getVariableType())
				|| VariableType.VARIABLE_OBJECT_FINAL.equals(variable.getVariableType())) {
//			Object[] objArray = buildChangeableAndFinalParams(variable);
//			return objArray[objArray.length - 1];
			return variable.getParameters().get(0).getValue();
		}
		
		return "";
	}

//	private Object[] buildChangeableAndFinalParams(Variable variable) {
//		VariableType variableType = variable.getVariableType();
//		List<String> params = variableType.getHelpAndFrameType().getLabels();
//		List<Object> paramValue = new ArrayList<>();
//		for (String nameParam : params) {
//			for (VariableParameter param : variable.getParameters()) {
//				if (nameParam.equalsIgnoreCase(param.getName())) {
//					paramValue.add(param.getValue());
//				}
//			}
//		}
//		return paramValue.toArray();
//	}
}
