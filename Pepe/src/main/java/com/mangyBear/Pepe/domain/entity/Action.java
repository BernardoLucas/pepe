package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mangyBear.Pepe.domain.types.action.ActionType;

@Entity
@Table(name="action")
public class Action implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;
    
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_action", nullable=false, unique=true)
    private Long id;
	
	@Column(name="tp_action", nullable=false)
	@Enumerated(EnumType.STRING)
    private ActionType actionType;
	
	@ManyToOne
	@JoinColumn(name="id_finder")
	private Finder finder;
	
	@ManyToOne
	@JoinColumn(name="id_step")
	private Step step;
	
	@Column(name="nr_order", nullable=false)
    private Integer order;
	
	@OneToMany(mappedBy="action", cascade = CascadeType.ALL)
    private List<ActionParameter> parameters;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public ActionType getActionType() {
		return actionType;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public Finder getFinder() {
		return finder;
	}

	public void setFinder(Finder finder) {
		this.finder = finder;
	}
    
	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public List<ActionParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<ActionParameter> parameters) {
		this.parameters = parameters;
	}
	
	public Action clone() {
        try {
        	Action clone = (Action) super.clone();
            clone.setId(null);
            setParametersClones(clone);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in Action. " );
            return this;
        }
    }
	
	private void setParametersClones(Action clone) {
		if (clone.getParameters() == null || clone.getParameters().isEmpty()) {
			return;
		}
		
		clone.setParameters(new ArrayList<ActionParameter>());
		for (ActionParameter parameter : parameters) {
			ActionParameter parameterClone = parameter.clone();
			parameterClone.setAction(clone);
			clone.getParameters().add(parameterClone);
		}
	}
}
