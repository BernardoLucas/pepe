package com.mangyBear.Pepe.domain.types.action;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.mangyBear.Pepe.domain.types.frame.FrameAddType;

/**
 * @author Lucas Silva Bernardo
 */
public enum FinderType {

	CONTROL(
			"Control|?|finded",
			FrameAddType.FINDER_CONTROL,
			ReflectionType.FINDER_CONTROL,
			true,
			Color.BLACK,
			3.0f,
			true),
	FILE(
			"File|?|finded",
			FrameAddType.FINDER_FILE,
			ReflectionType.FINDER_FILE,
			false,
			null,
			null,
			true),
	WEB_ALERT(
			"Web alert|?|finded",
			FrameAddType.FINDER_WEB_ALERT,
			ReflectionType.FINDER_WEB_ALERT,
			false,
			null,
			null,
			true),
	WEB_ELEMENT(
			"Web element|?|finded",
			FrameAddType.FINDER_WEB_ELEMENT,
			ReflectionType.FINDER_WEB_ELEMENT,
			false,
			null,
			null,
			true),
	NO_FINDER(
			"Desktop action that do not need finder",
			FrameAddType.FINDER_NO_FINDER,
			ReflectionType.FINDER_NO_FINDER,
			false,
			null,
			null,
			false),
	POSITION(
			"Position|?|finded",
			FrameAddType.FINDER_POSITION,
			ReflectionType.FINDER_POSITION,
			true,
			null,
			null,
			true),
	PROCESS(
			"Process|?|finded",
			FrameAddType.FINDER_PROCESS,
			ReflectionType.FINDER_PROCESS,
			false,
			null,
			null,
			true),
	REGISTRY(
			"Registry|?|finded",
			FrameAddType.FINDER_REGISTRY,
			ReflectionType.FINDER_REGISTRY,
			false,
			null,
			null,
			true),
	SCREEN_REGION(
			"Screen Region|?|finded",
			FrameAddType.FINDER_SCREEN_REGION,
			ReflectionType.FINDER_SCREEN_REGION,
			true,
			Color.RED,
			2.0f,
			true),
	WEB_NO_FINDER(
			"Web action that do not need finder",
			FrameAddType.FINDER_WEB_NO_FINDER,
			ReflectionType.FINDER_WEB_NO_FINDER,
			false,
			null,
			null,
			false),
	WINDOW(
			"Window|?|finded",
			FrameAddType.FINDER_WINDOW,
			ReflectionType.FINDER_WINDOW,
			true,
			Color.GRAY,
			3.0f,
			true);

	private final String message;
	private final FrameAddType frameAddType;
	private final ReflectionType reflectionType;
	private final boolean catchable;
	private final Color catchBorderColor;
	private final Float catchBorderWidth;
	private final boolean visible;

	private FinderType(String message, FrameAddType frameAddType, ReflectionType reflectionType,
			boolean catchable, Color catchBorderColor, Float catchBorderWidth, boolean visible) {
		this.message = message;
		this.frameAddType = frameAddType;
		this.reflectionType = reflectionType;
		this.catchable = catchable;
		this.catchBorderColor = catchBorderColor;
		this.catchBorderWidth = catchBorderWidth;
		this.visible = visible;
	}

	@Override
	public String toString() {
		return this.getFrameAddType().getName();
	}

	public String getMessage() {
		return message;
	}

	public String getSucessMessage() {
		return this.getMessage().replace("|?|", " ");
	}

	public String getErrorMessage() {
		return this.getMessage().replace("|?|", " not ");
	}
	
	public FrameAddType getFrameAddType() {
		return frameAddType;
	}

	public ReflectionType getReflectionType() {
		return reflectionType;
	}
	
	public boolean isCatchable() {
		return catchable;
	}

	public Color getCatchBorderColor() {
		return catchBorderColor;
	}

	public Float getCatchBorderWidth() {
		return catchBorderWidth;
	}

	public boolean isVisible() {
		return visible;
	}

	public static List<FinderType> getVisibleFinders(boolean byName) {
		List<FinderType> visibleFinders = new ArrayList<>();
		for (FinderType finder : FinderType.values()) {
			if (finder.isVisible()) {
				visibleFinders.add(finder);
			}
		}
		return byName ? sortFinderByName(visibleFinders) : visibleFinders;
	}
    
    public static List<FinderType> getFinderTypesLike(String name, boolean byName) {
    	List<FinderType> typeList = new ArrayList<>();
    	for (FinderType finder : FinderType.values()) {
    		if (finder.toString().toLowerCase().contains(name.toLowerCase())) {
    			typeList.add(finder);
    		}
    	}
    	return byName ? sortFinderByName(typeList) : typeList;
    }

    private static List<FinderType> sortFinderByName(List<FinderType> listToSort) {
    	Collections.sort(listToSort, new Comparator<FinderType>() {
            @Override
            public int compare(FinderType o1, FinderType o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        return listToSort;
    }
}
