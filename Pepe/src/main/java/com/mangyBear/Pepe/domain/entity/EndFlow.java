package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="endFlow")
public class EndFlow implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_end", nullable=false, unique=true)
	private Long id;
	
	@Column(name="bl_docx", nullable=false)
	private Boolean exportDocx;
	
	@Column(name="bl_log", nullable=false)
	private Boolean exportLog;
    
	@Override
	public String toString() {
		return this.getExportDocx() + " - " + this.getExportLog();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getExportDocx() {
		return exportDocx;
	}

	public void setExportDocx(Boolean exportDocx) {
		this.exportDocx = exportDocx;
	}

	public Boolean getExportLog() {
		return exportLog;
	}

	public void setExportLog(Boolean exportLog) {
		this.exportLog = exportLog;
	}
}
