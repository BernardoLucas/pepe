package com.mangyBear.Pepe.domain.types.action;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.security.Credentials;

import com.mangyBear.Pepe.action.control.BasicControlGroup;
import com.mangyBear.Pepe.action.control.CheckBoxGroup;
import com.mangyBear.Pepe.action.control.ComboBoxGroup;
import com.mangyBear.Pepe.action.control.CommandGroup;
import com.mangyBear.Pepe.action.control.ListViewGroup;
import com.mangyBear.Pepe.action.control.TreeViewGroup;
import com.mangyBear.Pepe.action.file.ReadGroup;
import com.mangyBear.Pepe.action.file.RunGroup;
import com.mangyBear.Pepe.action.file.WriteGroup;
import com.mangyBear.Pepe.action.noFinder.DesktopGroup;
import com.mangyBear.Pepe.action.noFinder.MouseGroup;
import com.mangyBear.Pepe.action.noFinder.QueryGroup;
import com.mangyBear.Pepe.action.position.PositionGroup;
import com.mangyBear.Pepe.action.process.ProcessGroup;
import com.mangyBear.Pepe.action.registry.RegistryGroup;
import com.mangyBear.Pepe.action.screenRegion.ImageGroup;
import com.mangyBear.Pepe.action.screenRegion.PixelGroup;
import com.mangyBear.Pepe.action.variableManagement.ListGroup;
import com.mangyBear.Pepe.action.variableManagement.MathGroup;
import com.mangyBear.Pepe.action.variableManagement.StringGroup;
import com.mangyBear.Pepe.action.web.AlertGroup;
import com.mangyBear.Pepe.action.web.BasicWebGroup;
import com.mangyBear.Pepe.action.web.ElementGroup;
import com.mangyBear.Pepe.action.web.JavascriptExecutorGroup;
import com.mangyBear.Pepe.action.web.ManageGroup;
import com.mangyBear.Pepe.action.web.ManageImeGroup;
import com.mangyBear.Pepe.action.web.ManageLogsGroup;
import com.mangyBear.Pepe.action.web.ManageNavigateGroup;
import com.mangyBear.Pepe.action.web.ManageSwitchToGroup;
import com.mangyBear.Pepe.action.web.ManageTimeoutsGroup;
import com.mangyBear.Pepe.action.web.ManageWindowGroup;
import com.mangyBear.Pepe.action.window.WindowGroup;
import com.mangyBear.Pepe.service.Finder.Control;
import com.mangyBear.Pepe.service.Finder.Locator;
import com.mangyBear.Pepe.service.Finder.Window;

public enum ReflectionType {
	
	FINDER_CONTROL(
			Arrays.asList(Object.class, String.class, String.class),
			Locator.class,
			"locateControl",
			Control.class),
	FINDER_FILE(
			Arrays.asList(String.class),
			Locator.class,
			"locateFile",
			File.class),
	FINDER_WEB_ALERT(
			Arrays.asList(WebDriver.class, String.class),
			Locator.class,
			"locateWebAlert",
			Alert.class),
	FINDER_WEB_ELEMENT(
			Arrays.asList(WebDriver.class, String.class, String.class, String.class),
			Locator.class,
			"locateWebElement",
			WebElement.class),
	FINDER_NO_FINDER(
			Arrays.asList(),
			null,
			"",
			null),
	FINDER_POSITION(
			Arrays.asList(String.class, String.class, String.class, String.class),
			Locator.class,
			"locatePosition",
			List.class),//Point to List<String>
	FINDER_PROCESS(
			Arrays.asList(String.class),
			Locator.class,
			"locateProcess",
			String.class),
	FINDER_REGISTRY(
			Arrays.asList(String.class),
			Locator.class,
			"locateRegistry",
			String.class),
	FINDER_SCREEN_REGION(
			Arrays.asList(String.class, String.class, String.class, String.class),
			Locator.class,
			"locateScreenRegion",
			List.class),//Rectangle to List<String>
	FINDER_WEB_NO_FINDER(
			Arrays.asList(),
			null,
			"",
			null),
	FINDER_WINDOW(
			Arrays.asList(Object.class, String.class),
			Locator.class,
			"locateWindow",
			Window.class),
    		//TODO: just for separation
    CONTROL_CLICK(
			Arrays.asList(Control.class, String.class, String.class),
			BasicControlGroup.class,
			"click",
			Boolean.class),
    CONTROL_DISABLE(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"disable",
			Boolean.class),
    CONTROL_ENABLE(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"enable",
			Boolean.class),
    CONTROL_FOCUS(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"focus",
			Boolean.class),
    CONTROL_GET_FOCUS(
			Arrays.asList(String.class),
			BasicControlGroup.class,
			"getFocus",
			String.class),
    CONTROL_GET_HANDLE(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"getHandle",
			String.class),
    CONTROL_GET_TEXT(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"getText",
			String.class),
    CONTROL_GET_POSITION(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"getPosition",
			List.class),//Point to List<String>
    CONTROL_GET_SIZE(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"getSize",
			List.class),//Dimension to List<String>
    CONTROL_HIDE(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"hide",
			Boolean.class),
    CONTROL_MOVE(
			Arrays.asList(Control.class, String.class, String.class),
			BasicControlGroup.class,
			"move",
			Boolean.class),
    CONTROL_RESIZE(
			Arrays.asList(Control.class, String.class, String.class),
			BasicControlGroup.class,
			"resize",
			Boolean.class),
    CONTROL_SEND(
			Arrays.asList(Control.class, String.class, String.class),
			BasicControlGroup.class,
			"send",
			Boolean.class),
    CONTROL_SET_TEXT(
			Arrays.asList(Control.class, String.class),
			BasicControlGroup.class,
			"setText",
			Boolean.class),
    CONTROL_SHOW(
			Arrays.asList(Control.class),
			BasicControlGroup.class,
			"show",
			Boolean.class),
    		//TODO: just for separation
    CONTROL_CHECKBOX_CHECK(
			Arrays.asList(Control.class),
			CheckBoxGroup.class,
			"check",
			null),
    CONTROL_CHECKBOX_ISCHECKED(
			Arrays.asList(Control.class),
			CheckBoxGroup.class,
			"isChecked",
			Boolean.class),
    CONTROL_CHECKBOX_UNCKECK(
			Arrays.asList(Control.class),
			CheckBoxGroup.class,
			"uncheck",
			null),
    		//TODO: just for separation
    CONTROL_COMBOBOX_ADD_STRING(
    		Arrays.asList(Control.class, String.class),
    		ComboBoxGroup.class,
    		"addString",
    		null),
    CONTROL_COMBOBOX_DELETE_STRING(
    		Arrays.asList(Control.class, String.class),
    		ComboBoxGroup.class,
    		"deleteString",
    		null),
    CONTROL_COMBOBOX_FIND_STRING(
    		Arrays.asList(Control.class, String.class),
    		ComboBoxGroup.class,
    		"findString",
    		Boolean.class),
    CONTROL_COMBOBOX_GET_CURRENT_SELECTION(
    		Arrays.asList(Control.class, String.class),
    		ComboBoxGroup.class,
    		"getCurrentSelection",
    		String.class),
    CONTROL_COMBOBOX_HIDE_DROPDOWN(
    		Arrays.asList(Control.class),
    		ComboBoxGroup.class,
    		"hideDropdown",
    		null),
    CONTROL_COMBOBOX_SELECT_STRING(
    		Arrays.asList(Control.class, String.class),
    		ComboBoxGroup.class,
    		"selectString",
    		null),
    CONTROL_COMBOBOX_SET_CURRENT_SELECTION(
    		Arrays.asList(Control.class, String.class),
    		ComboBoxGroup.class,
    		"setCurrentSelection",
    		null),
    CONTROL_COMBOBOX_SHOW_DROPDOWN(
    		Arrays.asList(Control.class),
    		ComboBoxGroup.class,
    		"showDropdown",
    		null),
			//TODO: just for separation
    CONTROL_COMMAND_CURRENT_TAB(
    		Arrays.asList(Control.class),
    		CommandGroup.class,
    		"currentTab",
    		null),
    CONTROL_COMMAND_EDIT_PASTE(
    		Arrays.asList(Control.class, String.class),
    		CommandGroup.class,
    		"editPaste",
    		null),
    CONTROL_COMMAND_GET_CURRENT_COL(
    		Arrays.asList(Control.class),
    		CommandGroup.class,
    		"getCurrentCol",
    		Integer.class),
    CONTROL_COMMAND_GET_CURRENT_LINE(
    		Arrays.asList(Control.class),
    		CommandGroup.class,
    		"getCurrentLine",
    		Integer.class),
    CONTROL_COMMAND_GET_LINE_COUNT(
    		Arrays.asList(Control.class),
    		CommandGroup.class,
    		"getLineCount",
    		Integer.class),
    CONTROL_COMMAND_GET_SELECTED(
    		Arrays.asList(Control.class, String.class),
    		CommandGroup.class,
    		"getSelected",
    		String.class),
    CONTROL_COMMAND_IS_ENABLED(
    		Arrays.asList(Control.class),
    		CommandGroup.class,
    		"isEnabled",
    		Boolean.class),
    CONTROL_COMMAND_IS_VISIBLE(
    		Arrays.asList(Control.class),
    		CommandGroup.class,
    		"isVisible",
    		Boolean.class),
    CONTROL_COMMAND_TAB_LEFT(
    		Arrays.asList(Control.class),
    		CommandGroup.class,
    		"tabLeft",
    		null),
    CONTROL_COMMAND_TAB_RIGHT(
    		Arrays.asList(Control.class),
    		CommandGroup.class,
    		"tabRight",
    		null),
			//TODO: just for separation
    CONTROL_LISTVIEW_ADD_STRING(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"addString",
    		null),
    CONTROL_LISTVIEW_DELETE_STRING(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"deleteString",
    		null),
    CONTROL_LISTVIEW_FIND_ITEM(
    		Arrays.asList(Control.class, String.class, String.class),
    		ListViewGroup.class,
    		"findItem",
    		Integer.class),
    CONTROL_LISTVIEW_FIND_STRING(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"findString",
    		Boolean.class),
    CONTROL_LISTVIEW_GET_CURRENT_SELECTION(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"getCurrentSelection",
    		String.class),
    CONTROL_LISTVIEW_GET_ITEM_COUNT(
    		Arrays.asList(Control.class),
    		ListViewGroup.class,
    		"getItemCount",
    		Integer.class),
    CONTROL_LISTVIEW_GET_SELECTED(
    		Arrays.asList(Control.class),
    		ListViewGroup.class,
    		"getSelected",
    		String.class),
    CONTROL_LISTVIEW_GET_SELECTED_ARRAY(
    		Arrays.asList(Control.class),
    		ListViewGroup.class,
    		"getSelectedArray",
    		List.class),//String[] to List<String>
    CONTROL_LISTVIEW_GET_SELECTED_COUNT(
    		Arrays.asList(Control.class),
    		ListViewGroup.class,
    		"getSelectedCount",
    		Integer.class),
    CONTROL_LISTVIEW_GET_SUBITEM_COUNT(
    		Arrays.asList(Control.class),
    		ListViewGroup.class,
    		"getSubItemCount",
    		Integer.class),
    CONTROL_LISTVIEW_GET_TEXT(
    		Arrays.asList(Control.class, String.class, String.class),
    		ListViewGroup.class,
    		"getText",
    		String.class),
    CONTROL_LISTVIEW_IS_SELECTED(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"isSelected",
    		Boolean.class),
    CONTROL_LISTVIEW_SELECT(
    		Arrays.asList(Control.class, String.class, String.class),
    		ListViewGroup.class,
    		"select",
    		null),
    CONTROL_LISTVIEW_SELECT_ALL(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"selectAll",
    		null),
    CONTROL_LISTVIEW_SELECT_CLEAR(
    		Arrays.asList(Control.class),
    		ListViewGroup.class,
    		"selectClear",
    		null),
    CONTROL_LISTVIEW_SELECT_INVERT(
    		Arrays.asList(Control.class),
    		ListViewGroup.class,
    		"selectInvert",
    		null),
    CONTROL_LISTVIEW_SELECT_STRING(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"selectString",
    		null),
    CONTROL_LISTVIEW_SELECT_VIEW_CHANGE(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"selectViewChange",
    		null),
    CONTROL_LISTVIEW_SET_CURRENT_SELECTION(
    		Arrays.asList(Control.class, String.class),
    		ListViewGroup.class,
    		"setCurrentSelection",
    		null),
			//TODO: just for separation
//    CONTROL_TREEVIEW_A_BOOLEAN(
//    		Arrays.asList(Control.class, String.class, String.class, String.class),
//    		TreeViewGroup.class,
//    		"aBoolean",
//    		Boolean.class),
    CONTROL_TREEVIEW_CHECK(
    		Arrays.asList(Control.class, String.class),
    		TreeViewGroup.class,
    		"check",
    		null),
    CONTROL_TREEVIEW_COLLAPSE(
    		Arrays.asList(Control.class, String.class),
    		TreeViewGroup.class,
    		"collapse",
    		null),
    CONTROL_TREEVIEW_EXISTS(
    		Arrays.asList(Control.class, String.class),
    		TreeViewGroup.class,
    		"exists",
    		Boolean.class),
    CONTROL_TREEVIEW_EXPAND(
    		Arrays.asList(Control.class, String.class),
    		TreeViewGroup.class,
    		"expand",
    		null),
    CONTROL_TREEVIEW_GET_ITEM_COUNT(
    		Arrays.asList(Control.class, String.class),
    		TreeViewGroup.class,
    		"getItemCount",
    		Integer.class),
    CONTROL_TREEVIEW_GET_SELECTED_ITEM_INDEX(
    		Arrays.asList(Control.class),
    		TreeViewGroup.class,
    		"getSelectedItemIndex",
    		Integer.class),
    CONTROL_TREEVIEW_GET_SELECTED_ITEM_TEXT(
    		Arrays.asList(Control.class),
    		TreeViewGroup.class,
    		"getSelectedItemText",
    		String.class),
    CONTROL_TREEVIEW_GET_TEXT(
    		Arrays.asList(Control.class, String.class),
    		TreeViewGroup.class,
    		"getText",
    		String.class),
    CONTROL_TREEVIEW_IS_CHECKED(
    		Arrays.asList(Control.class),
    		TreeViewGroup.class,
    		"isChecked",
    		Boolean.class),
    CONTROL_TREEVIEW_SELECT(
    		Arrays.asList(Control.class, String.class),
    		TreeViewGroup.class,
    		"select",
    		null),
    CONTROL_TREEVIEW_UNCHECK(
    		Arrays.asList(Control.class, String.class),
    		TreeViewGroup.class,
    		"uncheck",
    		null),
			//TODO: just for separation
    FILE_READ_CSV_READ_CELL(
    		Arrays.asList(File.class, String.class, String.class, String.class),
    		ReadGroup.class,
    		"csvReadCell",
    		String.class),
    FILE_READ_PROPERTY_READ(
    		Arrays.asList(File.class, String.class, String.class, String.class),
    		ReadGroup.class,
    		"propertyRead",
    		String.class),
    FILE_READ_XML_GET_ATTRIBUTE(
    		Arrays.asList(File.class, String.class, String.class, String.class, String.class),
    		ReadGroup.class,
    		"xmlGetAttribute",
    		String.class),
    FILE_READ_XML_GET_VALUE(
    		Arrays.asList(File.class, String.class, String.class, String.class),
    		ReadGroup.class,
    		"xmlGetValue",
    		String.class),
			//TODO: just for separation
    FILE_RUN(
    		Arrays.asList(File.class, String.class, String.class),
    		RunGroup.class,
    		"run",
    		Window.class),
    FILE_RUN_AS_SET(
    		Arrays.asList(String.class, String.class, String.class, String.class),
    		RunGroup.class,
    		"runAsSet",
    		Boolean.class),
    FILE_RUN_WAIT(
    		Arrays.asList(File.class, String.class, String.class),
    		RunGroup.class,
    		"runWait",
    		Window.class),
			//TODO: just for separation
    FILE_WRITE_CSV(
    		Arrays.asList(File.class, String.class, String.class, String.class),
    		WriteGroup.class,
    		"csvWrite",
    		null),
    FILE_WRITE_PROPERTY(
    		Arrays.asList(File.class, String.class, String.class, String.class),
    		WriteGroup.class,
    		"propertyWrite",
    		Boolean.class),
    FILE_WRITE_PROPERTY_DELETE(
    		Arrays.asList(File.class, String.class, String.class),
    		WriteGroup.class,
    		"propertyDelete",
    		Boolean.class),
    		//TODO: just for separation
    LIST_ADD(
    		Arrays.asList(List.class, Object.class),
    		ListGroup.class,
    		"add",
    		null),
    LIST_ADD_ALL(
    		Arrays.asList(List.class, List.class),
    		ListGroup.class,
    		"addAll",
    		null),
    LIST_ADD_ALL_INDEX(
    		Arrays.asList(List.class, List.class, String.class),
    		ListGroup.class,
    		"addAllIndex",
    		null),
    LIST_ADD_INDEX(
    		Arrays.asList(List.class, String.class, Object.class),
    		ListGroup.class,
    		"addIndex",
    		null),
    LIST_CLEAR(
    		Arrays.asList(List.class),
    		ListGroup.class,
    		"clear",
    		null),
    LIST_CONTAINS(
    		Arrays.asList(List.class, Object.class),
    		ListGroup.class,
    		"contains",
    		Boolean.class),
    LIST_GET(
    		Arrays.asList(List.class, String.class),
    		ListGroup.class,
    		"get",
    		Object.class),
    LIST_INDEX_OF(
    		Arrays.asList(List.class, Object.class),
    		ListGroup.class,
    		"indexOf",
    		Integer.class),
    LIST_IS_EMPTY(
    		Arrays.asList(List.class),
    		ListGroup.class,
    		"isEmpty",
    		Boolean.class),
    LIST_LAST_INDEX_OF(
    		Arrays.asList(List.class, Object.class),
    		ListGroup.class,
    		"lastIndexOf",
    		Integer.class),
    LIST_REMOVE_ELEMENT(
    		Arrays.asList(List.class, Object.class),
    		ListGroup.class,
    		"removeElement",
    		Boolean.class),
    LIST_REMOVE_INDEX(
    		Arrays.asList(List.class, String.class),
    		ListGroup.class,
    		"removeIndex",
    		Object.class),
    LIST_SET(
    		Arrays.asList(List.class, String.class, Object.class),
    		ListGroup.class,
    		"set",
    		Object.class),
    LIST_SIZE(
    		Arrays.asList(List.class),
    		ListGroup.class,
    		"size",
    		Integer.class),
			//TODO: just for separation
    MATH_DIVISION(
    		Arrays.asList(Double.class, Double.class),
    		MathGroup.class,
    		"division",
    		Double.class),
	MATH_GET_REST(
    		Arrays.asList(Double.class, Double.class),
    		MathGroup.class,
    		"rest",
    		Double.class),
	MATH_MULTIPLICATION(
    		Arrays.asList(Double.class, Double.class),
    		MathGroup.class,
    		"multiplication",
    		Double.class),
	MATH_SUBTRACTION(
    		Arrays.asList(Double.class, Double.class),
    		MathGroup.class,
    		"subtraction",
    		Double.class),
	MATH_SUMMATION(
    		Arrays.asList(Double.class, Double.class),
    		MathGroup.class,
    		"summation",
    		Double.class),
			//TODO: just for separation
    NOFINDER_DESKTOP_BLOCK_INPUT(
    		Arrays.asList(Boolean.class),
    		DesktopGroup.class,
    		"blockInput",
    		null),
    NOFINDER_DESKTOP_CD_TRAY(
    		Arrays.asList(String.class, String.class),
    		DesktopGroup.class,
    		"cdTray",
    		Boolean.class),
    NOFINDER_DESKTOP_CLIP_GET(
    		Arrays.asList(),
    		DesktopGroup.class,
    		"clipGet",
    		String.class),
    NOFINDER_DESKTOP_CLIP_PUT(
    		Arrays.asList(String.class),
    		DesktopGroup.class,
    		"clipPut",
    		null),
    NOFINDER_DESKTOP_DRIVE_MAP_ADD(
    		Arrays.asList(String.class, String.class, String.class, String.class, String.class),
    		DesktopGroup.class,
    		"driveMapAdd",
    		Boolean.class),
    NOFINDER_DESKTOP_DRIVE_MAP_DELETE(
    		Arrays.asList(String.class),
    		DesktopGroup.class,
    		"driveMapDelete",
    		Boolean.class),
    NOFINDER_DESKTOP_DRIVE_MAP_GET(
    		Arrays.asList(String.class),
    		DesktopGroup.class,
    		"driveMapGet",
    		String.class),
//    NOFINDER_DESKTOP_GET_ERROR(Arrays.asList(), DesktopGroup.class, "getError", String.class),
//    NOFINDER_DESKTOP_GET_VERSION(Arrays.asList(), DesktopGroup.class, "getVersion", String.class),
    NOFINDER_DESKTOP_GET_DATE(
    		Arrays.asList(String.class),
    		DesktopGroup.class,
    		"getDate",
    		String.class),
    NOFINDER_DESKTOP_IS_ADMIN(
    		Arrays.asList(),
    		DesktopGroup.class,
    		"isAdmin",
    		Boolean.class),
    NOFINDER_DESKTOP_PRINT_SCREEN(
    		Arrays.asList(String.class, String.class, String.class, String.class),
    		DesktopGroup.class,
    		"printScreen",
    		null),//It returns a BufferedImage.class
    NOFINDER_DESKTOP_SEND(
    		Arrays.asList(String.class, Boolean.class),
    		DesktopGroup.class,
    		"send",
    		null),
//    NOFINDER_DESKTOP_SET_OPTION(Arrays.asList(String.class, String.class), DesktopGroup.class, "setOption", null),
    NOFINDER_DESKTOP_SHUTDOWN(
    		Arrays.asList(String.class),
    		DesktopGroup.class,
    		"shutDown",
    		Boolean.class),
    NOFINDER_DESKTOP_SLEEP(
    		Arrays.asList(String.class),
    		DesktopGroup.class,
    		"sleep",
    		null),
    NOFINDER_DESKTOP_STATUSBAR_GET_TEXT(
    		Arrays.asList(String.class, String.class, String.class),
    		DesktopGroup.class,
    		"statusbarGetText",
    		String.class),
    NOFINDER_DESKTOP_TOOLTIP(
    		Arrays.asList(String.class, String.class, String.class),
    		DesktopGroup.class,
    		"toolTip",
    		null),
			//TODO: just for separation
    NOFINDER_MOUSE_DOWN(
    		Arrays.asList(String.class),
    		MouseGroup.class,
    		"down",
    		null),
    NOFINDER_MOUSE_GET_CURSOR(
    		Arrays.asList(),
    		MouseGroup.class,
    		"getCursor",
    		Integer.class),
    NOFINDER_MOUSE_GET_POSITION(
    		Arrays.asList(),
    		MouseGroup.class,
    		"getPosition",
    		List.class),//Point to List<String>
    NOFINDER_MOUSE_UP(
    		Arrays.asList(String.class),
    		MouseGroup.class,
    		"up",
    		null),
    NOFINDER_MOUSE_WHEEL(
    		Arrays.asList(String.class, String.class),
    		MouseGroup.class,
    		"wheel",
    		null),
			//TODO: just for separation
    POSITION_CLICK(
			Arrays.asList(List.class, String.class, String.class),
			PositionGroup.class,
			"click",
			null),
    POSITION_CLICK_DRAG(
			Arrays.asList(List.class, List.class, String.class),
			PositionGroup.class,
			"clickDrag",
			null),
    POSITION_GET_COLOR(
			Arrays.asList(List.class),
			PositionGroup.class,
			"getColor",
			Float.class),
    POSITION_MOVE(
			Arrays.asList(List.class),
			PositionGroup.class,
			"move",
			Boolean.class),
    POSITION_WAIT_COLOR(
			Arrays.asList(List.class, String.class, String.class),
			PositionGroup.class,
			"waitColor",
			Boolean.class),
			//TODO: just for separation
    PROCESS_CLOSE(
    		Arrays.asList(String.class),
    		ProcessGroup.class,
    		"close",
    		null),
    PROCESS_EXISTS(
    		Arrays.asList(String.class),
    		ProcessGroup.class,
    		"exists",
    		Boolean.class),
    PROCESS_SET_PRIORITY(
    		Arrays.asList(String.class, String.class),
    		ProcessGroup.class,
    		"setPriority",
    		Boolean.class),
    PROCESS_WAIT(
    		Arrays.asList(String.class, String.class),
    		ProcessGroup.class,
    		"wait",
    		Boolean.class),
    PROCESS_WAIT_CLOSE(
    		Arrays.asList(String.class, String.class),
    		ProcessGroup.class,
    		"waitClose",
    		Boolean.class),
    		//TODO: just for separation
    QUERY_EXECUTE(
    		Arrays.asList(String.class, String.class, String.class, String.class, String.class, String.class, String.class),
    		QueryGroup.class,
    		"execute",
    		String.class),
			//TODO: just for separation
    REGISTRY_DELETE_KEY(
    		Arrays.asList(String.class),
    		RegistryGroup.class,
    		"deleteKey",
    		Boolean.class),
    REGISTRY_DELETE_VAL(
    		Arrays.asList(String.class),
    		RegistryGroup.class,
    		"deleteVal",
    		Boolean.class),
    REGISTRY_ENUM_KEY(
    		Arrays.asList(String.class, String.class),
    		RegistryGroup.class,
    		"enumKey",
    		String.class),
    REGISTRY_ENUM_VAL(
    		Arrays.asList(String.class, String.class),
    		RegistryGroup.class,
    		"enumVal",
    		String.class),
    REGISTRY_READ(
    		Arrays.asList(String.class, String.class),
    		RegistryGroup.class,
    		"read",
    		String.class),
    REGISTRY_WRITE(
    		Arrays.asList(String.class, String.class, String.class, String.class),
    		RegistryGroup.class,
    		"write",
    		Boolean.class),
			//TODO: just for separation
    SCREENREGION_IMAGE_CLICK(
    		Arrays.asList(),
    		ImageGroup.class,
    		"click",
    		null),
    SCREENREGION_IMAGE_CLICK_DRAG(
    		Arrays.asList(),
    		ImageGroup.class,
    		"clickDrag",
    		null),
    SCREENREGION_IMAGE_GET_TEXT(
    		Arrays.asList(),
    		ImageGroup.class,
    		"getText",
    		String.class),
    SCREENREGION_IMAGE_MOVE(
    		Arrays.asList(),
    		ImageGroup.class,
    		"move",
    		Boolean.class),
    SCREENREGION_IMAGE_SEND(
    		Arrays.asList(),
    		ImageGroup.class,
    		"send",
    		null),
			//TODO: just for separation
    SCREENREGION_PIXEL_CHECKSUM(
    		Arrays.asList(List.class, String.class),
    		PixelGroup.class,
    		"checksum",
    		Double.class),
    SCREENREGION_PIXEL_SEARCH(
    		Arrays.asList(List.class, String.class, String.class, String.class),
    		PixelGroup.class,
    		"search",
    		List.class),//long[] to List<String>
			//TODO: just for separation
	STRING_CHAR_AT(
    		Arrays.asList(String.class, String.class),
    		StringGroup.class,
    		"charAt",
    		char.class),
	STRING_CONCAT(
    		Arrays.asList(String.class, String.class),
    		StringGroup.class,
    		"concat",
    		String.class),
	STRING_CONTAINS(
    		Arrays.asList(String.class, String.class),
    		StringGroup.class,
    		"contains",
    		Boolean.class),
	STRING_ENDS_WITH(
    		Arrays.asList(String.class, String.class),
    		StringGroup.class,
    		"endsWith",
    		Boolean.class),
	STRING_EQUALS(
    		Arrays.asList(String.class, String.class),
    		StringGroup.class,
    		"equals",
    		Boolean.class),
	STRING_EQUALS_IGNORE_CASE(
    		Arrays.asList(String.class, String.class),
    		StringGroup.class,
    		"equalsIgnoreCase",
    		Boolean.class),
	STRING_INSERT_IN_INDEX(
    		Arrays.asList(String.class, String.class, String.class),
    		StringGroup.class,
    		"insertInIndex",
    		String.class),
	STRING_IS_EMPTY(
    		Arrays.asList(String.class),
    		StringGroup.class,
    		"isEmpty",
    		Boolean.class),
	STRING_LENGTH(
    		Arrays.asList(String.class),
    		StringGroup.class,
    		"length",
    		String.class),
	STRING_REPLACE(
    		Arrays.asList(String.class, String.class, String.class),
    		StringGroup.class,
    		"replace",
    		String.class),
	STRING_REPLACE_ALL(
    		Arrays.asList(String.class, String.class, String.class),
    		StringGroup.class,
    		"replaceAll",
    		String.class),
	STRING_REPLACE_BETWEEN(
    		Arrays.asList(String.class, String.class, String.class, String.class),
    		StringGroup.class,
    		"replaceBetween",
    		String.class),
	STRING_REPLACE_FIRST(
    		Arrays.asList(String.class, String.class, String.class),
    		StringGroup.class,
    		"replaceFirst",
    		String.class),
	STRING_SPLIT(
    		Arrays.asList(String.class, String.class, String.class),
    		StringGroup.class,
    		"split",
    		List.class),//String[] to List<String>
	STRING_STARTS_WITH(
    		Arrays.asList(String.class, String.class, String.class),
    		StringGroup.class,
    		"startsWith",
    		Boolean.class),
	STRING_SUBSTRING(
    		Arrays.asList(String.class, String.class, String.class),
    		StringGroup.class,
    		"substring",
    		String.class),
	STRING_TO_CHAR_ARRAY(
    		Arrays.asList(String.class),
    		StringGroup.class,
    		"toCharArray",
    		List.class),//char[] to List<String>
	STRING_TO_LOWER_CASE(
    		Arrays.asList(String.class),
    		StringGroup.class,
    		"toLowerCase",
    		String.class),
	STRING_TO_UPPER_CASE(
    		Arrays.asList(String.class),
    		StringGroup.class,
    		"toUpperCase",
    		String.class),
	STRING_TRIM(
    		Arrays.asList(String.class),
    		StringGroup.class,
    		"trim",
    		String.class),
			//TODO: just for separation
    WEB_ALERT_ACCEPT(
    		Arrays.asList(Alert.class),
    		AlertGroup.class,
    		"accept",
    		null),
    WEB_ALERT_AUTHENTICATE_USING(
    		Arrays.asList(Alert.class, Credentials.class),
    		AlertGroup.class,
    		"authenticateUsing",
    		null),
    WEB_ALERT_DISMISS(
    		Arrays.asList(Alert.class),
    		AlertGroup.class,
    		"dismiss",
    		null),
    WEB_ALERT_GET_TEXT(
    		Arrays.asList(Alert.class),
    		AlertGroup.class,
    		"getText",
    		String.class),
    WEB_ALERT_SEND_KEYS(
    		Arrays.asList(Alert.class, String.class),
    		AlertGroup.class,
    		"sendKeys",
    		null),
			//TODO: just for separation
    WEB_BASIC_WD_CHROME(
    		Arrays.asList(String.class, String.class, String.class),
    		BasicWebGroup.class,
    		"chromeDriver",
    		WebDriver.class),
    WEB_BASIC_WD_FIREFOX(
    		Arrays.asList(String.class, String.class, String.class),
    		BasicWebGroup.class,
    		"firefoxDriver",
    		WebDriver.class),
    WEB_BASIC_WD_IE(
    		Arrays.asList(String.class, String.class),
    		BasicWebGroup.class,
    		"ieDriver",
    		WebDriver.class),
    WEB_BASIC_CLOSE(
    		Arrays.asList(WebDriver.class),
    		BasicWebGroup.class,
    		"close",
    		null),
    WEB_BASIC_GET(
    		Arrays.asList(WebDriver.class, String.class),
    		BasicWebGroup.class,
    		"get",
    		null),
    WEB_BASIC_GET_CURRENT_URL(
    		Arrays.asList(WebDriver.class),
    		BasicWebGroup.class,
    		"getCurrentUrl",
    		String.class),
    WEB_BASIC_GET_PAGE_SOURCE(
    		Arrays.asList(WebDriver.class),
    		BasicWebGroup.class,
    		"getPageSource",
    		String.class),
    WEB_BASIC_GET_TITLE(
    		Arrays.asList(WebDriver.class),
    		BasicWebGroup.class,
    		"getTitle",
    		String.class),
    WEB_BASIC_GET_WINDOW_HANDLE(
    		Arrays.asList(WebDriver.class),
    		BasicWebGroup.class,
    		"getWindowHandle",
    		String.class),
    WEB_BASIC_GET_WINDOW_HANDLES(
    		Arrays.asList(WebDriver.class),
    		BasicWebGroup.class,
    		"getWindowHandles",
    		List.class),//Set<String> to List<String>
    WEB_BASIC_QUIT(
    		Arrays.asList(WebDriver.class),
    		BasicWebGroup.class,
    		"quit",
    		null),
			//TODO: just for separation
	WEB_ELEMENT_CLEAR(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"clear",
			null),
	WEB_ELEMENT_CLICK(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"click",
			null),
	WEB_ELEMENT_GET_ATTRIBUTE(
			Arrays.asList(WebElement.class, String.class),
			ElementGroup.class,
			"getAttribute",
			String.class),
	WEB_ELEMENT_GET_CSS_VALUE(
			Arrays.asList(WebElement.class, String.class),
			ElementGroup.class,
			"getCssValue",
			String.class),
	WEB_ELEMENT_GET_LOCATION(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"getLocation",
			List.class),//Point to List<String>
	WEB_ELEMENT_GET_SIZE(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"getSize",
			List.class),//Dimension to List<String>
	WEB_ELEMENT_GET_TAG_NAME(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"getTagName",
			String.class),
	WEB_ELEMENT_GET_TEXT(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"getText",
			String.class),
	WEB_ELEMENT_IS_DISPLAYED(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"isDisplayed",
			Boolean.class),
	WEB_ELEMENT_IS_ENABLED(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"isEnabled",
			Boolean.class),
	WEB_ELEMENT_IS_SELECTED(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"isSelected",
			Boolean.class),
	WEB_ELEMENT_SEND_KEYS(
			Arrays.asList(WebElement.class, String.class),
			ElementGroup.class,
			"sendKeys",
			null),
	WEB_ELEMENT_SUBMIT(
			Arrays.asList(WebElement.class),
			ElementGroup.class,
			"submit",
			null),
			//TODO: just for separation
    WEB_JAVASCRIPT_EXECUTE(
    		Arrays.asList(WebDriver.class, String.class),
    		JavascriptExecutorGroup.class,
    		"execute",
    		null),
			//TODO: just for separation
    WEB_ADD_COOKIE(
    		Arrays.asList(WebDriver.class, Cookie.class),
    		ManageGroup.class,
    		"addCookie",
    		null),
    WEB_DELETE_All_COOKIES(
    		Arrays.asList(WebDriver.class),
    		ManageGroup.class,
    		"deleteAllCookies",
    		null),
    WEB_DELETE_COOKIE(
    		Arrays.asList(WebDriver.class, Cookie.class),
    		ManageGroup.class,
    		"deleteCookie",
    		null),
    WEB_DELETE_COOKIE_NAMED(
    		Arrays.asList(WebDriver.class, String.class),
    		ManageGroup.class,
    		"deleteCookieNamed",
    		null),
    WEB_GET_COOKIES(
    		Arrays.asList(WebDriver.class),
    		ManageGroup.class,
    		"getCookies",
    		List.class),//Set<Cookie> to List<String> TODO:Rever
    WEB_GET_COOKIE_NAMED(
    		Arrays.asList(WebDriver.class, String.class),
    		ManageGroup.class,
    		"getCookieNamed",
    		Cookie.class),
			//TODO: just for separation
    WEB_IME_ACTIVATE_ENGINE(
    		Arrays.asList(WebDriver.class, String.class),
    		ManageImeGroup.class,
    		"activateEngine",
    		null),
    WEB_IME_DEACTIVATE(
    		Arrays.asList(WebDriver.class),
    		ManageImeGroup.class,
    		"deactivate",
    		null),
    WEB_IME_GET_ACTIVE_ENGINE(
    		Arrays.asList(WebDriver.class),
    		ManageImeGroup.class,
    		"getActiveEngine",
    		String.class),
    WEB_IME_GET_AVAILABLE_ENGINES(
    		Arrays.asList(WebDriver.class),
    		ManageImeGroup.class,
    		"getAvailableEngines",
    		List.class),//List<String>
    WEB_IME_IS_ACTIVATED(
    		Arrays.asList(WebDriver.class),
    		ManageImeGroup.class,
    		"isActivated",
    		Boolean.class),
			//TODO: just for separation
    WEB_LOG_GET(
    		Arrays.asList(WebDriver.class, String.class),
    		ManageLogsGroup.class,
    		"get",
    		LogEntries.class),
    WEB_LOG_GET_AVAILABLE_TYPES(
    		Arrays.asList(WebDriver.class),
    		ManageLogsGroup.class,
    		"getAvailableLogTypes",
    		List.class),//Set<String> to List<String>
			//TODO: just for separation
    WEB_NAVIGATE_BACK(
    		Arrays.asList(WebDriver.class),
    		ManageNavigateGroup.class,
    		"back",
    		null),
    WEB_NAVIGATE_FORWARD(
    		Arrays.asList(WebDriver.class),
    		ManageNavigateGroup.class,
    		"forward",
    		null),
    WEB_NAVIGATE_REFRESH(
    		Arrays.asList(WebDriver.class),
    		ManageNavigateGroup.class,
    		"refresh",
    		null),
    WEB_NAVIGATE_TO(
    		Arrays.asList(WebDriver.class, Object.class),
    		ManageNavigateGroup.class,
    		"to",
    		null),
			//TODO: just for separation
    WEB_SWITCHTO_ACTIVE_ELEMENT(
    		Arrays.asList(WebDriver.class),
    		ManageSwitchToGroup.class,
    		"activeElement",
    		WebElement.class),
    WEB_SWITCHTO_ALERT(
    		Arrays.asList(WebDriver.class),
    		ManageSwitchToGroup.class,
    		"alert",
    		Alert.class),
    WEB_SWITCHTO_DEFAULT_CONTENT(
    		Arrays.asList(WebDriver.class),
    		ManageSwitchToGroup.class,
    		"defaultContent",
    		WebDriver.class),
    WEB_SWITCHTO_FRAME(
    		Arrays.asList(WebDriver.class, Object.class),
    		ManageSwitchToGroup.class,
    		"frame",
    		WebDriver.class),
    WEB_SWITCHTO_PARENT_FRAME(
    		Arrays.asList(WebDriver.class),
    		ManageSwitchToGroup.class,
    		"parentFrame",
    		WebDriver.class),
    WEB_SWITCHTO_WINDOW(
    		Arrays.asList(WebDriver.class, String.class),
    		ManageSwitchToGroup.class,
    		"window",
    		WebDriver.class),
			//TODO: just for separation
	WEB_TIMEOUT_IMPLICITLY_WAIT(
			Arrays.asList(WebDriver.class, Long.class),
			ManageTimeoutsGroup.class,
			"implicitlyWait",
			Timeouts.class),
    WEB_TIMEOUT_PAGE_LOAD(
			Arrays.asList(WebDriver.class, Long.class),
			ManageTimeoutsGroup.class,
			"pageLoadTimeout",
			Timeouts.class),
    WEB_TIMEOUT_SET_SCRIPT(
			Arrays.asList(WebDriver.class, Long.class),
			ManageTimeoutsGroup.class,
			"setScriptTimeout",
			Timeouts.class),
    		//TODO: just for separation
    WEB_WINDOW_GET_POSITION(
			Arrays.asList(WebDriver.class),
			ManageWindowGroup.class,
			"getPosition",
			List.class),//Point to List<String>
    WEB_WINDOW_GET_SIZE(
			Arrays.asList(WebDriver.class),
			ManageWindowGroup.class,
			"getSize",
			List.class),//Dimension to List<String>
    WEB_WINDOW_MAXIMIZE(
			Arrays.asList(WebDriver.class),
			ManageWindowGroup.class,
			"maximize",
			null),
    WEB_WINDOW_SET_POSITION(
			Arrays.asList(WebDriver.class, List.class),
			ManageWindowGroup.class,
			"setPosition",
			null),
    WEB_WINDOW_SET_SIZE(
			Arrays.asList(WebDriver.class, List.class),
			ManageWindowGroup.class,
			"setSize",
			null),
    		//TODO: just for separation
    WINDOW_ACTIVATE(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"activate",
			null),
	WINDOW_ACTIVE(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"active",
			null),
	WINDOW_CLOSE(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"close",
			null),
	WINDOW_EXISTS(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"exists",
			Boolean.class),
	WINDOW_GET_CARET_POSITION(
			Arrays.asList(),
			WindowGroup.class,
			"getCaretPosition",
			List.class),//Point to List<String>
	WINDOW_GET_CLASS_LIST(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"getClassList",
			String.class),
	WINDOW_GET_HANDLE(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"getHandle",
			String.class),
	WINDOW_GET_POSITION(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"getPosition",
			List.class),//Point to List<String>
	WINDOW_GET_PROCESS(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"getProcess",
			String.class),
	WINDOW_GET_SIZE(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"getSize",
			List.class),//Dimension to List<String>
	WINDOW_GET_STATE(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"getState",
			Integer.class),
	WINDOW_GET_TEXT(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"getText",
			String.class),
	WINDOW_GET_TITLE(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"getTitle",
			String.class),
	WINDOW_KILL(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"kill",
			null),
	WINDOW_LIST_HWND(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"listHwnd",
			List.class),//String[][] to List<String> getting [1] of second array
	WINDOW_LIST_TITLE(
			Arrays.asList(Window.class),
			WindowGroup.class,
			"listTitle",
			List.class),//String[][] to List<String> getting [0] of second array
	WINDOW_MENU_SELECT_ITEM(
			Arrays.asList(Window.class, String.class),
			WindowGroup.class,
			"menuSelectItem",
			Boolean.class),
	WINDOW_MINIMIZE_ALL(
    		Arrays.asList(),
			WindowGroup.class,
			"minimizeAll",
			null),
	WINDOW_MINIMIZE_ALL_UNDO(
    		Arrays.asList(),
			WindowGroup.class,
			"minimizeAllUndo",
			null),
	WINDOW_MOVE(
			Arrays.asList(Window.class, String.class, String.class),
			WindowGroup.class,
			"move",
			null),
	WINDOW_RESIZE(
			Arrays.asList(Window.class, String.class, String.class),
			WindowGroup.class,
			"resize",
			null),
	WINDOW_SET_ON_TOP(
			Arrays.asList(Window.class, Boolean.class),
			WindowGroup.class,
			"setOnTop",
			null),
	WINDOW_SET_STATE(
			Arrays.asList(Window.class, String.class),
			WindowGroup.class,
			"setState",
			null),
	WINDOW_SET_TITLE(
			Arrays.asList(Window.class, String.class),
			WindowGroup.class,
			"setTitle",
			null),
	WINDOW_SET_TRANS(
			Arrays.asList(Window.class, String.class),
			WindowGroup.class,
			"setTrans",
			Boolean.class),
	WINDOW_WAIT(
			Arrays.asList(Window.class, String.class),
			WindowGroup.class,
			"wait",
			Boolean.class),
	WINDOW_WAIT_ACTIVE(
			Arrays.asList(Window.class, String.class),
			WindowGroup.class,
			"waitActive",
			Boolean.class),
	WINDOW_WAIT_CLOSE(
			Arrays.asList(Window.class, String.class),
			WindowGroup.class,
			"waitClose",
			Boolean.class),
	WINDOW_WAIT_NO_ACTIVE(
			Arrays.asList(Window.class, String.class),
			WindowGroup.class,
			"waitNoActive",
			Boolean.class);

	private final List<?> reflectionParams;
	private final Class<?> reflectionClass;
	private final String reflectionMethod;
	private final Class<?> reflectionReturn;

    private ReflectionType(List<?> reflectionParams, Class<?> reflectionClass, String reflectionMethod, Class<?> reflectionReturn) {
        this.reflectionParams = reflectionParams;
        this.reflectionClass = reflectionClass;
        this.reflectionMethod = reflectionMethod;
        this.reflectionReturn = reflectionReturn;
    }

    public List<?> getReflectionParams() {
		return reflectionParams;
	}

	public Class<?> getReflectionClass() {
		return reflectionClass;
	}

	public String getReflectionMethod() {
		return reflectionMethod;
	}

	public Class<?> getReflectionReturn() {
		return reflectionReturn;
	}

    public static ReflectionType[] getAllReflectionTypes() {
        return ReflectionType.values();
    }
}