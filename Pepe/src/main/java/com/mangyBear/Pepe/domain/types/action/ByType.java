package com.mangyBear.Pepe.domain.types.action;


import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;

/**
 *
 * @author Lucas Silva Bernardo
 */
public enum ByType {
    
    BY_CLASS_NAME("By Class Name"),
    BY_CSS_SELECTOR("By Css Selector"),
    BY_ID("By Id"),
    BY_LINK_TEXT("By Link Text"),
    BY_NAME("By Name"),
    BY_PARTIAL_LINK_TEXT("By Partial Link Text"),
    BY_TAG_NAME("By Tag Name"),
    BY_XPATH("By Xpath");

    private final String byName;

    private ByType(String byName) {
        this.byName = byName;
    }
    
    @Override
    public String toString() {
    	return getByName();
    }

    public String getByName() {
		return byName;
	}
	
	public static List<ByType> getAllByTypes() {
		return Arrays.asList(ByType.values());
	}

	public static ByType getByType(String locator) {
    	for (ByType byType : ByType.values()) {
			if (byType.getByName().equalsIgnoreCase(locator)) {
				return byType;
			}
		}
        return null;
    }

	public static By getLocator(String locator, String byElement) {
		switch (getByType(locator)) {
	        case BY_CLASS_NAME:
	            return By.className(byElement);
	        case BY_CSS_SELECTOR:
	            return By.cssSelector(byElement);
	        case BY_ID:
	            return By.id(byElement);
	        case BY_LINK_TEXT:
	            return By.linkText(byElement);
	        case BY_NAME:
	            return By.name(byElement);
	        case BY_PARTIAL_LINK_TEXT:
	            return By.partialLinkText(byElement);
	        case BY_TAG_NAME:
	            return By.tagName(byElement);
	        case BY_XPATH:
	            return By.xpath(byElement);
		}
		
		return null;
	}
}
