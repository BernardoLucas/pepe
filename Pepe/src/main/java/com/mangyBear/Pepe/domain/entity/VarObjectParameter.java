package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "var_object_parameter")
public class VarObjectParameter implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_var_object_parameter", nullable = false, unique = true)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "value")
	private String value;

	@ManyToOne
	@JoinColumn(name = "id_var_object")
	private VarObject object;

	@Override
	public String toString() {
		return this.getName() + "=" + this.getValue();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public VarObject getObject() {
		return object;
	}

	public void setObject(VarObject object) {
		this.object = object;
	}
	
	public VarObjectParameter clone() {
        try {
        	VarObjectParameter clone = (VarObjectParameter) super.clone();
        	clone.setId(null);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in VarObjectParameter. " );
            return this;
        }
    }
}
