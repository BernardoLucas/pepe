package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="schedule")
@NamedQueries({
	@NamedQuery(name = "scheduleByName", query = "SELECT entity from Schedule entity where lower(nmSchedule) = :name"),
	@NamedQuery(name = "scheduleLikeName", query = "SELECT entity from Schedule entity where lower(nmSchedule) LIKE :name"),
	@NamedQuery(name = "scheduleLikeNameAndMac", query = "SELECT entity from Schedule entity where lower(nmSchedule) LIKE :name AND (public = :mac OR public = NULL)"),
	@NamedQuery(name = "scheduleLikeNameMacAndFlow", query = "SELECT entity from Schedule entity where lower(nmSchedule) LIKE :name AND (public = :mac OR public = NULL) AND id_flow = :id")
})
public class Schedule implements Serializable {

	private static final long serialVersionUID = 1L;
    
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_schedule", nullable=false, unique=true)
    private Long id;
	
	@Column(name="nm_schedule", nullable=false, unique = true)
    private String nmSchedule;
	
	@ManyToOne
	@JoinColumn(name="id_flow", nullable=false)
	private Flow flow;
	
	@Column(name="bl_sun")
    private Boolean exec_sun;
	
	@Column(name="bl_mon")
    private Boolean exec_mon;
	
	@Column(name="bl_tue")
    private Boolean exec_tue;
	
	@Column(name="bl_wed")
    private Boolean exec_wed;
	
	@Column(name="bl_thu")
    private Boolean exec_thu;
	
	@Column(name="bl_fri")
    private Boolean exec_fri;
	
	@Column(name="bl_sat")
	private Boolean exec_sat;
	
	@Column(name="time")
	private String exec_time;
	
	@Column(name="public")
    private String exec_public;
	
	@Column(name="bl_active")
    private Boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmSchedule() {
		return nmSchedule;
	}

	public void setNmSchedule(String nmSchedule) {
		this.nmSchedule = nmSchedule;
	}

	public Flow getFlow() {
		return flow;
	}

	public void setFlow(Flow flow) {
		this.flow = flow;
	}

	public Boolean isExec_sun() {
		return exec_sun;
	}

	public void setExec_sun(Boolean exec_sun) {
		this.exec_sun = exec_sun;
	}

	public Boolean isExec_mon() {
		return exec_mon;
	}

	public void setExec_mon(Boolean exec_mon) {
		this.exec_mon = exec_mon;
	}

	public Boolean isExec_tue() {
		return exec_tue;
	}

	public void setExec_tue(Boolean exec_tue) {
		this.exec_tue = exec_tue;
	}

	public Boolean isExec_wed() {
		return exec_wed;
	}

	public void setExec_wed(Boolean exec_wed) {
		this.exec_wed = exec_wed;
	}

	public Boolean isExec_thu() {
		return exec_thu;
	}

	public void setExec_thu(Boolean exec_thu) {
		this.exec_thu = exec_thu;
	}

	public Boolean isExec_fri() {
		return exec_fri;
	}

	public void setExec_fri(Boolean exec_fri) {
		this.exec_fri = exec_fri;
	}

	public Boolean isExec_sat() {
		return exec_sat;
	}

	public void setExec_sat(Boolean exec_sat) {
		this.exec_sat = exec_sat;
	}
	
	public String getExec_time() {
		return exec_time;
	}
	
	public void setExec_time(String exec_time) {
		this.exec_time = exec_time;
	}

	public String getExec_public() {
		return exec_public;
	}

	public void setExec_public(String exec_public) {
		this.exec_public = exec_public;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
