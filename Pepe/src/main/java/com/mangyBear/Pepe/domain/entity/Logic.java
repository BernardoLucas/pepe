package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="logic")
@NamedQueries({
	@NamedQuery(name = "logicByName", query = "SELECT entity from Logic entity where lower(nmLogic) = :name"),
	@NamedQuery(name = "logicLikeName", query = "SELECT entity from Logic entity where lower(nmLogic) LIKE :name")})
public class Logic implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_logic", nullable=false, unique=true)
	private Long id;
	
	@Column(name="nm_logic", nullable=false, unique=true)
	private String nmLogic;
    
	@Column(name="input", nullable=false)
    private String input;
	
	@Column(name="denial")
    private Boolean denial;
	
	@Column(name="expected")
    private String expected;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmLogic() {
		return nmLogic;
	}

	public void setNmLogic(String nmLogic) {
		this.nmLogic = nmLogic;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public Boolean getDenial() {
		return denial;
	}

	public void setDenial(Boolean denial) {
		this.denial = denial;
	}

	public String getExpected() {
		return expected;
	}

	public void setExpected(String expected) {
		this.expected = expected;
	}
	
	public Logic clone() {
        try {
        	Logic clone = (Logic) super.clone();
        	clone.setId(null);
            clone.setNmLogic(clone.getNmLogic() + "-CLONE-");
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in Logic. " );
            return this;
        }
    }
}
