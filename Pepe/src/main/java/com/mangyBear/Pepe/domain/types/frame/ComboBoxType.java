package com.mangyBear.Pepe.domain.types.frame;

import java.util.Arrays;
import java.util.List;

import com.mangyBear.Pepe.domain.types.action.ByType;
import com.mangyBear.Pepe.domain.types.desktopParams.DesktopGroupType;
import com.mangyBear.Pepe.domain.types.desktopParams.DesktopType;

/**
 *
 * @author Lucas Silva Bernardo
 */
public enum ComboBoxType {
    
	AUTOIT_OPTION(Arrays.asList("CaretCoordMode", "ExpandEnvStrings", "ExpandVarStrings", "GUICloseOnESC", "GUICoordMode", "GUIDataSeparatorChar")),
	BOOLEAN(Arrays.asList("True", "False")),
	CD_TRAY(Arrays.asList("Open", "Close")),
	DRIVE_OPTION(DesktopType.getDesktopTypeByGroup(DesktopGroupType.DRIVE_OPTION)),
	MOUSE_BUTTONS(Arrays.asList("Left", "Right", "Middle", "Main", "Menu", "Primary", "Secondary")),
    MOUSE_WHEEL(Arrays.asList("Up", "Down")),
    PROCESS_PRIORITY(DesktopType.getDesktopTypeByGroup(DesktopGroupType.PROCESS_PRIORITY)),
    REGISTRY_TYPE(Arrays.asList("REG_SZ", "REG_MULTI_SZ", "REG_EXPAND_SZ", "REG_DWORD", "REG_QWORD", "REG_BINARY")),
    RUN_STATE(DesktopType.getDesktopTypeByGroup(DesktopGroupType.RUN_STATE)),
    SHUTDOWN(DesktopType.getDesktopTypeByGroup(DesktopGroupType.SHUTDOWN)),
    WEB_LOCATOR(ByType.getAllByTypes()),
    WINDOW_STATE(DesktopType.getDesktopTypeByGroup(DesktopGroupType.WINDOW_STATE));

    private final List<?> comboItems;

    private ComboBoxType(List<?> params) {
        this.comboItems = params;
    }

    public List<?> getComboItems() {
        return comboItems;
    }

    public static List<ComboBoxType> getAllComboBoxTypes() {
        return Arrays.asList(ComboBoxType.values());
    }
}

