package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mangyBear.Pepe.domain.types.variable.VariableType;

@Entity
@Table(name = "variable")
@NamedQueries({
	@NamedQuery(name = "variableByFlow", query = "SELECT entity from Variable entity where entity.flowDad.id = :idFlowDad"),
	@NamedQuery(name = "variableByName", query = "SELECT entity from Variable entity where lower(name) = :name"),
	@NamedQuery(name = "variableLikeName", query = "SELECT entity from Variable entity where lower(name) LIKE :name"),
	@NamedQuery(name = "variableLikeNameAndGroupType", query = "SELECT entity from Variable entity where lower(name) LIKE :name AND entity.varObject.varObjectType = :type")})
public class Variable implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_var", nullable = false, unique = true)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "id_flow_dad")
	private Flow flowDad;

	@ManyToOne
	@JoinColumn(name = "id_var_object")
	private VarObject varObject;

	@Column(name = "nm_var", nullable = false, unique = true)
	private String name;
	
	@Column(name="tp_variable", nullable=false)
	@Enumerated(EnumType.STRING)
    private VariableType variableType;
	
	@OneToMany(mappedBy="variable", cascade = CascadeType.ALL)
    private List<VariableParameter> parameters;

	@Override
	public String toString() {
		return this.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idValue) {
		this.id = idValue;
	}

	public Flow getFlowDad() {
		return flowDad;
	}

	public void setFlowDad(Flow flowDad) {
		this.flowDad = flowDad;
	}

	public VarObject getVarObject() {
		return varObject;
	}

	public void setVarObject(VarObject varObject) {
		this.varObject = varObject;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public VariableType getVariableType() {
		return variableType;
	}

	public void setVariableType(VariableType variableType) {
		this.variableType = variableType;
	}

	public List<VariableParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<VariableParameter> parameters) {
		this.parameters = parameters;
	}
	
	public Variable clone() {
        try {
        	Variable clone = (Variable) super.clone();
        	clone.setId(null);
        	clone.setName(clone.getName() + "-CLONE-");
        	setParametersClones(clone);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in Variable. " );
            return this;
        }
    }
	
	private void setParametersClones(Variable clone) {
		if (clone.getParameters() == null || clone.getParameters().isEmpty()) {
			return;
		}
		
		clone.setParameters(new ArrayList<VariableParameter>());
		for (VariableParameter parameter : parameters) {
			VariableParameter parameterClone = parameter.clone();
			parameterClone.setVariable(clone);
			clone.getParameters().add(parameterClone);
		}
	}
}
