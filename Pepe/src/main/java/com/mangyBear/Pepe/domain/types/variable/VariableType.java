package com.mangyBear.Pepe.domain.types.variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.mangyBear.Pepe.domain.types.action.ReflectionType;
import com.mangyBear.Pepe.domain.types.frame.FrameAddType;


public enum VariableType {
	
	VARIABLE_FILE_CSV(FrameAddType.VARIABLE_FILE_CSV,
			ReflectionType.FILE_READ_CSV_READ_CELL,
			VariableGroupType.VARIABLE_FILE_GROUP,
			true),
	VARIABLE_FILE_PROPERTY(FrameAddType.VARIABLE_FILE_PROPERTY,
			ReflectionType.FILE_READ_PROPERTY_READ,
			VariableGroupType.VARIABLE_FILE_GROUP,
			true),
	VARIABLE_FILE_XML(FrameAddType.VARIABLE_FILE_XML,
			ReflectionType.FILE_READ_XML_GET_VALUE,
			VariableGroupType.VARIABLE_FILE_GROUP,
			true),
	VARIABLE_OBJECT_CHANGEABLE(FrameAddType.VARIABLE_OBJECT_CHANGEABLE,
			null,
			VariableGroupType.VARIABLE_OBJECT_GROUP,
			true),
	VARIABLE_OBJECT_FINAL(FrameAddType.VARIABLE_OBJECT_FINAL,
			null,
			VariableGroupType.VARIABLE_OBJECT_GROUP,
			true),
	VARIABLE_SQL_QUERY(FrameAddType.VARIABLE_SQL_QUERY,
			ReflectionType.QUERY_EXECUTE,
			VariableGroupType.VARIABLE_SQL_GROUP,
			true),
	VARIABLE_WEBDRIVER_CHROME(FrameAddType.VARIABLE_WEBDRIVER_CHROME,
			ReflectionType.WEB_BASIC_WD_CHROME,
			VariableGroupType.VARIABLE_WEBDRIVER_GROUP,
			true),
	VARIABLE_WEBDRIVER_FIREFOX(FrameAddType.VARIABLE_WEBDRIVER_FIREFOX,
			ReflectionType.WEB_BASIC_WD_FIREFOX,
			VariableGroupType.VARIABLE_WEBDRIVER_GROUP,
			true),
	VARIABLE_WEBDRIVER_IE(FrameAddType.VARIABLE_WEBDRIVER_IE,
			ReflectionType.WEB_BASIC_WD_IE,
			VariableGroupType.VARIABLE_WEBDRIVER_GROUP,
			true);
	
	private final FrameAddType frameAddType;
	private final ReflectionType reflectionType;
	private final VariableGroupType objectType;
	private final boolean visible;

	private VariableType(FrameAddType frameAddType, ReflectionType reflectionType, VariableGroupType objectType, boolean visible) {
		this.frameAddType = frameAddType;
		this.reflectionType = reflectionType;
		this.objectType = objectType;
		this.visible = visible;
	}
	
	@Override
	public String toString() {
		return getFrameAddType().getName();
	}

	public FrameAddType getFrameAddType() {
		return frameAddType;
	}

	public ReflectionType getReflectionType() {
		return reflectionType;
	}

	public VariableGroupType getObjectType() {
		return objectType;
	}

	public boolean isVisible() {
		return visible;
	}

	public boolean hasReflection() {
		return reflectionType != null;
	}
	
	public static List<VariableType> getVisibleVariableType() {
		List<VariableType> visibleVars = new ArrayList<>();
		for (VariableType variable : VariableType.values()) {
			if (variable.isVisible()) {
				visibleVars.add(variable);
			}
		}
		return visibleVars;
	}

    public static List<VariableType> getVariablesWithNoObject(boolean sortByName) {
        List<VariableType> varList = new ArrayList<>();
        for (VariableType var : VariableType.values()) {
            if (var.getObjectType() == null) {
            	varList.add(var);
            }
        }
        return sortByName ? sortVariableType(varList) : varList;
    }

    public static List<VariableType> getVariablesByGroup(VariableGroupType group, boolean sortByName) {
        List<VariableType> varList = new ArrayList<>();
        for (VariableType var : VariableType.values()) {
            if (var.getObjectType() != null && var.getObjectType().equals(group) && var.isVisible()) {
            	varList.add(var);
            }
        }
        return sortByName ? sortVariableType(varList) : varList;
    }
	
	private static List<VariableType> sortVariableType(List<VariableType> listToSort) {
		Collections.sort(listToSort, new Comparator<VariableType>() {
			@Override
			public int compare(VariableType o1, VariableType o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
		return listToSort;
	}
}
