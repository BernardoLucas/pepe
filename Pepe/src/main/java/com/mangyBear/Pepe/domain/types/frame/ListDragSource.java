package com.mangyBear.Pepe.domain.types.frame;


public enum ListDragSource {

	FAVORITES, SEARCH;
	
	public static ListDragSource getListDragSourceByName(String name) {
		for (ListDragSource dragSource : values()) {
			if (dragSource.toString().equalsIgnoreCase(name)) {
				return dragSource;
			}
		}
		return null;
	}
	
}