package com.mangyBear.Pepe.domain.types.frame;

import java.awt.Point;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import com.mangyBear.Pepe.ui.component.SuggestCombo;

public enum FrameFieldType {
	
	CHECK(JCheckBox.class, 0),
	COMBO(JComboBox.class, 0),
	COMBO_SUGGEST(SuggestCombo.class, 0),
	DOUBLE(Double.class, 1000),
	IMAGE(null, 0),
	INTEGER(Integer.class, 1000),
	NONE(null, 0),
	PANEL_FILE_CHOOSER(String.class, 0),
	PANEL_FILE_CHOOSER_SUGGEST(SuggestCombo.class, 0),
	PANEL_DIALOG(String.class, 0),
	PANEL_DIALOG_SUGGEST(SuggestCombo.class, 0),
	POSITION(Point.class, 4),
	STRING(String.class, 1000),
	SUGGEST(SuggestCombo.class, 0);

	private final Class<?> fieldClass;
	private final int length;

    private FrameFieldType(Class<?> fieldClass, int length) {
    	this.fieldClass = fieldClass;
        this.length = length;
    }
    
    public Class<?> getFieldClass() {
    	return fieldClass;
    }

	public int getLength() {
		return length;
	}
}
