package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "variable_parameter")
@NamedQueries({ @NamedQuery(name = "variableParamLikeValue", query = "SELECT entity from VariableParameter entity where lower(value) LIKE :value") })
public class VariableParameter implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_variable_parameter", nullable = false, unique = true)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "value")
	private String value;

	@ManyToOne
	@JoinColumn(name = "id_variable")
	private Variable variable;

	@Override
	public String toString() {
		return this.getName() + "=" + this.getValue();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Variable getVariable() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}
	
	public VariableParameter clone() {
        try {
        	VariableParameter clone = (VariableParameter) super.clone();
        	clone.setId(null);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in VariableParameter. " );
            return this;
        }
    }
}
