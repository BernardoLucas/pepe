package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "flow")
@NamedQueries({
	@NamedQuery(name = "activateHibernate", query = "SELECT id from Flow entity where id = 1"),
	@NamedQuery(name = "flowByName", query = "SELECT entity from Flow entity where lower(nmFlow) = :name"),
	@NamedQuery(name = "flowLikeName", query = "SELECT entity from Flow entity where lower(nmFlow) LIKE :name") })
public class Flow implements Cloneable, Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_flow", nullable = false, unique = true)
	private Long id;

	@Column(name = "nm_flow", nullable = false, unique = true)
	private String nmFlow;

	@OneToMany(mappedBy = "flowDad", fetch = FetchType.EAGER)
	private List<FlowItem> flowItems;

	@OneToMany(mappedBy = "flowDad", fetch = FetchType.EAGER)
	private List<VarObject> flowObjects;
	
	@OneToMany(mappedBy = "flowDad", fetch = FetchType.EAGER)
	private List<Variable> flowVariables;

	@Column(name = "width", nullable = false)
	private Double width;

	@Column(name = "height", nullable = false)
	private Double height;

	@Column(name = "x", nullable = false)
	private Double x;

	@Column(name = "y", nullable = false)
	private Double y;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmFlow() {
		return nmFlow;
	}

	public void setNmFlow(String nmFlow) {
		this.nmFlow = nmFlow;
	}

	public List<FlowItem> getFlowItems() {
		return flowItems;
	}

	public void setFlowItems(List<FlowItem> flowItems) {
		this.flowItems = flowItems;
	}
	
	public List<VarObject> getFlowObjects() {
		return flowObjects;
	}

	public void setFlowObjects(List<VarObject> flowObjects) {
		this.flowObjects = flowObjects;
	}

	public List<Variable> getFlowVariables() {
		return flowVariables;
	}

	public void setFlowVariables(List<Variable> flowVariables) {
		this.flowVariables = flowVariables;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}
	
	public Flow clone(boolean cloneLogics, boolean cloneSteps) {
        try {
        	Flow clone = (Flow) super.clone();
        	clone.setId(null);
            clone.setNmFlow(clone.getNmFlow() + "-CLONE-");
            Map<VarObject, VarObject> oldToNew = setObjectsClones(clone);
            setVariablesClones(clone, oldToNew);
            setFlowItemsClones(clone, cloneLogics, cloneSteps);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in Flow. " );
            return this;
        }
    }
	
	private void setFlowItemsClones(Flow clone, boolean cloneLogics, boolean cloneSteps) {
		if (clone.getFlowItems() == null || clone.getFlowItems().isEmpty()) {
			return;
		}
		
        Map<FlowItem, FlowItem> oldToNew = new HashMap<>();
		clone.setFlowItems(new ArrayList<FlowItem>());
		for (FlowItem flowItem : flowItems) {
			FlowItem cloneItem = flowItem;
			
			if ((flowItem.getLogic() != null && cloneLogics) || (flowItem.getStep() != null && cloneSteps)) {
				cloneItem = flowItem.clone();
			}
			
			cloneItem.setFlowDad(clone);
			clone.getFlowItems().add(cloneItem);
			oldToNew.put(flowItem, cloneItem);
		}
		
		updateConnectionsWithCorrectFlowItems(clone, oldToNew);
	}
	
	private void updateConnectionsWithCorrectFlowItems(Flow clone, Map<FlowItem, FlowItem> oldToNew) {
		for (FlowItem flowItem : clone.getFlowItems()) {
			for (FlowItemConnection connection : flowItem.getPreviousFlowItems()) {
				connection.setPreviousItem(oldToNew.get(connection.getPreviousItem()));
			}
			for (FlowItemConnection connection : flowItem.getNextFlowItems()) {
				connection.setCurrentItem(oldToNew.get(connection.getCurrentItem()));
			}
		}
	}
	
	private Map<VarObject, VarObject> setObjectsClones(Flow clone) {
		if (clone.getFlowObjects() == null || clone.getFlowObjects().isEmpty()) {
			return null;
		}
		
        Map<VarObject, VarObject> oldToNew = new HashMap<>();
		clone.setFlowObjects(new ArrayList<VarObject>());
		for (VarObject object : flowObjects) {
			VarObject cloneObject = object.clone();
			cloneObject.setFlowDad(clone);
			clone.getFlowObjects().add(cloneObject);
			oldToNew.put(object, cloneObject);
		}
		return oldToNew;
	}
	
	private void setVariablesClones(Flow clone, Map<VarObject, VarObject> oldToNew) {
        if (clone.getFlowVariables() == null || clone.getFlowVariables().isEmpty()) {
        	return;
        }
        
    	clone.setFlowVariables(new ArrayList<Variable>());
    	for (Variable variable : flowVariables) {
    		Variable cloneVariable = variable.clone();
    		cloneVariable.setVarObject(oldToNew.get(variable.getVarObject()));
			cloneVariable.setFlowDad(clone);
    		clone.getFlowVariables().add(cloneVariable);
    	}
	}
}
