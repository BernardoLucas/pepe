package com.mangyBear.Pepe.domain.types.graph;

import java.util.ArrayList;
import java.util.List;

import com.mxgraph.swing.mxGraphComponent;

public enum GraphGridStyleType {

	CROSS("Cross",
			mxGraphComponent.GRID_STYLE_CROSS,
			true),
	DASHED("Dashed",
			mxGraphComponent.GRID_STYLE_DASHED,
			true),
	DOT("Dot",
			mxGraphComponent.GRID_STYLE_DOT,
			true),
	LINE("Line",
			mxGraphComponent.GRID_STYLE_LINE,
			true);

	private final String name;
	private final int style;
	private final boolean activated;

	private GraphGridStyleType(String name, int style, boolean activated) {
		this.name = name;
		this.style = style;
		this.activated = activated;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public String getName() {
		return name;
	}

	public int getStyle() {
		return style;
	}

	public boolean isActivated() {
		return activated;
	}
	
	public static List<GraphGridStyleType> getEnableGridStyles() {
		List<GraphGridStyleType> enableGridStyles = new ArrayList<>();
		for (GraphGridStyleType style : GraphGridStyleType.values()) {
			if (style.isActivated()) {
				enableGridStyles.add(style);
			}
		}
		return enableGridStyles;
	}

	public static GraphGridStyleType getGridStylesByName(String name) {
		for (GraphGridStyleType style : GraphGridStyleType.values()) {
			if (style.getName().equals(name)) {
				return style;
			}
		}
		return null;
	}
}
