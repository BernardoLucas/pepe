package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="step")
@NamedQueries({
	@NamedQuery(name = "stepByName", query = "SELECT entity from Step entity where lower(nmStep) = :name"),
	@NamedQuery(name = "stepLikeName", query = "SELECT entity from Step entity where lower(nmStep) LIKE :name")})
public class Step implements Cloneable, Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_step", nullable=false, unique=true)
	private Long id;
	
	@Column(name="nm_step", nullable=false, unique=true)
	private String nmStep;
    
	@OneToMany(mappedBy="step", cascade = CascadeType.ALL)
    private List<Finder> finders;
	
	@OneToMany(mappedBy="step", cascade = CascadeType.ALL)
    private List<Action> actions;

	@Override
	public String toString() {
		return this.getNmStep();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmStep() {
		return nmStep;
	}

	public void setNmStep(String nmStep) {
		this.nmStep = nmStep;
	}

	public List<Finder> getFinders() {
		return finders;
	}

	public void setFinders(List<Finder> finders) {
		this.finders = finders;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public List<Action> getActionsByOrder() {
		return sortActions(actions);
	}

    private List<Action> sortActions(List<Action> listToSort) {
    	Collections.sort(listToSort, new Comparator<Action>() {
            @Override
            public int compare(Action o1, Action o2) {
                return o1.getOrder().compareTo(o2.getOrder());
            }
        });
        return listToSort;
    }
	
	public Step clone() {
        try {
        	Step clone = (Step) super.clone();
        	clone.setId(null);
            clone.setNmStep(clone.getNmStep() + "-CLONE-");
            Map<Finder, Finder> oldToNew = setFindersClones(clone);
            setActionsClones(clone, oldToNew);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in Step. " );
            return this;
        }
    }
	
	private Map<Finder, Finder> setFindersClones(Step clone) {
		if (clone.getFinders() == null || clone.getFinders().isEmpty()) {
			return null;
		}
		
        Map<Finder, Finder> oldToNew = new HashMap<>();
		clone.setFinders(new ArrayList<Finder>());
		for (Finder finder : finders) {
			Finder cloneFinder = finder.clone();
			cloneFinder.setStep(clone);
			clone.getFinders().add(cloneFinder);
			oldToNew.put(finder, cloneFinder);
		}
		return oldToNew;
	}
	
	private void setActionsClones(Step clone, Map<Finder, Finder> oldToNew) {
        if (clone.getActions() == null || clone.getActions().isEmpty()) {
        	return;
        }
        
    	clone.setActions(new ArrayList<Action>());
    	for (Action action : actions) {
    		Action cloneAction = action.clone();
    		cloneAction.setFinder(oldToNew.get(action.getFinder()));
			cloneAction.setStep(clone);
    		clone.getActions().add(cloneAction.clone());
    	}
	}
}
