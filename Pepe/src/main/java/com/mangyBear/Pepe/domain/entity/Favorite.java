package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="favorite")

@NamedQueries({
	@NamedQuery(name = "findByMac", query = "SELECT entity from Favorite entity where macAddress = :mac"),
	@NamedQuery(name = "findByMacLikeName", query = "SELECT entity from Favorite entity where lower(nmFavorite) LIKE :name AND macAddress = :mac")
})
public class Favorite implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_favorite", nullable=false, unique=true)
	private Long id;

	@Column(name="nm_favorite", nullable=false)
	private String nmFavorite;
	
	//Types
	@ManyToOne
	@JoinColumn(name="id_step")
	private Step step;
	
	@ManyToOne
	@JoinColumn(name="id_flow")
	private Flow flow;
	
	@ManyToOne
	@JoinColumn(name="id_logic")
	private Logic logic;
	//end of types
	
	@Column(name="mac_address", nullable=false)
	private String macAddress;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmFavorite() {
		return nmFavorite;
	}

	public void setNmFavorite(String nmFavorite) {
		this.nmFavorite = nmFavorite;
	}

	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public Flow getFlow() {
		return flow;
	}

	public void setFlow(Flow flow) {
		this.flow = flow;
	}

	public Logic getLogic() {
		return logic;
	}

	public void setLogic(Logic logic) {
		this.logic = logic;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
}
