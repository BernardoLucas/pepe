package com.mangyBear.Pepe.domain.types.graph;

import java.util.Arrays;
import java.util.List;

import com.mxgraph.util.mxConstants;

public enum GraphType {

	CELL_BEGIN("BEGIN",
			"BEGIN",
			60.0,
			60.0,
			"#ffffff",
			mxConstants.ALIGN_MIDDLE,
			mxConstants.ALIGN_CENTER,
			"#b8e1fe",
			"#149cfd",
			mxConstants.DIRECTION_SOUTH,
			"#149cfd",
			mxConstants.SHAPE_ELLIPSE,
			mxConstants.PERIMETER_ELLIPSE),
	CELL_FLOW("",
			"FLOW",
			100.0,
			60.0,
			"#ffffff",
			mxConstants.ALIGN_MIDDLE,
			mxConstants.ALIGN_CENTER,
			"#b7d7ef",
			"#107cca",
			mxConstants.DIRECTION_SOUTH,
			"#107cca",
			mxConstants.SHAPE_RECTANGLE,
			mxConstants.PERIMETER_RECTANGLE),
	CELL_LOGIC("",
			"LOGIC",
			100.0,
			60.0,
			"#000000",
			mxConstants.ALIGN_MIDDLE,
			mxConstants.ALIGN_CENTER,
			"#ffffd1",
			"#ffff66",
			mxConstants.DIRECTION_SOUTH,
			"#ffff66",
			mxConstants.SHAPE_RHOMBUS,
			mxConstants.PERIMETER_RHOMBUS),
	CELL_STEP("",
			"STEP",
			100.0,
			60.0,
			"#000000",
			mxConstants.ALIGN_MIDDLE,
			mxConstants.ALIGN_CENTER,
			"#ebebeb",
			"#bfbfbf",
			mxConstants.DIRECTION_SOUTH,
			"#bfbfbf",
			mxConstants.SHAPE_HEXAGON,
			mxConstants.PERIMETER_HEXAGON),
	CELL_END("END",
			"END",
			35.0,
			35.0,
			"#ffffff",
			mxConstants.ALIGN_MIDDLE,
			mxConstants.ALIGN_CENTER,
			"#cccccc",
			"#000000",
			mxConstants.DIRECTION_SOUTH,
			"#000000",
			mxConstants.SHAPE_ELLIPSE,
			mxConstants.PERIMETER_ELLIPSE);
	
	private final String label;
	private final String type;
	private final Double width;
	private final Double height;
	private final String fontHexColor;
	private final String fontVerticalAlign;
	private final String fontHorizontalAlign;
	private final String fillHexColor;
	private final String gradientHexColor;
	private final String gradientDirection;
	private final String strokeHexColor;
	private final String shape;
	private final String perimeter;

	private GraphType(String label, String type, Double width, Double height, String fontHexColor,
			String fontVerticalAlign, String fontHorizontalAlign, String fillHexColor, String gradientHexColor,
			String gradientDirection, String strokeHexColor, String shape, String perimeter) {
		this.label = label;
		this.type = type;
		this.width = width;
		this.height = height;
		this.fontHexColor = fontHexColor;
		this.fontVerticalAlign = fontVerticalAlign;
		this.fontHorizontalAlign = fontHorizontalAlign;
		this.fillHexColor = fillHexColor;
		this.gradientHexColor = gradientHexColor;
		this.gradientDirection = gradientDirection;
		this.strokeHexColor = strokeHexColor;
		this.shape = shape;
		this.perimeter = perimeter;
	}

	@Override
	public String toString() {
		return this.getType();
	}

	public String getLabel() {
		return label;
	}

	public String getType() {
		return type;
	}

	public Double getWidth() {
		return width;
	}

	public Double getHeight() {
		return height;
	}

	public String getFontHexColor() {
		return fontHexColor;
	}

	public String getFontVerticalAlign() {
		return fontVerticalAlign;
	}

	public String getFontHorizontalAlign() {
		return fontHorizontalAlign;
	}
	
	public String getFillHexColor() {
		return fillHexColor;
	}
	
	public String getGradientHexColor() {
		return gradientHexColor;
	}

	public String getGradientDirection() {
		return gradientDirection;
	}

	public String getStrokeHexColor() {
		return strokeHexColor;
	}

	public String getShape() {
		return shape;
	}

	public String getPerimeter() {
		return perimeter;
	}

	public static List<GraphType> getAllGraphTypes() {
		return Arrays.asList(GraphType.values());
	}
}
