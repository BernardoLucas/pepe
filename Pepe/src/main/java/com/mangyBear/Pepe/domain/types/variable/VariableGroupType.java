package com.mangyBear.Pepe.domain.types.variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.mangyBear.Pepe.domain.types.action.ReflectionType;
import com.mangyBear.Pepe.domain.types.frame.FrameAddType;

public enum VariableGroupType {
	
	VARIABLE_FILE_GROUP(FrameAddType.VARIABLE_FILE_GROUP,
			ReflectionType.FINDER_FILE,
			true),
	VARIABLE_OBJECT_GROUP(FrameAddType.VARIABLE_OBJECT_GROUP,
			null,
			true),
	VARIABLE_SQL_GROUP(FrameAddType.VARIABLE_SQL_GROUP,
			null,
			true),
	VARIABLE_WEBDRIVER_GROUP(FrameAddType.VARIABLE_WEBDRIVER_GROUP,
			null,
			true);
	
	private final FrameAddType frameAddType;
	private final ReflectionType reflectionType;
	private final boolean visible;

	private VariableGroupType(FrameAddType frameAddType, ReflectionType reflectionType, boolean visible) {
		this.frameAddType = frameAddType;
		this.reflectionType = reflectionType;
		this.visible = visible;
	}
	
	@Override
	public String toString() {
		return getFrameAddType().getName();
	}
	
	public FrameAddType getFrameAddType() {
		return frameAddType;
	}

	public ReflectionType getReflectionType() {
		return reflectionType;
	}

	public boolean isVisible() {
		return visible;
	}
	
	public static List<VariableGroupType> getVisibleVariableGroupType(boolean sortByName) {
		List<VariableGroupType> visibleObjs = new ArrayList<>();
		for (VariableGroupType obj : VariableGroupType.values()) {
			if (obj.isVisible()) {
				visibleObjs.add(obj);
			}
		}
		return sortByName ? sortObjectByName(visibleObjs) : visibleObjs;
	}

    private static List<VariableGroupType> sortObjectByName(List<VariableGroupType> listToSort) {
    	Collections.sort(listToSort, new Comparator<VariableGroupType>() {
            @Override
            public int compare(VariableGroupType o1, VariableGroupType o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        return listToSort;
    }
}
