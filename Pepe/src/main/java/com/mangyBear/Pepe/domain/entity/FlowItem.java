package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="flow_item")

@NamedQueries({
	@NamedQuery(name = "findByStep", query = "SELECT entity from FlowItem entity where entity.step.id = :idStep"),
	@NamedQuery(name = "findByLogic", query = "SELECT entity from FlowItem entity where entity.logic.id = :idLogic"),
	@NamedQuery(name = "flowItemByFlowAndFirstConnection",
				query = "SELECT entity.currentItem from FlowItemConnection entity "
						+ "where entity.currentItem.flowDad.id = :idFlowDad and entity.previousItem is null")
})
public class FlowItem implements Cloneable, Serializable  {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_flow_item", nullable=false, unique=true)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_flow_dad", nullable=false)
	private Flow flowDad;
	
	//Types
	@ManyToOne
	@JoinColumn(name="id_step")
	private Step step;
	
	@ManyToOne
	@JoinColumn(name="id_flow")
	private Flow flow;
	
	@ManyToOne
	@JoinColumn(name="id_logic")
	private Logic logic;
	
	@ManyToOne
	@JoinColumn(name="id_end")
	private EndFlow end;
	//end of types
	
	@OneToMany(mappedBy="currentItem", fetch = FetchType.EAGER)
    private List<FlowItemConnection> previousFlowItems;
	
	@OneToMany(mappedBy="previousItem", fetch = FetchType.EAGER)
    private List<FlowItemConnection> nextFlowItems;
	
	@Column(name="width", nullable=false)
	private Double width;
	
	@Column(name="height", nullable=false)
	private Double height;
	
	@Column(name="x", nullable=false)
	private Double x;
	
	@Column(name="y", nullable=false)
	private Double y;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Flow getFlowDad() {
		return flowDad;
	}

	public void setFlowDad(Flow flowDad) {
		this.flowDad = flowDad;
	}

	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public Flow getFlow() {
		return flow;
	}

	public void setFlow(Flow flow) {
		this.flow = flow;
	}

	public Logic getLogic() {
		return logic;
	}

	public void setLogic(Logic logic) {
		this.logic = logic;
	}

	public EndFlow getEnd() {
		return end;
	}

	public void setEnd(EndFlow end) {
		this.end = end;
	}

	public List<FlowItemConnection> getPreviousFlowItems() {
		return previousFlowItems;
	}

	public void setPreviousFlowItems(List<FlowItemConnection> previousFlowItems) {
		this.previousFlowItems = previousFlowItems;
	}

	public List<FlowItemConnection> getNextFlowItems() {
		return nextFlowItems;
	}

	public void setNextFlowItems(List<FlowItemConnection> nextFlowItems) {
		this.nextFlowItems = nextFlowItems;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}
	
	public FlowItem clone() {
        try {
        	FlowItem clone = (FlowItem) super.clone();
        	clone.setId(null);
        	setTypeClone(clone);
        	setPreviousFlowItemsClones(clone);
        	setNextFlowItemsClones(clone);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not supported in FlowItem. " );
            return this;
        }
    }
	
	private void setTypeClone(FlowItem clone) {
		// Flow has no need to be cloned, because it's just a "pointer"
		if (logic != null) {
			clone.setLogic(logic.clone());
			return;
		}
		if (step != null) {
			clone.setStep(step.clone());
			return;
		}
	}
	
	private void setPreviousFlowItemsClones(FlowItem clone) {
		clone.setPreviousFlowItems(new ArrayList<FlowItemConnection>());
		for (FlowItemConnection connection : previousFlowItems) {
			FlowItemConnection cloneConnection = connection.clone();
			cloneConnection.setCurrentItem(clone);
			clone.getPreviousFlowItems().add(cloneConnection);
		}
	}
	
	private void setNextFlowItemsClones(FlowItem clone) {
		clone.setNextFlowItems(new ArrayList<FlowItemConnection>());
		for (FlowItemConnection connection : nextFlowItems) {
			FlowItemConnection cloneConnection = connection.clone();
			cloneConnection.setPreviousItem(clone);
			clone.getNextFlowItems().add(cloneConnection);
		}
	}
}
