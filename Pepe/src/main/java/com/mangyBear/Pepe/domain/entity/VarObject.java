package com.mangyBear.Pepe.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;

@Entity
@Table(name = "var_object")
@NamedQueries({
	@NamedQuery(name = "varObjectByName", query = "SELECT entity from VarObject entity where lower(nameObject) LIKE :name"),
	@NamedQuery(name = "varObjectLikeName", query = "SELECT entity from VarObject entity where lower(nameObject) LIKE :name")})
public class VarObject implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_var_object", nullable = false, unique = true)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_flow_dad")
	private Flow flowDad;

	@Column(name = "nm_var_object", unique = true)
	private String nameObject;
	
	@Column(name="tp_var_object", nullable=false)
	@Enumerated(EnumType.STRING)
    private VariableGroupType varObjectType;
	
	@OneToMany(mappedBy="object", cascade = CascadeType.ALL)
    private List<VarObjectParameter> parameters;

	@Override
	public String toString() {
		return this.getNameObject();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idValue) {
		this.id = idValue;
	}

	public Flow getFlowDad() {
		return flowDad;
	}

	public void setFlowDad(Flow flowDad) {
		this.flowDad = flowDad;
	}

	public String getNameObject() {
		return nameObject;
	}

	public void setNameObject(String nameObject) {
		this.nameObject = nameObject;
	}

	public VariableGroupType getType() {
		return varObjectType;
	}

	public void setType(VariableGroupType varObjectType) {
		this.varObjectType = varObjectType;
	}

	public List<VarObjectParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<VarObjectParameter> parameters) {
		this.parameters = parameters;
	}
	
	public VarObject clone() {
        try {
        	VarObject clone = (VarObject) super.clone();
        	clone.setId(null);
        	clone.setNameObject(clone.getNameObject() + "-CLONE-");
        	setParametersClones(clone);
            return clone;
        } catch (CloneNotSupportedException e) {
            System.out.println (" Cloning not allowed in VarObject. " );
            return this;
        }
    }
	
	private void setParametersClones(VarObject clone) {
		if (clone.getParameters() == null || clone.getParameters().isEmpty()) {
			return;
		}
		
		clone.setParameters(new ArrayList<VarObjectParameter>());
		for (VarObjectParameter parameter : parameters) {
			VarObjectParameter parameterClone = parameter.clone();
			parameterClone.setObject(clone);
			clone.getParameters().add(parameterClone);
		}
	}
}
