package com.mangyBear.Pepe.ui.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragSource;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.AbstractCellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.mangyBear.Pepe.domain.types.frame.FrameFieldType;

public class ComponentsTable extends JTable {

	private static final long serialVersionUID = -2975203699339504586L;

	private String[] titles;
	private Integer[] checkBoxColumns;
	private Integer[][] checkBoxCells;
	private boolean byColumn;
	private Integer[][] notEditableCells;
	private Integer[] notEditableColumns;
	private DefaultTableModel model;
	private Map<Integer[], JComboBox<?>> comboMap = new HashMap<>();
	private Map<Integer[], JPanel> panelDialogMap = new HashMap<>();
	private Map<Integer[], JPanel> suggestDialogMap = new HashMap<>();
	private Map<Integer[], JPanel> panelFileChooserMap = new HashMap<>();
	private Map<Integer[], JPanel> suggestFileChooserMap = new HashMap<>();
	private Map<Integer[], SuggestCombo> suggestMap = new HashMap<>();
	private Map<Integer[], SuggestCombo> suggestButtonMap = new HashMap<>();
	private Map<Integer[], FrameFieldType> filteringCells;
	private boolean enterPressed;

	public ComponentsTable(String[] titles, Integer[] checkBoxColumns, Integer[] notEditableColumns, boolean rowSorter,
			boolean cellSelection, boolean dragDrop) {
		this.byColumn = true;
		this.titles = titles;
		this.checkBoxColumns = checkBoxColumns;
		this.notEditableColumns = notEditableColumns;
		buildTableByColumnModel();
		getTableHeader().setReorderingAllowed(false);
		getTableHeader().setResizingAllowed(true);
		((DefaultTableCellRenderer) getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
		setAutoCreateRowSorter(rowSorter);
		if (cellSelection) {
			setCellSelectionEnabled(cellSelection);
		}
		setDragDrop(dragDrop);

		if (checkBoxColumns != null) {
			for (int i = 0; i < checkBoxColumns.length; i++) {
				int booleanColumn = checkBoxColumns[i];
				String label = titles[booleanColumn];
				TableColumn tableColumn = getColumnModel().getColumn(booleanColumn);
				int size = label.length() * 12;
				tableColumn.setMaxWidth(size);
				tableColumn.setMinWidth(size);
				tableColumn.setHeaderRenderer(new HeaderRenderer(this, booleanColumn, label));
				tableColumn.setCellRenderer(new CellRenderer(this, null, booleanColumn));
			}
		}
	}

	private void setDragDrop(boolean enable) {
		if (enable) {
			setDragEnabled(true);
			setDropMode(DropMode.INSERT_ROWS);
			setTransferHandler(new TableRowTransferHandler(this));
		}
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		if (notEditableCells != null) {
			for (Integer[] notEditableCell : notEditableCells) {
				if (row == notEditableCell[0] && col == notEditableCell[1]) {
					return false;
				}
			}
		}
		if (notEditableColumns != null) {
			for (Integer notEditableColumn : notEditableColumns) {
				if (col == notEditableColumn) {
					return false;
				}
			}
		}
		return super.isCellEditable(row, col);
	}

	private void buildTableByColumnModel() {
		this.model = new DefaultTableModel(null, titles) {

			private static final long serialVersionUID = -4810162939912486155L;

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				if (checkBoxColumns != null) {
					for (int booleanColumn : checkBoxColumns) {
						if (columnIndex == booleanColumn) {
							return Boolean.class;
						}
					}
				}
				return String.class;
			}
		};
		this.setModel(model);
	}

	public ComponentsTable(String[] titles, Integer[][] checkBoxCells, Integer[][] comboBoxCells,
			Integer[][] panelDialogCells, Integer[][] suggestDialogCells, Integer[][] panelFileChooserCells,
			Integer[][] suggestFileChooserCells, Integer[][] suggestCells, Integer[][]  suggestButtonCells,
			Integer[][] notEditableCells, Map<Integer[], FrameFieldType> filteringCells, boolean rowSorter) {
		this.byColumn = false;
		this.titles = titles;
		this.checkBoxCells = checkBoxCells;
		this.notEditableCells = notEditableCells;
		this.filteringCells = filteringCells;
		buildTableByCellModel();
		getTableHeader().setReorderingAllowed(false);
		getTableHeader().setResizingAllowed(true);
		((DefaultTableCellRenderer) getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
		setAutoCreateRowSorter(rowSorter);
		setCellSelectionEnabled(true);

		createComboMap(comboBoxCells);
		createPanelDialogMap(panelDialogCells);
		createSuggestDialogMap(suggestDialogCells);
		createPanelFileChooserMap(panelFileChooserCells);
		createSuggestFileChooserMap(suggestFileChooserCells);
		createSuggestMap(suggestCells);
		createSuggestButtonMap(suggestButtonCells);
	}

	private void buildTableByCellModel() {
		this.model = new DefaultTableModel(null, titles);
		this.setModel(model);
	}
	
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
	    return new JPanel();
	}

	private void createComboMap(Integer[][] comboBoxCells) {
		for (int i = 0; i < comboBoxCells.length; i++) {
			comboMap.put(comboBoxCells[i], new JComboBox<>());
		}
	}
	
	private void createPanelDialogMap(Integer[][] panelDialogCells) {
		for (int i = 0; i < panelDialogCells.length; i++) {
			panelDialogMap.put(panelDialogCells[i], createPanelTextField());
		}
	}

	private void createSuggestDialogMap(Integer[][] suggestDialogCells) {
		for (int i = 0; i < suggestDialogCells.length; i++) {
			suggestDialogMap.put(suggestDialogCells[i], createPanelSuggest());
		}
	}
	
	private void createPanelFileChooserMap(Integer[][] panelFileChooserCells) {
		for (int i = 0; i < panelFileChooserCells.length; i++) {
			panelFileChooserMap.put(panelFileChooserCells[i], createPanelTextField());
		}
	}

	private void createSuggestFileChooserMap(Integer[][] suggestFileChooserCells) {
		for (int i = 0; i < suggestFileChooserCells.length; i++) {
			suggestFileChooserMap.put(suggestFileChooserCells[i], createPanelSuggest());
		}
	}
	
	private void createSuggestMap(Integer[][] suggestCells) {
		for (int i = 0; i < suggestCells.length; i++) {
			suggestMap.put(suggestCells[i], new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK));
		}
	}

	private void createSuggestButtonMap(Integer[][] suggestButtonCells) {
		for (int i = 0; i < suggestButtonCells.length; i++) {
			suggestButtonMap.put(suggestButtonCells[i], new SuggestCombo(null, false, Color.BLACK, 0, Color.BLACK));
		}
	}

	private JPanel createPanelTextField() {
		final JPanel panel = new JPanel();
		final JTextField panelTextField = new JTextField();
		panel.setLayout(new BorderLayout());
		panel.add(panelTextField, BorderLayout.CENTER);
		panel.add(new JButton(), BorderLayout.EAST);
		panel.addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				panelTextField.requestFocusInWindow();
			}
		});
		return panel;
	}
	
	private JPanel createPanelSuggest() {
		final JPanel panel = new JPanel();
		final SuggestCombo panelSuggest = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		panel.setLayout(new BorderLayout());
		panel.add(panelSuggest, BorderLayout.CENTER);
		panel.add(new JButton(), BorderLayout.EAST);
		panel.addFocusListener(new FocusAdapter() {
			
			@Override
			public void focusGained(FocusEvent e) {
				panelSuggest.requestFocusInWindow();
			}
		});
		return panel;
	}

	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {
		int modelColumn = convertColumnIndexToModel(column);
		int modelRow = convertRowIndexToModel(row);

		if (checkBoxCells != null) {
			for (Integer[] booleanCell : checkBoxCells) {
				if (modelRow == booleanCell[0] && modelColumn == booleanCell[1]) {
					return new CellRenderer(this, booleanCell[0], booleanCell[1]);
				}
			}
		}
		return super.getCellRenderer(row, column);
	}

	@Override
	public TableCellEditor getCellEditor(int row, int column) {
		int modelColumn = convertColumnIndexToModel(column);
		int modelRow = convertRowIndexToModel(row);

		if (comboMap != null) {
			for (Entry<Integer[], JComboBox<?>> entrySet : comboMap.entrySet()) {
				Integer[] cell = entrySet.getKey();
				if ((byColumn && modelColumn == cell[1]) || (modelRow == cell[0] && modelColumn == cell[1])) {
					return new DefaultCellEditor((JComboBox<?>) entrySet.getValue());
				}
			}
		}
		
		if (panelDialogMap != null) {
			for (Entry<Integer[], JPanel> entrySet : panelDialogMap.entrySet()) {
				Integer[] cell = entrySet.getKey();
				if ((byColumn && modelColumn == cell[1]) || (modelRow == cell[0] && modelColumn == cell[1])) {
					return new TextCellEditor((JPanel) entrySet.getValue());
				}
			}
		}

		if (suggestDialogMap != null) {
			for (Entry<Integer[], JPanel> entrySet : suggestDialogMap.entrySet()) {
				Integer[] cell = entrySet.getKey();
				if ((byColumn && modelColumn == cell[1]) || (modelRow == cell[0] && modelColumn == cell[1])) {
					return new SuggestCellEditor((JPanel) entrySet.getValue());
				}
			}
		}
		
		if (panelFileChooserMap != null) {
			for (Entry<Integer[], JPanel> entrySet : panelFileChooserMap.entrySet()) {
				Integer[] cell = entrySet.getKey();
				if ((byColumn && modelColumn == cell[1]) || (modelRow == cell[0] && modelColumn == cell[1])) {
					return new TextCellEditor((JPanel) entrySet.getValue());
				}
			}
		}

		if (suggestFileChooserMap != null) {
			for (Entry<Integer[], JPanel> entrySet : suggestFileChooserMap.entrySet()) {
				Integer[] cell = entrySet.getKey();
				if ((byColumn && modelColumn == cell[1]) || (modelRow == cell[0] && modelColumn == cell[1])) {
					return new SuggestCellEditor((JPanel) entrySet.getValue());
				}
			}
		}
		
		if (suggestMap != null) {
			for (Entry<Integer[], SuggestCombo> entrySet : suggestMap.entrySet()) {
				Integer[] cell = entrySet.getKey();
				if ((byColumn && modelColumn == cell[1]) || (modelRow == cell[0] && modelColumn == cell[1])) {
					return createCellEditor((SuggestCombo) entrySet.getValue(), this);
				}
			}
		}

		if (suggestButtonMap != null) {
			for (Entry<Integer[], SuggestCombo> entrySet : suggestButtonMap.entrySet()) {
				Integer[] cell = entrySet.getKey();
				if ((byColumn && modelColumn == cell[1]) || (modelRow == cell[0] && modelColumn == cell[1])) {
					return createCellEditor((SuggestCombo) entrySet.getValue(), this);
				}
			}
		}

		if (filteringCells != null) {
			for (Entry<Integer[], FrameFieldType> entrySet : filteringCells.entrySet()) {
				Integer[] cell = entrySet.getKey();
				if ((byColumn && modelColumn == cell[1]) || (modelRow == cell[0] && modelColumn == cell[1])) {
					JTextField textField = new JTextField();
					textField.setDocument(new FieldFilter(entrySet.getValue()));
					return new DefaultCellEditor(textField);
				}
			}
		}

		if (checkBoxCells != null) {
			for (Integer[] booleanCell : checkBoxCells) {
				if (modelRow == booleanCell[0] && modelColumn == booleanCell[1]) {
					return super.getDefaultEditor(Boolean.class);
				}
			}
		}

		return super.getCellEditor(row, column);
	}

	private DefaultCellEditor createCellEditor(final SuggestCombo suggest, final JTable table) {
		suggest.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				setEnterPressed(e.getKeyCode() == KeyEvent.VK_ENTER);
			}
		});
		return new DefaultCellEditor(suggest);
	}

	@Override
	public void removeEditor() {
		if (!enterPressed) {
			super.removeEditor();
		}
	}

	public void setEnterPressed(boolean enterPressed) {
		this.enterPressed = enterPressed;
	}

	class SuggestCellEditor extends AbstractCellEditor implements TableCellEditor {
		
		private static final long serialVersionUID = -8767273708303631544L;
		
		private final JPanel panelComp;
		private final SuggestCombo suggest;
		
		public SuggestCellEditor(JPanel panelComp) {
			this.panelComp = panelComp;
			this.suggest = getSuggest();
		}
		
		private SuggestCombo getSuggest() {
			for (Component component : panelComp.getComponents()) {
			    if (SuggestCombo.class.equals(component.getClass())) {
			        return (SuggestCombo) component;
			    }
			}
			return null;
		}
		
		@Override
		public Object getCellEditorValue() {
			return suggest.getSelectedItem();
		}
		
		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			if (value == null) {
				suggest.setSelectedItem("");
			} else {
				suggest.setSelectedItem(value.toString());
			}
			return panelComp;
		}
	}
	
	class TextCellEditor extends AbstractCellEditor implements TableCellEditor {

		private static final long serialVersionUID = -8767273708303631544L;

		private final JPanel panelComp;
		private final JTextField text;

		public TextCellEditor(JPanel panelComp) {
			this.panelComp = panelComp;
			this.text = getTextField();
		}
		
		private JTextField getTextField() {
			for (Component component : panelComp.getComponents()) {
			    if (JTextField.class.equals(component.getClass())) {
			        return (JTextField) component;
			    }
			}
			return null;
		}

		@Override
		public Object getCellEditorValue() {
			return text.getText();
		}

		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			if (value == null) {
				text.setText("");
			} else {
				text.setText(value.toString());
			}
			return panelComp;
		}
	}
}

class HeaderRenderer extends TriStateCheckBox implements TableCellRenderer {

	private static final long serialVersionUID = 2691675767008276134L;

	private final JTable table;
	private final JTableHeader header;
	private final TableModel tableModel;
	private final JLabel label;
	private final int targetColumn;
	private int viewColumn;
	private final TableColumnModel columnModel;

	public HeaderRenderer(JTable table, int targetColumn, String name) {
		this.table = table;
		this.header = table.getTableHeader();
		this.tableModel = table.getModel();
		this.label = new JLabel(name);
		this.targetColumn = targetColumn;
		this.columnModel = table.getColumnModel();
		setOpaque(false);
		setFont(header.getFont());
		setHorizontalAlignment(CENTER);
		createListeners();
	}

	private void createListeners() {
		header.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				viewColumn = header.columnAtPoint(e.getPoint());
				TableColumn column = columnModel.getColumn(viewColumn);
				int modelColumn = column.getModelIndex();
				if (modelColumn == targetColumn) {
					Object v = column.getHeaderValue();
					boolean b = State.DESELECTED.equals(v);
					TableModel m = table.getModel();
					for (int i = 0; i < m.getRowCount(); i++) {
						m.setValueAt(b, i, modelColumn);
					}
					column.setHeaderValue(b ? State.SELECTED : State.DESELECTED);
				}
			}
		});
		tableModel.addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				int viewColumn = table.convertColumnIndexToView(targetColumn);
				TableColumn column = table.getColumnModel().getColumn(viewColumn);
				int type = e.getType();
				int col = e.getColumn();
				boolean eventToChange = type == TableModelEvent.UPDATE || type == TableModelEvent.DELETE;
				boolean colToChange = col == targetColumn || col == TableModelEvent.ALL_COLUMNS;
				if (eventToChange && colToChange) {
					column.setHeaderValue(discoverState());
					header.repaint(header.getHeaderRect(viewColumn));
				}
			}
		});
	}

	@Override
	public Component getTableCellRendererComponent(JTable tbl, Object val, boolean isS, boolean hasF, int row,
			int col) {
		TableCellRenderer r = tbl.getTableHeader().getDefaultRenderer();
		JLabel l = (JLabel) r.getTableCellRendererComponent(tbl, val, isS, hasF, row, col);
		if (targetColumn == tbl.convertColumnIndexToModel(col)) {
			if (!(val instanceof State)) {
				val = discoverState();
			}
			setState((State) val);
			label.setIcon(new HeaderComponentIcon(this));
			l.setIcon(new HeaderComponentIcon(label));
			l.setText(null);
		}
		header.repaint(header.getHeaderRect(viewColumn));
		return l;
	}

	private State discoverState() {
		boolean selected = true;
		boolean deselected = true;
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			Boolean b = (Boolean) tableModel.getValueAt(i, targetColumn);
			if (b != null) {
				selected &= b;
				deselected &= !b;
				if (selected == deselected) {
					break;
				}
			}
		}
		if (deselected) {
			return State.DESELECTED;
		}
		if (selected) {
			return State.SELECTED;
		}
		return State.INDETERMINATE;
	}
}

class HeaderComponentIcon implements Icon {

	private final JComponent cmp;

	public HeaderComponentIcon(JComponent cmp) {
		this.cmp = cmp;
	}

	@Override
	public int getIconWidth() {
		return cmp.getPreferredSize().width;
	}

	@Override
	public int getIconHeight() {
		return cmp.getPreferredSize().height;
	}

	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		SwingUtilities.paintComponent(g, cmp, c.getParent(), x, y, getIconWidth(), getIconHeight());
	}
}

class TriStateCheckBox extends JCheckBox {

	private static final long serialVersionUID = 4639551346595651083L;

	public TriStateCheckBox() {
		this(null, State.DESELECTED);
	}

	public TriStateCheckBox(String text, State initial) {
		super.setText(text);
		setModel(new TriStateModel(initial));
		setRolloverEnabled(false);
	}

	public void setState(State state) {
		((TriStateModel) model).setState(state);
	}

	public State getState() {
		return ((TriStateModel) model).getState();
	}

	@Override
	public void setSelected(boolean selected) {
		((TriStateModel) model).setSelected(selected);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.darkGray);
		int width = getWidth();
		if (((TriStateCheckBox.TriStateModel) model).getState() == State.INDETERMINATE) {

			int cx = width / 2;
			int cy = getHeight() / 2;

			g2.fillRect(cx - 3, cy - 3, 7, 7);
		}
		// if (((TriStateCheckBox.TriStateModel) model).getState() ==
		// State.SELECTED) {
		// int x = getX();
		// int y = getY();
		// // Traço Esquerdo
		// g2.drawLine(x + 4, y + 6, x + 6, y + (width - 8));
		// g2.drawLine(x + 3, y + 6, x + 6, y + (width - 7));
		// g2.drawLine(x + 2, y + 6, x + 6, y + (width - 6));
		// // Traço Direito
		// g2.drawLine(x + (width - 3), y + 2, x + 7, y + (width - 8));
		// g2.drawLine(x + (width - 2), y + 2, x + 7, y + (width - 7));
		// g2.drawLine(x + (width - 1), y + 2, x + 7, y + (width - 6));
		// }
	}

	private static class TriStateModel extends JToggleButton.ToggleButtonModel {

		private static final long serialVersionUID = 1448510326421905615L;

		protected State state;

		public TriStateModel(State state) {
			this.state = state;
		}

		@Override
		public boolean isSelected() {
			return state == State.SELECTED;
		}

		public State getState() {
			return state;
		}

		public void setState(State state) {
			this.state = state;
			fireStateChanged();
		}

		@Override
		public void setPressed(boolean pressed) {
			if (pressed) {
				switch (state) {
				case DESELECTED:
					state = State.SELECTED;
					break;
				case INDETERMINATE:
				case SELECTED:
					state = State.DESELECTED;
					break;
				}
			}
		}

		@Override
		public void setSelected(boolean selected) {
			if (selected) {
				this.state = State.SELECTED;
			} else {
				this.state = State.DESELECTED;
			}
		}
	}
}

enum State {
	SELECTED, DESELECTED, INDETERMINATE
}

class CellRenderer extends TriStateCheckBox implements TableCellRenderer {

	private static final long serialVersionUID = 3331312701995336163L;

	private final DefaultTableCellRenderer tableRenderer;
	private final JLabel label;
	private final int targetColumn;
	private final Integer targetRow;

	public CellRenderer(JTable table, Integer targetRow, int targetColumn) {
		this.tableRenderer = new DefaultTableCellRenderer();
		this.label = new JLabel();
		this.targetRow = targetRow;
		this.targetColumn = targetColumn;
		setOpaque(false);
		setHorizontalAlignment(CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(JTable tbl, Object val, boolean isS, boolean hasF, int row,
			int col) {
		JLabel l = (JLabel) tableRenderer.getTableCellRendererComponent(tbl, val, isS, hasF, row, col);
		int rowIndex = tbl.convertRowIndexToModel(row);
		int colIndex = tbl.convertColumnIndexToModel(col);
		if (targetRow != null && targetRow == rowIndex && targetColumn == colIndex) {
			setBoolean(val instanceof Boolean ? val : false, l);
		}
		if (targetRow == null && targetColumn == colIndex) {
			setBoolean((val != null) ? val : false, l);
		}
		tbl.repaint(tbl.getCellRect(row, col, true));
		return l;
	}

	private void setBoolean(Object val, JLabel l) {
		setSelected((Boolean) val);
		label.setIcon(new HeaderComponentIcon(this));
		l.setIcon(new HeaderComponentIcon(label));
		l.setText(null);
		l.setHorizontalAlignment(CENTER);
	}
}

/**
 * Handles drag & drop row reordering
 */
class TableRowTransferHandler extends TransferHandler {

	private static final long serialVersionUID = 7139230452126526957L;

	private final DataFlavor localObjectFlavor = new ActivationDataFlavor(Integer.class,
			"application/x-java-Integer;class=java.lang.Integer", "Integer Row Index");
	private JTable table = null;

	public TableRowTransferHandler(JTable table) {
		this.table = table;
	}

	@Override
	protected Transferable createTransferable(JComponent c) {
		assert (c == table);
		return new DataHandler(new Integer(table.getSelectedRow()), localObjectFlavor.getMimeType());
	}

	@Override
	public boolean canImport(TransferHandler.TransferSupport info) {
		boolean b = info.getComponent() == table && info.isDrop() && info.isDataFlavorSupported(localObjectFlavor);
		table.setCursor(b ? DragSource.DefaultMoveDrop : DragSource.DefaultMoveNoDrop);
		return b;
	}

	@Override
	public int getSourceActions(JComponent c) {
		return TransferHandler.COPY_OR_MOVE;
	}

	@Override
	public boolean importData(TransferHandler.TransferSupport info) {
		JTable target = (JTable) info.getComponent();
		JTable.DropLocation dl = (JTable.DropLocation) info.getDropLocation();
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		int index = dl.getRow();
		int max = model.getRowCount();
		if (index < 0 || index > max) {
			index = max;
		}

		target.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		try {
			Integer rowFrom = (Integer) info.getTransferable().getTransferData(localObjectFlavor);
			if (rowFrom < 0 || rowFrom == index) {
				return false;
			}

			if (index > rowFrom) {
				index--;
			}
			model.moveRow(rowFrom, rowFrom, index);
			target.getSelectionModel().setSelectionInterval(index, index);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void exportDone(JComponent c, Transferable t, int act) {
		if ((act == TransferHandler.MOVE) || (act == TransferHandler.NONE)) {
			table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
}
