package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;

public class ClosableTabbedPane extends JTabbedPane {

	private static final long serialVersionUID = -7843639458199247247L;
	
	private Color contentAreaColor;
	
	public ClosableTabbedPane() {
		super();
		super.addMouseListener(new TabbedPaneListener());
	}

	public Component add(int index, String title, Component component) {
		super.add(getValidSizeTitle(title), component);
		super.setIconAt(index, new CloseIcon());
		super.setSelectedIndex(index);
		super.setToolTipTextAt(index, title);
		super.setBackground(Color.LIGHT_GRAY);
		
		return component;
	}
	
	private String getValidSizeTitle(String title) {
		if (title.length() > 20) {
			return title.substring(0, 19);
		}
		
		return title;
	}
	
	public void setTitle(int index, String title) {
		super.setTitleAt(index, title);
		super.setToolTipTextAt(index, title);
	}

	public void setContentAreaColor(Color contentAreaColor) {
		this.contentAreaColor = contentAreaColor;
	}
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
		if (contentAreaColor != null) {
	        g.setColor(contentAreaColor);
	        g.fillRect(0, 0, getWidth(), getHeight());
		}
        g.drawImage(FramesHelpType.FRAME_MAIN.getIcons().get(1).getImage(), (getWidth() / 2) - 100 , (getHeight() / 2) - 100, this);
    }
}

class CloseIcon implements Icon {

	private int SIZE = 8;
	private Rectangle iconRect;
	private Color COLOR_CROSS = UIManager.getColor("BLUE");

	public CloseIcon() {
	}

	public void paintIcon(Component c, Graphics g, int x, int y) {
		drawCross(g, x, y);
		iconRect = new Rectangle(x, y, SIZE, SIZE);
	}

	private void drawCross(Graphics g, int xo, int yo) {

		g.setColor(COLOR_CROSS);
		g.drawLine(xo, yo, xo + SIZE, yo + SIZE);
		g.drawLine(xo, yo + 1, xo + (SIZE - 1), yo + SIZE);
		g.drawLine(xo, yo + SIZE, xo + SIZE, yo);
		g.drawLine(xo, yo + (SIZE - 1), xo + (SIZE - 1), yo);
		g.drawLine(xo + 1, yo, xo + SIZE, yo + (SIZE - 1));
		g.drawLine(xo + 1, yo + SIZE, xo + SIZE, yo + 1);

		g.setColor(Color.WHITE);
		g.drawLine(xo + 2, yo, xo + 4, yo + 2);
		g.drawLine(xo + 6, yo + 4, xo + SIZE, yo + 6);
		g.drawLine(xo + 2, yo + SIZE, xo + 4, yo + 6);
		g.drawLine(xo + 6, yo + 4, xo + SIZE, yo + 2);
	}

	public boolean coordinatenInIcon(int x, int y) {
		boolean isInIcon = false;
		if (iconRect != null) {
			isInIcon = iconRect.contains(x, y);
		}
		return isInIcon;
	}

	public int getIconWidth() {
		return SIZE;
	}

	public int getIconHeight() {
		return SIZE;
	}
}

class TabbedPaneListener extends MouseAdapter {

	public void mouseReleased(MouseEvent e) {

		super.mouseReleased(e);

		ClosableTabbedPane tabPane = (ClosableTabbedPane) e.getSource();
		if (tabPane.isEnabled()) {

			int tabIndex = getTabByCoordinate((JTabbedPane) tabPane, e.getX(), e.getY());

			if (tabIndex >= 0 && tabPane.isEnabledAt(tabIndex)) {

				CloseIcon closeIcon = (CloseIcon) tabPane.getIconAt(tabIndex);
				if (closeIcon.coordinatenInIcon(e.getX(), e.getY())) {
					if (Color.RED.equals(tabPane.getSelectedComponent().getBackground())) {
						int option = JOptionPane.showConfirmDialog(tabPane,
								"Your work has not been saved.\nDo you really want to close it?", "Closing Tab",
								JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
						if (option == JOptionPane.NO_OPTION) {
							return;
						}
						tabPane.remove(tabIndex);
					} else {
						tabPane.remove(tabIndex);
					}
				}
			}
		}
	}

	private int getTabByCoordinate(JTabbedPane pane, int x, int y) {

		Point p = new Point(x, y);

		int tabCount = pane.getTabCount();
		for (int i = 0; i < tabCount; i++) {
			if (pane.getBoundsAt(i).contains(p.x, p.y)) {
				return i;
			}
		}
		return -1;
	}
}