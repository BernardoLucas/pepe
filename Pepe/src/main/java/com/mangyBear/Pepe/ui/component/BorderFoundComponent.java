package com.mangyBear.Pepe.ui.component;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Area;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Lucas Silva Bernardo
 */

public class BorderFoundComponent extends JFrame{

	private static final long serialVersionUID = -1878569076308573646L;
	
	private static Rectangle lastRectangle;

	public BorderFoundComponent(Color borderColor, Float borderWidth) {

		if (isInvalidBorder(borderColor, borderWidth)) {
			return;
		}
		
		lastRectangle = new Rectangle(0, 0, 0, 0);
		buildFrame(borderColor, borderWidth);
	}

	private boolean isInvalidBorder(Color borderColor, Float borderWidth) {
		return borderColor == null || (borderWidth == null || borderWidth.isNaN() || borderWidth.isInfinite());
	}

	private void buildFrame(Color borderColor, float borderWidth) {
		setUndecorated(true);
		setBackground(new Color(0, 0, 0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setType(Type.UTILITY);
		setLocation(lastRectangle.getLocation());
		setSize(lastRectangle.getSize());
		setLayout(new BorderLayout());
		add(new CapturePane(borderColor, borderWidth));
		setAlwaysOnTop(true);
		setVisible(true);
	}

	public void frameDispose() {
		setVisible(false);
		dispose();
	}

	public void resize(Rectangle rectangle) {
		
		if (rectangle == null) {
			return;
		}
		
		if (lastRectangle != null && lastRectangle.equals(rectangle)) {
			return;
		}

		lastRectangle = rectangle;
		
		setLocation(lastRectangle.getLocation());
		setSize(lastRectangle.getSize());
			
	}
}

class CapturePane extends JPanel {

	private static final long serialVersionUID = 3494235750745773675L;

	private Color borderColor;
	private Float borderWidth;

	public CapturePane(Color borderColor, float borderWidth) {
		this.borderColor = borderColor;
		this.borderWidth = borderWidth;
		setOpaque(false);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setColor(new Color(255, 255, 255, 128));

		Dimension size = getSize();
		Dimension subSize = new Dimension(size.width - 3, size.height - 3);
		Area fill = new Area(new Rectangle(new Point(0, 0), size));
		fill.subtract(new Area(new Rectangle(new Point(0, 0), subSize)));
		g2d.fill(fill);
		g2d.setColor(borderColor != null ? borderColor : Color.BLACK);// Border color
		g2d.setStroke(new BasicStroke(borderWidth != null ? borderWidth : 3.0f));// Border width
		g2d.draw(new Rectangle(new Point(1, 1), subSize));
		g2d.dispose();
	}
}
