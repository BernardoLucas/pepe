package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.accessibility.Accessible;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.plaf.basic.BasicComboPopup;

import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.Schedule;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;
import com.mangyBear.Pepe.ui.component.SuggestCombo;
import com.mangyBear.Pepe.ui.component.TimeField;
import com.mangyBear.Pepe.ui.component.schedule.SchedulerFlow;
import com.mangyBear.Pepe.ui.controller.FlowController;
import com.mangyBear.Pepe.ui.controller.ScheduleController;
import com.mangyBear.Pepe.ui.domain.CellValue;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

public class FrameScheduler extends BaseFrame {

	private static final long serialVersionUID = -2884068132135272901L;

	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
	private final FrameMain frameMain;
	private final int VERTICAL_SPACING = 10;
	private Schedule schedule;

	private JLabel labelName;
	private SuggestCombo suggestName;
	private JList<?> suggestNameList;
	private JLabel labelFlow;
	private SuggestCombo suggestFlow;
	private JCheckBox checkSun;
	private JCheckBox checkMon;
	private JCheckBox checkTue;
	private JCheckBox checkWed;
	private JCheckBox checkThu;
	private JCheckBox checkFri;
	private JCheckBox checkSat;
	private JLabel labelTime;
	private TimeField txtTime;
	private JCheckBox checkPublic;
	private JToggleButton btnSchedule;
	private JLabel labelInformation;
	private JButton btnSave;
	private JButton btnDelete;

	private boolean wasArrows;
	private boolean isUpdating;

	public FrameScheduler(FrameMain frameMain, Flow flow) {
		super(frameMain, FramesHelpType.FRAME_SCHEDULER.getName(), true);
		this.blockerLoading = new BlockerLoading(this, null, true);
		this.bindingFactory = new BindingFactory();
		this.frameMain = frameMain;
		initGUI();
		createComponents();
		addComponents();
		addListeners();
		if (flow != null) {
			suggestFlow.setSelectedItem(new SuggestDoubleColumn(new CellValue(flow.getId(), flow.getNmFlow(), GraphType.CELL_FLOW, flow), flow.getNmFlow()));
		}
	}

	private void initGUI() {
		setLayout(new RelativeLayout());
		setSize(new Dimension(450, 190));
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(frameMain);
	}

	private void createComponents() {
		labelName = new JLabel(FramesHelpType.FRAME_SCHEDULER.getLabels().get(0));
		suggestName = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		suggestNameList = getSuggestNameList();
		labelFlow = new JLabel(FramesHelpType.FRAME_SCHEDULER.getLabels().get(1));
		suggestFlow = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		checkSun = new JCheckBox("Sun");
		checkMon = new JCheckBox("Mon");
		checkTue = new JCheckBox("Tue");
		checkWed = new JCheckBox("Wed");
		checkThu = new JCheckBox("Thu");
		checkFri = new JCheckBox("Fri");
		checkSat = new JCheckBox("Sat");
		labelTime = new JLabel(FramesHelpType.FRAME_SCHEDULER.getLabels().get(3));
		txtTime = new TimeField();
		txtTime.setPreferredSize(new Dimension(52, 20));
		checkPublic = new JCheckBox(FramesHelpType.FRAME_SCHEDULER.getLabels().get(4));
		btnSchedule = new JToggleButton();
		btnSchedule.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.setColor(btnSchedule.isSelected() ? Color.decode("#32ff32") : Color.decode("#ff3232"));
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
		        btnSchedule.setText(btnSchedule.isSelected() ? "Active" : "Inactive");
				super.paint(g, c);
			}
		});
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_SCHEDULER.getIcons().get(0));
		btnSave = new JButton("Save");
		btnDelete = new JButton("Delete");
	}

	private JList<?> getSuggestNameList() {
		Accessible a = suggestName.getAccessibleContext().getAccessibleChild(0);
		if (a instanceof BasicComboPopup) {
			return ((BasicComboPopup) a).getList();
		}
		return null;
	}

	private void addComponents() {
		add(blockerLoading.createLoading(20, 5), loadingBinding());

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		add(labelName, labelNameBinding());
		add(suggestName, suggestNameBinding());
		add(labelFlow, labelFlowBinding());
		add(suggestFlow, suggestFlowBinding());
		add(checkSun, checkSunBinding());
		add(checkMon, checkMonBinding());
		add(checkTue, checkTueBinding());
		add(checkWed, checkWedBinding());
		add(checkThu, checkThuBinding());
		add(checkFri, checkFriBinding());
		add(checkSat, checkSatBinding());
		add(labelTime, labelTimeBinding());
		add(txtTime, txtTimeBinding());
		add(checkPublic, checkPublicBinding());
		add(btnSchedule, btnScheduleBinding());
		add(labelInformation, labelInformationBinding());
		add(btnSave, btnSaveBinding());
		add(btnDelete, btnDeleteBinding());
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private RelativeConstraints labelNameBinding() {
		Binding[] labelNameBinding = new Binding[2];
		labelNameBinding[0] = bindingFactory.leftEdge();
		labelNameBinding[1] = bindingFactory.verticallyCenterAlignedWith(suggestName);
		return new RelativeConstraints(labelNameBinding);
	}

	private RelativeConstraints suggestNameBinding() {
		Binding[] suggestNameBinding = new Binding[3];
		suggestNameBinding[0] = bindingFactory.topEdge();
		suggestNameBinding[1] = bindingFactory.rightOf(labelName);
		suggestNameBinding[2] = bindingFactory.rightEdge();
		return new RelativeConstraints(suggestNameBinding);
	}

	private RelativeConstraints labelFlowBinding() {
		Binding[] labelFlowBinding = new Binding[2];
		labelFlowBinding[0] = bindingFactory.leftAlignedWith(labelName);
		labelFlowBinding[1] = bindingFactory.verticallyCenterAlignedWith(suggestFlow);
		return new RelativeConstraints(labelFlowBinding);
	}

	private RelativeConstraints suggestFlowBinding() {
		Binding[] suggestFlowBinding = new Binding[3];
		suggestFlowBinding[0] = bindingFactory.below(suggestName);
		suggestFlowBinding[1] = bindingFactory.leftAlignedWith(suggestName);
		suggestFlowBinding[2] = bindingFactory.rightAlignedWith(suggestName);
		return new RelativeConstraints(suggestFlowBinding);
	}

	private RelativeConstraints checkSunBinding() {
		Binding[] checkSunBinding = new Binding[2];
		checkSunBinding[0] = bindingFactory.leftOf(checkMon);
		checkSunBinding[1] = bindingFactory.verticallyCenterAlignedWith(checkWed);
		return new RelativeConstraints(checkSunBinding);
	}

	private RelativeConstraints checkMonBinding() {
		Binding[] checkMonBinding = new Binding[2];
		checkMonBinding[0] = bindingFactory.leftOf(checkTue);
		checkMonBinding[1] = bindingFactory.verticallyCenterAlignedWith(checkWed);
		return new RelativeConstraints(checkMonBinding);
	}

	private RelativeConstraints checkTueBinding() {
		Binding[] checkTueBinding = new Binding[2];
		checkTueBinding[0] = bindingFactory.leftOf(checkWed);
		checkTueBinding[1] = bindingFactory.verticallyCenterAlignedWith(checkWed);
		return new RelativeConstraints(checkTueBinding);
	}

	private RelativeConstraints checkWedBinding() {
		Binding[] checkWedBinding = new Binding[2];
		checkWedBinding[0] = bindingFactory.below(suggestFlow);
		checkWedBinding[1] = new Binding(Edge.HORIZONTAL_CENTER, 12, Direction.LEFT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
		return new RelativeConstraints(checkWedBinding);
	}

	private RelativeConstraints checkThuBinding() {
		Binding[] checkThuBinding = new Binding[2];
		checkThuBinding[0] = bindingFactory.rightOf(checkWed);
		checkThuBinding[1] = bindingFactory.verticallyCenterAlignedWith(checkWed);
		return new RelativeConstraints(checkThuBinding);
	}

	private RelativeConstraints checkFriBinding() {
		Binding[] checkFriBinding = new Binding[2];
		checkFriBinding[0] = bindingFactory.rightOf(checkThu);
		checkFriBinding[1] = bindingFactory.verticallyCenterAlignedWith(checkWed);
		return new RelativeConstraints(checkFriBinding);
	}
	
	private RelativeConstraints checkSatBinding() {
		Binding[] checkSatBinding = new Binding[2];
		checkSatBinding[0] = bindingFactory.rightOf(checkFri);
		checkSatBinding[1] = bindingFactory.verticallyCenterAlignedWith(checkWed);
		return new RelativeConstraints(checkSatBinding);
	}
	
	private RelativeConstraints labelTimeBinding() {
		Binding[] labelTimeBinding = new Binding[2];
		labelTimeBinding[0] = bindingFactory.leftAlignedWith(checkSun);
		labelTimeBinding[1] = bindingFactory.verticallyCenterAlignedWith(txtTime);
		return new RelativeConstraints(labelTimeBinding);
	}
	
	private RelativeConstraints txtTimeBinding() {
		Binding[] txtTimeBinding = new Binding[2];
		txtTimeBinding[0] = bindingFactory.rightOf(labelTime);
		txtTimeBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnSchedule);
		return new RelativeConstraints(txtTimeBinding);
	}

	private RelativeConstraints checkPublicBinding() {
		Binding[] checkPublicBinding = new Binding[2];
		checkPublicBinding[0] = bindingFactory.leftAlignedWith(checkSat);
		checkPublicBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnSchedule);
		return new RelativeConstraints(checkPublicBinding);
	}

	private RelativeConstraints btnScheduleBinding() {
		Binding[] btnScheduleBinding = new Binding[2];
		btnScheduleBinding[0] = bindingFactory.below(checkWed);
		btnScheduleBinding[1] = bindingFactory.horizontallyCenterAlignedWith(Binding.PARENT);
		return new RelativeConstraints(btnScheduleBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private RelativeConstraints btnSaveBinding() {
		Binding[] btnSaveBinding = new Binding[2];
		btnSaveBinding[0] = bindingFactory.leftOf(btnDelete);
		btnSaveBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnDelete);
		return new RelativeConstraints(btnSaveBinding);
	}

	private RelativeConstraints btnDeleteBinding() {
		Binding[] btnDeleteBinding = new Binding[2];
		btnDeleteBinding[0] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		btnDeleteBinding[1] = new Binding(Edge.RIGHT, 9, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		return new RelativeConstraints(btnDeleteBinding);
	}

	private void addListeners() {
		if (suggestNameList != null) {
			suggestNameList.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					JList<?> list = (JList<?>) e.getComponent();
					Point pt = e.getPoint();
					int index = list.locationToIndex(pt);
					if (index >= 0) {
						setSelectedSchedule(suggestName.getItemAt(index));
					}
				}
			});
		}
		suggestName.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = suggestName.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasArrows && key == KeyEvent.VK_ENTER) {
								suggestName.hidePopup();
								wasArrows = false;
								setSelectedSchedule(item);
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(suggestName, item.toString());
							} else {
								wasArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		suggestFlow.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = suggestFlow.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasArrows && key == KeyEvent.VK_ENTER) {
								suggestFlow.hidePopup();
								wasArrows = false;
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(suggestFlow, item.toString());
							} else {
								wasArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		btnSave.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (validateFields()) {
					saveSchedule();
				}
			}
		});
		btnDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ev) {
					if (schedule != null && schedule.getId() == null) {
						blockerLoading.start("Deleting Schedule");
						new SwingWorker<Void, Void>() {
							@Override
							protected Void doInBackground() {
								try {
									(new ScheduleController()).remove(schedule);
								} catch (Exception e) {
									showError(e);
								}
								blockerLoading.stop();
								return null;
							}
						}.execute();
					}
			}
		});
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameScheduler.this, FramesHelpType.FRAME_SCHEDULER)).setVisible(true);
			}
		});
	}

	private void comboFilter(final SuggestCombo suggest, final String searchFor) {
		blockerLoading.start("Searching equivalences");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				List<SuggestDoubleColumn> items = getItems(suggest, searchFor);
				if (items.size() > 0) {
					suggest.setModel(new DefaultComboBoxModel(items.toArray()));
					suggest.setSelectedItem(searchFor.isEmpty() ? "" : searchFor);
					JTextField editor = (JTextField) suggest.getEditor().getEditorComponent();
					editor.setCaretPosition(editor.getDocument().getLength());
					suggest.showPopup();
				} else {
					suggest.hidePopup();
				}
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}

	private List<SuggestDoubleColumn> getItems(SuggestCombo suggest, String searchFor) {
		List<SuggestDoubleColumn> returnList = new ArrayList<>();
		try {
			if (suggestName.equals(suggest)) {
				Long flowId = (suggestFlow.getSelectedItem() instanceof SuggestDoubleColumn && ((SuggestDoubleColumn) suggestFlow.getSelectedItem()).getLeftText() instanceof CellValue)
						? ((SuggestDoubleColumn) suggestFlow.getSelectedItem()).getLeftText().getId() : 0;
				
				for (Schedule schedule : (new ScheduleController()).findLikeNameAndFlow(searchFor, flowId)) {
					returnList.add(new SuggestDoubleColumn(new CellValue(schedule.getId(), schedule.getNmSchedule(), null, schedule), searchFor));
				}
			}
			if (suggestFlow.equals(suggest)) {
				for (Flow flow : (new FlowController()).findLikeName(searchFor)) {
					returnList.add(new SuggestDoubleColumn(new CellValue(flow.getId(), flow.getNmFlow(), null, flow), searchFor));
				}
			}
		} catch (Exception e) {
			showError(e);
		}
		return returnList;
	}
	
	private void setSelectedSchedule(Object item) {
		if (item instanceof SuggestDoubleColumn && ((SuggestDoubleColumn) item).getLeftText() instanceof CellValue
				&& ((SuggestDoubleColumn) item).getLeftText().getFullItem() instanceof Schedule) {
			
			isUpdating = true;
			schedule = (Schedule)((SuggestDoubleColumn) item).getLeftText().getFullItem();
			suggestFlow.setSelectedItem(new SuggestDoubleColumn(new CellValue(schedule.getFlow().getId(), schedule.getFlow().getNmFlow(), GraphType.CELL_FLOW, schedule.getFlow()), schedule.getFlow().getNmFlow()));
			checkSun.setSelected(schedule.isExec_sun());
			checkMon.setSelected(schedule.isExec_mon());
			checkTue.setSelected(schedule.isExec_tue());
			checkWed.setSelected(schedule.isExec_wed());
			checkThu.setSelected(schedule.isExec_thu());
			checkFri.setSelected(schedule.isExec_fri());
			checkSat.setSelected(schedule.isExec_sat());
			txtTime.setText(schedule.getExec_time());
			checkPublic.setSelected(schedule.getExec_public() == null);
			btnSchedule.setSelected(schedule.isActive());
		}
	}

	private boolean validateFields() {
		if (suggestName.getSelectedItem() == null || suggestName.getSelectedItem().toString().length() < 3) {
			JOptionPane.showMessageDialog(FrameScheduler.this, "The scheduler name entered is less than three characters or null.", "Scheduler Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (suggestFlow.getSelectedItem() == null || suggestFlow.getSelectedItem().toString().length() < 3) {
			JOptionPane.showMessageDialog(FrameScheduler.this, "The flow name entered is less than three characters or null.", "Scheduler Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (!(suggestFlow.getSelectedItem() instanceof SuggestDoubleColumn) || !(((SuggestDoubleColumn) suggestFlow.getSelectedItem()).getLeftText() instanceof CellValue)
				|| !(((SuggestDoubleColumn) suggestFlow.getSelectedItem()).getLeftText().getFullItem() instanceof Flow)) {
			JOptionPane.showMessageDialog(FrameScheduler.this, "Please, select a valid flow.", "Scheduler Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (!checkSun.isSelected() && !checkMon.isSelected() && !checkTue.isSelected() && !checkWed.isSelected()
				&& !checkThu.isSelected() && !checkFri.isSelected() && !checkSat.isSelected()) {
			JOptionPane.showMessageDialog(FrameScheduler.this, "Please, select at least one day.", "Scheduler Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (txtTime.isInsertError()) {
			JOptionPane.showMessageDialog(FrameScheduler.this, "Please, insert a valid time.", "Scheduler Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	private void saveSchedule() {
		blockerLoading.start("Saving Schedule");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				String name = suggestName.getSelectedItem().toString();
				if (!isUpdating) {
					schedule = new Schedule();
				}
				
				String validScheduleName = name;
				ScheduleController scheduleController = new ScheduleController();
				
				try {
					if (!isUpdating || !name.equals(schedule.getNmSchedule())) {
						validScheduleName = name.equalsIgnoreCase(schedule.getNmSchedule()) ? name : getValidScheduleName(scheduleController, name);
						if (validScheduleName == null) {
							isUpdating = true;
							blockerLoading.stop();
							return null;
						}
						schedule.setNmSchedule(validScheduleName);
					}
					
					schedule.setFlow((Flow)((SuggestDoubleColumn) suggestFlow.getSelectedItem()).getLeftText().getFullItem());
					schedule.setExec_sun(checkSun.isSelected());
					schedule.setExec_mon(checkMon.isSelected());
					schedule.setExec_tue(checkTue.isSelected());
					schedule.setExec_wed(checkWed.isSelected());
					schedule.setExec_thu(checkThu.isSelected());
					schedule.setExec_fri(checkFri.isSelected());
					schedule.setExec_sat(checkSat.isSelected());
					schedule.setExec_time(txtTime.getText());
					schedule.setExec_public(!checkPublic.isSelected() ? MyConstants.MAC_ADDRESS : null);
					schedule.setActive(btnSchedule.isSelected());
					
					scheduleController.save(schedule);
					
					blockerLoading.setMessage("Scheduling");
					(new SchedulerFlow()).schedule();
				} catch (Exception e) {
					showError(e);
				}
				
				blockerLoading.stop();
				dispose();
				return null;
			}
		}.execute();
	}
	
	private String getValidScheduleName(ScheduleController scheduleController, String name) throws Exception {
		Schedule result = scheduleController.findByName(name);
		if (result != null) {
			Object newName = JOptionPane.showInputDialog(FrameScheduler.this, "This name already exist, please insert a new one with more than three characters.", "Scheduler Alert", JOptionPane.WARNING_MESSAGE, null, null, name);
			if (newName == null || newName.toString().isEmpty() || newName.toString().length() < 3) {
				return null;
			}
			return getValidScheduleName(scheduleController, newName.toString());
		}
		
		return name;
	}
}
