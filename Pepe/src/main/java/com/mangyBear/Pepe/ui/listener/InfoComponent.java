package com.mangyBear.Pepe.ui.listener;

import java.awt.Rectangle;

import com.sun.jna.platform.win32.WinDef.HWND;

public class InfoComponent {

	private HWND hwnd;
	private String className;
	private Rectangle rect;

	public InfoComponent(HWND hwnd, String className, Rectangle rect) {
		this.hwnd = hwnd;
		this.className = className;
		this.rect = rect;
	}
	
	@Override
	public String toString() {
		return getClassName();
	}
	
	public boolean isEmpity() {
		return getClassName() == null || getClassName().isEmpty();
	}

	public HWND getHwnd() {
		return hwnd;
	}

	public void setHwnd(HWND hwnd) {
		this.hwnd = hwnd;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Rectangle getRectangle() {
		return rect;
	}

	public void setRectangle(Rectangle rect) {
		this.rect = rect;
	}
}