package com.mangyBear.Pepe.ui.controller;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.service.ActionService;
import com.mangyBear.Pepe.service.FinderService;
import com.mangyBear.Pepe.service.StepService;
import com.mangyBear.Pepe.ui.component.readWriteFile.ExportLog;

public class StepController {

	private final StepService service = new StepService();
	
	public Step buildStep(String nmStep, List<Finder> finders, List<Action> actions) {
		Step step = new Step();
		step.setNmStep(nmStep);
		step.setFinders(finders);
		step.setActions(actions);
		
		return step;
	}
	
	public Step clone(Long id) throws Exception {
		Step clone = service.findById(id).clone();
		
		service.store(clone);
		return clone;
	}

	public Step save(Step step, DefaultTableModel findersModel, DefaultTableModel actionsModel) throws Exception {
		if (step.getId() != null) {
			service.update(step);
		} else {
			service.store(step);
		}
		
		finderTreatmentSaveEverything(step, findersModel);
		
		actionTreatmentSaveEverything(step, actionsModel);
		
		return step;
	}
	
	private void finderTreatmentSaveEverything(Step step, DefaultTableModel findersModel) throws Exception {
		FinderService finderService = new FinderService();
		for (int i = 0; i < findersModel.getRowCount(); i++) {
			Finder finder = (Finder) findersModel.getValueAt(i, 0);
			finder.setStep(step);
			
			if (finder.getId() == null) {
				finderService.store(finder);
			} else {
				finderService.update(finder);
			}
		}
	}
	
	private void actionTreatmentSaveEverything(Step step, DefaultTableModel actionsModel) throws Exception {
		ActionService actionService = new ActionService();
		for (int i = 0; i < actionsModel.getRowCount(); i++) {
			Action action = (Action) actionsModel.getValueAt(i, 0);
			action.setStep(step);
			action.setOrder(i);
			
			if (action.getId() == null) {
				actionService.store(action);
			} else {
				actionService.update(action);
			}
		}
	}
	
	public Step findByName(String name) throws Exception {
		return service.findByName(name);
	}
	
	public List<Step> findLikeName(String name) throws Exception {
		return service.findLikeName(name);
	}
	
	public List<Step> findLikeNameLimited(String name, int limit) throws Exception {
		return service.findLikeNameLimited(name, limit);
	}

	public void execute(ExportLog log, Step step) {
		service.execute(log, step);
	}
}
