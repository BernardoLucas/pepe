package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import com.mangyBear.Pepe.domain.entity.EndFlow;
import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.FlowItem;
import com.mangyBear.Pepe.domain.entity.FlowItemConnection;
import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.graph.GraphGridStyleType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.controller.FlowController;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mangyBear.Pepe.ui.graph.Graph;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameMain extends JFrame {

	private static final long serialVersionUID = 4167088791640732609L;

	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;

	// North bar
	private JToolBar northBar;
	//North bar components
	private JButton btnSettings;
	private JButton btnHelp;
	private JSeparator firstSeparator;
	private JButton btnRun;
	private JButton btnSchedule;
	private JButton btnRecord;
	private JSeparator secondSeparator;
	private JButton btnNewFlow;
	private JButton btnNewCell;
	private JSeparator thirdSeparator;
	private JButton btnSave;
	private JButton btnTrash;
	private JLabel labelInformation;
	// Cells list and Tabs
	private JSplitPane mainSplitPane;
	private ClosableTabbedPane tabbedPane;

	private Point selectedCellPos;

	public FrameMain() {
		super(FramesHelpType.FRAME_MAIN.getName());
		this.blockerLoading = new BlockerLoading(null, this, true);
		this.bindingFactory = new BindingFactory();
		initGUI();
		addComponents();
		addListeners();
	}

	private void initGUI() {
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setLayout(new RelativeLayout());
		setMinimumSize(new Dimension(900, 600));
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setTaskBarIcon();
	}
	
	private void setTaskBarIcon() {
		List<Image> icons = new ArrayList<>();
		icons.add(FramesHelpType.FRAME_MAIN.getIcons().get(2).getImage());
		icons.add(FramesHelpType.FRAME_MAIN.getIcons().get(3).getImage());
		icons.add(FramesHelpType.FRAME_MAIN.getIcons().get(4).getImage());
		setIconImages(icons);
	}

	private void addComponents() {
		add(blockerLoading.createLoading(73, 5), loadingBinding());
		buildNorthBar();
		add(northBar, northBarBinding());
		buildCenter();
		add(mainSplitPane, mainSplitPaneBinding());
		enableBtns(false);
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 0, Direction.ABOVE, Edge.TOP, tabbedPane);
		loadingBinding[1] = new Binding(Edge.LEFT, 0, Direction.LEFT, Edge.LEFT, tabbedPane);
		loadingBinding[2] = new Binding(Edge.RIGHT, 0, Direction.RIGHT, Edge.RIGHT, tabbedPane);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 0, Direction.BELOW, Edge.BOTTOM, tabbedPane);
		return new RelativeConstraints(loadingBinding);
	}

	private void buildNorthBar() {
		createNorthBar();
		createNorthComponents();
		addNorthComponents();
	}

	private void createNorthBar() {
		northBar = new JToolBar();
		northBar.setLayout(new RelativeLayout());
		northBar.setPreferredSize(new Dimension(getWidth(), 45));
		northBar.setFloatable(false);
		northBar.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
	}

	private void createNorthComponents() {
		//Button Settings
		btnSettings = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(0));
		btnSettings.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(5));
		btnSettings.setFocusPainted(false);
		//Button Help
		btnHelp = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(1));
		btnHelp.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(6));
		btnHelp.setFocusPainted(false);
		//First Separator
		firstSeparator = new JSeparator(SwingConstants.VERTICAL);
		firstSeparator.setPreferredSize(new Dimension(2, 25));
		//Button Run
		btnRun = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(2));
		btnRun.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(7));
		btnRun.setFocusPainted(false);
		//Button Schedule
		btnSchedule = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(3));
		btnSchedule.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(8));
		btnSchedule.setFocusPainted(false);
		//Button Record
		btnRecord = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(4));
		btnRecord.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(9));
		btnRecord.setFocusPainted(false);
		//Second Separator
		secondSeparator = new JSeparator(SwingConstants.VERTICAL);
		secondSeparator.setPreferredSize(new Dimension(2, 25));
		//Button New Flow
		btnNewFlow = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(5));
		btnNewFlow.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(10));
		btnNewFlow.setFocusPainted(false);
		//Button New Cell
		btnNewCell = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(6));
		btnNewCell.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(11));
		btnNewCell.setFocusPainted(false);
		//Third Separator
		thirdSeparator = new JSeparator(SwingConstants.VERTICAL);
		thirdSeparator.setPreferredSize(new Dimension(2, 25));
		//Button Save
		btnSave = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(7));
		btnSave.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(12));
		btnSave.setFocusPainted(false);
		//Button Trash
		btnTrash = new JButton(FramesHelpType.FRAME_MAIN.getLabels().get(8));
		btnTrash.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(13));
		btnTrash.setFocusPainted(false);
		//Information icon
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_MAIN.getIcons().get(0));
	}
	
	private void addNorthComponents() {
		bindingFactory.setHorizontalSpacing(10);
		//Configuration area
		northBar.add(btnSettings, btnSettingsBinding());
		northBar.add(btnHelp, btnHelpBinding());
		//First Separator
		northBar.add(firstSeparator, firstSeparatorBinding());
		//Execution area
		northBar.add(btnRun, btnRunBinding());
		northBar.add(btnSchedule, btnScheduleBinding());
		northBar.add(btnRecord, btnRecordBinding());
		//Second Separator
		northBar.add(secondSeparator, secondSeparatorBinding());
		//Edit area
		northBar.add(btnNewFlow, btnNewFlowBinding());
		northBar.add(btnNewCell, btnNewCellBinding());
		//Third Separator
		northBar.add(thirdSeparator, thirdSeparatorBinding());
		//DB area
		northBar.add(btnSave, btnSaveBinding());
		northBar.add(btnTrash, btnTrashBinding());
		//Information icon
		northBar.add(labelInformation, labelInformationBinding());
	}

	private RelativeConstraints btnSettingsBinding() {
		Binding[] btnSettingsBinding = new Binding[3];
		btnSettingsBinding[0] = new Binding(Edge.TOP, 2, Direction.BELOW, Edge.TOP, Binding.PARENT);
		btnSettingsBinding[1] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		btnSettingsBinding[2] = new Binding(Edge.BOTTOM, 4, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(btnSettingsBinding);
	}

	private RelativeConstraints btnHelpBinding() {
		Binding[] btnHelpBinding = new Binding[3];
		btnHelpBinding[0] = bindingFactory.rightOf(btnSettings);
		btnHelpBinding[1] = bindingFactory.topAlign(btnSettings);
		btnHelpBinding[2] = bindingFactory.bottomAlignedWith(btnSettings);
		return new RelativeConstraints(btnHelpBinding);
	}

	private RelativeConstraints firstSeparatorBinding() {
		Binding[] firstSeparatorBinding = new Binding[2];
		firstSeparatorBinding[0] = bindingFactory.rightOf(btnHelp);
		firstSeparatorBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnSettings);
		return new RelativeConstraints(firstSeparatorBinding);
	}
	
	private RelativeConstraints btnRunBinding() {
		Binding[] btnRunBinding = new Binding[3];
		btnRunBinding[0] = bindingFactory.rightOf(firstSeparator);
		btnRunBinding[1] = bindingFactory.topAlign(btnSettings);
		btnRunBinding[2] = bindingFactory.bottomAlignedWith(btnSettings);
		return new RelativeConstraints(btnRunBinding);
	}
	
	private RelativeConstraints btnScheduleBinding() {
		Binding[] btnScheduleBinding = new Binding[3];
		btnScheduleBinding[0] = bindingFactory.rightOf(btnRun);
		btnScheduleBinding[1] = bindingFactory.topAlign(btnSettings);
		btnScheduleBinding[2] = bindingFactory.bottomAlignedWith(btnSettings);
		return new RelativeConstraints(btnScheduleBinding);
	}

	private RelativeConstraints btnRecordBinding() {
		Binding[] btnRecordBinding = new Binding[3];
		btnRecordBinding[0] = bindingFactory.rightOf(btnSchedule);
		btnRecordBinding[1] = bindingFactory.topAlign(btnSettings);
		btnRecordBinding[2] = bindingFactory.bottomAlignedWith(btnSettings);
		return new RelativeConstraints(btnRecordBinding);
	}

	private RelativeConstraints secondSeparatorBinding() {
		Binding[] secondSeparatorBinding = new Binding[2];
		secondSeparatorBinding[0] = bindingFactory.rightOf(btnRecord);
		secondSeparatorBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnSettings);
		return new RelativeConstraints(secondSeparatorBinding);
	}

	private RelativeConstraints btnNewFlowBinding() {
		Binding[] btnNewFlowBinding = new Binding[3];
		btnNewFlowBinding[0] = bindingFactory.rightOf(secondSeparator);
		btnNewFlowBinding[1] = bindingFactory.topAlign(btnSettings);
		btnNewFlowBinding[2] = bindingFactory.bottomAlignedWith(btnSettings);
		return new RelativeConstraints(btnNewFlowBinding);
	}

	private RelativeConstraints btnNewCellBinding() {
		Binding[] btnNewCellBinding = new Binding[3];
		btnNewCellBinding[0] = bindingFactory.rightOf(btnNewFlow);
		btnNewCellBinding[1] = bindingFactory.topAlign(btnSettings);
		btnNewCellBinding[2] = bindingFactory.bottomAlignedWith(btnSettings);
		return new RelativeConstraints(btnNewCellBinding);
	}

	private RelativeConstraints thirdSeparatorBinding() {
		Binding[] thirdSeparatorBinding = new Binding[2];
		thirdSeparatorBinding[0] = bindingFactory.rightOf(btnNewCell);
		thirdSeparatorBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnSettings);
		return new RelativeConstraints(thirdSeparatorBinding);
	}

	private RelativeConstraints btnSaveBinding() {
		Binding[] btnSaveBinding = new Binding[3];
		btnSaveBinding[0] = bindingFactory.rightOf(thirdSeparator);
		btnSaveBinding[1] = bindingFactory.topAlign(btnSettings);
		btnSaveBinding[2] = bindingFactory.bottomAlignedWith(btnSettings);
		return new RelativeConstraints(btnSaveBinding);
	}

	private RelativeConstraints btnTrashBinding() {
		Binding[] btnTrashBinding = new Binding[3];
		btnTrashBinding[0] = bindingFactory.rightOf(btnSave);
		btnTrashBinding[1] = bindingFactory.topAlign(btnSettings);
		btnTrashBinding[2] = bindingFactory.bottomAlignedWith(btnSettings);
		return new RelativeConstraints(btnTrashBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = bindingFactory.rightEdge();
		labelInformationBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnSettings);
		return new RelativeConstraints(labelInformationBinding);
	}

	private RelativeConstraints northBarBinding() {
		Binding[] northBarBinding = new Binding[3];
		northBarBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.TOP, Binding.PARENT);
		northBarBinding[1] = new Binding(Edge.LEFT, 0, Direction.RIGHT,	Edge.LEFT, Binding.PARENT);
		northBarBinding[2] = new Binding(Edge.RIGHT, 0, Direction.LEFT,	Edge.RIGHT, Binding.PARENT);
		return new RelativeConstraints(northBarBinding);
	}
	
	private void buildCenter() {
		tabbedPane = new ClosableTabbedPane();
		tabbedPane.setContentAreaColor(Color.LIGHT_GRAY);
		mainSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		mainSplitPane.setDividerLocation(300);
		BasicSplitPaneDivider mainDivider = ((BasicSplitPaneUI) mainSplitPane.getUI()).getDivider();
        mainDivider.addMouseListener(new MouseAdapter() {
        	
            @Override
            public void mouseEntered(MouseEvent e) {
            	setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });
		mainSplitPane.setLeftComponent(new SearchPanel(FrameMain.this));
		mainSplitPane.setRightComponent(tabbedPane);
	}
	
	private RelativeConstraints mainSplitPaneBinding() {
		Binding[] mainSplitPaneBinding = new Binding[4];
		mainSplitPaneBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.BOTTOM, northBar);
		mainSplitPaneBinding[1] = new Binding(Edge.BOTTOM, 0, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		mainSplitPaneBinding[2] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		mainSplitPaneBinding[3] = new Binding(Edge.RIGHT, 2, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		return new RelativeConstraints(mainSplitPaneBinding);
	}

	private void enableBtns(boolean state) {
		btnRun.setEnabled(state);
		btnNewCell.setEnabled(state);
		btnSave.setEnabled(state);
		btnTrash.setEnabled(state);
	}

	private void addListeners() {
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				validateClosingEvent();
			}
		});
		btnSettings.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				(new FrameSettings(FrameMain.this)).setVisible(true);
			}
		});
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameMain.this, FramesHelpType.FRAME_MAIN)).setVisible(true);
			}
		});
		btnHelp.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				(new FrameHelp(FrameMain.this)).setVisible(true);
			}
		});
		getTabbedPane().addContainerListener(new ContainerListener() {

			@Override
			public void componentAdded(ContainerEvent e) {
				enableBtns(true);
				getTabbedPane().setContentAreaColor(null);
			}

			@Override
			public void componentRemoved(ContainerEvent e) {
				if (getTabbedPane().getTabCount() == 0) {
					enableBtns(false);
					getTabbedPane().setContentAreaColor(Color.LIGHT_GRAY);
				}
			}
		});
		btnRun.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ae) {
				mxGraphComponent graphComponent = getSelectedGraphComponent();
				if (graphComponent != null) {
					minimizeFrameMain();
					mxCell begin = ((Graph) graphComponent.getGraph()).getCellMap().get(0);
					try {
						FlowController.execute((CellValue) begin.getValue());
					} catch (Exception e) {
						showError(e);
					}
				}
			}
		});
		btnSchedule.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mxGraphComponent graphComponent = getSelectedGraphComponent();
				if (graphComponent != null) {
					mxCell begin = ((Graph) graphComponent.getGraph()).getCellMap().get(0);
					(new FrameScheduler(FrameMain.this, (Flow)((CellValue) begin.getValue()).getFullItem())).setVisible(true);
				} else {
					(new FrameScheduler(FrameMain.this, null)).setVisible(true);
				}
			}
		});
		btnRecord.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				(new FrameRecorder(FrameMain.this)).setVisible(true);
				//String startURL = "https://www.abebooks.com/servlet/SearchEntry?cm_sp=TopNav-_-Home-_-Advs";
				//driverVariable - Criar variavel webdriver e buscar ela (chrome com OPTIONS test-type)
				//startURL - Qq url pra começar a gravação
			}
		});
		btnNewFlow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ae) {
				final String newFlowName = JOptionPane.showInputDialog(FrameMain.this, "Please, give a name for your new flow.", "New Flow", JOptionPane.INFORMATION_MESSAGE);
				if (newFlowName != null && !newFlowName.isEmpty()) {
					blockerLoading.start("Creating new Flow");

					new SwingWorker<Void, Void>() {
						
						@Override
						protected Void doInBackground() {
							if (!hasInternalFrameAlreadyOpen(newFlowName)) {
								addTab(newFlowName);
								
								Double x = (getSelectedGraphComponent().getWidth() / 2) - (GraphType.CELL_BEGIN.getWidth() / 2);
								CellValue cell = buildCellValue(0l, GraphType.CELL_BEGIN.getLabel(), GraphType.CELL_BEGIN, null);
								Flow flow = null;
								try {
									flow = FlowController.save(newFlowName, cell, x);
									
									cell.setId(flow.getId());
									cell.setFullItem(flow);
									
									mxGraphComponent graphComponent = addDefaultCellToSelectedGraphComponent(cell, null);
									if (graphComponent != null) {
										graphComponent.getParent().setBackground(Color.GREEN);
									}
								} catch (Exception e) {
									showError(e);
								}
							} else {
								JOptionPane.showMessageDialog(null,
										"This Flow is already open.",
										"Flow Openning",
										JOptionPane.WARNING_MESSAGE);
							}
							blockerLoading.stop();
							return null;
						}
					}.execute();
				}
			}
		});
		btnNewCell.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mxGraphComponent graphComponent = getSelectedGraphComponent();
				if (graphComponent != null) {
					(new FrameAskNewCell(FrameMain.this)).setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null,
							"Please, choose a Flow to work.", "Flow Openning",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final mxGraphComponent graphComponent = getSelectedGraphComponent();
				if (graphComponent != null) {
					blockerLoading.start("Saving cells and connections");

					new SwingWorker<Void, Void>() {
						@Override
						protected Void doInBackground() {
							// TODO: Update the selected flow with items
							try {
								mxGraph graph = graphComponent.getGraph();
								FlowController.storeFlowAndItems(((Graph) graph).getCellMap(), ((Graph) graph).getConnMap());
								graphComponent.getParent().setBackground(Color.GREEN);
							} catch (Exception e) {
								showError(e);
							}
							blockerLoading.stop();
							return null;
						}

					}.execute();
				}
			}
		});
		
		btnTrash.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final mxGraphComponent graphComponent = getSelectedGraphComponent();
				if (graphComponent == null) {
					return;
				}
				blockerLoading.start("");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						blockerLoading.setMessage("Getting Elements to Delete");
						try {
							Graph graph = (Graph) graphComponent.getGraph();
							
							Object[] objects = graph.getSelectionCells();//Get selected cells and connections
							Map<Integer, mxCell> cellMap = graph.getCellMap();//Get map of existing cells
							
							List<mxCell> listCellsToRemove = new ArrayList<>();
							List<mxCell> listEdgesToRemove = new ArrayList<>();
							if (objects.length > 0) {
								List<mxCell> listNoBegin = removeBeginCell(objects);//Remove Begin cell
								listCellsToRemove = getCellsToRemoveCount(listNoBegin);//Get selected cells to remove
								listEdgesToRemove = getEdgesToRemoveCount(listNoBegin);//Get selected edges to remove
							}
	
							if ((listCellsToRemove.isEmpty() && listEdgesToRemove.isEmpty())
									|| listCellsToRemove.size() == cellMap.size()) {
								blockerLoading.setMessage("Deleting Whole Flow");
								FlowController.remove((Flow)((CellValue) (cellMap.get(0)).getValue()).getFullItem());
								getTabbedPane().removeTabAt(0);
							}
							if (!listEdgesToRemove.isEmpty()) {
								for (mxCell edgeToRemove : listEdgesToRemove) {
									Object edgeValue = edgeToRemove.getValue();
									if (edgeValue instanceof FlowItemConnection) {
										blockerLoading.setMessage("Deleting Connection");
										FlowController.removeFlowItemConnection((FlowItemConnection)edgeValue);
									}
								}
								graph.removeCells(listEdgesToRemove.toArray());
							}
							if (!listCellsToRemove.isEmpty()) {
								int opt = JOptionPane.showConfirmDialog(null, "Do you want to delete all items not linked to another flows?", "Trash", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
								if (opt != JOptionPane.CANCEL_OPTION) {
									for (mxCell mxCell : listCellsToRemove) {
										blockerLoading.setMessage("Deleting Cell");
										FlowController.removeFlowItem((FlowItem)((CellValue)mxCell.getValue()).getFullItem(), opt == JOptionPane.YES_OPTION);
									}
									graph.removeCells(listCellsToRemove.toArray());
								}
							}
						} catch (Exception e) {
							showError(e);
						}
						blockerLoading.stop();
						return null;
					}
				}.execute();
			}
		});
	}
	
	private void showError(Exception e) {
		JOptionPane.showMessageDialog(FrameMain.this,
				e.getMessage(),
				"Error",
				JOptionPane.ERROR_MESSAGE);
	}

	private void validateClosingEvent() {
		int unsaved = 0;
		for (int i = 0; i < getTabbedPane().getTabCount(); i++) {
			Component comp = getTabbedPane().getComponentAt(i);
			if (comp != null && comp instanceof GraphPanel) {
				GraphPanel graphPanel = (GraphPanel) comp;
				if (Color.RED.equals(graphPanel.getBackground())) {
					unsaved++;
				}
			}
		}
		
		if (unsaved <= 0) {
			System.exit(0);
		}
		
		String works = unsaved + (unsaved > 1 ? " works" : " work");
		int option = JOptionPane.showConfirmDialog(null,
						"You have " + works + " unsaved.\nDo you really want to close the system?",
						"Closing System", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		if (option == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}
	
	private List<mxCell> getCellsToRemoveCount(List<mxCell> listNoBegin) {
		List<mxCell> listCells = new ArrayList<>();
		for (mxCell object : listNoBegin) {
			if (object.isVertex()) {
				listCells.add(object);
			}
		}
		return listCells;
	}
	
	private List<mxCell> getEdgesToRemoveCount(List<mxCell> listNoBegin) {
		List<mxCell> listEdges = new ArrayList<>();
		for (mxCell cell : listNoBegin) {
			if (cell.isEdge() && cell.isVisible()) {
				listEdges.add(cell);
			}
		}
		return listEdges;
	}
	
	private List<mxCell> removeBeginCell(Object[] objects) {
		List<mxCell> toRemoveList = new ArrayList<>();
		for (Object object : objects) {
			mxCell cell = (mxCell) object;
			if (!(cell.isVertex() && GraphType.CELL_BEGIN.equals(((CellValue) cell.getValue()).getGraphType()))) {
				toRemoveList.add(cell);
			}
		}
		return toRemoveList;
	}

	public void openCell(CellValue cell, Point selectedCellPos) {
		this.selectedCellPos = selectedCellPos;
		switch (cell.getGraphType()) {
			case CELL_BEGIN:
				openBegin(cell);
				break;
			case CELL_END:
				openEndFlow(cell);
				break;
			case CELL_FLOW:
				openFlow(cell);
				break;
			case CELL_LOGIC:
				openLogic(cell);
				break;
			case CELL_STEP:
				openStep(cell);//TODO:after open cant't import
				break;
		}
	}
	
	private void openBegin(final CellValue cell) {
		new FrameFlow(FrameMain.this, (Flow) cell.getFullItem()).setVisible(true);
	}

	private void openEndFlow(final CellValue cell) {
		if (cell.getFullItem() instanceof EndFlow) {
			(new FrameEnd(FrameMain.this, (EndFlow) cell.getFullItem())).setVisible(true);
		} else {
			(new FrameEnd(FrameMain.this, ((FlowItem) cell.getFullItem()).getEnd())).setVisible(true);
		}
	}
	
	private void openFlow(final CellValue cell) {
		if (!hasInternalFrameAlreadyOpen(cell.getName())) {
			if (cell.getFullItem() instanceof Flow) {
				callSavedFlowGraphComponent((Flow) cell.getFullItem());
			} else {
				callSavedFlowGraphComponent(((FlowItem) cell.getFullItem()).getFlow());
			}
		} else {
			JOptionPane.showMessageDialog(null, "This Flow is already open", "Flow Openning", JOptionPane.WARNING_MESSAGE);
		}
	}

	private void openLogic(final CellValue cell) {
		if (cell.getFullItem() instanceof Logic) {
			(new FrameLogic(FrameMain.this, (Logic) cell.getFullItem(), true)).setVisible(true);
		} else {
			(new FrameLogic(FrameMain.this, ((FlowItem) cell.getFullItem()).getLogic(), false)).setVisible(true);
		}
	}
    
	private void openStep(final CellValue cell) {
		if (cell.getFullItem() instanceof Step) {
			(new FrameStep(FrameMain.this, (Step) cell.getFullItem(), true)).setVisible(true);
		} else {
			(new FrameStep(FrameMain.this, ((FlowItem) cell.getFullItem()).getStep(), false)).setVisible(true);
		}
	}

	public void callSavedFlowGraphComponent(Flow flow) {
		addTab(flow.getNmFlow());
		mxGraphComponent graphComponent = getSelectedGraphComponent();
		if (graphComponent != null) {
			Graph graphToAdd = (Graph) graphComponent.getGraph();
			mxCell beginCell = graphToAdd.addCellBeginToFlow(flow);
			List<FlowItem> flowItems = flow.getFlowItems();
			for (FlowItem item : flowItems) {
				graphToAdd.addFlowItem(item);
			}
			for (FlowItem item : flowItems) {
				addConnections(graphToAdd, item.getPreviousFlowItems(), beginCell);
			}
			graphComponent.getParent().setBackground(Color.GREEN);
		}
	}

	private void addConnections(Graph graphToAdd, List<FlowItemConnection> connections, mxCell beginCell) {
		if (connections != null && !connections.isEmpty()) {
			for (FlowItemConnection connection : connections) {
				graphToAdd.addEdge(connection, beginCell);
			}
		}
	}

	private void addTab(String flowName) {
		getTabbedPane().add(getTabbedPane().getTabCount(), flowName, new GraphPanel(this, flowName));
	}
	
	public void updateTabTitle(String title) {
		getTabbedPane().setTitle(getTabbedPane().getSelectedIndex(), title);
	}
	
	public mxGraphComponent addDefaultCellToSelectedGraphComponent(CellValue cellValue, GraphType type) {
		if (cellValue == null) {
			cellValue = buildCellValue(0l, null, type, null);
		}
		mxGraphComponent graphComponent = getSelectedGraphComponent();
		if (graphComponent != null) {
			((Graph) graphComponent.getGraph()).addNewCell(cellValue);
		}
		return graphComponent;
	}

	public mxGraphComponent addSavedCellToSelectedGraphComponent(FlowItem item) {
		mxGraphComponent graphComponent = getSelectedGraphComponent();
		if (graphComponent != null) {
			((Graph) graphComponent.getGraph()).addFlowItem(item);
		}
		return graphComponent;
	}
	
	public void changeCellValueName(String name) {
		mxGraphComponent graphComponent = getSelectedGraphComponent();
		if (selectedCellPos == null || name == null || graphComponent == null) {
			return;
		}
		
		Object cell = graphComponent.getCellAt(selectedCellPos.x, selectedCellPos.y);
		if (cell != null) {
            ((CellValue) ((mxCell) cell).getValue()).setName(name);
            graphComponent.refresh();
		}
	}
	
	public GraphPanel getSelectedGraphPanel() {
		Component selectedComp = getTabbedPane().getSelectedComponent();
		if (selectedComp != null && selectedComp instanceof GraphPanel) {
			GraphPanel graphPanel = (GraphPanel) selectedComp;
			graphPanel.requestFocus();
			return graphPanel;
		}
		return null;
	}
	
	public mxGraphComponent getSelectedGraphComponent() {
		GraphPanel graphPanel = getSelectedGraphPanel();
		if (graphPanel != null) {
			return (mxGraphComponent) graphPanel.getComponent(0);
		}
		return null;
	}

	public void changeStyleOfSelectedGraphComponent(GraphGridStyleType style) {
		mxGraphComponent graphComponent = getSelectedGraphComponent();
		if (graphComponent != null) {
			graphComponent.setGridStyle(style.getStyle());
			graphComponent.repaint();
		}
	}

	private boolean hasInternalFrameAlreadyOpen(String name) {
		if (name == null) {
			return false;
		}
		
		for (int i = 0; i < getTabbedPane().getTabCount(); i++) {
			Component comp = getTabbedPane().getComponentAt(i);
			if (comp == null || !(comp instanceof GraphPanel)) {
				continue;
			}
			
			GraphPanel graphComp = (GraphPanel) comp;
			if (name.equals(graphComp.getFlowName())) {
				return true;
			}
		}
		return false;
	}

	private CellValue buildCellValue(Long id, String name, GraphType type, Object fullItem) {
		return new CellValue(id, (name != null && !name.isEmpty()) ? name : type.getLabel(), type, fullItem);
	}
	
	public void minimizeFrameMain() {
		setState(ICONIFIED);
	}

	public ClosableTabbedPane getTabbedPane() {
		return tabbedPane;
	}
}
