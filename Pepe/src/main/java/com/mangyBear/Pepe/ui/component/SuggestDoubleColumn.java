package com.mangyBear.Pepe.ui.component;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import com.mangyBear.Pepe.domain.types.frame.ListDragSource;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.ui.domain.CellValue;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class SuggestDoubleColumn implements Transferable {

	protected static DataFlavor lrItemFlavor = new DataFlavor(SuggestDoubleColumn.class, "Left Right Item");
	protected static DataFlavor[] supportedFlavors = { lrItemFlavor };

	private final CellValue leftText;
	private final String rightText;
	private final String searchFor;
	private ListDragSource dragSource;

	public SuggestDoubleColumn(CellValue strLeft, String searchFor) {
		this.leftText = strLeft;
		this.rightText = getRightText(strLeft.getGraphType());
		this.searchFor = searchFor;
	}

	@Override
	public String toString() {
		return getLeftText().getName();
	}

	public static DataFlavor getLrItemFlavor() {
		return lrItemFlavor;
	}

	public DataFlavor[] getTransferDataFlavors() {
		return supportedFlavors;
	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {
		if (flavor.equals(lrItemFlavor) || flavor.equals(DataFlavor.stringFlavor)) {
			return true;
		}
		return false;
	}

	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
		if (flavor.equals(lrItemFlavor)) {
			return this;
		} else if (flavor.equals(DataFlavor.stringFlavor)) {
			return this.toString();
		}

		throw new UnsupportedFlavorException(flavor);
	}

	private String getRightText(GraphType graphType) {
		String strRight = graphType != null ? graphType.getType() : "";
		return strRight != null ? strRight : "";
	}

	public CellValue getLeftText() {
		return leftText;
	}

	public String getRightText() {
		return rightText;
	}

	public String getSearchFor() {
		return searchFor;
	}
	
	public ListDragSource getDragSource() {
		return dragSource;
	}
	
	public void setDragSource(String name) {
		this.dragSource = ListDragSource.getListDragSourceByName(name);
	}
}
