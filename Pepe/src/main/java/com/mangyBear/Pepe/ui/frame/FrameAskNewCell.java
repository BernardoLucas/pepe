package com.mangyBear.Pepe.ui.frame;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameAskNewCell extends JDialog {

	private static final long serialVersionUID = 6824925852023704982L;
	
	private final BindingFactory bindingFactory;
    private final FrameMain frameMain;

    private JLabel message;
	private JLabel labelInformation;
    private JButton btnAskStep;
    private JButton btnAskLogic;
    private JButton btnAskEnd;

    public FrameAskNewCell(FrameMain frameMain) {
        super(frameMain, FramesHelpType.FRAME_ASK_NEW_CELL.getName(), true);
        this.bindingFactory = new BindingFactory();
        this.frameMain = frameMain;
        initGUI();
        createComponents();
        addComponents();
        addListeners();
    }

    private void initGUI() {
        setLayout(new RelativeLayout());
        setSize(new Dimension(290, 135));
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(frameMain);
    }

    private void createComponents() {
        message = new JLabel("Please, choose an option.");
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_ASK_NEW_CELL.getIcons().get(0));
        btnAskStep = new JButton(FramesHelpType.FRAME_ASK_NEW_CELL.getLabels().get(0));
        btnAskStep.setIcon(FramesHelpType.FRAME_ASK_NEW_CELL.getIcons().get(1));
        btnAskLogic = new JButton(FramesHelpType.FRAME_ASK_NEW_CELL.getLabels().get(1));
        btnAskLogic.setIcon(FramesHelpType.FRAME_ASK_NEW_CELL.getIcons().get(2));
        btnAskEnd = new JButton(FramesHelpType.FRAME_ASK_NEW_CELL.getLabels().get(2));
        btnAskEnd.setIcon(FramesHelpType.FRAME_ASK_NEW_CELL.getIcons().get(3));
    }

    private void addComponents() {
        add(message, messageBinding());
        add(labelInformation, labelInformationBinding());
        add(btnAskStep, btnAskStepBinding());
        add(btnAskLogic, btnAskLogicBinding());
        add(btnAskEnd, btnAskEndBinding());
    }

    private RelativeConstraints messageBinding() {
        Binding[] messageBinding = new Binding[2];
        messageBinding[0] = new Binding(Edge.HORIZONTAL_CENTER, 0, Direction.RIGHT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
        messageBinding[1] = new Binding(Edge.TOP, 15, Direction.BELOW, Edge.TOP, Binding.PARENT);
        return new RelativeConstraints(messageBinding);
    }

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

    private RelativeConstraints btnAskStepBinding() {
        Binding[] btnAskStepBinding = new Binding[2];
        btnAskStepBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnAskLogic);
        btnAskStepBinding[1] = new Binding(Edge.RIGHT, 5, Direction.LEFT, Edge.LEFT, btnAskLogic);
        return new RelativeConstraints(btnAskStepBinding);
    }
    
    private RelativeConstraints btnAskLogicBinding() {
    	Binding[] btnAskLogicBinding = new Binding[2];
    	btnAskLogicBinding[0] = new Binding(Edge.TOP, 10, Direction.BELOW, Edge.BOTTOM, message);
    	btnAskLogicBinding[1] = new Binding(Edge.HORIZONTAL_CENTER, 1, Direction.RIGHT, Edge.HORIZONTAL_CENTER, message);
    	return new RelativeConstraints(btnAskLogicBinding);
    }

    private RelativeConstraints btnAskEndBinding() {
        Binding[] btnAskEndBinding = new Binding[2];
        btnAskEndBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnAskLogic);                           
        btnAskEndBinding[1] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.RIGHT, btnAskLogic);
        return new RelativeConstraints(btnAskEndBinding);
    }

    private void addListeners() {
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameAskNewCell.this, FramesHelpType.FRAME_ASK_NEW_CELL)).setVisible(true);
			}
		});
        btnAskStep.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                (new FrameStep(frameMain, null, false)).setVisible(true);
                dispose();
            }
        });
        btnAskLogic.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                (new FrameLogic(frameMain, null, false)).setVisible(true);
                dispose();
            }
        });
        btnAskEnd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	(new FrameEnd(frameMain, null)).setVisible(true);
                dispose();
            }
        });
    }
}
