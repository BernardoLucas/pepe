package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.mangyBear.Pepe.domain.types.frame.ListDragSource;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mangyBear.Pepe.ui.graph.Graph;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class GraphPanel extends JPanel {

	private static final long serialVersionUID = 1178555521429917021L;

	private final Graph graph;
	private final FrameMain frameMain;
	private final String flowName;

	private int changeCount;
	private Color previousC1;
	private Color previousC2;

	private DropTarget oldDropTarget;

	public GraphPanel(FrameMain frameMain, String flowName) {
		this.graph = new Graph();
		this.frameMain = frameMain;
		this.flowName = flowName;
		initGUI();
		buildBasePanel();
		addListeners();
	}

	private void initGUI() {
		setLayout(new RelativeLayout());
	}

	private void buildBasePanel() {
		setBackground(Color.GREEN);
		add(graph.getGraphComponent(), graphComponentBinding());
	}

	private RelativeConstraints graphComponentBinding() {
		Binding[] graphComponentBinding = new Binding[4];
		graphComponentBinding[0] = new Binding(Edge.TOP, 2, Direction.BELOW, Edge.TOP, Binding.PARENT);
		graphComponentBinding[2] = new Binding(Edge.LEFT, 2, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		graphComponentBinding[3] = new Binding(Edge.RIGHT, 2, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		graphComponentBinding[1] = new Binding(Edge.BOTTOM, 2, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(graphComponentBinding);
	}

	private void addListeners() {
		graph.addListener(mxEvent.CELLS_ADDED, new ChangeCellsEvent2(graph, this));
		// graph.addListener(mxEvent.CELLS_REMOVED, new ChangeCellsEvent2(graph, this));
		graph.addListener(mxEvent.CELLS_MOVED, new ChangeCellsEvent2(graph, this));
		graph.addListener(mxEvent.CELLS_RESIZED, new ChangeCellsEvent2(graph, this));
		graph.getGraphComponent().getGraphControl().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				graph.setLastClickX(e.getX());
				graph.setLastClickY(e.getY());
				Object cell = frameMain.getSelectedGraphComponent().getCellAt(e.getX(), e.getY());
				if (cell != null) {
					mxCell validCell = (mxCell) cell;
					if (e.getClickCount() == 2) {
						if (validCell.getValue() instanceof CellValue) {
							frameMain.openCell((CellValue) validCell.getValue(), new Point(e.getX(), e.getY()));
						}
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				removeDropTargetToSelectedTab();
			}
		});
	}

	public void addDropTargetToSelectedTab() {
		oldDropTarget = oldDropTarget == null ? graph.getGraphComponent().getDropTarget() : oldDropTarget;
		graph.getGraphComponent().setDropTarget(new DropTarget() {

			private static final long serialVersionUID = -1722255433513223659L;

			@Override
			public synchronized void drop(DropTargetDropEvent dtde) {
				try {
					this.changeToNormal();
					//Save drop position
					graph.setLastClickX(dtde.getLocation().x);
					graph.setLastClickY(dtde.getLocation().y);
					// Open FrameAsk for all items being dragged
					for (CellValue cellValue : MyConstants.getCellValuesToDrop(dtde)) {
						dropAsk(cellValue, MyConstants.getDragSource(dtde));
					}
				} catch (UnsupportedFlavorException | IOException e) {
					showError(e);
				}
			}

			@Override
			public synchronized void dragEnter(DropTargetDragEvent dtde) {
			}

			@Override
			public synchronized void dragExit(DropTargetEvent dtde) {
				this.changeToNormal();
			}

			private void changeToNormal() {
				// Set cursor to default.
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		});
	}

	public void removeDropTargetToSelectedTab() {
		if (oldDropTarget == null) {
			return;
		}
		graph.getGraphComponent().setDropTarget(oldDropTarget);
		oldDropTarget = null;
	}

	private void dropAsk(CellValue cellValue, ListDragSource dragSource) {
		if (ListDragSource.FAVORITES.equals(dragSource)) {
			(new FrameAskFavorites(frameMain, cellValue)).setVisible(true);
		}
		if (ListDragSource.SEARCH.equals(dragSource)) {
			(new FrameAskSearch(frameMain, cellValue)).setVisible(true);
		}
	}

	public String getFlowName() {
		return flowName;
	}

	public int getChangeCount() {
		return changeCount;
	}

	public void changeCount() {
		this.changeCount++;
	}

	public Color getPreviousC1() {
		return previousC1;
	}

	public void setPreviousC1(Color previousC1) {
		this.previousC1 = previousC1;
	}

	public Color getPreviousC2() {
		return previousC2;
	}

	public void setPreviousC2(Color previousC2) {
		this.previousC2 = previousC2;
	}
	
	private void showError(Exception e) {
		JOptionPane.showMessageDialog(frameMain,
				e.getMessage(),
				"Error",
				JOptionPane.ERROR_MESSAGE);
	}
}

class ChangeCellsEvent2 implements mxEventSource.mxIEventListener {

	private final GraphPanel graphPanel;
	private final Graph graph;

	public ChangeCellsEvent2(Graph graph, GraphPanel graphPanel) {
		this.graph = graph;
		this.graphPanel = graphPanel;
	}

	@Override
	public void invoke(Object o, mxEventObject eo) {
		Color pc1 = graphPanel.getPreviousC1();
		graphPanel.setPreviousC2(pc1 != null ? pc1 : Color.GREEN);
		graphPanel.setPreviousC1(graphPanel.getBackground());

		if (graph.isSelfEdition()) {
			graphPanel.setBackground(graphPanel.getPreviousC2());
			graph.setSelfEdition(false);
		} else {
			graphPanel.setBackground(Color.RED);
		}
	}
}
