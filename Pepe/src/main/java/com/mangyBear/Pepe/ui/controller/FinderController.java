package com.mangyBear.Pepe.ui.controller;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.entity.FinderParameter;
import com.mangyBear.Pepe.domain.types.action.ByType;
import com.mangyBear.Pepe.domain.types.action.FinderType;
import com.mangyBear.Pepe.service.FinderService;
import com.mangyBear.Pepe.service.MyConstants;

public class FinderController {

	private FinderService service = new FinderService();
	
	public void update(Finder finder) throws Exception {
		service.update(finder);
	}
	
	public void remove(Finder finder) throws Exception {
		service.remove(finder);
	}
	
	public Finder updateModelParams(Finder finder, DefaultTableModel model) {
		boolean isPosition = finder.getFinderType().equals(FinderType.POSITION);
		
		List<FinderParameter> finderParamList = finder.getParameters();
		if (isPosition) {
			updateScreenSize(finderParamList);
		}
		for (int i = 0; i < model.getRowCount(); i++) {
			FinderParameter finderParameter = finderParamList.get(isPosition ? (i + 2) : i);
			finderParameter.setName(MyConstants.setAsString(model.getValueAt(i, 0)));
			finderParameter.setValue(MyConstants.setAsString(model.getValueAt(i, 1)));
		}
		
		return finder;
	}
	
	private void updateScreenSize(List<FinderParameter> finderParamList) {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		FinderParameter finderParameter = finderParamList.get(0);
		finderParameter.setName("Width");
		finderParameter.setValue(String.valueOf(dimension.getWidth()));
		
		finderParameter = finderParamList.get(1);
		finderParameter.setName("Height");
		finderParameter.setValue(String.valueOf(dimension.getHeight()));
	}

	public Finder buildFinder(String name, FinderType type, DefaultTableModel model) {
		Finder finder = new Finder();

		finder.setNmFinder(name);
		finder.setFinderType(type);

		if (FinderType.POSITION.equals(type)) {
			finder.setParameters(setScreenDimensionForPosition(finder));
		} else {
			finder.setParameters(new ArrayList<FinderParameter>());
		}
		
		for (int i = 0; i < model.getRowCount(); i++) {
			FinderParameter finderParameter = new FinderParameter();
			finderParameter.setName(MyConstants.setAsString(model.getValueAt(i, 0)));
			finderParameter.setValue(MyConstants.setAsString(model.getValueAt(i, 1)));
			finderParameter.setFinder(finder);
			finder.getParameters().add(finderParameter);
		}
		
		return finder;
	}
	
	private List<FinderParameter> setScreenDimensionForPosition(Finder finder) {
		List<FinderParameter> screenDimension = new ArrayList<>();
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		for (String name : new String[] { "Width", "Height" }) {
			FinderParameter finderParameter = new FinderParameter();
			finderParameter.setName(name);
			finderParameter.setValue(String.valueOf(name.equals("Width") ? dimension.getWidth() : dimension.getHeight()));
			finderParameter.setFinder(finder);
			screenDimension.add(finderParameter);
		}
		return screenDimension;
	}

	public static Finder buildFinderToWebRecord(String string, ByType byType, Object webDriver) {
		Finder finder = new Finder();

		finder.setNmFinder(string);
		finder.setFinderType(FinderType.WEB_ELEMENT);
		finder.setParameters(new ArrayList<FinderParameter>());

		//Build list of labels
		List<String> finderTypeLabels = FinderType.WEB_ELEMENT.getFrameAddType().getLabels();
		//Build array of parameters
		String[] params = new String[] { webDriver.toString(), byType.toString(), string, "0" };
		for (int i = 0; i < finderTypeLabels.size(); i++) {
			FinderParameter finderParameter = new FinderParameter();
			finderParameter.setName(finderTypeLabels.get(i));
			finderParameter.setValue(params[i]);
			finderParameter.setFinder(finder);
			finder.getParameters().add(finderParameter);
		}
		
		return finder;
	}

	public Finder findByName(String name) throws Exception {
		return service.findByName(name);
	}

	public List<Finder> findLikeName(String name) throws Exception {
		return service.findLikeName(name);
	}
}
