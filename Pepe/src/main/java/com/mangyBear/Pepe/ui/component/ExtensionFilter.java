package com.mangyBear.Pepe.ui.component;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ExtensionFilter extends FileFilter {
	
	private final String[] acceptedExtensions;
	
	public ExtensionFilter(String[] acceptedExtensions) {
		this.acceptedExtensions = acceptedExtensions;
	}

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        String fName = f.getName();
        if (acceptedExtensions != null) {
            for (String extension : acceptedExtensions) {
                if (fName.endsWith(extension)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "Image File Filter";
    }
}
