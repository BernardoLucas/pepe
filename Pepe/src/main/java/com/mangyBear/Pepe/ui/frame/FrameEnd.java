package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.SwingWorker;
import javax.swing.plaf.basic.BasicButtonUI;

import com.mangyBear.Pepe.domain.entity.EndFlow;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.controller.EndFlowController;
import com.mangyBear.Pepe.ui.domain.CellValue;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

public class FrameEnd extends BaseFrame {

	private static final long serialVersionUID = 9096353697741838152L;
	
	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
	private final FrameMain frameMain;
    private EndFlow endFlow;
    private boolean isUpdating;

	private JLabel labelAsk;
	private JToggleButton btnDocx;
	private JToggleButton btnLog;
	private JLabel labelInformation;
	private JButton btnAdd;

	public FrameEnd(FrameMain frameMain, EndFlow endFlow) {
		super(frameMain, FramesHelpType.FRAME_END.getName(), true);
		this.blockerLoading = new BlockerLoading(this, null, true);
		this.bindingFactory = new BindingFactory();
		this.frameMain = frameMain;
        this.endFlow = endFlow;
        this.isUpdating = endFlow != null;
		initGUI();
		createComponents();
		addComponents();
		addListeners();
		if (isUpdating) {
			btnDocx.setSelected(endFlow.getExportDocx());
			btnLog.setSelected(endFlow.getExportLog());
		}
	}

	private void initGUI() {
		setLayout(new RelativeLayout());
		setSize(new Dimension(220, 140));
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(frameMain);
	}

	private void createComponents() {
		labelAsk = new JLabel("Export as:");
		btnDocx = new JToggleButton(FramesHelpType.FRAME_END.getLabels().get(0));
		btnDocx.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.setColor(btnDocx.isSelected() ? Color.decode("#32ff32") : Color.decode("#ff3232"));
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
				super.paint(g, c);
			}
		});
		btnLog = new JToggleButton(FramesHelpType.FRAME_END.getLabels().get(1));
		btnLog.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.setColor(btnLog.isSelected() ? Color.decode("#32ff32") : Color.decode("#ff3232"));
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
				super.paint(g, c);
			}
		});
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_SCHEDULER.getIcons().get(0));
		btnAdd = new JButton(isUpdating ? "Update" : "Add");
	}

	private void addComponents() {
		add(blockerLoading.createLoading(20, 5), loadingBinding());

		add(labelAsk, labelAskBinding());
		add(btnDocx, btnDocxBinding());
		add(btnLog, btnLogBinding());
		add(labelInformation, labelInformationBinding());
		add(btnAdd, btnAddBinding());
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private RelativeConstraints labelAskBinding() {
		Binding[] labelAskBinding = new Binding[2];
		labelAskBinding[0] = new Binding(Edge.TOP, 15, Direction.BELOW, Edge.TOP, Binding.PARENT);
		labelAskBinding[1] = bindingFactory.horizontallyCenterAlignedWith(Binding.PARENT);
		return new RelativeConstraints(labelAskBinding);
	}
	
	private RelativeConstraints btnDocxBinding() {
		Binding[] btnDocxBinding = new Binding[2];
		btnDocxBinding[0] = new Binding(Edge.TOP, 15, Direction.BELOW, Edge.BOTTOM, labelAsk);
		btnDocxBinding[1] = new Binding(Edge.RIGHT, 5, Direction.LEFT, Edge.HORIZONTAL_CENTER, labelAsk);
		return new RelativeConstraints(btnDocxBinding);
	}

	private RelativeConstraints btnLogBinding() {
		Binding[] btnLogBinding = new Binding[2];
		btnLogBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnDocx);
		btnLogBinding[1] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.HORIZONTAL_CENTER, labelAsk);
		return new RelativeConstraints(btnLogBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private RelativeConstraints btnAddBinding() {
		Binding[] btnAddBinding = new Binding[2];
		btnAddBinding[0] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		btnAddBinding[1] = new Binding(Edge.HORIZONTAL_CENTER, 0, Direction.LEFT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
		return new RelativeConstraints(btnAddBinding);
	}

	private void addListeners() {
		btnAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				saveEnd();
			}
		});
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameEnd.this, FramesHelpType.FRAME_END)).setVisible(true);
			}
		});
	}

	private void saveEnd() {
		blockerLoading.start((isUpdating ? "Updating" : "Saving") + " End");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				try {
					if (!isUpdating) {
						endFlow = new EndFlow();
					}
					
					endFlow.setExportDocx(btnDocx.isSelected());
					endFlow.setExportLog(btnLog.isSelected());
					
						endFlow = (new EndFlowController()).save(endFlow);
					
					if (!isUpdating) {
						frameMain.addDefaultCellToSelectedGraphComponent(new CellValue(endFlow.getId(), GraphType.CELL_END.getLabel(), GraphType.CELL_END, endFlow), GraphType.CELL_END);
					}
				} catch (Exception e) {
					showError(e);
				}
				
				blockerLoading.stop();
				dispose();
				return null;
			}
		}.execute();
	}
}
