package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.controller.FlowController;
import com.mangyBear.Pepe.ui.controller.LogicController;
import com.mangyBear.Pepe.ui.controller.StepController;
import com.mangyBear.Pepe.ui.domain.CellValue;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameAskSearch extends BaseFrame {

	private static final long serialVersionUID = -5451902758311339425L;
	
	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
	private final FrameMain frameMain;
	private final CellValue cellValue;
	
	private boolean showNewOptions = true;

	private JLabel message;
	private JLabel labelInformation;
	private JButton btnAskImport;
	private JButton btnAskClone;
	// only appears if cloneFlow is pressed
	private JCheckBox chkAskLogic;
	private JCheckBox chkAskStep;
	private JButton btnGo;

	public FrameAskSearch(FrameMain frameMain, CellValue cellValue) {
		super(frameMain, FramesHelpType.FRAME_ASK_SEARCH.getName(), true);
		this.blockerLoading = new BlockerLoading(this, null, true);
		this.bindingFactory = new BindingFactory();
		this.frameMain = frameMain;
		this.cellValue = cellValue;
		initGUI(frameMain);
		createComponents();
		addComponents();
		addListeners();
	}

	private void initGUI(JFrame parent) {
		setLayout(new RelativeLayout());
		setSize(new Dimension(237, 135));
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(parent);
	}

	private void createComponents() {
		message = new JLabel("Please, choose an option.");
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_ASK_SEARCH.getIcons().get(0));
		btnAskImport = new JButton(FramesHelpType.FRAME_ASK_SEARCH.getLabels().get(0));
		btnAskImport.setIcon(FramesHelpType.FRAME_ASK_SEARCH.getIcons().get(1));
		btnAskClone = new JButton(FramesHelpType.FRAME_ASK_SEARCH.getLabels().get(1));
		btnAskClone.setIcon(FramesHelpType.FRAME_ASK_SEARCH.getIcons().get(2));

		chkAskLogic = new JCheckBox(FramesHelpType.FRAME_ASK_SEARCH.getLabels().get(2));
		chkAskLogic.setVisible(false);
		chkAskStep = new JCheckBox(FramesHelpType.FRAME_ASK_SEARCH.getLabels().get(3));
		chkAskStep.setVisible(false);
		btnGo = new JButton("GO") {
			
			private static final long serialVersionUID = -2937450557757927275L;

			@Override
			public void paintComponent(Graphics g) {
				g.setColor(Color.GREEN);
				g.fillRect(0, 0, getSize().width, getSize().height);
				super.paintComponent(g);
			}
		};
		btnGo.setContentAreaFilled(false);
		btnGo.setFont(new Font(btnGo.getFont().getFontName(), Font.BOLD, 12));
		btnGo.setVisible(false);
	}

	private void addComponents() {
		add(blockerLoading.createLoading(15, 5), loadingBinding());

		add(message, messageBinding());
		add(labelInformation, labelInformationBinding());
		add(btnAskImport, btnAskImportBinding());
		add(btnAskClone, btnAskCloneBinding());

		add(chkAskLogic, chkAskLogicBinding());
		add(chkAskStep, chkAskStepBinding());
		add(btnGo, btnGoBinding());
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private RelativeConstraints messageBinding() {
		Binding[] messageBinding = new Binding[2];
		messageBinding[0] = new Binding(Edge.HORIZONTAL_CENTER, 0, Direction.RIGHT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
		messageBinding[1] = new Binding(Edge.TOP, 15, Direction.BELOW, Edge.TOP, Binding.PARENT);
		return new RelativeConstraints(messageBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}
	
	private RelativeConstraints btnAskImportBinding() {
		Binding[] btnAskImportBinding = new Binding[2];
		btnAskImportBinding[0] = new Binding(Edge.TOP, 10, Direction.BELOW, Edge.BOTTOM, message);
		btnAskImportBinding[1] = new Binding(Edge.RIGHT, 2, Direction.LEFT, Edge.HORIZONTAL_CENTER, message);
		return new RelativeConstraints(btnAskImportBinding);
	}

	private RelativeConstraints btnAskCloneBinding() {
		Binding[] btnAskCloneBinding = new Binding[2];
		btnAskCloneBinding[0] = new Binding(Edge.TOP, 10, Direction.BELOW, Edge.BOTTOM, message);
		btnAskCloneBinding[1] = new Binding(Edge.LEFT, 2, Direction.RIGHT, Edge.HORIZONTAL_CENTER, message);
		return new RelativeConstraints(btnAskCloneBinding);
	}

	private RelativeConstraints chkAskLogicBinding() {
		Binding[] chkAskLogicBinding = new Binding[2];
		chkAskLogicBinding[0] = new Binding(Edge.TOP, 10, Direction.BELOW, Edge.BOTTOM, btnAskClone);
		chkAskLogicBinding[1] = new Binding(Edge.RIGHT, 5, Direction.LEFT, Edge.HORIZONTAL_CENTER, message);
		return new RelativeConstraints(chkAskLogicBinding);
	}

	private RelativeConstraints chkAskStepBinding() {
		Binding[] chkAskStepBinding = new Binding[2];
		chkAskStepBinding[0] = bindingFactory.verticallyCenterAlignedWith(chkAskLogic);
		chkAskStepBinding[1] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.HORIZONTAL_CENTER, message);
		return new RelativeConstraints(chkAskStepBinding);
	}

	private RelativeConstraints btnGoBinding() {
		Binding[] btnGoBinding = new Binding[2];
		btnGoBinding[0] = bindingFactory.rightEdge();
		btnGoBinding[1] = bindingFactory.bottomEdge();
		return new RelativeConstraints(btnGoBinding);
	}

	private void addListeners() {
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameAskSearch.this, FramesHelpType.FRAME_ASK_SEARCH)).setVisible(true);
			}
		});
		btnAskImport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (frameMain.getSelectedGraphComponent() == null) {
					JOptionPane.showMessageDialog(FrameAskSearch.this,
							"There's no open flow to import the selected item to.", "Import Alert",
							JOptionPane.WARNING_MESSAGE);
					return;
				}
				frameMain.addDefaultCellToSelectedGraphComponent(cellValue, null);
				dispose();
			}
		});
		btnAskClone.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (!GraphType.CELL_FLOW.equals(cellValue.getGraphType()) && frameMain.getSelectedGraphComponent() == null) {
					JOptionPane.showMessageDialog(FrameAskSearch.this,
							"There's no open flow to clone the selected item to.", "Clone Alert",
							JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				cloneCell(cellValue);
			}
		});
		btnGo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cloneFlow(cellValue);
			}
		});
	}

	private void cloneCell(CellValue cell) {
		GraphType type = cell.getGraphType();
		if (GraphType.CELL_FLOW.equals(type)) {
			setNewOptionsVisible(cell);
			showNewOptions = !showNewOptions;
		}
		if (GraphType.CELL_LOGIC.equals(type)) {
			cloneLogic(cell);
		}
		if (GraphType.CELL_STEP.equals(type)) {
			cloneStep(cell);
		}
	}
	
	private void setNewOptionsVisible(final CellValue cell) {
		setSize(getSize().width, showNewOptions ? 170 : 135);
		chkAskLogic.setVisible(showNewOptions);
		chkAskStep.setVisible(showNewOptions);
		btnGo.setVisible(showNewOptions);
	}

	private void cloneFlow(final CellValue cell) {
		blockerLoading.start("Cloning Flow");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				MyConstants.easterEggDarthVader(frameMain);
				
				try {
					Flow clone = (new FlowController()).clone(cell.getId(), chkAskLogic.isSelected(), chkAskStep.isSelected());
					CellValue cloneCell = createCell(clone.getId(), GraphType.CELL_BEGIN.getLabel(), GraphType.CELL_BEGIN, clone, false);
					frameMain.openCell(cloneCell, null);
				} catch (Exception e) {
					showError(e);
				}

				blockerLoading.stop();
				JOptionPane.showMessageDialog(FrameAskSearch.this,
						"Every clone has its name and its objects names changed, please check.", "Flow Alert",
						JOptionPane.INFORMATION_MESSAGE);
				dispose();
				return null;
			}
		}.execute();
	}

	private void cloneLogic(final CellValue cell) {
		blockerLoading.start("Cloning Logic");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				MyConstants.easterEggDarthVader(frameMain);
				
				try {
					Logic clone = (new LogicController()).clone(cell.getId());
					createCell(clone.getId(), clone.getNmLogic(), GraphType.CELL_LOGIC, clone, true);
				} catch (Exception e) {
					showError(e);
				}

				blockerLoading.stop();
				JOptionPane.showMessageDialog(FrameAskSearch.this,
						"Every clone has its name and its objects names changed, please check.", "Logic Alert",
						JOptionPane.INFORMATION_MESSAGE);
				dispose();
				return null;
			}
		}.execute();
	}

	private void cloneStep(final CellValue cell) {
		blockerLoading.start("Cloning Step");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				MyConstants.easterEggDarthVader(frameMain);

				try {
					Step clone = (new StepController()).clone(cell.getId());
					createCell(clone.getId(), clone.getNmStep(), GraphType.CELL_STEP, clone, true);
				} catch (Exception e) {
					showError(e);
				}

				blockerLoading.stop();
				JOptionPane.showMessageDialog(FrameAskSearch.this,
						"Every clone has its name and its objects names changed, please check.", "Step Alert",
						JOptionPane.INFORMATION_MESSAGE);
				dispose();
				return null;
			}
		}.execute();
	}

	private CellValue createCell(Long id, String name, GraphType type, Object fullItem, boolean addCell) {
		CellValue cell = new CellValue(id, name, type, fullItem);
		if (addCell) {
			frameMain.addDefaultCellToSelectedGraphComponent(cell, type);
		}

		return cell;
	}
}
