package com.mangyBear.Pepe.ui.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.table.DefaultTableModel;

import com.mangyBear.Pepe.domain.entity.EndFlow;
import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.FlowItem;
import com.mangyBear.Pepe.domain.entity.FlowItemConnection;
import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.domain.entity.VarObject;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.FlowItemConnectionService;
import com.mangyBear.Pepe.service.FlowItemService;
import com.mangyBear.Pepe.service.FlowService;
import com.mangyBear.Pepe.service.VarObjectService;
import com.mangyBear.Pepe.service.VariableService;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;

public class FlowController {

	private static FlowService service = new FlowService();
	private static FlowItemService flowItemService = new FlowItemService();
	private static FlowItemConnectionService flowItemConnectionService = new FlowItemConnectionService();
	
	public Flow clone(Long id, boolean cloneLogics, boolean cloneSteps) throws Exception {
		Flow flow = service.findById(id);
		Flow clone = flow.clone(cloneLogics, cloneSteps);
		
		service.store(clone);
		return clone;
	}
	
	public void update(Flow flow, DefaultTableModel fileTableModel, DefaultTableModel objectTableModel, DefaultTableModel sqlTableModel, DefaultTableModel webDriverTableModel)  throws Exception {
		service.update(flow);
		
		saveVariable(fileTableModel);
		saveVariable(objectTableModel);
		saveVariable(sqlTableModel);
		saveVariable(webDriverTableModel);
	}
	
	private void saveVariable(DefaultTableModel variablesModel) throws Exception {
		VariableService variableService = new VariableService();
		for (int i = 0; i < variablesModel.getRowCount(); i++) {
			Variable variable = (Variable) variablesModel.getValueAt(i, 0);
			//Save/Update VariableGroup
			saveVarGroup(variable.getVarObject());
			//Save/Update Variable
			if (variable.getId() == null) {
				variableService.store(variable);
			} else {
				variableService.update(variable);
			}
		}
	}
	
	private void saveVarGroup(VarObject group) throws Exception {
		VarObjectService groupService = new VarObjectService();
		if (group.getId() == null) {
			groupService.store(group);
		} else {
			groupService.update(group);
		}
	}
	
	public Long store(Flow flow)  throws Exception  {
		service.store(flow);
		return flow.getId();
	}

	public static Flow save(String name, CellValue cell, Double x)  throws Exception  {
		GraphType type = cell.getGraphType();
		Flow flow = new Flow();
		flow.setNmFlow(name);
		flow.setHeight(type.getHeight());
		flow.setWidth(type.getWidth());
		flow.setX(x);
		flow.setY(30.0);
		service.store(flow);
		return flow;
	}

	public static void storeFlowAndItems(Map<Integer, mxCell> cellMap, Map<Integer, mxCell> edgeMap) throws Exception {
		storeCells(cellMap);
		storeEdges(edgeMap);
	}
	
	public static void execute(CellValue beginCell)  throws Exception  {
		service.execute(beginCell);
	}

	private static void storeCells(Map<Integer, mxCell> cellMap) throws Exception {
		mxCell beginCell = cellMap.get(0);

		Flow flowDad = service.findById(((CellValue) beginCell.getValue()).getId());
		updateFlowBounds(flowDad, beginCell);
		
		if (flowDad.getFlowItems() == null) {
			flowDad.setFlowItems(new ArrayList<FlowItem>());
		}
		for (int i = 1; i < cellMap.size(); i++) {
			mxCell cellInfo = cellMap.get(i);
			CellValue cellValue = (CellValue) cellInfo.getValue();
			
			FlowItem item;
			if (cellValue.getFullItem() != null && cellValue.getFullItem() instanceof FlowItem) {
				item = (FlowItem) cellValue.getFullItem();
			} else {
				item = new FlowItem();
				item.setFlowDad(flowDad);
				
				switch (cellValue.getGraphType()) {
					case CELL_END:
						item.setEnd((EndFlow) cellValue.getFullItem());
						break;
					case CELL_FLOW:
						item.setFlow((Flow) cellValue.getFullItem());
						break;
					case CELL_LOGIC:
						item.setLogic((Logic) cellValue.getFullItem());
						break;
					case CELL_STEP:
						item.setStep((Step) cellValue.getFullItem());
					default:
				}
			}
			
			item.setHeight(cellInfo.getGeometry().getHeight());
			item.setWidth(cellInfo.getGeometry().getWidth());
			item.setX(cellInfo.getGeometry().getX());
			item.setY(cellInfo.getGeometry().getY());
//			flowDad.getFlowItems().add(item);
			if (item.getId() == null) {
				flowItemService.store(item);
			} else {
				flowItemService.update(item);
			}
			cellValue.setFullItem(item);
		}
//		service.update(flowDad);
	}
	
	private static void updateFlowBounds(Flow flow, mxCell beginCell) throws Exception {
		mxGeometry geometry = beginCell.getGeometry();
		flow.setNmFlow(flow.getNmFlow());
		flow.setHeight(geometry.getHeight());
		flow.setWidth(geometry.getWidth());
		flow.setX(geometry.getX());
		flow.setY(geometry.getY());
		service.update(flow);
	}
	
	private static void storeEdges(Map<Integer, mxCell> edgeMap) throws Exception {
		for(Entry<Integer, mxCell> entry : edgeMap.entrySet()) {
			FlowItemConnection connection = new FlowItemConnection();
			mxCell edgeInfo = entry.getValue();
			mxCell source = (mxCell) edgeInfo.getSource();
			mxCell target = (mxCell) edgeInfo.getTarget();
			if (source == null || target == null) {
				continue;
			}
			
			FlowItem previousItem = (((CellValue) source.getValue()).getFullItem() instanceof FlowItem) ? (FlowItem) ((CellValue) source.getValue()).getFullItem() : null;
			FlowItem currentItem = (FlowItem) ((CellValue) target.getValue()).getFullItem();
			if (previousItem != null && flowItemConnectionService.existsConnection(previousItem.getId(), currentItem.getId())) {
				continue;
			}
			connection.setCondition(edgeInfo.getValue().toString());
			connection.setPreviousItem(previousItem);
			connection.setCurrentItem(currentItem);
			flowItemConnectionService.store(connection);
		}
	}
	
	public static void remove(Flow flow) throws Exception {
		service.remove(flow);
	}
	
	public static void removeFlowItem(FlowItem flowItem, Boolean removeAll) throws Exception {
		flowItemService.remove(flowItem, removeAll);
	}
	
	public static void removeFlowItemConnection(FlowItemConnection connection) throws Exception {
		flowItemConnectionService.remove(connection);
	}
	
	public static Flow findById(Long id) throws Exception {
		return service.findById(id);
	}
	
	public Flow findByName(String name) throws Exception {
		return service.findByName(name);
	}

	public List<Flow> findLikeName(String name) throws Exception {
		return service.findLikeName(name);
	}

	public List<Flow> findLikeNameLimited(String name, int limit) throws Exception {
		return service.findLikeNameLimited(name, limit);
	}
}
