package com.mangyBear.Pepe.ui.component.schedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import javax.swing.JOptionPane;

import com.mangyBear.Pepe.domain.entity.Schedule;
import com.mangyBear.Pepe.ui.controller.ScheduleController;

public class SchedulerFlow {

	private final Timer timer = new Timer();
	
	public void schedule() {
		try {
			for (Schedule schedule : getDaySchedules()) {
				Date scheduleDate = getScheduleDate(schedule);
				if (scheduleDate == null) {
					continue;
				}
				
				timer.schedule(new ScheduledFlowExecutor(timer, schedule.getFlow()), scheduleDate);
			}
		} catch (Exception e) {
			showError(e);
		}
	}
	
	private List<Schedule> getDaySchedules() {
		try {
			return (new ScheduleController()).findActiveMacSchedulesByDay(new SimpleDateFormat("EEE").format(new Date()).toLowerCase());
		} catch (Exception e) {
			showError(e);
		}
		
		return Arrays.asList();
	}
	
	private Date getScheduleDate(Schedule schedule) throws ParseException {
		Date scheduleDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " " + schedule.getExec_time());
		return new Date().before(scheduleDate) ? scheduleDate : null;
	}
	
	private void showError(Exception e) {
		JOptionPane.showMessageDialog(null,
				e.getMessage(),
				"Error",
				JOptionPane.ERROR_MESSAGE);
	}
}
