package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.StringUtils;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.domain.entity.ActionParameter;
import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.types.action.ActionGroupType;
import com.mangyBear.Pepe.domain.types.action.ActionType;
import com.mangyBear.Pepe.domain.types.action.ByType;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.component.ComponentsTable;
import com.mangyBear.Pepe.ui.component.SuggestCombo;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;
import com.mangyBear.Pepe.ui.controller.StepController;
import com.mangyBear.Pepe.ui.controller.VariableController;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mangyBear.Pepe.ui.listener.entity.WebRecord;
import com.mangyBear.Pepe.ui.listener.web.RecorderManager;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

public class FrameRecorder extends JFrame {

	private static final long serialVersionUID = 5290078849066213857L;

	private final FrameMain frameMain;
	private final RecorderManager recorderManager;
	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
	private final int VERTICAL_SPACING = 10;
	private final int TABLE_VERTICAL_SPACING = 2;
	
	// Top components
	private JLabel webDriverLabel;
	private SuggestCombo webDriverSuggest;
	private JLabel startUrlLabel;
	private JTextField openUrlTxt;
	// Bottom components
	private JLabel labelInformation;
	private JButton btnImport;
	// Actions tool bar
	private JToolBar toolBar;
	private JToggleButton btnRecord;
	private JToggleButton btnOpenBrowser;
	// Tab pane (Table, Finders and Steps)
	private JTabbedPane tabbedPane;
	// Action panel components
	private JPanel actionPanel;
	private JLabel labelActionFinder;
	private JComboBox<Finder> comboActionFinder;
	private JLabel labelAction;
	private JComboBox<ActionType> comboAction;
	private JLabel labelValue;
	private JTextField txtValue;
	private JLabel labelActionStep;
	private JComboBox<Step> comboActionStep;
	private JButton btnSaveAction;
	private JButton btnRemoveAction;
	private JTable actionTable;
	private JLabel labelActionInformation;
	// Finder panel components
	private JPanel finderPanel;
	private JLabel labelFinderName;
	private JTextField txtFinderName;
	private JLabel labelFinderType;
	private JComboBox<ByType> comboFinderType;
	private JLabel labelElement;
	private JTextField txtElement;
	private JButton btnSaveFinder;
	private JButton btnRemoveFinder;
	private JTable finderTable;
	private JLabel labelFinderInformation;
	// Step panel components
	private JPanel stepPanel;
	private JLabel labelStepName;
	private SuggestCombo suggestStepName;
	private JButton btnAddStep;
	private JButton btnRemoveStep;
	private JTable stepTable;
	private JLabel labelStepInformation;
	
	private boolean wasArrows;
	private int editingActionRow = -1;
	private Finder editingFinder = null;
	private int editingStepRow = -1;
	
	public FrameRecorder(FrameMain frameMain) {
		super(FramesHelpType.FRAME_RECORDER.getName());
		this.frameMain = frameMain;
		this.recorderManager = new RecorderManager(this);
		this.blockerLoading = new BlockerLoading(null, this, true);
		this.bindingFactory = new BindingFactory();
		initGUI();
		addComponents();
		addListeners();
	}

	private void initGUI() {
		setLayout(new RelativeLayout());
		setSize(new Dimension(550, 700));
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(frameMain);
	}

	private void addComponents() {
		add(blockerLoading.createLoading(70, 5), loadingBinding());

		buildActionPanel();
		buildFinderPanel();
		buildStepPanel();
		buildFrameConstants();
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private void buildActionPanel() {
		actionPanel = new JPanel(new RelativeLayout());
		createActionPanelComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		actionPanel.add(labelActionFinder, labelActionFinderBinding());
		actionPanel.add(comboActionFinder, comboActionFinderBinding());
		actionPanel.add(labelAction, labelActionBinding());
		actionPanel.add(comboAction, comboActionBinding());
		actionPanel.add(labelValue, labelValueBinding());
		actionPanel.add(txtValue, txtValueBinding());
		actionPanel.add(labelActionStep, labelActionStepBinding());
		actionPanel.add(comboActionStep, comboActionStepBinding());
		actionPanel.add(btnSaveAction, btnSaveActionBinding());
		actionPanel.add(btnRemoveAction, btnRemoveActionBinding());
		actionPanel.add(labelActionInformation, labelInformationBinding());

		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);
		Integer[] checkBoxColumns = new Integer[] { 1 };
		actionTable = new ComponentsTable(new String[] { "Entity", "Delete", "Finder",
				"Action", "Value", "Step" }, checkBoxColumns,
				new Integer[] { 0, 2, 3, 4, 5 }, true, false, true);
		setInvisibleColumn(actionTable);
		centerTableLabels(checkBoxColumns, actionTable);
		actionPanel.add(new JScrollPane(actionTable), actionTableBinding());
	}

	private void createActionPanelComponents() {
		labelActionFinder = new JLabel(FramesHelpType.FRAME_RECORDER_ACTION.getLabels().get(0));
		comboActionFinder = new JComboBox<>();
		labelAction = new JLabel(FramesHelpType.FRAME_RECORDER_ACTION.getLabels().get(1));
		createComboAction();
		labelValue = new JLabel(FramesHelpType.FRAME_RECORDER_ACTION.getLabels().get(2));
		txtValue = new JTextField();
		labelActionStep = new JLabel(FramesHelpType.FRAME_RECORDER_ACTION.getLabels().get(3));
		comboActionStep = new JComboBox<>();
		btnSaveAction = new JButton(FramesHelpType.FRAME_RECORDER_ACTION.getIcons().get(2));
		btnRemoveAction = new JButton(FramesHelpType.FRAME_RECORDER_ACTION.getIcons().get(3));
		labelActionInformation = new JLabel();
		labelActionInformation.setIcon(FramesHelpType.FRAME_RECORDER_ACTION.getIcons().get(0));
	}
	
	private void createComboAction() {
		comboAction = new JComboBox<>();
		for (ActionType actionType : ActionType.getActionTypesByGroup(ActionGroupType.WEB_ELEMENT_GROUP, true)) {
			comboAction.addItem(actionType);
		}
	}

	private RelativeConstraints labelActionFinderBinding() {
		Binding[] labelActionFinderBinding = new Binding[2];
		labelActionFinderBinding[0] = bindingFactory.leftEdge();
		labelActionFinderBinding[1] = bindingFactory.verticallyCenterAlignedWith(comboActionFinder);
		return new RelativeConstraints(labelActionFinderBinding);
	}

	private RelativeConstraints comboActionFinderBinding() {
		Binding[] comboActionFinderBinding = new Binding[3];
		comboActionFinderBinding[0] = bindingFactory.topEdge();
		comboActionFinderBinding[1] = bindingFactory.rightOf(labelActionFinder);
		comboActionFinderBinding[2] = bindingFactory.leftOf(labelAction);
		return new RelativeConstraints(comboActionFinderBinding);
	}

	private RelativeConstraints labelActionBinding() {
		Binding[] labelActionBinding = new Binding[2];
		labelActionBinding[0] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
		labelActionBinding[1] = bindingFactory.verticallyCenterAlignedWith(comboActionFinder);
		return new RelativeConstraints(labelActionBinding);
	}

	private RelativeConstraints comboActionBinding() {
		Binding[] comboActionBinding = new Binding[3];
		comboActionBinding[0] = bindingFactory.rightOf(labelAction);
		comboActionBinding[1] = bindingFactory.rightAlignedWith(btnRemoveAction);
		comboActionBinding[2] = bindingFactory.verticallyCenterAlignedWith(comboActionFinder);
		return new RelativeConstraints(comboActionBinding);
	}

	private RelativeConstraints labelValueBinding() {
		Binding[] labelValueBinding = new Binding[2];
		labelValueBinding[0] = bindingFactory.leftEdge();
		labelValueBinding[1] = bindingFactory.verticallyCenterAlignedWith(txtValue);
		return new RelativeConstraints(labelValueBinding);
	}

	private RelativeConstraints txtValueBinding() {
		Binding[] txtValueBinding = new Binding[3];
		txtValueBinding[0] = bindingFactory.below(comboActionFinder);
		txtValueBinding[1] = bindingFactory.leftAlignedWith(comboActionFinder);
		txtValueBinding[2] = bindingFactory.rightAlignedWith(btnRemoveAction);
		return new RelativeConstraints(txtValueBinding);
	}

	private RelativeConstraints labelActionStepBinding() {
		Binding[] labelActionStepBinding = new Binding[2];
		labelActionStepBinding[0] = bindingFactory.leftEdge();
		labelActionStepBinding[1] = bindingFactory.verticallyCenterAlignedWith(comboActionStep);
		return new RelativeConstraints(labelActionStepBinding);
	}

	private RelativeConstraints comboActionStepBinding() {
		Binding[] comboActionStepBinding = new Binding[3];
		comboActionStepBinding[0] = bindingFactory.leftAlignedWith(comboActionFinder);
		comboActionStepBinding[1] = bindingFactory.leftOf(btnSaveAction);
		comboActionStepBinding[2] = new Binding(Edge.TOP, 1, Direction.BELOW, Edge.TOP, btnRemoveAction);
		return new RelativeConstraints(comboActionStepBinding);
	}

	private RelativeConstraints btnSaveActionBinding() {
		Binding[] btnSaveActionBinding = new Binding[2];
		btnSaveActionBinding[0] = bindingFactory.leftOf(btnRemoveAction);
		btnSaveActionBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveAction);
		return new RelativeConstraints(btnSaveActionBinding);
	}

	private RelativeConstraints btnRemoveActionBinding() {
		Binding[] btnRemoveActionBinding = new Binding[2];
		btnRemoveActionBinding[0] = bindingFactory.below(txtValue);
		btnRemoveActionBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveActionBinding);
	}

	private RelativeConstraints actionTableBinding() {
		Binding[] actionTableBinding = new Binding[4];
		actionTableBinding[0] = bindingFactory.below(btnRemoveAction);
		actionTableBinding[1] = bindingFactory.leftEdge();
		actionTableBinding[2] = bindingFactory.rightEdge();
		actionTableBinding[3] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.TOP, labelActionInformation);
		return new RelativeConstraints(actionTableBinding);
	}

	private void buildFinderPanel() {
		finderPanel = new JPanel(new RelativeLayout());
		createFinderPanelComponents();
		enableFinderFields(false);

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		finderPanel.add(labelFinderName, labelFinderNameBinding());
		finderPanel.add(txtFinderName, txtFinderNameBinding());
		finderPanel.add(labelElement, labelElementBinding());
		finderPanel.add(txtElement, txtElementBinding());
		finderPanel.add(labelFinderType, labelFinderTypeBinding());
		finderPanel.add(comboFinderType, comboFinderTypeBinding());
		finderPanel.add(btnSaveFinder, btnSaveFinderBinding());
		finderPanel.add(btnRemoveFinder, btnRemoveFinderBinding());
		finderPanel.add(labelFinderInformation, labelInformationBinding());

		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);

		Integer[] checkBoxColumns = new Integer[] { 1 };
		finderTable = new ComponentsTable(new String[] { "Entity", "Delete", "Name",
				"Type", "Element" }, checkBoxColumns,
				new Integer[] { 0, 2, 3, 4 }, true, false, false);
		setInvisibleColumn(finderTable);
		centerTableLabels(checkBoxColumns, finderTable);
		finderPanel.add(new JScrollPane(finderTable), finderTableBinding());
	}

	private void createFinderPanelComponents() {
		labelFinderName = new JLabel(FramesHelpType.FRAME_RECORDER_FINDER.getLabels().get(0));
		txtFinderName = new JTextField();
		labelFinderType = new JLabel(FramesHelpType.FRAME_RECORDER_FINDER.getLabels().get(1));
		createComboFinderType();
		labelElement = new JLabel(FramesHelpType.FRAME_RECORDER_FINDER.getLabels().get(2));
		txtElement = new JTextField();
		btnSaveFinder = new JButton(FramesHelpType.FRAME_RECORDER_FINDER.getIcons().get(2));
		btnRemoveFinder = new JButton(FramesHelpType.FRAME_RECORDER_FINDER.getIcons().get(3));
		labelFinderInformation = new JLabel();
		labelFinderInformation.setIcon(FramesHelpType.FRAME_RECORDER_FINDER.getIcons().get(0));
	}
	
	private void createComboFinderType() {
		comboFinderType = new JComboBox<>();
		comboFinderType.setPreferredSize(new Dimension(150, 22));
		List<ByType> byTypeList = ByType.getAllByTypes();
		for (ByType byType : byTypeList) {
			comboFinderType.addItem(byType);
		}
	}
	
	private void enableFinderFields(boolean state) {
		txtFinderName.setEnabled(state);
		comboFinderType.setEnabled(state);
		txtElement.setEnabled(state);
		btnSaveFinder.setEnabled(state);
	}
	
	private RelativeConstraints labelFinderNameBinding() {
		Binding[] labelFinderNameBinding = new Binding[2];
		labelFinderNameBinding[0] = bindingFactory.leftEdge();
		labelFinderNameBinding[1] = bindingFactory.verticallyCenterAlignedWith(txtFinderName);
		return new RelativeConstraints(labelFinderNameBinding);
	}
	
	private RelativeConstraints txtFinderNameBinding() {
		Binding[] suggestFinderNameBinding = new Binding[3];
		suggestFinderNameBinding[0] = bindingFactory.topEdge();
		suggestFinderNameBinding[1] = bindingFactory.leftAlignedWith(txtElement);
		suggestFinderNameBinding[2] = bindingFactory.rightAlignedWith(btnRemoveFinder);
		return new RelativeConstraints(suggestFinderNameBinding);
	}

	private RelativeConstraints labelElementBinding() {
		Binding[] labelFinderBinding = new Binding[2];
		labelFinderBinding[0] = bindingFactory.leftEdge();
		labelFinderBinding[1] = bindingFactory.verticallyCenterAlignedWith(txtElement);
		return new RelativeConstraints(labelFinderBinding);
	}
	
	private RelativeConstraints txtElementBinding() {
		Binding[] comboFinderBinding = new Binding[3];
		comboFinderBinding[0] = bindingFactory.rightOf(labelElement);
		comboFinderBinding[1] = bindingFactory.rightAlignedWith(btnRemoveFinder);
		comboFinderBinding[2] = bindingFactory.below(txtFinderName);
		return new RelativeConstraints(comboFinderBinding);
	}
	
	private RelativeConstraints labelFinderTypeBinding() {
		Binding[] labelFinderTypeBinding = new Binding[2];
		labelFinderTypeBinding[0] = bindingFactory.leftEdge();
		labelFinderTypeBinding[1] = bindingFactory.verticallyCenterAlignedWith(comboFinderType);
		return new RelativeConstraints(labelFinderTypeBinding);
	}
	
	private RelativeConstraints comboFinderTypeBinding() {
		Binding[] comboFinderTypeBinding = new Binding[3];
		comboFinderTypeBinding[0] = bindingFactory.leftAlignedWith(txtElement);
		comboFinderTypeBinding[1] = bindingFactory.leftOf(btnSaveFinder);
		comboFinderTypeBinding[2] = new Binding(Edge.TOP, 1, Direction.BELOW, Edge.TOP, btnRemoveFinder);
		return new RelativeConstraints(comboFinderTypeBinding);
	}

	private RelativeConstraints btnSaveFinderBinding() {
		Binding[] btnSaveFinderBinding = new Binding[2];
		btnSaveFinderBinding[0] = bindingFactory.leftOf(btnRemoveFinder);
		btnSaveFinderBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveFinder);
		return new RelativeConstraints(btnSaveFinderBinding);
	}

	private RelativeConstraints btnRemoveFinderBinding() {
		Binding[] btnRemoveFinderBinding = new Binding[2];
		btnRemoveFinderBinding[0] = bindingFactory.below(txtElement);
		btnRemoveFinderBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveFinderBinding);
	}

	private RelativeConstraints finderTableBinding() {
		Binding[] finderTableBinding = new Binding[4];
		finderTableBinding[0] = bindingFactory.below(btnRemoveFinder);
		finderTableBinding[1] = bindingFactory.leftEdge();
		finderTableBinding[2] = bindingFactory.rightEdge();
		finderTableBinding[3] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.TOP, labelFinderInformation);
		return new RelativeConstraints(finderTableBinding);
	}

	private void buildStepPanel() {
		stepPanel = new JPanel(new RelativeLayout());
		createStepPanelComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		stepPanel.add(labelStepName, labelStepNameBinding());
		stepPanel.add(suggestStepName, suggestStepNameBinding());
		stepPanel.add(btnAddStep, btnAddStepBinding());
		stepPanel.add(btnRemoveStep, btnRemoveStepBinding());
		stepPanel.add(labelStepInformation, labelInformationBinding());

		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);

		Integer[] checkBoxColumns = new Integer[] { 1 };
		stepTable = new ComponentsTable(new String[] { "Entity", "Delete", "Name",
				"Finders", "Actions" }, checkBoxColumns,
				new Integer[] { 0, 2, 3, 4 }, true, false, false);
		setInvisibleColumn(stepTable);
		centerTableLabels(checkBoxColumns, stepTable);
		stepPanel.add(new JScrollPane(stepTable), stepTableBinding());
	}

	private void createStepPanelComponents() {
		labelStepName = new JLabel(FramesHelpType.FRAME_RECORDER_STEP.getLabels().get(0));
		suggestStepName = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		btnAddStep = new JButton(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(2));
		btnRemoveStep = new JButton(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(4));
		labelStepInformation = new JLabel();
		labelStepInformation.setIcon(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(0));
	}
	
	private RelativeConstraints labelStepNameBinding() {
		Binding[] labelStepNameBinding = new Binding[2];
		labelStepNameBinding[0] = bindingFactory.leftEdge();
		labelStepNameBinding[1] = bindingFactory.verticallyCenterAlignedWith(suggestStepName);
		return new RelativeConstraints(labelStepNameBinding);
	}
	
	private RelativeConstraints suggestStepNameBinding() {
		Binding[] suggestStepNameBinding = new Binding[3];
		suggestStepNameBinding[0] = new Binding(Edge.TOP, 1, Direction.BELOW, Edge.TOP, btnRemoveStep);
		suggestStepNameBinding[1] = bindingFactory.rightOf(labelStepName);
		suggestStepNameBinding[2] = bindingFactory.leftOf(btnAddStep);
		return new RelativeConstraints(suggestStepNameBinding);
	}
	
	private RelativeConstraints btnAddStepBinding() {
		Binding[] btnSaveFinderBinding = new Binding[2];
		btnSaveFinderBinding[0] = bindingFactory.leftOf(btnRemoveStep);
		btnSaveFinderBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveStep);
		return new RelativeConstraints(btnSaveFinderBinding);
	}

	private RelativeConstraints btnRemoveStepBinding() {
		Binding[] btnRemoveFinderBinding = new Binding[2];
		btnRemoveFinderBinding[0] = bindingFactory.topEdge();
		btnRemoveFinderBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveFinderBinding);
	}

	private RelativeConstraints stepTableBinding() {
		Binding[] finderTableBinding = new Binding[4];
		finderTableBinding[0] = bindingFactory.below(btnRemoveStep);
		finderTableBinding[1] = bindingFactory.leftEdge();
		finderTableBinding[2] = bindingFactory.rightEdge();
		finderTableBinding[3] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.TOP, labelStepInformation);
		return new RelativeConstraints(finderTableBinding);
	}

	private void buildFrameConstants() {
		createComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		add(webDriverLabel, webDriverLabelBinding());
		add(webDriverSuggest, webDriverSuggestBinding());
		add(startUrlLabel, startUrlLabelBinding());
		add(openUrlTxt, openUrlTxtBinding());
		add(toolBar, toolBarBinding());
		add(tabbedPane, tabbedPaneBinding());
		add(labelInformation, labelInformationBinding());
		add(btnImport, btnImportBinding());

		addTabs();
	}
	
	private void createComponents() {
		webDriverLabel = new JLabel(FramesHelpType.FRAME_RECORDER.getLabels().get(0));
		webDriverSuggest = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		startUrlLabel = new JLabel(FramesHelpType.FRAME_RECORDER.getLabels().get(1));
		openUrlTxt = new JTextField();
		createToolBar();
		tabbedPane = new JTabbedPane();
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_RECORDER.getIcons().get(0));
		btnImport = new JButton("Import");
	}

	private void createToolBar() {
		buildToolBar();
		
		bindingFactory.setHorizontalSpacing(10);
		//Configuration area
		toolBar.add(btnRecord, btnRecordBinding());
		toolBar.add(btnOpenBrowser, btnOpenBrowserBinding());
	}
	
	private void buildToolBar() {
		toolBar = new JToolBar();
		toolBar.setLayout(new RelativeLayout());
		toolBar.setPreferredSize(new Dimension(getWidth(), 30));
		toolBar.setFloatable(false);
		toolBar.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));

		btnRecord = new JToggleButton();
		btnRecord.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
		        btnRecord.setText(FramesHelpType.FRAME_RECORDER.getLabels().get(2));
		        btnRecord.setIcon(btnRecord.isSelected() ? FramesHelpType.FRAME_RECORDER.getIcons().get(2) : FramesHelpType.FRAME_RECORDER.getIcons().get(1));
		        btnOpenBrowser.setEnabled(btnRecord.isSelected());
		        super.paint(g, c);
			}
		});
		btnOpenBrowser = new JToggleButton();
		btnOpenBrowser.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
		        btnOpenBrowser.setText(FramesHelpType.FRAME_RECORDER.getLabels().get(3));
		        btnOpenBrowser.setIcon(btnOpenBrowser.isSelected() ? FramesHelpType.FRAME_RECORDER.getIcons().get(4) : FramesHelpType.FRAME_RECORDER.getIcons().get(3));
		        super.paint(g, c);
			}
		});
	}

	private RelativeConstraints btnRecordBinding() {
		Binding[] btnRecordBinding = new Binding[3];
		btnRecordBinding[0] = new Binding(Edge.TOP, 2, Direction.BELOW, Edge.TOP, Binding.PARENT);
		btnRecordBinding[1] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		btnRecordBinding[2] = new Binding(Edge.BOTTOM, 4, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(btnRecordBinding);
	}

	private RelativeConstraints btnOpenBrowserBinding() {
		Binding[] btnOpenBrowserBinding = new Binding[3];
		btnOpenBrowserBinding[0] = bindingFactory.rightOf(btnRecord);
		btnOpenBrowserBinding[1] = bindingFactory.topAlign(btnRecord);
		btnOpenBrowserBinding[2] = bindingFactory.bottomAlignedWith(btnRecord);
		return new RelativeConstraints(btnOpenBrowserBinding);
	}
	
	private RelativeConstraints webDriverLabelBinding() {
		Binding[] webDriverLabelBinding = new Binding[2];
		webDriverLabelBinding[0] = bindingFactory.leftEdge();
		webDriverLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(webDriverSuggest);
		return new RelativeConstraints(webDriverLabelBinding);
	}
	
	private RelativeConstraints webDriverSuggestBinding() {
		Binding[] webDriverSuggestBinding = new Binding[3];
		webDriverSuggestBinding[0] = bindingFactory.topEdge();
		webDriverSuggestBinding[1] = bindingFactory.rightOf(webDriverLabel);
		webDriverSuggestBinding[2] = bindingFactory.rightAlignedWith(tabbedPane);
		return new RelativeConstraints(webDriverSuggestBinding);
	}

	private RelativeConstraints startUrlLabelBinding() {
		Binding[] startUrlLabelBinding = new Binding[2];
		startUrlLabelBinding[0] = bindingFactory.leftEdge();
		startUrlLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(openUrlTxt);
		return new RelativeConstraints(startUrlLabelBinding);
	}

	private RelativeConstraints openUrlTxtBinding() {
		Binding[] openUrlTxtBinding = new Binding[3];
		openUrlTxtBinding[0] = bindingFactory.below(webDriverSuggest);
		openUrlTxtBinding[1] = bindingFactory.leftAlignedWith(webDriverSuggest);
		openUrlTxtBinding[2] = bindingFactory.rightAlignedWith(tabbedPane);
		return new RelativeConstraints(openUrlTxtBinding);
	}
	
	private RelativeConstraints toolBarBinding() {
		Binding[] toolBarBinding = new Binding[3];
		toolBarBinding[0] = bindingFactory.below(openUrlTxt);
		toolBarBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		toolBarBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		return new RelativeConstraints(toolBarBinding);
	}

	private RelativeConstraints tabbedPaneBinding() {
		Binding[] tabbedPaneBinding = new Binding[4];
		tabbedPaneBinding[0] = bindingFactory.below(toolBar);
		tabbedPaneBinding[1] = bindingFactory.leftEdge();
		tabbedPaneBinding[2] = bindingFactory.rightAlignedWith(btnImport);
		tabbedPaneBinding[3] = bindingFactory.above(btnImport);
		return new RelativeConstraints(tabbedPaneBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private RelativeConstraints btnImportBinding() {
		Binding[] btnImportBinding = new Binding[2];
		btnImportBinding[0] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		btnImportBinding[1] = new Binding(Edge.RIGHT, 9, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		return new RelativeConstraints(btnImportBinding);
	}

	private void addTabs() {
		tabbedPane.addTab(FramesHelpType.FRAME_RECORDER_ACTION.getName(), FramesHelpType.FRAME_RECORDER_ACTION.getIcons().get(1), actionPanel);
		tabbedPane.addTab(FramesHelpType.FRAME_RECORDER_FINDER.getName(), FramesHelpType.FRAME_RECORDER_FINDER.getIcons().get(1), finderPanel);
		tabbedPane.addTab(FramesHelpType.FRAME_RECORDER_STEP.getName(), FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(1), stepPanel);
	}
    
    private void setInvisibleColumn(JTable table) {
		table.getColumnModel().getColumn(0).setWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
    }

	private void centerTableLabels(Integer[] checkBoxColumns, JTable table) {
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < table.getColumnCount(); i++) {
			if (!isCheckBoxCol(checkBoxColumns, i)) {
				table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
			}
		}
	}
	
	private boolean isCheckBoxCol(Integer[] checkBoxColumns, int i) {
		for (Integer column : checkBoxColumns) {
			if (i == column) {
				return true;
			}
		}
		return false;
	}
	
	private void addListeners() {
		addTabActionListeners();
		addTabFinderListeners();
		addTabStepListeners();
		//Add base frame listeners
		webDriverSuggest.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = webDriverSuggest.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasArrows && key == KeyEvent.VK_ENTER) {
								webDriverSuggest.hidePopup();
								wasArrows = false;
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(webDriverSuggest, item.toString());
							} else {
								wasArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		btnRecord.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!btnRecord.isSelected()) {
					recorderManager.closeServer();
					recorderManager.closeWebDriver();
				} else {
					recorderManager.startServer();
				}
			}
		});
		btnOpenBrowser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!btnOpenBrowser.isSelected()) {
					recorderManager.closeWebDriver();
				} else {
					Object item = webDriverSuggest.getSelectedItem();
					if (item instanceof SuggestDoubleColumn && (openUrlTxt.getText() != null && !openUrlTxt.getText().isEmpty())) {
						recorderManager.record((Variable) ((SuggestDoubleColumn) item).getLeftText().getFullItem(), openUrlTxt.getText());
					}
				}
			}
		});
		labelInformation.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameRecorder.this, FramesHelpType.FRAME_RECORDER)).setVisible(true);
			}
		});
		btnImport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO: Import steps
			}
		});
	}
	
	private void addTabActionListeners() {
		btnSaveAction.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (editingActionRow < 0) {
					return;
				}
				
				Action action = (Action) actionTable.getValueAt(editingActionRow, 0);
				if (action == null) {
					action = new Action();
				}
				
				action.setFinder((Finder) comboActionFinder.getSelectedItem());
				action.setActionType((ActionType) comboAction.getSelectedItem());
				action.getParameters().get(0).setValue(txtValue.getText());
				action.setStep((Step) comboActionStep.getSelectedItem());
				
				updateActionTable(action);
				updateStepTable(action.getStep());
				
				setActionFields(null);
				editingActionRow = -1;
			}
		});
		btnRemoveAction.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (editingActionRow >= 0) {
					setActionFields(null);
					editingActionRow = -1;
				} else {
					DefaultTableModel actionTableModel = getActionTableModel();
					List<Integer> actionsToDelete = new ArrayList<>();
					for (int i = 0; i < actionTableModel.getRowCount(); i++) {
						if ((boolean) actionTableModel.getValueAt(i, 1)) {
							actionsToDelete.add(i);
						}
					}
					
					if (actionsToDelete.isEmpty()) {
						JOptionPane.showMessageDialog(FrameRecorder.this, "Select at least one row.", "Action Alert", JOptionPane.WARNING_MESSAGE);
						return;
					}
					
					if (askBeforeDelete("Action Alert")) {
						deleteSelectedRows(actionsToDelete, actionTableModel);
					}
				}
			}
		});
		actionTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					editingActionRow = actionTable.getSelectedRow();
					Action action = (Action) actionTable.getValueAt(editingActionRow, 0);
					if (action == null) {
						editingActionRow = -1;
						return;
					}
					
					setActionFields(action);
				}
			}
		});
	}
	
	private void updateActionTable(Action action) {
		DefaultTableModel tableModel = getActionTableModel();
		if (tableModel == null || action == null) {
			return;
		}

		tableModel.setValueAt(action, editingActionRow, 0);//Entity
		tableModel.setValueAt(false, editingActionRow, 1);//Delete
		tableModel.setValueAt(action.getFinder(), editingActionRow, 2);//action finder
		tableModel.setValueAt(action.getActionType(), editingActionRow, 3);//action type
		tableModel.setValueAt(action.getParameters().get(0).getValue(), editingActionRow, 4);//action value
		tableModel.setValueAt(action.getStep(), editingActionRow, 5);//action step
	}
	
	private void updateStepTable(Step step) {
		DefaultTableModel actionTableModel = getActionTableModel();
		DefaultTableModel stepTableModel = getStepTableModel();
		if (actionTableModel == null || stepTableModel == null || step == null) {
			return;
		}
		int actionCount = 0;
		HashSet<Finder> finderSet = new HashSet<>();
		//Count all finders and actions with this step
		for (int i = 0; i < actionTableModel.getRowCount(); i++) {
			Step tableStep = (Step) actionTableModel.getValueAt(i, 5);
			if (step.equals(tableStep)) {
				actionCount++;
				finderSet.add((Finder) actionTableModel.getValueAt(i, 2));
			}
		}
		//Update step row
		for (int i = 0; i < stepTableModel.getRowCount(); i++) {
			Step tableStep = (Step) stepTableModel.getValueAt(i, 0);
			if (step.equals(tableStep)) {
				stepTableModel.setValueAt(finderSet.size(), i, 3);
				stepTableModel.setValueAt(actionCount, i, 4);
			}
		}
	}
	
	private void setActionFields(Action action) {
		comboActionFinder.setSelectedItem(action != null ? action.getFinder() : null);
		comboAction.setSelectedItem(action != null ? action.getActionType() : null);
		txtValue.setText(action != null ? action.getParameters().get(0).getValue() : "");
		comboActionStep.setSelectedItem(action != null ? action.getStep() : null);
		btnRemoveAction.setIcon(FramesHelpType.FRAME_RECORDER_ACTION.getIcons().get(action != null ? 4 : 3));
	}
	
	public void addWebRecordInActionTable(WebRecord webRecord) {
		DefaultTableModel tableModel = getActionTableModel();
		if (tableModel == null || webRecord == null) {
			return;
		}

		//Add all Finders from webRecord to FinderTable
		//If duplicate, change to existing
		//If not, add to finderTable
		findersTreatment(webRecord);
		//convert WebRecord to Action
		Action action = convertWebRecordToAction(webRecord);
		
		int rowCount = tableModel.getRowCount();
		tableModel.setRowCount(rowCount + 1);
		tableModel.setValueAt(action, rowCount, 0);//Entity
		tableModel.setValueAt(false, rowCount, 1);//Delete
		tableModel.setValueAt(action.getFinder(), rowCount, 2);//Element finder (xpath when adding)
		tableModel.setValueAt(action.getActionType(), rowCount, 3);//Element action
		tableModel.setValueAt(action.getParameters().get(0).getValue(), rowCount, 4);//Element value
	}
	
	private Action convertWebRecordToAction(WebRecord webRecord) {
		Action action = new Action();
		action.setFinder(webRecord.getXpath());
		ActionType actionType = convertEventToActionType(webRecord);
		action.setActionType(actionType);
		action.setParameters(buildActionParams(action, actionType, webRecord.getValue()));
		
		return action;
	}
	
	private ActionType convertEventToActionType(WebRecord webRecord) {
		switch (webRecord.getEvent()) {
		case "change":
			if ("select-one".equalsIgnoreCase(webRecord.getType())) {
				return ActionType.WEB_ELEMENT_SEND_KEYS;
			}
			return ActionType.WEB_ELEMENT_CLICK;
		case "click":
			return ActionType.WEB_ELEMENT_CLICK;
		case "keyup":
			return ActionType.WEB_ELEMENT_SEND_KEYS;
		case "blur"://change (check type - radio/checkbox = click, select-one = send keys, click
			webRecord.setValue("TAB");
			return ActionType.WEB_ELEMENT_SEND_KEYS;
		}
		return null;
	}
	
	private List<ActionParameter> buildActionParams(Action action, ActionType actionType, String value) {
		ActionParameter param = new ActionParameter();
		param.setAction(action);
		param.setName(actionType.getFrameAddType().getLabels().get(0));
		param.setValue(value);

		List<ActionParameter> actionParamList = new ArrayList<>();
		actionParamList.add(param);
		
		return actionParamList;
	}
	
	private void findersTreatment(WebRecord webRecord) {
		int count = -1;
		for (Finder finder : new Finder[]{webRecord.getAttid(), webRecord.getId(), webRecord.getName(), webRecord.getXpath()}) {
			count++;
			int tableIndex = isDuplicated(finder, getFinderTableModel());
			if (tableIndex >= 0) {
				finder = (Finder) getFinderTableModel().getValueAt(tableIndex, 0);
				updateWebRecordFinder(webRecord, finder, count);
			} else {
				addInFinderTable(finder);
				refreshComboActionFinder();
			}
		}
	}
	
	private int isDuplicated(Finder finder, DefaultTableModel tableModel) {
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			Finder compare = (Finder) tableModel.getValueAt(i, 0);
			if (finder.getParameters().get(2).equals(compare.getParameters().get(2))) {
				return i;
			}
		}
		
		return -1;
	}
	
	private void updateWebRecordFinder(WebRecord webRecord, Finder finder, int count) {
		switch (count) {
		case 0:
			webRecord.setAttid(finder);
			break;
		case 1:
			webRecord.setId(finder);
			break;
		case 2:
			webRecord.setName(finder);
			break;
		case 3:
			webRecord.setXpath(finder);
			break;
		}
	}
	
	private void addTabFinderListeners() {
		btnSaveFinder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (editingFinder == null) {
					return;
				}
				String finderName = txtFinderName.getText();
				String element = txtElement.getText();
				if (StringUtils.isBlank(finderName) || finderName.length() <= 3) {
					JOptionPane.showMessageDialog(FrameRecorder.this, "The name entered is less than three characters or null", "Finder Alert", JOptionPane.WARNING_MESSAGE);
					return;
				}
				if (StringUtils.isBlank(element)) {
					JOptionPane.showMessageDialog(FrameRecorder.this, "The element entered is null", "Finder Alert", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				editingFinder.setNmFinder(finderName);//Update name
				editingFinder.getParameters().get(1).setValue(comboFinderType.getSelectedItem().toString());//Update by type
				editingFinder.getParameters().get(2).setValue(element);//Update element
				
				updateFinderRows(editingFinder);
				editingFinder = null;
				setFinderFields();
				enableFinderFields(false);
				btnRemoveFinder.setIcon(FramesHelpType.FRAME_RECORDER_FINDER.getIcons().get(3));
			}
		});
		btnRemoveFinder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				if (editingFinder != null) {
					editingFinder = null;
					setFinderFields();
					enableFinderFields(false);
					btnRemoveFinder.setIcon(FramesHelpType.FRAME_RECORDER_FINDER.getIcons().get(3));
				} else {
					//Get all selected rows
					DefaultTableModel finderTableModel = getFinderTableModel();
					List<Integer> findersToDelete = new ArrayList<>();
					for (int i = 0; i < finderTableModel.getRowCount(); i++) {
						if ((boolean) finderTableModel.getValueAt(i, 1)) {
							findersToDelete.add(i);
						}
					}
					
					if (findersToDelete.isEmpty()) {
						JOptionPane.showMessageDialog(FrameRecorder.this, "Select at least one row.", "Finder Alert", JOptionPane.WARNING_MESSAGE);
						return;
					}
					
					if (askBeforeDelete("Finder Alert")) {
						deleteSelectedRows(findersToDelete, finderTableModel);
						refreshComboActionFinder();
					}
				}
			}
		});
		finderTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					editingFinder = (Finder) finderTable.getValueAt(finderTable.getSelectedRow(), 0);
					if (editingFinder == null) {
						return;
					}
					btnRemoveFinder.setIcon(FramesHelpType.FRAME_RECORDER_FINDER.getIcons().get(4));
					setFinderFields();
					enableFinderFields(true);
				}
			}
		});
	}
	
	private void updateFinderRows(Finder finder) {
		DefaultTableModel tableModel = getFinderTableModel();
		if (tableModel == null || finder == null || finder.getParameters().size() < 3) {
			return;
		}
		
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			Finder tableFinder = (Finder) tableModel.getValueAt(i, 0);
			if (finder.equals(tableFinder)) {
				tableModel.setValueAt(finder.getNmFinder(), i, 2);//Element name (xpath when adding)
				tableModel.setValueAt(finder.getParameters().get(1).getValue(), i, 3);//Element ByType
				tableModel.setValueAt(finder.getParameters().get(2).getValue(), i, 4);//Element element
			}
		}
	}
	
	private void setFinderFields() {
		txtFinderName.setText(editingFinder != null ? editingFinder.getNmFinder() : "");
		txtElement.setText(editingFinder != null ? editingFinder.getParameters().get(2).getValue() : "");
		setComboItem(comboFinderType, (editingFinder != null ? editingFinder.getParameters().get(1).getValue() : ""));
	}
	
	private void setComboItem(JComboBox<ByType> combo, String strToFind) {
		if (combo == null || strToFind == null) {
			return;
		}
		if (StringUtils.isBlank(strToFind)) {
			combo.setSelectedIndex(0);
		}
		for (int i = 0; i < combo.getItemCount(); i++) {
			String itmToCompare = combo.getItemAt(i).toString();
			if (strToFind.equals(itmToCompare)) {
				combo.setSelectedIndex(i);
			}
		}
	}
	
	private void addInFinderTable(Finder finder) {
		DefaultTableModel tableModel = getFinderTableModel();
		if (tableModel == null || finder == null || finder.getParameters().size() < 3) {
			return;
		}
		
		int rowCount = tableModel.getRowCount();
		tableModel.setRowCount(rowCount + 1);
		tableModel.setValueAt(finder, rowCount, 0);//Entity
		tableModel.setValueAt(false, rowCount, 1);//Delete
		tableModel.setValueAt(finder.getNmFinder(), rowCount, 2);//Element name (xpath when adding)
		tableModel.setValueAt(finder.getParameters().get(1).getValue(), rowCount, 3);//Element ByType
		tableModel.setValueAt(finder.getParameters().get(2).getValue(), rowCount, 4);//Element element
	}
	
	private void refreshComboActionFinder() {
		DefaultTableModel tableModel = getFinderTableModel();
		List<Finder> finderList = new ArrayList<>();
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			finderList.add((Finder) tableModel.getValueAt(i, 0));
		}

		sortFinderByName(finderList);
		if (comboActionFinder.getItemCount() > 0) {
			comboActionFinder.removeAllItems();
		}
		
		for (Finder finder : finderList) {
			comboActionFinder.addItem(finder);
		}
	}

    private List<Finder> sortFinderByName(List<Finder> listToSort) {
    	Collections.sort(listToSort, new Comparator<Finder>() {
            @Override
            public int compare(Finder o1, Finder o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        return listToSort;
    }
	
	private void addTabStepListeners() {
		suggestStepName.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = suggestStepName.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasArrows && key == KeyEvent.VK_ENTER) {
								suggestStepName.hidePopup();
								wasArrows = false;
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(suggestStepName, item.toString());
							} else {
								wasArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		btnAddStep.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object obj = suggestStepName.getSelectedItem();
				if (obj == null) {
					return;
				}
				if (editingStepRow >= 0) {
					btnAddStep.setIcon(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(2));
					btnRemoveStep.setIcon(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(4));
				}
				
				if (obj instanceof SuggestDoubleColumn) {
					addInStepTable((Step)((SuggestDoubleColumn) obj).getLeftText().getFullItem());
				} else if (obj.toString().length() >= 3) {
					addInStepTable((new StepController()).buildStep(obj.toString(), null, null));
				} else {
					JOptionPane.showMessageDialog(FrameRecorder.this, "The name entered is less than three characters or null", "Step Alert", JOptionPane.WARNING_MESSAGE);
				}
				
				editingStepRow = -1;
			}
		});
		btnRemoveStep.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (editingStepRow >= 0) {
					suggestStepName.setSelectedItem("");
					btnAddStep.setIcon(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(2));
					btnRemoveStep.setIcon(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(4));
					editingStepRow = -1;
				} else {					
					DefaultTableModel stepTableModel = getStepTableModel();
					List<Integer> stepsToDelete = new ArrayList<>();
					for (int i = 0; i < stepTableModel.getRowCount(); i++) {
						if ((boolean) stepTableModel.getValueAt(i, 1)) {
							stepsToDelete.add(i);
						}
					}
					
					if (stepsToDelete.isEmpty()) {
						JOptionPane.showMessageDialog(FrameRecorder.this, "Select at least one row.", "Step Alert", JOptionPane.WARNING_MESSAGE);
						return;
					}
					if (askBeforeDelete("Step Alert")) {
						deleteSelectedRows(stepsToDelete, stepTableModel);
						refreshComboActionStep();
					}
				}
			}
		});
		stepTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					editingStepRow = stepTable.getSelectedRow();
					Step step = (Step) stepTable.getValueAt(editingStepRow, 0);
					if (step == null) {
						editingStepRow = -1;
						return;
					}
					suggestStepName.setSelectedItem(buildSuggestDoubleColumn(step, ""));
					btnAddStep.setIcon(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(3));
					btnRemoveStep.setIcon(FramesHelpType.FRAME_RECORDER_STEP.getIcons().get(5));
				}
			}
		});
	}

	private void addInStepTable(Step step) {
		DefaultTableModel tableModel = getStepTableModel();
		int rowCount;
		if (tableModel == null || step == null) {
			return;
		}
		
		if (editingStepRow < 0) {
			rowCount = tableModel.getRowCount();
			tableModel.setRowCount(rowCount + 1);
		} else {
			rowCount = editingStepRow;
		}
		
		tableModel.setValueAt(step, rowCount, 0);// Entity
		tableModel.setValueAt(false, rowCount, 1);//Delete
		tableModel.setValueAt(step.getNmStep(), rowCount, 2);//Step name
		if (editingStepRow < 0) {
			tableModel.setValueAt(0, rowCount, 3);//Finder count
			tableModel.setValueAt(0, rowCount, 4);//Action count
		}
		
		refreshComboActionStep();
		suggestStepName.setSelectedItem("");
	}
	
	private void refreshComboActionStep() {
		DefaultTableModel tableModel = getStepTableModel();
		List<Step> stepList = new ArrayList<>();
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			stepList.add((Step) tableModel.getValueAt(i, 0));
		}

		sortStepByName(stepList);
		if (comboActionStep.getItemCount() > 0) {
			comboActionStep.removeAllItems();
		}
		
		for (Step step : stepList) {
			comboActionStep.addItem(step);
		}
	}

    private List<Step> sortStepByName(List<Step> listToSort) {
    	Collections.sort(listToSort, new Comparator<Step>() {
            @Override
            public int compare(Step o1, Step o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        return listToSort;
    }
    
    private boolean askBeforeDelete(String title) {
    	return JOptionPane.showConfirmDialog(this, "Are you sure you want to proceed with the deletion?", title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION;
    }
	
	private void deleteSelectedRows(List<Integer> rowsToDelete, DefaultTableModel tableModel) {
		for (Integer toDelete : rowsToDelete) {
			tableModel.removeRow(toDelete);
		}
	}

	private void comboFilter(final SuggestCombo suggest, final String searchFor) {
		blockerLoading.start("Searching equivalences");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				List<SuggestDoubleColumn> items = getItems(suggest, searchFor);
				if (items.size() > 0) {
					suggest.setModel(new DefaultComboBoxModel(items.toArray()));
					suggest.setSelectedItem(searchFor.isEmpty() ? "" : searchFor);
					JTextField editor = (JTextField) suggest.getEditor().getEditorComponent();
					editor.setCaretPosition(editor.getDocument().getLength());
					suggest.showPopup();
				} else {
					suggest.hidePopup();
				}
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}

	private List<SuggestDoubleColumn> getItems(SuggestCombo suggest, String searchFor) {
		List<SuggestDoubleColumn> returnList = new ArrayList<>();
		try {
			if (webDriverSuggest.equals(suggest)) {
				for (Variable variable : (new VariableController()).findLikeNameAndGroupType(searchFor, VariableGroupType.VARIABLE_WEBDRIVER_GROUP)) {
					returnList.add(buildSuggestDoubleColumn(variable, searchFor));
				}
			}
			if (suggestStepName.equals(suggest)) {
				for (Step step : (new StepController()).findLikeName(searchFor)) {
					returnList.add(buildSuggestDoubleColumn(step, searchFor));
				}
			}
		} catch (Exception e) {
			showError(e);
		}
		return returnList;
	}
	
	private SuggestDoubleColumn buildSuggestDoubleColumn(Object objToBuild, String searchFor) {
		SuggestDoubleColumn returnSuggest = null;
		if (objToBuild instanceof Step) {
			Step step = (Step) objToBuild;
			returnSuggest = new SuggestDoubleColumn(new CellValue(step.getId(), step.getNmStep(), null, step), searchFor);
		} else if (objToBuild instanceof Variable) {
			Variable variable = (Variable) objToBuild;
			returnSuggest = new SuggestDoubleColumn(new CellValue(variable.getId(), variable.getName(), null, variable), searchFor);
		}
		
		return returnSuggest;
	}

	private void showError(Exception e) {
		JOptionPane.showMessageDialog(this,
				e.getMessage(),
				"Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public DefaultTableModel getActionTableModel() {
		return (DefaultTableModel) actionTable.getModel();
	}

	public DefaultTableModel getFinderTableModel() {
		return (DefaultTableModel) finderTable.getModel();
	}

	public DefaultTableModel getStepTableModel() {
		return (DefaultTableModel) stepTable.getModel();
	}
}
