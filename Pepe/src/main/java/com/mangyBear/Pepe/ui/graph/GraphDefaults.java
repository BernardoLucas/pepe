package com.mangyBear.Pepe.ui.graph;

import java.awt.Adjustable;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JScrollPane;

import com.mangyBear.Pepe.domain.entity.FlowItemConnection;
import com.mangyBear.Pepe.domain.types.graph.GraphGridStyleType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.mxGraphComponent.mxGraphControl;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.view.mxGraph;

/*
 Copyright (c) 2001-2014, JGraph Ltd
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the JGraph nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL JGRAPH BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
public class GraphDefaults extends mxGraph {

	private int idEdge;
	protected mxGraphComponent graphComponent;
	protected Map<Integer, mxCell> cellMap = new HashMap<>();
	protected Map<Integer, mxCell> connectionMap = new HashMap<>();

	private boolean selfEdition;
	private int lastClickX;
	private int lastClickY;

	public GraphDefaults() {
		setCellsEditable(false);
		setAllowDanglingEdges(false);
		setAllowLoops(false);
		setVertexLabelsMovable(false);
		setEdgeLabelsMovable(false);
		setGraphComponent();
		setGraphStyles();
		buildScrollBars();
		addListeners();
	}

	private void setGraphComponent() {
		graphComponent = new mxGraphComponent(this);
		getGraphComponent().getViewport().setBackground(Color.WHITE);
		getGraphComponent().setGridVisible(true);
		GraphGridStyleType style = MyConstants.getGridStyle();
		getGraphComponent().setGridStyle(style != null ? style.getStyle() : 0);
	}

	private void buildScrollBars() {
		getGraphComponent().getVerticalScrollBar().addAdjustmentListener(new GraphAdjustmentListener());
		getGraphComponent().getHorizontalScrollBar().addAdjustmentListener(new GraphAdjustmentListener());
	}
	
	private void setGraphStyles() {
		stylesheet = getStylesheet();
		for (GraphType graphType : GraphType.getAllGraphTypes()) {
			stylesheet.putCellStyle(graphType.getType(), buildCellStyle(graphType));
		}
		stylesheet.setDefaultEdgeStyle(buildConnectionStyle());
		setStylesheet(stylesheet);
	}
	
	private Map<String, Object> buildCellStyle(GraphType graphType) {
		Map<String, Object> cell = new HashMap<>();
		cell.put(mxConstants.STYLE_FONTCOLOR, graphType.getFontHexColor());
		cell.put(mxConstants.STYLE_VERTICAL_ALIGN, graphType.getFontVerticalAlign());
		cell.put(mxConstants.STYLE_ALIGN, graphType.getFontHorizontalAlign());
		cell.put(mxConstants.STYLE_FILLCOLOR, graphType.getFillHexColor());
		cell.put(mxConstants.STYLE_GRADIENTCOLOR, graphType.getGradientHexColor());
		cell.put(mxConstants.STYLE_GRADIENT_DIRECTION, graphType.getGradientDirection());
		cell.put(mxConstants.STYLE_STROKECOLOR, graphType.getStrokeHexColor());
		cell.put(mxConstants.STYLE_SHAPE, graphType.getShape());
		cell.put(mxConstants.STYLE_PERIMETER, graphType.getPerimeter());
		cell.put(mxConstants.STYLE_WHITE_SPACE, "wrap");
		cell.put(mxConstants.STYLE_OVERFLOW, "hidden");
		cell.put(mxConstants.STYLE_SHADOW, true);
		return cell;
	}

	private Map<String, Object> buildConnectionStyle() {
		Map<String, Object> conn = new HashMap<>();
		conn.put(mxConstants.STYLE_ROUNDED, true);
		conn.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_TOPTOBOTTOM);
		conn.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_CONNECTOR);
		conn.put(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_DIAMOND);
		conn.put(mxConstants.STYLE_LABEL_POSITION, mxConstants.ALIGN_RIGHT);
		conn.put(mxConstants.STYLE_STROKECOLOR, "#000000");
		conn.put(mxConstants.STYLE_FONTCOLOR, "#446299");
		return conn;
	}

	private void addListeners() {
		getGraph().getSelectionModel().addListener(mxEvent.CHANGE, new mxIEventListener() {

					@Override
					public void invoke(Object sender, mxEventObject evt) {
						Object propRemoved = evt.getProperty("removed");
						if (propRemoved != null) {
							mxCell edge = blockEdge(propRemoved);
							labelLogicEdges(edge);
							if (edge != null) {
								getConnMap().put(idEdge++, edge);
							}
						} else {
							List<?> propAdded = ((ArrayList<?>) evt.getProperty("added"));
							if (propAdded == null || propAdded.isEmpty()) {
								return;
							}
							
							mxCell cellToRemoved = ((mxCell) propAdded.get(0));
							if (cellToRemoved == null) {
								return;
							}
							
							for (int i = 0; i < getConnMap().size(); i++) {
								mxCell edgeToRemove = getConnMap().get(i);
								if (cellToRemoved.equals(edgeToRemove)) {
									getConnMap().remove(i);
									break;
								}
							}
						}
					}
				});
		getGraphComponent().removeMouseWheelListener(getGraphComponent().getMouseWheelListeners()[0]);
		getGraphComponent().addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.isControlDown()) {
					getGraphComponent().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
					Adjustable hAdj = getGraphComponent().getHorizontalScrollBar();
					int scroll = e.getUnitsToScroll() * hAdj.getBlockIncrement();
					hAdj.setValue(hAdj.getValue() + scroll);
				} else {
					getGraphComponent().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
					Adjustable vAdj = getGraphComponent().getVerticalScrollBar();
					int scroll = e.getUnitsToScroll() * vAdj.getBlockIncrement();
					vAdj.setValue(vAdj.getValue() + scroll);
				}
			}
		});

	}

	private mxCell blockEdge(Object propRemoved) {
		mxCell cell = null;
		Object propValue = ((ArrayList<?>) propRemoved).get(0);
		
		if (!(propValue instanceof mxCell)) {
			return cell;
		}
		cell = ((mxCell) propValue);
		
		if (cell != null && cell.isEdge()) {
			cell.setVisible(!(blockTargetBegin(cell) || blockSourceEnd(cell) || blockSameConnectionOrSameSource(cell)));
			
			removeNotVisableEdges((mxCell) cell.getSource());
			removeNotVisableEdges((mxCell) cell.getTarget());
		}
		return cell;
	}

	private boolean blockTargetBegin(mxCell validCell) {
		GraphType type = ((CellValue) validCell.getTarget().getValue()).getGraphType();
		return GraphType.CELL_BEGIN.equals(type);
	}

	private boolean blockSourceEnd(mxCell validCell) {
		GraphType type = ((CellValue) validCell.getSource().getValue()).getGraphType();
		return GraphType.CELL_END.equals(type);
	}

	private boolean blockSameConnectionOrSameSource(mxCell validCell) {
		mxCell source = (mxCell) validCell.getSource();
		mxCell target = (mxCell) validCell.getTarget();
		return hasDuplicates(source, target);
	}

	private boolean hasDuplicates(mxCell source, mxCell target) {
		GraphType type = ((CellValue) source.getValue()).getGraphType();
		Object[] connectionList = getGraph().getConnections(source);
		int countSameConnection = 0;
		int countSameSource = 0;
		for (Object connection : connectionList) {
			mxCell edge = (mxCell) connection;
			mxCell sourceEdge = (mxCell) edge.getSource();
			mxCell targetEdge = (mxCell) edge.getTarget();
			if ((sourceEdge.equals(source) && targetEdge.equals(target))
					|| (sourceEdge.equals(target) && targetEdge.equals(source))) {
				countSameConnection++;
			}
			if (sourceEdge.equals(source)) {
				countSameSource++;
			}
		}
		int sameSourcePermitted = GraphType.CELL_LOGIC.equals(type) ? 2 : 1;
		return (countSameConnection > 1)
				|| (countSameSource > sameSourcePermitted);
	}

	private void removeNotVisableEdges(mxCell validCell) {
		List<mxCell> toRemove = new ArrayList<>();
		Object[] connectionList = getGraph().getConnections(validCell);
		for (Object connection : connectionList) {
			mxCell edge = (mxCell) connection;
			if (!edge.isVisible()) {
				toRemove.add(edge);
			}
		}
		if (!toRemove.isEmpty()) {
			setSelfEdition(true);
			getGraph().removeCells(toRemove.toArray());
		}
	}

	private void labelLogicEdges(mxCell validCell) {
		if (validCell == null) {
			return;
		}
		
		mxCell source = (mxCell) validCell.getSource();
		if (source == null) {
			return;
		}
		
		GraphType type = ((CellValue) source.getValue()).getGraphType();
		if (!GraphType.CELL_LOGIC.equals(type)) {
			return;
		}
		
		if (validCell.getValue() instanceof FlowItemConnection) {
			return;
		}
		
		int edgeCount = getSourceEdgesCount(source);
		if (edgeCount > 2) {
			return;
		}
		
		boolean label = chooseLabel(source);
		validCell.setValue(Boolean.toString(label).toUpperCase());
		
	}
	
	private int getSourceEdgesCount(mxCell source) {
		int count = 0;
		for (int i = 0; i < source.getEdgeCount(); i++) {
			mxCell edge = (mxCell) source.getEdgeAt(i);
			if (source.equals(edge.getSource())) {
				count++;
			}
		}
		return count;
	}
	
	private boolean chooseLabel(mxCell source) {
		for (int i = 0; i < source.getEdgeCount(); i++) {
			mxCell edge = (mxCell) source.getEdgeAt(i);
			if (!source.equals(edge.getSource())) {
				continue;
			}
			if (edge.getValue() instanceof FlowItemConnection) {
				FlowItemConnection connEdge = (FlowItemConnection) edge.getValue();
				return "TRUE".equalsIgnoreCase(connEdge.getCondition())? false : true ;
			}
			if ("TRUE".equalsIgnoreCase(edge.getValue().toString())) {
				return false;
			}
		}
		return true;
	}

	public mxGraph getGraph() {
		return this;
	}

	public Object getGraphDefaultParent() {
		return getDefaultParent();
	}

	public mxGraphComponent getGraphComponent() {
		return graphComponent;
	}

	public Map<Integer, mxCell> getCellMap() {
		return cellMap;
	}

	public Map<Integer, mxCell> getConnMap() {
		return connectionMap;
	}

	public boolean isSelfEdition() {
		return selfEdition;
	}
	
	public void setSelfEdition(boolean selfEdition) {
		this.selfEdition = selfEdition;
	}

	public int getLastClickX() {
		return lastClickX;
	}

	public void setLastClickX(int lastClickX) {
		this.lastClickX = lastClickX;
	}

	public int getLastClickY() {
		return lastClickY;
	}

	public void setLastClickY(int lastClickY) {
		this.lastClickY = lastClickY;
	}

	class GraphAdjustmentListener implements AdjustmentListener {

		@Override
		public void adjustmentValueChanged(AdjustmentEvent evt) {
			Adjustable source = evt.getAdjustable();

			if (evt.getValueIsAdjusting()) {
				return;
			}

			mxGraphControl control = getGraphComponent().getGraphControl();
			Rectangle rect = control.getVisibleRect();

			int orientation = source.getOrientation();
			if (orientation == Adjustable.HORIZONTAL) {
				rect.x += 60;
			} else {
				rect.y += 60;
			}
			try {
				Method protectedMethod = control.getClass().getDeclaredMethod("extendComponent", new Class[] { Rectangle.class });
				protectedMethod.setAccessible(true);
				protectedMethod.invoke(control, rect);
			} catch (NoSuchMethodException | SecurityException
					| IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				System.out.println("|ERROR| - " + e.getMessage());
			}
		}
	}
}
