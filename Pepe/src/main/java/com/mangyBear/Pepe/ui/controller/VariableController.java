package com.mangyBear.Pepe.ui.controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.VarObject;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.entity.VariableParameter;
import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.domain.types.variable.VariableType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.VariableService;

public class VariableController {

	private VariableService service = new VariableService();
	
	public void update(Variable variable) throws Exception {
		service.update(variable);
	}
	
	public void remove(Variable varObject) throws Exception {
		service.remove(varObject);
	}
	
	public Variable updateParams(Variable variable, DefaultTableModel model) {
		setVariableParams(variable, model);
		return variable;
	}
	
	private void setVariableParams(Variable variable, DefaultTableModel model) {
		List<VariableParameter> varParamList = variable.getParameters();
		for (int i = 0; i < model.getRowCount(); i++) {
			VariableParameter varParameter = varParamList.get(i);
			varParameter.setName(MyConstants.setAsString(model.getValueAt(i, 0)));
			varParameter.setValue(MyConstants.setAsString(model.getValueAt(i, 1)));
			varParameter.setVariable(variable);
		}
	}

    public Variable buildVariable(Long varId, String name, Flow flowDad, VarObject varObject, VariableType type, DefaultTableModel model, Object value) {
    	Variable variable = new Variable();
    	
    	variable.setId(varId);
    	variable.setFlowDad(flowDad);
    	variable.setVarObject((VarObject) varObject);
    	variable.setName(putBracket(name));
    	variable.setVariableType(type);

        variable.setParameters(new ArrayList<VariableParameter>());
        if (model != null) {
	        for (int i = 0; i < model.getRowCount(); i++) {
	        	VariableParameter parameter = new VariableParameter();
	            parameter.setName(MyConstants.setAsString(model.getValueAt(i, 0)));
	            parameter.setValue(MyConstants.setAsString(model.getValueAt(i, 1)));
	            parameter.setVariable(variable);
	            variable.getParameters().add(parameter);
	        }
        }
        
        if (value != null) {
        	VariableParameter parameter = new VariableParameter();
            parameter.setName("Value");
            parameter.setValue(value.toString());
            parameter.setVariable(variable);
            variable.getParameters().add(parameter);
        }
        
        return variable;
    }
    
    private String putBracket(String name) {
    	if (name == null) {
    		return name;
    	}
    	
    	if (!name.startsWith("[")) {
    		name = "[" + name;
    	}
    	if (!name.endsWith("]")) {
    		name = name + "]";
    	}
    	return name;
    }
    
    public Variable findById(Long id) throws Exception {
    	return service.findById(id);
    }

	public Variable findByName(String name) throws Exception {
		return service.findByName(name);
	}
	
	public List<Variable> findLikeName(String name) throws Exception {
		return service.findLikeName(name);
	}

	public List<Variable> findLikeNameAndGroupType(String name, VariableGroupType type) throws Exception {
		return service.findLikeNameAndGroupType(name, type);
	}
}
