package com.mangyBear.Pepe.ui.component.readWriteFile;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class PropertyReader extends TextFileReader {

	protected final String OPEN_SECTION = "[";
	protected final String CLOSE_SECTION = "]";
	protected final String EQUALS = "=";
    
    public String getKeyValue(File filePath, String section, String key, String defaultVal) {
    	Map<String, List<String>> propMap = readAll(filePath);
        if (propMap == null || propMap.isEmpty()) {
        	return defaultVal;
        }
        
        List<String> sectionRows = findSection(propMap, section);
        if (sectionRows == null || sectionRows.isEmpty()) {
        	return defaultVal;
        }
        
        return getValue(sectionRows, key, defaultVal);
    }

    protected Map<String, List<String>> readAll(File filePath) {
        List<String> fileRows = readFile(filePath);
        if (fileRows == null || fileRows.isEmpty()) {
        	return null;
        }

        Map<String, List<String>> propMap = new LinkedHashMap<>();
        List<String> sections = getSections(fileRows);
        
        for (String section : sections) {
        	propMap.put(section, getSectionRows(fileRows, section));
		}
        
        return propMap;
    }
    
    protected List<String> getSections(List<String> fileRows) {
    	List<String> sections = new ArrayList<>();
    	for (String row : fileRows) {
		    if (row.startsWith(OPEN_SECTION) && row.endsWith(CLOSE_SECTION)) {
		    	sections.add(row);
		    }
    	}
        
    	return sections;
    }

    protected List<String> getSectionRows(List<String> fileRows, String section) {
    	List<String> sectionRows = new ArrayList<>();
    	boolean found = false;
    	for (String row : fileRows) {
		    if (found && row.startsWith(OPEN_SECTION)) {
		    	break;
		    }
    		if (found || row.contains(section)) {
    			found = true;
    			sectionRows.add(row);
    		}
		    
    	}
    	
    	sectionRows.remove(0);
        
    	return sectionRows;
    }

    protected List<String> findSection(Map<String, List<String>> propMap, String section) {
        if (section == null) {
        	section = "";
        }
    	section = putBracket(section);
        
    	return propMap.get(section);
    }
    
    protected String putBracket(String section) {
    	if (!section.startsWith(OPEN_SECTION)) {
    		section = OPEN_SECTION + section;
    	}
    	if (!section.endsWith(CLOSE_SECTION)) {
    		section = section + CLOSE_SECTION;
    	}
    	
    	return section;
    }

    protected String getValue(List<String> sectionRows, String key, String defaultVal) {
        if (key == null || key.isEmpty()) {
            return defaultVal;
        }
        
        String findKey = key + EQUALS;
        for (String row : sectionRows) {
			if (row.contains(findKey)) {
				return row.substring(row.indexOf(EQUALS) + 1);
			}
		}
        
        return defaultVal;
    }
}
