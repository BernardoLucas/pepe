package com.mangyBear.Pepe.ui.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.text.PlainDocument;

import com.mangyBear.Pepe.ui.domain.CellValue;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class SuggestCombo extends JComboBox<SuggestDoubleColumn> {

	private static final long serialVersionUID = 6371193478189821961L;
	
	private final boolean hideButton;
	
	public SuggestCombo(String beginText, boolean hideButton, Color beginColor, int secondColumnSize, Color secondColumnColor) {
		this.hideButton = hideButton;
		updateUI();
        setEditable(true);
        setSelectedIndex(-1);
        setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
        setPreferredSize(new Dimension(getPreferredSize().width, getPreferredSize().height + 2));
        if (secondColumnSize >= 0) {
            setRenderer(new MultiColumn(secondColumnSize, secondColumnColor));
        }
        new FilterColumn(this, beginText, beginColor);
    }

    @Override
    public void updateUI() {
        super.updateUI();
        if (hideButton) {
	        UIManager.put("ComboBox.squareButton", Boolean.FALSE);
	        setUI(new BasicComboBoxUI() {
	            @Override
	            protected JButton createArrowButton() {
	                JButton b = new JButton();
	                b.setBorder(BorderFactory.createEmptyBorder());
	                b.setVisible(false);
	                return b;
	            }
	        });
        }
    }
}

class MultiColumn extends JPanel implements ListCellRenderer<SuggestDoubleColumn> {

	private static final long serialVersionUID = 8087482217463528023L;
	
	private final JLabel leftLabel;
    private final JLabel rightLabel;

    public MultiColumn(int rightWidth, Color secondColumnColor) {
        super(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));

        leftLabel = new JLabel();
        leftLabel.setOpaque(false);
        leftLabel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));

        final Dimension dim = new Dimension(rightWidth, 0);
        rightLabel = new JLabel() {
        	
			private static final long serialVersionUID = 7817541139173692169L;

			@Override
            public Dimension getPreferredSize() {
                return dim;
            }
        };
        rightLabel.setOpaque(false);
        rightLabel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        rightLabel.setForeground(secondColumnColor);
        rightLabel.setHorizontalAlignment(SwingConstants.CENTER);

        this.add(leftLabel);
        this.add(rightLabel, BorderLayout.EAST);
    }

    @Override
    public Component getListCellRendererComponent(JList list, SuggestDoubleColumn value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value != null) {
            String html = changeColor(((CellValue) value.getLeftText()).toString(), value.getSearchFor());
            leftLabel.setText("<html><head></head><body style=\"color: darkgray;\">" + html + "</body></head>");
            rightLabel.setText(value.getRightText());
        }

        leftLabel.setFont(list.getFont());
        rightLabel.setFont(list.getFont());

        if (index < 0) {
            leftLabel.setForeground(list.getForeground());
            this.setOpaque(false);
        } else {
            leftLabel.setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
            this.setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
            this.setOpaque(true);
        }
        return this;
    }

    private String changeColor(String string, String searchFor) {
        List<String> notMatching = new ArrayList<>();
        int searchForLength = 0;
        if (searchFor != null && !searchFor.isEmpty()) {
            searchForLength = searchFor.length();
            String upperString = string.toUpperCase();
            String upperSearchFor = searchFor.toUpperCase();
            int fs;
            int lastFs = 0;
            while ((fs = upperString.indexOf(upperSearchFor, (lastFs == 0) ? -1 : lastFs)) > -1) {
                notMatching.add(string.substring(lastFs, fs));
                lastFs = fs + searchForLength;
            }
            notMatching.add(string.substring(lastFs));
        }
        String html = string;
        if (notMatching.size() > 1) {
            html = notMatching.get(0);
            int start = html.length();
            for (int i = 1; i < notMatching.size(); i++) {
                String t = notMatching.get(i);
                html += "<b style=\"color: black;\">" + string.substring(start, start + searchForLength) + "</b>" + t;
                start += searchForLength + t.length();
            }
        }
        return html;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        return new Dimension(0, d.height + 2);
    }

    @Override
    public void updateUI() {
        super.updateUI();
        this.setName("List.cellRenderer");
    }
}

class FilterColumn extends PlainDocument {

	private static final long serialVersionUID = 4801766926091468648L;
	
	private final String text;
	private final Color beginColor;
    private final JTextField textfield;
    private boolean isFilled = false;

    public FilterColumn(JComboBox<SuggestDoubleColumn> comboBox, String beginText, Color beginColor) {
    	this.text = beginText;
        this.beginColor = beginColor;
        this.textfield = (JTextField) comboBox.getEditor().getEditorComponent();
        this.textfield.setForeground(beginColor);
        if (beginText != null && !beginText.isEmpty()) {
            this.textfield.setText(beginText);
        }
        addListener();
    }

    private void addListener() {
        textfield.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                if (!isFilled && textfield.getText().equals(text) && textfield.getForeground().equals(beginColor)) {
                    textfield.setForeground(Color.BLACK);
                    textfield.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (textfield.getText().isEmpty()) {
                    textfield.setForeground(beginColor);
                    if (text != null && !text.isEmpty()) {
                        textfield.setText(text);
                    }
                    isFilled = false;
                } else {
                    isFilled = true;
                }
            }
        });
    }
}
