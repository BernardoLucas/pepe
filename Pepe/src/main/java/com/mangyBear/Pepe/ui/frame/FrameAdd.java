package com.mangyBear.Pepe.ui.frame;

import static com.mangyBear.Pepe.service.MyConstants.OS_NAME;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.domain.entity.ActionParameter;
import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.entity.FinderParameter;
import com.mangyBear.Pepe.domain.entity.VarObject;
import com.mangyBear.Pepe.domain.entity.VarObjectParameter;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.entity.VariableParameter;
import com.mangyBear.Pepe.domain.types.action.ActionType;
import com.mangyBear.Pepe.domain.types.action.ByType;
import com.mangyBear.Pepe.domain.types.action.FinderType;
import com.mangyBear.Pepe.domain.types.desktopParams.DesktopType;
import com.mangyBear.Pepe.domain.types.frame.ComboBoxType;
import com.mangyBear.Pepe.domain.types.frame.FrameAddType;
import com.mangyBear.Pepe.domain.types.frame.FrameFieldType;
import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.domain.types.variable.VariableType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.component.ComponentsTable;
import com.mangyBear.Pepe.ui.component.ExtensionFilter;
import com.mangyBear.Pepe.ui.component.SuggestCombo;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;
import com.mangyBear.Pepe.ui.controller.ActionParameterController;
import com.mangyBear.Pepe.ui.controller.FinderParameterController;
import com.mangyBear.Pepe.ui.controller.VariableController;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mangyBear.Pepe.ui.listener.CaptureListener;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameAdd extends BaseFrame {

	private static final long serialVersionUID = 2195626589862617607L;

	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
	private final JDialog parent;
	private final int VERTICAL_SPACING = 10;
	private CaptureListener captureListener;
	private int editingType = 0;
	private JLabel labelInformation;
	private JButton btnCapture;
	private JButton btnAdd;
	private JButton btnCancel;

	private Object selected;
	private Object objectToShow;
	private FrameAddType helpAndFrameType;
	private List<FrameFieldType> frameFieldTypeList;
	private ComboBoxType comboBoxType;
	private boolean hasActionReturn = false;
	private Integer[][] checkBoxCells;
	private Integer[][] comboBoxCells;
	private Integer[][] panelDialogCells;
	private Integer[][] suggestDialogCells;
	private Integer[][] panelFileChooserCells;
	private Integer[][] suggestFileChooserCells;
	private Integer[][] suggestCells;
	private Integer[][] suggestButtonCells;
	private Integer[][] notEditableCells;
	private Map<Integer[], FrameFieldType> filteringCells;
	private final ComponentsTable table;
	public DefaultTableModel model;
	private boolean suggestWasArrows;

	private String[] valuesToChange;

	public FrameAdd(JDialog parent, Object selected) {
		super(parent, "Add", true);
		this.blockerLoading = new BlockerLoading(this, null, true);
		this.bindingFactory = new BindingFactory();
		this.parent = parent;
		this.selected = selected;
		initGUI();
		getParamsAndValues(selected);
		createComponents();
		addComponents();

		findRowValueType();
		table = new ComponentsTable(new String[] { "Parameter", "Value" },
				checkBoxCells, comboBoxCells, panelDialogCells, suggestDialogCells,
				panelFileChooserCells, suggestFileChooserCells, suggestCells,
				suggestButtonCells, notEditableCells, filteringCells, false);
		setTable();
		addListeners();
		setSizeAndLocation();
		addTable();
	}

	private void initGUI() {
		setLayout(new RelativeLayout());
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	private void getParamsAndValues(Object selected) {
		if (selected instanceof List<?>) { // Editing an already added Finder/Action/VarObject/Variable
			editingType = 1;
			List<?> valueList = (List<?>) selected;
			Object obj = valueList.get(0);
			if (obj instanceof Action || obj instanceof Variable) {
				selected = valueList.get(4);
			} else {
				selected = valueList.get(3);
			}
			valuesToChange = valueList.get(valueList.size() - 1).toString().split(";");
		}
		if (selected instanceof Finder) { // Editing an already saved but not added Finder
			editingType = 2;
			Finder finder = (Finder) selected;
			selected = finder.getFinderType();
			valuesToChange = paramToArray(finder.getParameters());
		}
		if (selected instanceof FinderType) {
			FinderType finderType = (FinderType) selected;
			setObjectToShow(finderType);
			helpAndFrameType = finderType.getFrameAddType();
			frameFieldTypeList = finderType.getFrameAddType().getFrameFieldType();
			comboBoxType = finderType.getFrameAddType().getComboBoxType();
		}
		if (selected instanceof ActionType) {
			ActionType actionType = (ActionType) selected;
			setObjectToShow(actionType);
			helpAndFrameType = actionType.getFrameAddType();
			frameFieldTypeList = actionType.getFrameAddType().getFrameFieldType();
			comboBoxType = actionType.getFrameAddType().getComboBoxType();
			hasActionReturn = actionType.getReflectionType().getReflectionReturn() != null;
		}
		if (selected instanceof Variable) { // Editing an already saved but not added Variable
			editingType = 2;
			Variable var = (Variable) selected;
			selected = var.getVariableType();
			valuesToChange = paramToArray(var.getParameters());
		}
		if (selected instanceof VariableType) {
			VariableType varType = (VariableType) selected;
			setObjectToShow(varType);
			helpAndFrameType = varType.getFrameAddType();
			frameFieldTypeList = varType.getFrameAddType().getFrameFieldType();
			comboBoxType = varType.getFrameAddType().getComboBoxType();
		}
		if (selected instanceof VarObject) { // Editing an already saved but not added VarObject
			editingType = 2;
			VarObject varObj = (VarObject) selected;
			selected = varObj.getType();
			valuesToChange = paramToArray(varObj.getParameters());
		}
		if (selected instanceof VariableGroupType) {
			VariableGroupType varObjType = (VariableGroupType) selected;
			setObjectToShow(varObjType);
			helpAndFrameType = varObjType.getFrameAddType();
			frameFieldTypeList = varObjType.getFrameAddType().getFrameFieldType();
			comboBoxType = varObjType.getFrameAddType().getComboBoxType();
		}
	}
	
	private String[] paramToArray(List<?> paramList) {
		String[] toReturn = new String[paramList.size()];
		for (int i = 0; i < paramList.size(); i++) {
			Object objParam = paramList.get(i);
			if (objParam instanceof FinderParameter) {
				toReturn[i] = ((FinderParameter) objParam).toString();
			}
			if (objParam instanceof VariableParameter) {
				toReturn[i] = ((VariableParameter) objParam).toString();
			}
			if (objParam instanceof VarObjectParameter) {
				toReturn[i] = ((VarObjectParameter) objParam).toString();
			}
		}
		return toReturn;
	}
	
	private void createComponents() {
		labelInformation = new JLabel(MyConstants.setIconSize("images/FlatInformation.png", 18, 18));
		btnCapture = new JButton("Capture");
		btnCapture.setVisible(false);
		btnAdd = new JButton(editingType == 1 ? "Update" : "Add");
		btnCancel = new JButton("Cancel");
	}

	private void addComponents() {
		add(blockerLoading.createLoading(getLoadingDivider(), 5), loadingBinding());

		add(labelInformation, labelInformationBinding());
		add(btnCapture, btnCaptureBinding());
		add(btnAdd, btnAddBinding());
		add(btnCancel, btnCancelBinding());
	}

	private int getLoadingDivider() {
		int tableRows = helpAndFrameType.getLabels().size();
		if (tableRows == 2) {
			return 15 + tableRows;
		}
		if (tableRows > 2) {
			return 18 + tableRows;
		}
		return 12;
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private RelativeConstraints btnCaptureBinding() {
		Binding[] btnCaptureBinding = new Binding[2];
		btnCaptureBinding[0] = bindingFactory.horizontallyCenterAlignedWith(Binding.PARENT);
		btnCaptureBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnCancel);
		return new RelativeConstraints(btnCaptureBinding);
	}

	private RelativeConstraints btnAddBinding() {
		Binding[] btnAddBinding = new Binding[2];
		btnAddBinding[0] = bindingFactory.leftOf(btnCancel);
		btnAddBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnCancel);
		return new RelativeConstraints(btnAddBinding);
	}

	private RelativeConstraints btnCancelBinding() {
		Binding[] btnCancelBinding = new Binding[2];
		btnCancelBinding[0] = bindingFactory.rightEdge();
		btnCancelBinding[1] = bindingFactory.bottomEdge();
		return new RelativeConstraints(btnCancelBinding);
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private void findRowValueType() {
		if (frameFieldTypeList != null && !frameFieldTypeList.isEmpty()) {
			checkBoxCells = new Integer[][] {};
			comboBoxCells = new Integer[][] {};
			panelDialogCells = new Integer[][] {};
			suggestDialogCells = new Integer[][] {};
			panelFileChooserCells = new Integer[][] {};
			suggestFileChooserCells = new Integer[][] {};
			suggestCells = new Integer[][] {};
			suggestButtonCells = new Integer[][] {};
			filteringCells = new HashMap<>();
			notEditableCells = new Integer[][] {};
			int fieldSize = frameFieldTypeList.size();
			for (int i = 0; i < fieldSize; i++) {
				FrameFieldType frameFieldType = frameFieldTypeList.get(i);
				switch (frameFieldType) {
					case CHECK:
						checkBoxCells = addElement(checkBoxCells, new Integer[] { i, 1 });
						break;
					case COMBO:
						comboBoxCells = addElement(comboBoxCells, new Integer[] { i, 1 });
						break;
					case COMBO_SUGGEST:
						suggestButtonCells = addElement(suggestButtonCells, new Integer[] { i, 1 });
						break;
					case PANEL_DIALOG:
						panelDialogCells = addElement(panelDialogCells, new Integer[] { i, 1 });
						break;
					case PANEL_DIALOG_SUGGEST:
						suggestDialogCells = addElement(suggestDialogCells, new Integer[] { i, 1 });
						break;
					case PANEL_FILE_CHOOSER:
						panelFileChooserCells = addElement(panelFileChooserCells, new Integer[] { i, 1 });
						break;
					case PANEL_FILE_CHOOSER_SUGGEST:
						suggestFileChooserCells = addElement(suggestFileChooserCells, new Integer[] { i, 1 });
						break;
					case SUGGEST:
						suggestCells = addElement(suggestCells, new Integer[] { i, 1 });
						break;
					default:
						filteringCells.put(new Integer[] { i, 1 }, frameFieldType);
				}
				notEditableCells = addElement(notEditableCells, new Integer[] { i, 0 });
			}
		}
	}

	private Integer[][] addElement(Integer[][] a, Integer[] e) {
		a = Arrays.copyOf(a, a.length + 1);
		a[a.length - 1] = e;
		return a;
	}

	private void setTable() {
		model = (DefaultTableModel) table.getModel();

		int columnZeroWidth = calcColumnWidth();
		table.setRowHeight(24);
		table.getColumnModel().getColumn(0).setMinWidth(columnZeroWidth);
		table.getColumnModel().getColumn(0).setMaxWidth(columnZeroWidth);
		model.setRowCount(helpAndFrameType.getLabels().size());
		putParmsOnColumnZero();
		activateComponentsAndPopulate();
	}

	private int calcColumnWidth() {
		int headerLength = 9;
		int biggerLength = 0;
		for (String parameter : helpAndFrameType.getLabels()) {
			int paramLength = parameter.length();
			biggerLength = (biggerLength < paramLength) ? paramLength : biggerLength;
		}
		return (headerLength >= biggerLength) ? headerLength * 10 : biggerLength * 8;
	}

	private void putParmsOnColumnZero() {
		for (int i = 0; i < helpAndFrameType.getLabels().size(); i++) {
			String parmaterName = helpAndFrameType.getLabels().get(i);
			model.setValueAt(parmaterName, i, 0);
		}
	}

	private void activateComponentsAndPopulate() {
		for (int i = 0; i < table.getRowCount(); i++) {
			TableCellEditor tce = table.getCellEditor(i, 1);
			Component component = tce.getTableCellEditorComponent(table, tce, false, i, 1);
			if (component != null) {
				
				if (component instanceof JComboBox && !(component instanceof SuggestCombo)) {
					JComboBox combo = (JComboBox) component;
					((JLabel)combo.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
					if (comboBoxType != null) {
						for (Object item : comboBoxType.getComboItems()) {
							combo.addItem(item);
						}
					}
				}
				
				boolean lastRow = i == table.getRowCount() - 1;
				
				if (component instanceof JPanel) {
					activatePanelComponent((JPanel) component, i, lastRow);
				}
				
				if (component instanceof SuggestCombo) {
					activateSuggest((SuggestCombo) component, i, lastRow);
				}
				
				if (editingType > 0) {
					populateTable(table, component, i);
				}
			}
		}
	}
	
	private void populateTable(JTable table, Component component, int i) {
		String colNameOne = ((String) table.getValueAt(i, 0)).toUpperCase();
		for (String value : valuesToChange) {
			String[] values = value.split("=", 2);
			String colNameTwo = values[0] != null ? values[0].toUpperCase() : "";
			if (colNameOne.equals(colNameTwo)) {
				populateRowValue(component, values.length > 1 ? values[1] : "", i);
			}
		}
	}
	
	private void populateRowValue(Component component, String toPut, int i) {
		if (component instanceof JCheckBox) {
			table.setValueAt(Boolean.getBoolean(toPut), i, 1);
		} else if (component instanceof JComboBox) {
			table.setValueAt(getComboBoxItem((JComboBox) component, toPut), i, 1);
		} else {
			table.setValueAt(toPut, i, 1);
		}
	}
	
	private Object getComboBoxItem(JComboBox<Object> combo, String toSelect) {
		for (int i = 0; i < combo.getItemCount(); i++) {
			Object obj = combo.getItemAt(i);
			if (obj.toString().equals(toSelect)) {
				return obj;
			}
		}
		
		return toSelect;
	}

	private void activatePanelComponent(JPanel panel, int row, boolean lastRow) {
		if (getObjectToShow() instanceof FinderType && FinderType.FILE.equals(((FinderType) getObjectToShow()))) {
			createFileChooserButton(panel, new String[] { ".exe", ".bat", ".com", ".pif", ".csv", ".ini", ".pepe", ".xml" }, row, lastRow);
		}
		if (getObjectToShow() instanceof VariableGroupType && VariableGroupType.VARIABLE_FILE_GROUP.equals(((VariableGroupType) getObjectToShow()))) {
			createFileChooserButton(panel, new String[] { ".csv", ".ini", ".pepe", ".xml" }, row, lastRow);
		}
		if (getObjectToShow() instanceof VariableGroupType && VariableGroupType.VARIABLE_WEBDRIVER_GROUP.equals(((VariableGroupType) getObjectToShow()))) {
			createFileChooserButton(panel, new String[] { ".exe" }, row, lastRow);
		}
		
		if (getObjectToShow() instanceof VariableType && 
				(VariableType.VARIABLE_SQL_QUERY.equals(((VariableType) getObjectToShow()))
						|| VariableType.VARIABLE_WEBDRIVER_CHROME.equals(((VariableType) getObjectToShow()))
						|| VariableType.VARIABLE_WEBDRIVER_FIREFOX.equals(((VariableType) getObjectToShow()))
						|| VariableType.VARIABLE_WEBDRIVER_IE.equals(((VariableType) getObjectToShow())))) {
			createQueryShowButton(panel, row, lastRow);
		}
	}

	private void createFileChooserButton(final JPanel panel, final String[] extensions, final int row, boolean lastRow) {
		final SuggestCombo suggest = getPanelSuggestMultiColumn(panel);
		final JTextField text = getPanelTextField(panel);
		if (suggest != null) {
			activateSuggest(suggest, row, lastRow);
		}
		
		JButton button = getPanelJButton(panel);
		button.setText("...");
		button.setMargin(new Insets(0, 2, 0, 2));
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new ExtensionFilter(extensions));
				Object filePath = getFieldText(suggest, text);
				if (filePath != null && filePath.toString().length() > 0) {
					chooser.setCurrentDirectory(new File(filePath.toString()));
				}
				int returnVal = chooser.showOpenDialog(panel);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					setFieldText(suggest, text, chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
	}

	private void createQueryShowButton(final JPanel panel, final int row, boolean lastRow) {
		final SuggestCombo suggest = getPanelSuggestMultiColumn(panel);
		final JTextField text = getPanelTextField(panel);
		if (suggest != null) {
			activateSuggest(suggest, row, lastRow);
		}
		
		JButton button = getPanelJButton(panel);
		button.setText("...");
		button.setMargin(new Insets(0, 2, 0, 2));
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				final JTextArea textArea = new JTextArea();
				textArea.setLineWrap(true);
				JScrollPane scrollbar = new JScrollPane(textArea);
				JDialog dialogQuery = new JDialog(FrameAdd.this, "", true);
				dialogQuery.setSize(400, 300);
				dialogQuery.setLocationRelativeTo(FrameAdd.this);
				dialogQuery.setLayout(new BorderLayout());
				dialogQuery.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				dialogQuery.addWindowListener(new WindowAdapter() {
					
					@Override
					public void windowClosing(WindowEvent windowEvent) {
						setFieldText(suggest, text, textArea.getText());
					}
				});
				
				Object fieldQuery = getFieldText(suggest, text);
				if (fieldQuery != null) {
					textArea.setText(fieldQuery.toString());
				}
				dialogQuery.add(scrollbar, BorderLayout.CENTER);
				dialogQuery.setVisible(true);
			}
		});
	}
	
	private JTextField getPanelTextField(JPanel panel) {
		Component[] components = panel.getComponents();
		for (Component component : components) {
			if (component instanceof JTextField) {
				return (JTextField) component;
			}
		}
		return null;
	}

	private SuggestCombo getPanelSuggestMultiColumn(JPanel panel) {
		Component[] components = panel.getComponents();
		for (Component component : components) {
			if (component instanceof SuggestCombo) {
				return (SuggestCombo) component;
			}
		}
		return null;
	}

	private JButton getPanelJButton(JPanel panel) {
		Component[] components = panel.getComponents();
		for (Component component : components) {
			if (component instanceof JButton) {
				return (JButton) component;
			}
		}
		return null;
	}
	
	private Object getFieldText(SuggestCombo suggest, JTextField text) {
		if (suggest != null) {
			return suggest.getSelectedItem();
		}
		if (text != null){
			return text.getText();
		}
		
		return null;
	}
	
	private void setFieldText(SuggestCombo suggest, JTextField text, String stringToSet) {
		if (suggest != null) {
			suggest.setSelectedItem(stringToSet != null ? stringToSet : "");
		}
		if (text != null) {
			text.setText(stringToSet != null ? stringToSet : "");
		}
	}

	private void activateSuggest(final SuggestCombo suggest, final int row, final boolean lastRow) {
		suggest.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = suggest.getSelectedItem();
						if (item != null) {
							if (suggestWasArrows && key == KeyEvent.VK_ENTER) {
								suggest.hidePopup();
								suggestWasArrows = false;
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(suggest, item.toString(), row, lastRow);
							} else {
								suggestWasArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		for (Integer[] cell : suggestButtonCells) {
			if (row == cell[0]) {
				suggest.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						showSuggestions(suggest, "", row, lastRow);
					}
				});
			}
		}
	}

	public void comboFilter(final SuggestCombo suggest, final String searchFor, final int row, final boolean lastRow) {
		blockerLoading.start("Searching equivalences");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				showSuggestions(suggest, searchFor, row, lastRow);
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}
	
	private void showSuggestions(SuggestCombo suggest, String searchFor, int row, boolean lastRow) {
		List<SuggestDoubleColumn> items = getItems(searchFor, row, lastRow);
		if (items.size() > 0) {
			suggest.setModel(new DefaultComboBoxModel(items.toArray()));
			suggest.setSelectedItem(searchFor.isEmpty() ? "" : searchFor);
			JTextField editor = (JTextField) suggest.getEditor().getEditorComponent();
			editor.setCaretPosition(editor.getDocument().getLength());
			suggest.showPopup();
		} else {
			suggest.hidePopup();
		}
		table.setEnterPressed(false);
	}

	private List<SuggestDoubleColumn> getItems(String searchFor, int row, boolean lastRow) {
		List<SuggestDoubleColumn> returnList = new ArrayList<>();
		for (Object param : populateList(searchFor, row, lastRow)) {
			returnList.add(populateSuggestLRItem(param, searchFor));
		}
		return returnList;
	}
	
	private List<?> populateList(String searchFor, int row, boolean lastRow) {
		if (searchFor == null || searchFor.isEmpty()) {
			return getFixedElements(row);
		}
		try {
			if (searchFor.startsWith("[")) {
				return (new VariableController()).findLikeName(searchFor);
			}
			
			if (getObjectToShow() instanceof FinderType) {
				return (new FinderParameterController()).findParamLikeValue(searchFor);
			}
			
			if (hasActionReturn && lastRow) {
				return (new VariableController()).findLikeName(searchFor);
			}
			
			return (new ActionParameterController()).findParamLikeValue(searchFor);

		} catch (Exception e) {
			showError(e);
			blockerLoading.stop();
		}
		
		return Arrays.asList();
	}
	
	private List<?> getFixedElements(int row) {
		for (Integer[] cell : suggestButtonCells) {
			if (row == cell[0]) {
				return comboBoxType.getComboItems();
			}
		}
		
		return new ArrayList<>();
	}
	
	private SuggestDoubleColumn populateSuggestLRItem(Object param, String searchFor) {
		if (param instanceof FinderParameter) {
			FinderParameter finderParam = (FinderParameter) param;
			return new SuggestDoubleColumn(new CellValue(finderParam.getId(), finderParam.getValue(), null, finderParam), searchFor);
		}
		if (param instanceof Variable) {
			Variable variable = (Variable) param;
			return new SuggestDoubleColumn(new CellValue(variable.getId(), variable.getName(), null, variable), searchFor);
		}
		
		if (param instanceof String || param instanceof ByType || param instanceof DesktopType) {
			return new SuggestDoubleColumn(new CellValue(0l, param.toString(), null, param), searchFor);
		}
		
		ActionParameter actionParam = (ActionParameter) param;
		return new SuggestDoubleColumn(new CellValue(actionParam.getId(), actionParam.getValue(), null, actionParam), searchFor);
	}

	private void addListeners() {
		labelInformation();
		btnCapture();
		btnAdd();
		btnCancelAndClosing();
	}

	private void labelInformation() {
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameAdd.this, getObjectToShow())).setVisible(true);
			}
		});
	}

	private void btnCapture() {
		if (getObjectToShow() instanceof FinderType && ((FinderType) getObjectToShow()).isCatchable()) {
			activeBtnCapture((FinderType) getObjectToShow());
		}
		if (getObjectToShow() instanceof ActionType && ActionType.NOFINDER_DESKTOP_PRINT_SCREEN.equals(((ActionType) getObjectToShow()))) {
			activeBtnCapture(FinderType.SCREEN_REGION);
		}
	}
	
	private void activeBtnCapture(final FinderType type) {
		btnCapture.setVisible(true);
		btnCapture.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				captureListener = new CaptureListener(FrameAdd.this, type);
			}
		});
	}

	private void btnAdd() {
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				blockerLoading.start("Adding parameter");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						boolean success = false;
						if (table.isEditing()) {
							table.getCellEditor().stopCellEditing();
						}
						
						if (parent instanceof FrameStep) {
							success = doFrameStepCallBack();
						}
						
						if (parent instanceof FrameFlow) {
							success = doFrameFlowCallBack();
						}
						
						blockerLoading.stop();
						if (success) {
							dispose();
						}
						return null;
					}
				}.execute();
			}
		});
	}
	
	private boolean doFrameStepCallBack() {
		if (editingType == 1) {
			((FrameStep) parent).changeCallBack(getObjectToShow(), model);
		} else {
			return ((FrameStep) parent).addCallBack(getObjectToShow(), model);
		}
		
		return true;
	}
	
	private boolean doFrameFlowCallBack() {
		return ((FrameFlow) parent).frameAddCallBack(model);
	}

	private void btnCancelAndClosing() {
		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (captureListener != null) {
					captureListener.clear();
				}
				if (parent instanceof FrameFlow) {
//					doFrameFlowCallBack();
				}
				dispose();
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (captureListener != null) {
					captureListener.clear();
				}
			}
		});
	}

	private void setSizeAndLocation() {
		setSize(new Dimension(650, calcFrameHeight()));
		setLocationRelativeTo(parent);
	}

	private int calcTableHeight() {
		int headerHeight = 17;
		return (table.getRowHeight() * helpAndFrameType.getLabels().size()) + headerHeight;
	}

	private int calcFrameHeight() {
		return calcTableHeight() + (OS_NAME.equals("Windows 10") ? 89 : 88);
	}

	private void addTable() {
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		add(new JScrollPane(table), actionTableBinding());
	}

	private RelativeConstraints actionTableBinding() {
		Binding[] actionsTableBinding = new Binding[4];
		actionsTableBinding[0] = bindingFactory.topEdge();
		actionsTableBinding[1] = bindingFactory.leftEdge();
		actionsTableBinding[2] = bindingFactory.rightEdge();
		actionsTableBinding[3] = bindingFactory.above(btnAdd);
		return new RelativeConstraints(actionsTableBinding);
	}

	public Object getObjectToShow() {
		return objectToShow;
	}

	public void setObjectToShow(Object objectToShow) {
		this.objectToShow = objectToShow;
	}
}
