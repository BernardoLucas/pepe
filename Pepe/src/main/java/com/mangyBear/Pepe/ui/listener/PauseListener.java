package com.mangyBear.Pepe.ui.listener;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import com.mangyBear.Pepe.service.ExecuteFlowService;

public class PauseListener implements NativeKeyListener {


	public PauseListener() {
		regNativeHook();
		GlobalScreen.addNativeKeyListener(this);
	}

	// NativeKeyListener
	public void nativeKeyTyped(NativeKeyEvent e) {
	}

	public void nativeKeyPressed(NativeKeyEvent e) {
	}

	public void nativeKeyReleased(NativeKeyEvent e) {
		int keyCode = e.getKeyCode();
		if (keyCode == NativeKeyEvent.VC_ESCAPE) {
			ExecuteFlowService.setExecStatus(1);
		}
	}

	public void clear() {
		GlobalScreen.removeNativeKeyListener(this);
		unregNativeHook();
	}

	// Methods
	private void regNativeHook() {
		if (!GlobalScreen.isNativeHookRegistered()) {
			try {
				GlobalScreen.registerNativeHook();
				// Get the logger for "org.jnativehook" and set the level to warning.
				Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
				logger.setLevel(Level.WARNING);
			} catch (NativeHookException ex) {
				System.err.println(ex.getMessage());
			}
		}
	}

	private void unregNativeHook() {
		if (GlobalScreen.isNativeHookRegistered()) {
			try {
				GlobalScreen.unregisterNativeHook();
			} catch (NativeHookException ex) {
				System.err.println(ex.getMessage());
			}
		}
	}
}
