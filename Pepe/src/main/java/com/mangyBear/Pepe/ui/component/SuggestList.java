package com.mangyBear.Pepe.ui.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.TransferHandler;

public class SuggestList extends JList<SuggestDoubleColumn> {

	private static final long serialVersionUID = 2837743696028703380L;
	
	private final DefaultListModel<SuggestDoubleColumn> addedModel;
	
	public SuggestList(DefaultListModel<SuggestDoubleColumn> addedModel) {
		this.addedModel = addedModel;
		addModel();
		setCellRenderer(new SuggestLRList(90, Color.DARK_GRAY));
		setTransferHandler(new ListTransferHandler());
		setBackground(Color.WHITE);
	}
	
	private void addModel() {
		if (addedModel != null) {
			setModel(addedModel);
		}
	}

	public DefaultListModel<SuggestDoubleColumn> getAddedModel() {
		return addedModel;
	}
}

class SuggestLRList extends JPanel implements ListCellRenderer<SuggestDoubleColumn> {

	private static final long serialVersionUID = 8087482217463528023L;

	private final JLabel leftLabel;
	private final JLabel rightLabel;

	public SuggestLRList(int rightWidth, Color secondColumnColor) {
		super(new BorderLayout());
		this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));

		leftLabel = new JLabel();
		leftLabel.setOpaque(false);
		leftLabel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		leftLabel.setHorizontalAlignment(SwingConstants.LEADING);

		final Dimension dim = new Dimension(rightWidth, 0);
		rightLabel = new JLabel() {

			private static final long serialVersionUID = 7817541139173692169L;

			@Override
			public Dimension getPreferredSize() {
				return dim;
			}
		};
		rightLabel.setOpaque(false);
		rightLabel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
		rightLabel.setForeground(secondColumnColor);
		rightLabel.setHorizontalAlignment(SwingConstants.CENTER);

		this.add(leftLabel);
		this.add(rightLabel, BorderLayout.EAST);
	}

	@Override
	public Component getListCellRendererComponent(JList list, SuggestDoubleColumn value, int index, boolean isSelected, boolean cellHasFocus) {
		if (value != null) {
			String html = changeColor(value.getLeftText().toString(), value.getSearchFor());
			leftLabel.setText("<html><head></head><body style=\"color:#0077be;\">" + html + "</body></head>");
			rightLabel.setText(value.getRightText().toString());
		}

		leftLabel.setFont(list.getFont());
		rightLabel.setFont(list.getFont());

		if (index < 0) {
			leftLabel.setForeground(list.getForeground());
			this.setOpaque(false);
		} else {
			leftLabel.setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
			this.setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
			this.setOpaque(true);
		}
		return this;
	}

	private String changeColor(String string, String searchFor) {
		List<String> notMatching = new ArrayList<>();
		int searchForLength = 0;
		if (searchFor != null && !searchFor.isEmpty()) {
			searchForLength = searchFor.length();
			String upperString = string.toUpperCase();
			String upperSearchFor = searchFor.toUpperCase();
			int fs;
			int lastFs = 0;
			while ((fs = upperString.indexOf(upperSearchFor, (lastFs == 0) ? -1 : lastFs)) > -1) {
				notMatching.add(string.substring(lastFs, fs));
				lastFs = fs + searchForLength;
			}
			notMatching.add(string.substring(lastFs));
		}
		String html = string;
		if (notMatching.size() > 1) {
			html = notMatching.get(0);
			int start = html.length();
			for (int i = 1; i < notMatching.size(); i++) {
				String t = notMatching.get(i);
				html += "<b style=\"color:#064273;\">" + string.substring(start, start + searchForLength) + "</b>" + t;
				start += searchForLength + t.length();
			}
		}
		return html;
	}

	@Override
	public Dimension getPreferredSize() {
		Dimension d = super.getPreferredSize();
		return new Dimension(0, d.height + 2);
	}

	@Override
	public void updateUI() {
		super.updateUI();
		this.setName("List.cellRenderer");
	}
}

class ListTransferHandler extends TransferHandler {

	private static final long serialVersionUID = 392485633366842635L;

	@Override
    public boolean canImport(TransferSupport support) {
        return support.isDataFlavorSupported(SuggestDoubleColumn.getLrItemFlavor());
    }

    @Override
    public int getSourceActions(JComponent c) {
        return DnDConstants.ACTION_COPY_OR_MOVE;
    }

    @Override
    protected Transferable createTransferable(JComponent component) {
        if (component instanceof JList) {
            JList<?> list = (JList<?>) component;
            List<?> values = list.getSelectedValuesList();
            List<SuggestDoubleColumn> validValues = new ArrayList<>();
            for (Object value : values) {
            	if (value instanceof SuggestDoubleColumn) {
            		SuggestDoubleColumn suggestValue = (SuggestDoubleColumn) value;
            		suggestValue.setDragSource(list.getName());
            		validValues.add((SuggestDoubleColumn) value);
            	}
			}
            return new DataHandler(validValues, SuggestDoubleColumn.getLrItemFlavor().getMimeType());
        }
        return null;
    }
}
