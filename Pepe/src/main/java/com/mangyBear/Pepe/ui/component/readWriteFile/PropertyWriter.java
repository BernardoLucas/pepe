package com.mangyBear.Pepe.ui.component.readWriteFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class PropertyWriter extends PropertyReader{

	public void writeProperty(File filePath, String section, String key, String value) {
		if (!filePath.exists()) {
			updateFile(filePath, createPropMap(section, key, value));
		} else {
			Map<String, List<String>> propMap = readAll(filePath);
			
			List<String> sectionRows = findSection(propMap, section);
			if (sectionRows == null || sectionRows.isEmpty()) {
				propMap.put(section, Arrays.asList(key + EQUALS + value)); 
	    	} else {
	    		updateProperty(findSection(propMap, section), key, value);
	    	}
			updateFile(filePath, propMap);
		}
	}
	
	private Map<String, List<String>> createPropMap(String section, String key, String value) {
		Map<String, List<String>> propMap = new HashMap<>();
		propMap.put(section, Arrays.asList(key + EQUALS + value));
		
		return propMap;
	}
	
    public void deleteProperty(File filePath, String section, String key) {
        Map<String, List<String>> propMap = readAll(filePath);
        removeProperty(findSection(propMap, section), key);
        updateFile(filePath, propMap);
    }
    
    private void updateProperty(List<String> sectionRows, String key, String value) {
    	boolean found = false;
    	String findKey = key + EQUALS;
        List<String> tempList = new ArrayList<>();
    	for (String row : sectionRows) {
    		if (row.contains(findKey)) {
    			row = findKey + value;
    			found = true;
    		}
    		tempList.add(row);
    	}
    	
    	if (!found) {
    		sectionRows.add(findKey + value);
    	} else {
	    	sectionRows.clear();
	        for (String temp : tempList) {
	        	sectionRows.add(temp);
			}
    	}
    }
    
    private void removeProperty(List<String> sectionRows, String key) {
        String findKey = key + EQUALS;
        List<String> tempList = new ArrayList<>();
        for (String row : sectionRows) {
        	if (!row.contains(findKey)) {
        		tempList.add(row);
        	}
        }
        
        if (tempList.size() == sectionRows.size()) {
        	return;
        }
        
        sectionRows.clear();
        for (String temp : tempList) {
        	sectionRows.add(temp);
		}
    }

    private void updateFile(File filepath, Map<String, List<String>> propMap) {
        boolean firstLine = true;
        try {
			Charset charset = getDefaultCharset();
			OutputStreamWriter osw;
			if (charset == null) {
				osw = new OutputStreamWriter(new FileOutputStream(filepath));
			} else {
				osw = new OutputStreamWriter(new FileOutputStream(filepath), getDefaultCharset());
			}
			
			BufferedWriter writer = new BufferedWriter(osw);
            for (Map.Entry<String, List<String>> entrySet : propMap.entrySet()) {
                if (!firstLine) {
                	writer.newLine();
                }
                
            	writer.append(entrySet.getKey());
            	
            	List<String> sectionRows = entrySet.getValue();
            	for (String row : sectionRows) {
            		writer.newLine();
                    writer.append(row);
				}
                firstLine = false;
            }
            writer.close();
		} catch (IOException e) {
		}
    }
}
