package com.mangyBear.Pepe.ui.listener;

import java.awt.Point;
import java.awt.Rectangle;

import com.mangyBear.Pepe.service.interfaces.User32;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinUser.WNDENUMPROC;

public class GetInfoWindow {

	private User32 user32 = User32.INSTANCE;
	private JNAWindow window;

	public JNAWindow getByPoint(final Point point) {

		if (point == null) {
			return null;
		}

		getInfoByPoint(point);

		return window;
	}

	public JNAWindow getByPid(int targetPid) {

		if (targetPid == 0) {
			return null;
		}
		
		getInfoByPid(targetPid);

		return window;
	}

	private void getInfoByPoint(final Point point) {
		user32.EnumWindows(new WNDENUMPROC() {

			@Override
			public boolean callback(HWND hWnd, Pointer arg1) {
				window = new JNAWindow(hWnd);

				if (!window.isVisible()) {
					return true;
				}

				Rectangle rect = window.getRectangle();
				if (rect == null || rect.x <= -32000) {// not minimized
					return true;
				}

				String title = window.getTitle();
				if (title.isEmpty()) {
					return true;
				}

				if (isClickOnFrame(point, rect)) {
					return false;
				}

				window = null;
				return true;
			}
		}, null);
	}

	private void getInfoByPid(final int targetPid) {
		JNAWindow.sleep(1500);
		window = JNAWindow.getProcessWindow(targetPid);
		if (window.isNull()) {
			JNAWindow.sleep(1500);
			window = JNAWindow.getProcessWindow(targetPid);
		}
	}

	private boolean isClickOnFrame(Point point, Rectangle rect) {
		return (point.x > rect.x && point.x < (rect.x + rect.width))
				&& (point.y > rect.y && point.y < (rect.y + rect.height));
	}
}
