package com.mangyBear.Pepe.ui.controller;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.ActionParameter;
import com.mangyBear.Pepe.service.ActionParameterService;

public class ActionParameterController {
	
	private ActionParameterService service = new ActionParameterService();

	public List<ActionParameter> findParamLikeValue(String value) throws Exception {
		return service.findParamLikeValue(value);
	}
}
