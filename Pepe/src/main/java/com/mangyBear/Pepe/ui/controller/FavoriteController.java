package com.mangyBear.Pepe.ui.controller;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Favorite;
import com.mangyBear.Pepe.service.FavoriteService;

public class FavoriteController {

	private final FavoriteService service = new FavoriteService();
	
	public void remove(Favorite favorite) throws Exception {
		service.remove(favorite);
	}
	
	public Favorite save(Favorite favorite) throws Exception {
		if (favorite.getId() != null) {
			service.update(favorite);
		} else {
			service.store(favorite);
		}
		
		return favorite;
	}
	
	public List<Favorite> findByMacLimited(int limit) throws Exception {
		return service.findByMacLimited(limit);
	}

	public List<Favorite> findByMacLikeName(String nmFavorite) throws Exception {
		return service.findByMacLikeName(nmFavorite);
	}
}
