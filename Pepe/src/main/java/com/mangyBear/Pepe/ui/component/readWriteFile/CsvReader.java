package com.mangyBear.Pepe.ui.component.readWriteFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class CsvReader extends TextFileReader {
	
	protected boolean isComma;
	
    public List<String[]> readAll(File filePath) {
        List<String[]> matrix = new ArrayList<>();
    	List<String> fileRows = readFile(filePath);
    	if (fileRows == null || fileRows.isEmpty()) {
    		return null;
    	}

        for (String fileRow : fileRows) {
            matrix.add(fileRow.split(commaOrSemicolon(fileRow)));
        }
        
        return matrix.isEmpty() ? null : matrix;
    }

    private String commaOrSemicolon(String row) {
        int countComma = 0;
        int countSemicolon = 0;
        for (int i = 0; i < row.length(); i++) {
            char c = row.charAt(i);
            if (c == ',') {
                countComma++;
            }
            if (c == ';') {
                countSemicolon++;
            }
        }
        
        setComma(countComma > countSemicolon ? true : false);
        return countComma > countSemicolon ? "," : ";";
    }

    public String readCell(File filePath, int row, int column, String defaultVal) {
        int validRow = validateInt(row);
        int validColumn = validateInt(column);

        List<String[]> matrixCsv = readAll(filePath);
        if (matrixCsv == null || matrixCsv.isEmpty()) {
        	return defaultVal;
        }

        if (validRow < matrixCsv.size() && validColumn < (matrixCsv.get(validRow)).length) {
            return (matrixCsv.get(validRow))[validColumn];
        }

        return defaultVal;
    }

	public boolean isComma() {
		return isComma;
	}

	public void setComma(boolean isComma) {
		this.isComma = isComma;
	}
}
