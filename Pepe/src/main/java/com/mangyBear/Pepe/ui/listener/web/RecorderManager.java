package com.mangyBear.Pepe.ui.listener.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.entityRun.VariableRun;
import com.mangyBear.Pepe.domain.types.action.ByType;
import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.service.ExecuteVariableService;
import com.mangyBear.Pepe.ui.controller.FinderController;
import com.mangyBear.Pepe.ui.frame.FrameRecorder;
import com.mangyBear.Pepe.ui.listener.entity.WebRecord;

public class RecorderManager {
	
	private WebDriver webDriver;
	private static HTTPSServer server;
	private PageChangeDetector detector;
	private int pageDetectorActivationTime = 2;// activate at every 2 seconds
	private final FrameRecorder frameRecorder;
	private WebRecord webRecordOld;
	
	public RecorderManager(FrameRecorder frameRecorder) {
		this.frameRecorder = frameRecorder;
	}
	
	public void addInTables(String interaction) {
		frameRecorder.addWebRecordInActionTable(buildWebRecord(interaction));
	}
	
	private WebRecord buildWebRecord(String interaction) {
		WebRecord webRecord = new WebRecord();
		for (String elementInfo : interaction.split("\t")) {
			if (!elementInfo.contains("attid:") && elementInfo.contains("id:")) {
				webRecord.setId(FinderController.buildFinderToWebRecord(elementInfo.substring(3), ByType.BY_ID, webDriver));
			} else if (elementInfo.contains("xpath:")) {
				webRecord.setXpath(FinderController.buildFinderToWebRecord(elementInfo.substring(6), ByType.BY_XPATH, webDriver));
			} else if (elementInfo.contains("name:")) {
				webRecord.setName(FinderController.buildFinderToWebRecord(elementInfo.substring(5), ByType.BY_NAME, webDriver));
			} else if (elementInfo.contains("type:")) {
				webRecord.setType(elementInfo.substring(5));
			} else if (elementInfo.contains("tag:")) {
				webRecord.setTag(elementInfo.substring(4));
			} else if (elementInfo.contains("attid:")) {
				webRecord.setAttid(FinderController.buildFinderToWebRecord(elementInfo.substring(6), ByType.BY_TAG_NAME, webDriver));
			} else if (elementInfo.contains("event:")) {
				webRecord.setEvent(elementInfo.substring(6));
			} else if (elementInfo.contains("value:")) {
				webRecord.setValue(elementInfo.substring(6));
			}
		}
		
		if (webRecordOld != null && webRecordOld.equals(webRecord)) {
			return null;
		}
		
		webRecordOld = webRecord;
		return webRecord;
	}

	//This will be called when PageChangeDetector detects a page change
	public void reportPageChanged() {
		record(null, null);
	}
	
	public void startServer() {
		//Start server to listen events on browser.
		if (server == null) {
			server = new HTTPSServer(this);
			server.start();
		}
	}
	
	public void closeServer() {
		if (server != null) {
			server.closeServer();
			server = null;
		}
	}
	
	public void closeWebDriver() {
		if (webDriver != null) {
			webDriver.quit();
			webDriver = null;
		}
	}
	
	public void record(Variable driverVariable, String openURL) {
		try {
			startServer();
			
			//Open webdriver on requested URL
			if (webDriver == null && driverVariable != null && VariableGroupType.VARIABLE_WEBDRIVER_GROUP.equals(driverVariable.getVarObject().getType())) {
				webDriver = (WebDriver) (new ExecuteVariableService()).executeVariable(new VariableRun(driverVariable));
				webDriver.get(openURL != null ? openURL : "");
			}

			// Inject JS listeners into webdriver
			if (webDriver != null && server != null && server.getPort() >= 0) {
				attachJSListeners();
			}

			//Start the page change detector
			if (webDriver != null) {
				detector = new PageChangeDetector(this, webDriver, pageDetectorActivationTime);
				detector.start();
			}

		} catch (Exception e) {
			e.printStackTrace();
			detector.stopDetecting();
			closeServer();
			closeWebDriver();
		}
	}

	private void attachJSListeners() throws FileNotFoundException {
		Boolean isHttps = isHTTPS();
		if (isHttps == null) {
			return;
		}
		
		JavascriptExecutor jse = (JavascriptExecutor) webDriver;
		File scriptFolder = new File("js");
		InputStream stream = new FileInputStream(scriptFolder.getAbsolutePath() + File.separator + "jquery.min.js");
		String jQuery = convertStreamToString(stream) + "\n";
		stream = new FileInputStream(scriptFolder.getAbsolutePath() + File.separator + "BRAP.js");
		String brapJS = convertStreamToString(stream) + "\n";
		brapJS = brapJS.replace("[PORT]", String.valueOf(server.getPort()));
		brapJS = brapJS.replace("[PROTOCOL]", isHttps ? "https" : "http");
		jse.executeScript(jQuery + brapJS);
	}
	
	private Boolean isHTTPS() {
		String currentUrl = webDriver.getCurrentUrl();
		if (currentUrl == null || currentUrl.isEmpty() || !StringUtils.containsIgnoreCase(currentUrl, "http")) {
			return null;
		}
		
		return StringUtils.containsIgnoreCase(currentUrl, "https://");
	}

	private String convertStreamToString(InputStream inputStream) {
		Scanner scanner = new Scanner(inputStream);
		String streamAsString = scanner.useDelimiter("\\A").next();
		scanner.close();
		return streamAsString;
	}
}
