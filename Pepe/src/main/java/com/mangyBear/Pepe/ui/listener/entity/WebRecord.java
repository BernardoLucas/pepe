package com.mangyBear.Pepe.ui.listener.entity;

import org.apache.commons.lang3.StringUtils;

import com.mangyBear.Pepe.domain.entity.Finder;

public class WebRecord {

	private Finder id;//Change to Finder
	private Finder xpath;//Change to Finder
	private Finder name;//Change to Finder
	private String type;
	private String tag;
	private Finder attid;//Change to Finder
	private String event;
	private String value;

	public WebRecord() {}

	public WebRecord(Finder id, Finder xpath, Finder name, String type, String tag, Finder attid, String event, String value) {
		this.id = id;
		this.xpath = xpath;
		this.name = name;
		this.type = type;
		this.tag = tag;
		this.attid = attid;
		this.event = event;
		this.value = value;
	}

	@Override
	public String toString() {
		return this.getXpath().getNmFinder();
	}

	public Finder getId() {
		return id;
	}

	public void setId(Finder id) {
		this.id = id;
	}

	public Finder getXpath() {
		return xpath;
	}

	public void setXpath(Finder xpath) {
		this.xpath = xpath;
	}

	public Finder getName() {
		return name;
	}

	public void setName(Finder name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Finder getAttid() {
		return attid;
	}

	public void setAttid(Finder attid) {
		this.attid = attid;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof WebRecord)) {
			return false;
		}
		WebRecord anObject = (WebRecord) obj;
		
		return compareFinder(this.getXpath(), anObject.getXpath())
				&& compareString(this.getEvent(), anObject.getEvent())
				&& compareString(this.getValue(), anObject.getValue());
	}
	
	private boolean compareFinder(Finder finderOne, Finder finderTwo) {
		if (finderOne == null || finderTwo == null) {
			return false;
		}
		
		return finderOne.getNmFinder().equals(finderTwo.getNmFinder());
	}
	
	private boolean compareString(String stringOne, String stringTwo) {
		if (StringUtils.isBlank(stringOne) || StringUtils.isBlank(stringTwo)) {
			return false;
		}
		
		return stringOne.equals(stringTwo);
	}
}
