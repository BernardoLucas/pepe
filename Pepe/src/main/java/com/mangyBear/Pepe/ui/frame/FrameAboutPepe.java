package com.mangyBear.Pepe.ui.frame;

import com.mangyBear.Pepe.service.MyConstants;
import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameAboutPepe extends JDialog {

	private static final long serialVersionUID = -4204427879044353476L;
	
	private final int VERTICAL_SPACING = 10;
    private final BindingFactory bindingFactory;
    private final String html1 = "<html><body style='width: ";
    private final String html2 = "px'>";
    private final int panelsWidth = 250;
    private final String lineWrap = html1 + panelsWidth + html2;
    private final String propValue = lineWrap + "<b>prop</b><p>value</html>";

    private final JPanel panelImage;
    private JLabel labelImage;
    private final JPanel panelLicense;
    private JLabel labelLicense;
    private final JPanel panelInformation;
    private JLabel labelPepeVersion;
    private JLabel labelJavaVersion;
    private JLabel labelUserDirectory;
    private JLabel labelMacAddress;

    public FrameAboutPepe(JDialog parent) {
        super(parent, "About Pépe", true);
        this.bindingFactory = new BindingFactory();
        this.panelImage = new JPanel(new RelativeLayout());
        this.panelLicense = new JPanel(new RelativeLayout());
        this.panelInformation = new JPanel(new RelativeLayout());
        initGUI(parent);
        setPanels();
    }

    private void initGUI(JDialog parent) {
        setLayout(new RelativeLayout());
        setSize(new Dimension(360, 400));
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(parent);
    }

    private void setPanels() {
        createPanelImage();
        createPanelLicense();
        createPanelInformation();
        add(panelImage, panelImageBinding());
        add(panelLicense, panelLicenseBinding());
        add(panelInformation, panelInformationBinding());
    }

    private RelativeConstraints panelImageBinding() {
        Binding[] panelImageBinding = new Binding[3];
        panelImageBinding[0] = new Binding(Edge.TOP, 5, Direction.BELOW, Edge.TOP, Binding.PARENT);
        panelImageBinding[1] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        panelImageBinding[2] = new Binding(Edge.RIGHT, 5, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        return new RelativeConstraints(panelImageBinding);
    }

    private RelativeConstraints panelLicenseBinding() {
        Binding[] panelLicenseBinding = new Binding[4];
        panelLicenseBinding[0] = bindingFactory.below(panelImage);
        panelLicenseBinding[1] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        panelLicenseBinding[2] = new Binding(Edge.RIGHT, 5, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        panelLicenseBinding[3] = new Binding(Edge.BOTTOM, 0, Direction.ABOVE, Edge.TOP, panelInformation);
        return new RelativeConstraints(panelLicenseBinding);
    }

    private RelativeConstraints panelInformationBinding() {
        Binding[] panelInformationBinding = new Binding[3];
        panelInformationBinding[0] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        panelInformationBinding[1] = new Binding(Edge.RIGHT, 5, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        panelInformationBinding[2] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
        return new RelativeConstraints(panelInformationBinding);
    }

    private void createPanelImage() {
        panelImage.setBackground(Color.WHITE);
        panelImage.setSize(new Dimension(panelsWidth, 100));
        labelImage = new JLabel();
        labelImage.setIcon(MyConstants.setIconSize("images/MangyBear.png", (panelImage.getWidth() / 5) * 4, panelImage.getHeight() / 2));
        panelImage.add(labelImage, labelImageBinding());
    }

    private RelativeConstraints labelImageBinding() {
        Binding[] labelImageBinding = new Binding[2];
        labelImageBinding[0] = bindingFactory.verticallyCenterAlignedWith(Binding.PARENT);
        labelImageBinding[1] = bindingFactory.horizontallyCenterAlignedWith(Binding.PARENT);
        return new RelativeConstraints(labelImageBinding);
    }

    private void createPanelLicense() {
        panelLicense.setBackground(Color.WHITE);
        labelLicense = new JLabel();
        labelLicense.setText(lineWrap + "Pépe Automation Center is a software from Mangy Bear Company. All rights reserved. Pépe Automation Center, Mangy Bear and their respective logos are trademarks of Mangy Bear Company. These logos cannot be altered without Mangy Bear permission. For more information, please visit www.mangybear.com/pepe");
        panelLicense.add(labelLicense, labelLicenseBinding());
    }

    private RelativeConstraints labelLicenseBinding() {
        Binding[] labelLicenseBinding = new Binding[3];
        labelLicenseBinding[0] = new Binding(Edge.TOP, 10, Direction.BELOW, Edge.TOP, Binding.PARENT);
        labelLicenseBinding[1] = new Binding(Edge.LEFT, 10, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        labelLicenseBinding[2] = new Binding(Edge.RIGHT, 10, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        return new RelativeConstraints(labelLicenseBinding);
    }

    private void createPanelInformation() {
        panelInformation.setBackground(Color.WHITE);
        labelPepeVersion = new JLabel(setText("Pépe Version: ", "Pépe Test Automation 1.0.2"));
        labelJavaVersion = new JLabel(setText("Java Version: ", System.getProperty("java.version")));
        labelUserDirectory = new JLabel(setText("Directory: ", System.getProperty("user.dir")));
        labelMacAddress = new JLabel(setText("MAC Address: ", MyConstants.MAC_ADDRESS));
        
        bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
        panelInformation.add(labelPepeVersion, labelPepeVersionBinding());
        panelInformation.add(labelJavaVersion, labelJavaVersionBinding());
        panelInformation.add(labelUserDirectory, labelUserDirectoryBinding());
        panelInformation.add(labelMacAddress, labelMacAddressBinding());
    }
    
    private String setText(String prop, String value) {
        String stringToReturn = propValue.replace("prop", prop);
        return stringToReturn.replace("value", value);
    }

    private RelativeConstraints labelPepeVersionBinding() {
        Binding[] labelPepeVersionBinding = new Binding[3];
        labelPepeVersionBinding[0] = new Binding(Edge.TOP, 10, Direction.BELOW, Edge.TOP, Binding.PARENT);
        labelPepeVersionBinding[1] = new Binding(Edge.LEFT, 10, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        labelPepeVersionBinding[2] = new Binding(Edge.RIGHT, 10, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        return new RelativeConstraints(labelPepeVersionBinding);
    }

    private RelativeConstraints labelJavaVersionBinding() {
        Binding[] labelJavaVersionBinding = new Binding[3];
        labelJavaVersionBinding[0] = bindingFactory.below(labelPepeVersion);
        labelJavaVersionBinding[1] = new Binding(Edge.LEFT, 10, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        labelJavaVersionBinding[2] = new Binding(Edge.RIGHT, 10, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        return new RelativeConstraints(labelJavaVersionBinding);
    }
    
    private RelativeConstraints labelUserDirectoryBinding() {
    	Binding[] labelUserDirectoryBinding = new Binding[3];
    	labelUserDirectoryBinding[0] = bindingFactory.below(labelJavaVersion);
    	labelUserDirectoryBinding[1] = new Binding(Edge.LEFT, 10, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
    	labelUserDirectoryBinding[2] = new Binding(Edge.RIGHT, 10, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
    	return new RelativeConstraints(labelUserDirectoryBinding);
    }

    private RelativeConstraints labelMacAddressBinding() {
        Binding[] labelMacAddressBinding = new Binding[3];
        labelMacAddressBinding[0] = bindingFactory.below(labelUserDirectory);
        labelMacAddressBinding[1] = new Binding(Edge.LEFT, 10, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        labelMacAddressBinding[2] = new Binding(Edge.RIGHT, 10, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        return new RelativeConstraints(labelMacAddressBinding);
    }
}
