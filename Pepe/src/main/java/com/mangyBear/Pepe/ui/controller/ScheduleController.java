package com.mangyBear.Pepe.ui.controller;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Schedule;
import com.mangyBear.Pepe.service.ScheduleService;

public class ScheduleController {

	private final ScheduleService service = new ScheduleService();
	
	public void remove(Schedule schedule) throws Exception {
		service.remove(schedule);
	}
	
	public Schedule save(Schedule schedule) throws Exception {
		if (schedule.getId() != null) {
			service.update(schedule);
		} else {
			service.store(schedule);
		}
		
		return schedule;
	}
	
	public Schedule findByName(String name) throws Exception {
		return service.findByName(name);
	}
	
	public List<Schedule> findLikeName(String name) throws Exception {
		return service.findLikeName(name);
	}

	public List<Schedule> findLikeNameAndFlow(String name, Long flowId) throws Exception {
		return (flowId == null || flowId < 1) ? service.findLikeNameAndMac(name) : service.findLikeNameMacAndFlow(name, flowId);
	}
	
	public List<Schedule> findActiveMacSchedulesByDay(String scheduleDay) throws Exception {
		return service.findActiveMacSchedulesByDay(scheduleDay);
	}
}
