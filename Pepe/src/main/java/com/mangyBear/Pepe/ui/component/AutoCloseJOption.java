package com.mangyBear.Pepe.ui.component;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

public class AutoCloseJOption {

	private int TIME_VISIBLE;
    private long startTime = -1;
    private Timer timer;
    private JLabel labelCountDown;
    private JLabel labelMessage;
    
//    private JOptionPane pane;
    
    public int showConfirmDialog(Component parentComponent, String message, String title, int optionType, int messageType, int timeMilliVisible) {
    	this.TIME_VISIBLE = timeMilliVisible;
//    	pane = new JOptionPane();
    	return JOptionPane.showConfirmDialog(parentComponent, createPanel(message), title, optionType, messageType);
    }
    
    private JPanel createPanel(String message) {
    	JPanel countDownPanel = new JPanel(new RelativeLayout());
    	labelCountDown = new JLabel();
    	labelMessage = new JLabel(message);
    	timer = buildTimer();
        timer.setInitialDelay(0);
        timer.start();
        countDownPanel.add(labelCountDown, labelCountDownBinding());
    	countDownPanel.add(labelMessage, labelMessageBinding());
    	
    	countDownPanel.setPreferredSize(new Dimension(getMessageLength(), 80));
    	return countDownPanel;
    }
    
    private Timer buildTimer() {
    	return new Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (startTime < 0) {
                    startTime = System.currentTimeMillis();
                }
                long now = System.currentTimeMillis();
                long clockTime = now - startTime;
                if (clockTime >= TIME_VISIBLE) {
                    clockTime = TIME_VISIBLE;
                    timer.stop();
                    JOptionPane.getRootFrame().dispose();
                }
                SimpleDateFormat df = new SimpleDateFormat("mm:ss:SSS");
                labelCountDown.setText(df.format(TIME_VISIBLE - clockTime));
            }
        });
    }
    
    private RelativeConstraints labelCountDownBinding() {
    	Binding[] labelCountDownBinding = new Binding[2];
    	labelCountDownBinding[0] = new Binding(Edge.TOP, 5, Direction.BELOW, Edge.TOP, Binding.PARENT);
    	labelCountDownBinding[1] = new Binding(Edge.RIGHT, 5, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
    	return new RelativeConstraints(labelCountDownBinding);
    }

    private RelativeConstraints labelMessageBinding() {
        Binding[] labelMessageBinding = new Binding[2];
        labelMessageBinding[0] = new Binding(Edge.VERTICAL_CENTER, 10, Direction.BELOW, Edge.VERTICAL_CENTER, Binding.PARENT);
        labelMessageBinding[1] = new Binding(Edge.LEFT, 15, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        return new RelativeConstraints(labelMessageBinding);
    }

    private int getMessageLength() {
        FontMetrics fm = labelMessage.getFontMetrics(labelMessage.getFont());
        return fm.stringWidth(labelMessage.getText()) + 15;
    }
}