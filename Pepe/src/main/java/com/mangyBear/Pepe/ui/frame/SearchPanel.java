package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import com.mangyBear.Pepe.domain.entity.Favorite;
import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.component.HintTextField;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;
import com.mangyBear.Pepe.ui.component.SuggestList;
import com.mangyBear.Pepe.ui.controller.FavoriteController;
import com.mangyBear.Pepe.ui.controller.FlowController;
import com.mangyBear.Pepe.ui.controller.LogicController;
import com.mangyBear.Pepe.ui.controller.StepController;
import com.mangyBear.Pepe.ui.domain.CellValue;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class SearchPanel extends JPanel {

	private static final long serialVersionUID = 627520838256416485L;

	private final BlockerLoading blockerLoading;
	private final FrameMain frameMain;

	// Favorites result area
	private JPanel favoritesPanel;
	// Search area
	private JToolBar favoritesToolBar;
	private HintTextField favoritesTextSearch;
	// Result area
	private JLabel favoritesLabel;
	private SuggestList favoritesList;
	private DefaultListModel<SuggestDoubleColumn> favoritesModel;
	// Search result area
	private JPanel searchResultsPanel;
	// Search area
	private JToolBar searchResultsToolBar;
	private HintTextField searchResultsTextSearch;
	// Result area
	private JLabel searchResultsLabel;
	private SuggestList searchResultsList;
	private DefaultListModel<SuggestDoubleColumn> searchResultsModel;
	// Horizontal splitPane
	private JSplitPane splitPane;
	// Information icon
	private JLabel labelInformation;

	private boolean wasArrows;

	public SearchPanel(FrameMain frameMain) {
		this.frameMain = frameMain;
		this.blockerLoading = new BlockerLoading(null, frameMain, true);
		initGUI();
		addComponents();
		addListeners();
	}

	private void initGUI() {
		setLayout(new RelativeLayout());
	}

	private void addComponents() {
		// Creates loading
		add(blockerLoading.createLoading(40, 5), loadingBinding());
		// Creates Information icon
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.PANEL_SEARCH.getIcons().get(0));
		add(labelInformation, labelInformationBinding());
		// Creates panels
		createFavoritesPanel();
		createSearchResultsPanel();
		// Create splitPane
		createSplitPane();
		add(splitPane, splitPaneBinding());
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 0, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 0, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 0, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 0, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private void createFavoritesPanel() {
		favoritesPanel = new JPanel();
		favoritesPanel.setLayout(new RelativeLayout());
		// Creates toolbar with search field
		createFavoritesToolBar();
		favoritesPanel.add(favoritesToolBar, toolBarBinding());
		// Creates separator 'Favorites'
		favoritesLabel = setLabel(FramesHelpType.PANEL_SEARCH.getLabels().get(0));
		favoritesPanel.add(favoritesLabel, labelSeparatorBinding(favoritesToolBar));
		// Creates list 'Favorites'
		setFavoritesList();
		favoritesPanel.add(new JScrollPane(favoritesList), listBinding(favoritesLabel));
	}

	private void createFavoritesToolBar() {
		favoritesToolBar = new JToolBar();
		favoritesToolBar.setLayout(new RelativeLayout());
		favoritesToolBar.setFloatable(false);
		favoritesToolBar.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));

		favoritesTextSearch = new HintTextField("Search Favorites List", Color.GRAY);
		favoritesToolBar.add(favoritesTextSearch, textSearchBinding());
	}

	private void setFavoritesList() {
		favoritesModel = new DefaultListModel<>();
		favoritesList = new SuggestList(favoritesModel);
		favoritesList.setName("Favorites");
		favoritesList.setDragEnabled(true);
		startFavoriteList(30);
	}

	private void createSearchResultsPanel() {
		searchResultsPanel = new JPanel();
		searchResultsPanel.setLayout(new RelativeLayout());
		// Creates toolbar with search field
		createSearchResultsToolBar();
		searchResultsPanel.add(searchResultsToolBar, toolBarBinding());
		// Creates separator 'Search Results'
		searchResultsLabel = setLabel(FramesHelpType.PANEL_SEARCH.getLabels().get(1));
		searchResultsPanel.add(searchResultsLabel, labelSeparatorBinding(searchResultsToolBar));
		// Creates list 'Search Results'
		setListSearchResults();
		searchResultsPanel.add(new JScrollPane(searchResultsList), listBinding(searchResultsLabel));
	}

	private void createSearchResultsToolBar() {
		searchResultsToolBar = new JToolBar();
		searchResultsToolBar.setLayout(new RelativeLayout());
		searchResultsToolBar.setFloatable(false);
		searchResultsToolBar.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));

		searchResultsTextSearch = new HintTextField("Search All Kind of Cells", Color.GRAY);
		searchResultsToolBar.add(searchResultsTextSearch, textSearchBinding());
	}

	private RelativeConstraints textSearchBinding() {
		Binding[] textSearchBinding = new Binding[4];
		textSearchBinding[0] = new Binding(Edge.TOP, 10, Direction.BELOW, Edge.TOP, Binding.PARENT);
		textSearchBinding[1] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		textSearchBinding[2] = new Binding(Edge.RIGHT, 8, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		textSearchBinding[3] = new Binding(Edge.BOTTOM, 10, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(textSearchBinding);
	}

	private RelativeConstraints toolBarBinding() {
		Binding[] toolBarBinding = new Binding[4];
		toolBarBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.TOP, Binding.PARENT);
		toolBarBinding[1] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		toolBarBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		toolBarBinding[3] = new Binding(Edge.BOTTOM, 40, Direction.BELOW, Edge.TOP, Binding.PARENT);
		return new RelativeConstraints(toolBarBinding);
	}

	private JLabel setLabel(String text) {
		JLabel labelToSet = new JLabel(text);
		labelToSet.setOpaque(true);
		labelToSet.setBackground(Color.LIGHT_GRAY);
		labelToSet.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		labelToSet.setHorizontalAlignment(SwingConstants.CENTER);

		return labelToSet;
	}

	private void setListSearchResults() {
		searchResultsModel = new DefaultListModel<>();
		searchResultsList = new SuggestList(searchResultsModel);
		searchResultsList.setName("Search");
		searchResultsList.setDragEnabled(true);
	}

	private RelativeConstraints labelSeparatorBinding(JToolBar toolBar) {
		Binding[] labelSeparatorBinding = new Binding[4];
		labelSeparatorBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.BOTTOM, toolBar);
		labelSeparatorBinding[1] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelSeparatorBinding[2] = new Binding(Edge.RIGHT, 0, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		labelSeparatorBinding[3] = new Binding(Edge.BOTTOM, 12, Direction.BELOW, Edge.BOTTOM, toolBar);
		return new RelativeConstraints(labelSeparatorBinding);
	}

	private RelativeConstraints listBinding(JLabel labelSeparator) {
		Binding[] listBinding = new Binding[4];
		listBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.BOTTOM, labelSeparator);
		listBinding[1] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		listBinding[2] = new Binding(Edge.RIGHT, 0, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		listBinding[3] = new Binding(Edge.BOTTOM, 0, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(listBinding);
	}

	private void createSplitPane() {
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		splitPane.setDividerLocation(150);
		BasicSplitPaneDivider divider = ((BasicSplitPaneUI) splitPane.getUI()).getDivider();
		divider.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseEntered(MouseEvent e) {
				setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		});
		splitPane.setTopComponent(favoritesPanel);
		splitPane.setBottomComponent(searchResultsPanel);
	}

	private RelativeConstraints splitPaneBinding() {
		Binding[] splitPaneBinding = new Binding[4];
		splitPaneBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.TOP, Binding.PARENT);
		splitPaneBinding[1] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		splitPaneBinding[2] = new Binding(Edge.RIGHT, 0, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		splitPaneBinding[3] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.TOP, labelInformation);
		return new RelativeConstraints(splitPaneBinding);
	}

	private void addListeners() {
		favoritesTextSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						String searchFor = favoritesTextSearch.getText();
						if (ke.getKeyCode() == KeyEvent.VK_ENTER && searchFor != null && searchFor.length() > 3) {
							filterFavorites(searchFor.toString());
						}
					}
				});
			}
		});
		searchResultsTextSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						String searchFor = searchResultsTextSearch.getText();
						if (ke.getKeyCode() == KeyEvent.VK_ENTER && searchFor != null && searchFor.length() > 3) {
							filterSearchResults(searchFor.toString());
						}
					}
				});
			}
		});
		addKeyListenerToList(favoritesList);
		addMouseListenerToList(favoritesList);
		addKeyListenerToList(searchResultsList);
		addMouseListenerToList(searchResultsList);
		favoritesList.setDropTarget(new DropTarget() {

			private static final long serialVersionUID = 5783614583202427354L;

			@Override
			public synchronized void drop(DropTargetDropEvent dtde) {
				this.changeToNormal();
				try {
					blockerLoading.start("Getting Valid Elements");
					final List<CellValue> dropList = MyConstants.getCellValuesToDrop(dtde);
	
					new SwingWorker<Void, Void>() {
						@Override
						protected Void doInBackground() throws Exception {
							addElementToFavoriteList(dropList);
							blockerLoading.stop();
							return null;
						}
					}.execute();
				} catch (Exception e) {
					showError(e);
				}
			}

			@Override
			public synchronized void dragEnter(DropTargetDragEvent dtde) {
				// Change JList background...
				favoritesList.setBackground(Color.LIGHT_GRAY);
				// Store item been dragged
				GraphPanel graphPanel = frameMain.getSelectedGraphPanel();
				if (graphPanel != null) {
					graphPanel.addDropTargetToSelectedTab();
				}
			}

			@Override
			public synchronized void dragExit(DropTargetEvent dtde) {
				this.changeToNormal();
			}

			private void changeToNormal() {
				// Set cursor to default.
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				// Set background to normal...
				favoritesList.setBackground(Color.WHITE);
			}
		});
		searchResultsList.setDropTarget(new DropTarget() {

			private static final long serialVersionUID = -1722255433513223659L;

			@Override
			public synchronized void drop(DropTargetDropEvent dtde) {
				this.changeToNormal();
			}

			@Override
			public synchronized void dragEnter(DropTargetDragEvent dtde) {
				// Change JList background...
				searchResultsList.setBackground(Color.LIGHT_GRAY);
				// Store item been dragged
				GraphPanel graphPanel = frameMain.getSelectedGraphPanel();
				if (graphPanel != null) {
					graphPanel.addDropTargetToSelectedTab();
				}
			}

			@Override
			public synchronized void dragExit(DropTargetEvent dtde) {
				this.changeToNormal();
			}

			private void changeToNormal() {
				// Set cursor to default.
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				// Set background to normal...
				searchResultsList.setBackground(Color.WHITE);
			}
		});
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(frameMain, FramesHelpType.PANEL_SEARCH)).setVisible(true);
			}
		});
	}

	private void addKeyListenerToList(final JList<SuggestDoubleColumn> listToAdd) {
		listToAdd.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				int key = ke.getKeyCode();
				if (!wasArrows && key != KeyEvent.VK_ENTER) {
					wasArrows = (key == KeyEvent.VK_KP_DOWN) || (key == KeyEvent.VK_KP_UP) || (key == KeyEvent.VK_DOWN)
							|| (key == KeyEvent.VK_UP);
					return;
				}

				SuggestDoubleColumn selectedItem = listToAdd.getSelectedValue();
				if (selectedItem == null) {
					return;
				}

				frameMain.openCell((CellValue) selectedItem.getLeftText(), null);
			}
		});
	}

	private void addMouseListenerToList(final JList<SuggestDoubleColumn> listToAdd) {
		listToAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ke) {
				if (ke.getClickCount() == 2) {
					SuggestDoubleColumn selectedItem = listToAdd.getSelectedValue();
					if (selectedItem == null) {
						return;
					}

					frameMain.openCell((CellValue) selectedItem.getLeftText(), null);
				}
			}
		});
	}
	
	private void addElementToFavoriteList(final List<CellValue> dropList) throws Exception {
		//Get all elements to drop
		for (int i = 0; i < dropList.size(); i++) {
			//Set loading message
			blockerLoading.setMessage("Validating Element " + (i + 1));
			CellValue cellValue = dropList.get(i);
			boolean drop = true;
			//Validate if element isn't already in the model
			for (int j = 0; j < favoritesModel.size(); j++) {
				CellValue modelCell = favoritesModel.getElementAt(j).getLeftText();
				if (cellValue.getId().equals(modelCell.getId())
						|| GraphType.CELL_END.equals(cellValue.getGraphType())) {
					drop = false;
				}
			}
			//Drop element if isn't already in the model
			if (drop) {
				//Set loading message
				blockerLoading.setMessage("Saving Element " + (i + 1));
				//Store Favorite
				CellValue cellToAdd = saveFavorite(cellValue);
				//Set loading message
				blockerLoading.setMessage("Adding Element " + (i + 1));
				//Add favorite to model
				favoritesModel.addElement(new SuggestDoubleColumn(cellToAdd, ""));
			}
		}
	}
	
	private CellValue saveFavorite(CellValue cellValue) throws Exception {
		if (cellValue == null) {
			return null;
		}
		
		Favorite favorite = buildFavorite(cellValue.getFullItem());
		if (favorite == null) {
			return null;
		}
		
		(new FavoriteController()).save(favorite);
		
		if (GraphType.CELL_BEGIN.equals(cellValue.getGraphType())) {
			Flow flow = (Flow) cellValue.getFullItem();
			return new CellValue(flow.getId(), flow.getNmFlow(), GraphType.CELL_FLOW, flow);
		} else {
			return cellValue;
		}
	}
	
	private Favorite buildFavorite(Object item) {
		if (item == null) {
			return null;
		}
		
		Favorite favorite = new Favorite();
		if (item instanceof Flow) {
			Flow flow = (Flow) item;
			favorite.setNmFavorite(flow.getNmFlow());
			favorite.setFlow(flow);
		}
		if (item instanceof Logic) {
			Logic logic = (Logic) item;
			favorite.setNmFavorite(logic.getNmLogic());
			favorite.setLogic(logic);
		}
		if (item instanceof Step) {
			Step step = (Step) item;
			favorite.setNmFavorite(step.getNmStep());
			favorite.setStep(step);
		}
		
		favorite.setMacAddress(MyConstants.MAC_ADDRESS);
		return favorite;
	}
	
	private void filterFavorites(final String searchFor) {
		blockerLoading.start("Searching equivalences");
		
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				try {
					List<SuggestDoubleColumn> items = getFavorites(searchFor);
					favoritesModel.clear();
					for (SuggestDoubleColumn item : items) {
						favoritesModel.addElement(item);
					}
					favoritesList.repaint();
					
				} catch (Exception e) {
					showError(e);
				}
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}

	private List<SuggestDoubleColumn> getFavorites(String searchFor) throws Exception {
		return buildFavoritesReturnList((new FavoriteController()).findByMacLikeName(searchFor), searchFor);
	}
	
	private List<SuggestDoubleColumn> buildFavoritesReturnList(List<Favorite> favorites, String searchFor) {
		List<SuggestDoubleColumn> returnList = new ArrayList<>();
		for (Favorite favorite : favorites) {
			if (favorite.getFlow() != null) {
				Flow flow = favorite.getFlow();
				returnList.add(new SuggestDoubleColumn(buildCellValue(flow.getId(), flow.getNmFlow(), GraphType.CELL_FLOW, flow), searchFor));
			} else if (favorite.getLogic() != null) {
				Logic logic = favorite.getLogic();
				returnList.add(new SuggestDoubleColumn(buildCellValue(logic.getId(), logic.getNmLogic(), GraphType.CELL_LOGIC, logic), searchFor));
			} else if (favorite.getStep() != null) {
				Step step = favorite.getStep();
				returnList.add(new SuggestDoubleColumn(buildCellValue(step.getId(), step.getNmStep(), GraphType.CELL_STEP, step), searchFor));
			}
		}
		
		return returnList;
	}
	
	private void startFavoriteList(final int limit) {
		blockerLoading.start("Getting Favorites");
		
		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				try {
					List<SuggestDoubleColumn> items = getFirstFavorites(limit);
					favoritesModel.clear();
					for (SuggestDoubleColumn item : items) {
						favoritesModel.addElement(item);
					}
					favoritesList.repaint();
					
				} catch (Exception e) {
					showError(e);
				}
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}

	private List<SuggestDoubleColumn> getFirstFavorites(int limit) throws Exception {
		return buildFavoritesReturnList((new FavoriteController()).findByMacLimited(limit), "");
	}

	private void filterSearchResults(final String searchFor) {
		blockerLoading.start("Searching equivalences");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				try {
					List<SuggestDoubleColumn> items = getSearchResults(searchFor);
					searchResultsModel.clear();
					for (SuggestDoubleColumn item : items) {
						searchResultsModel.addElement(item);
					}
					searchResultsList.repaint();

				} catch (Exception e) {
					showError(e);
				}
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}

	private List<SuggestDoubleColumn> getSearchResults(String searchFor) throws Exception {
		int logicLimit = 30;
		List<Logic> logics = (new LogicController()).findLikeNameLimited(searchFor, logicLimit);
		logicLimit = (logicLimit - logics.size()) / 2;
		int nextLimit = 30 + logicLimit;
		List<Flow> flows = (new FlowController()).findLikeNameLimited(searchFor, nextLimit);
		nextLimit = (30 + logicLimit) + (nextLimit - flows.size());
		List<Step> steps = (new StepController()).findLikeNameLimited(searchFor, nextLimit);

		List<SuggestDoubleColumn> returnList = new ArrayList<>();
		for (Flow flow : flows) {
			returnList.add(new SuggestDoubleColumn(buildCellValue(flow.getId(), flow.getNmFlow(), GraphType.CELL_FLOW, flow), searchFor));
		}
		for (Logic logic : logics) {
			returnList.add(new SuggestDoubleColumn(buildCellValue(logic.getId(), logic.getNmLogic(), GraphType.CELL_LOGIC, logic), searchFor));
		}
		for (Step step : steps) {
			returnList.add(new SuggestDoubleColumn(buildCellValue(step.getId(), step.getNmStep(), GraphType.CELL_STEP, step), searchFor));
		}
		return returnList;
	}

	private CellValue buildCellValue(Long id, String name, GraphType type, Object fullItem) {
		return new CellValue(id, (name != null && !name.isEmpty()) ? name : type.getLabel(), type, fullItem);
	}
	
	private void showError(Exception e) {
		JOptionPane.showMessageDialog(frameMain,
				e.getMessage(),
				"Error",
				JOptionPane.ERROR_MESSAGE);
		blockerLoading.stop();
	}
}
