package com.mangyBear.Pepe.ui.component.readWriteFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalJc;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.entityRun.ActionResultRun;
import com.mangyBear.Pepe.domain.entityRun.LogicResultRun;
import com.mangyBear.Pepe.domain.types.action.ActionType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.domain.types.run.StatusType;
import com.mangyBear.Pepe.service.MyConstants;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ExportDocx {

	private static XWPFDocument doc;
	
    private int rowCt;
    private int colCt;

    public void run(String flowName, Map<GraphType, Map<String, Object>> resultMap) {
            gerarDocx(flowName, resultMap);
            rowCt = 0;
    }

    private void gerarDocx(String flowName, Map<GraphType, Map<String, Object>> resultMap) {
		try {
			String tmplPath = MyConstants.getDocxTmplPath();
			if (tmplPath == null || tmplPath.isEmpty()) {
				tmplPath = MyConstants.getAppHome() + "/template/base_document.docx";
			}
			doc = new XWPFDocument(new FileInputStream(tmplPath));
		} catch (Exception e) {
			doc = new XWPFDocument();
		}
		
        int nRows = criarTabela(resultMap);
        if (nRows > 1) {
        	String exportPath = MyConstants.getDocxExportPath();
			if (exportPath == null || exportPath.isEmpty()) {
				exportPath = MyConstants.USER_HOME + "/Desktop";
			}
            File diretory = new File(exportPath);
            diretoryExists(diretory);
            try (FileOutputStream out = new FileOutputStream(diretory + "/" + flowName + " - " + new SimpleDateFormat("ddMMyy_hhmmss").format(new Date()) + ".docx")) {
                doc.write(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }

    private int criarTabela(Map<GraphType, Map<String, Object>> resultMap) {
        int nRows = resultMap.size() + 1;
        int nCols = 3;
        XWPFTable table = doc.createTable(nRows, nCols);
        if (nRows > 1) {
            criarLinhas(table, resultMap);
        }
        
        return nRows;
    }

    private void criarLinhas(XWPFTable table, Map<GraphType, Map<String, Object>> resultMap) {
        List<XWPFTableRow> rows = table.getRows();
        
        createHeader(rows.get(rowCt));
        colCt = 0;
        rowCt++;

        for (Map.Entry<GraphType, Map<String, Object>> results : resultMap.entrySet()) {
            createAndPopulateCells(rows.get(rowCt), results);
            colCt = 0;
            rowCt++;
        }
    }

    private void createHeader(XWPFTableRow row) {
        CTHeight ht = row.getCtRow().addNewTrPr().addNewTrHeight();
        ht.setVal(BigInteger.valueOf(700));
        for (XWPFTableCell cell : row.getTableCells()) {
        	setCellWidthAndVAlign(cell, STVerticalJc.BOTTOM);
        	
            XWPFRun rh = cell.getParagraphArray(0).createRun();
            cell.getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
            cell.setColor("A7BFDE");
            switch (colCt) {
                case 0:
                    rh.setText("Type");
                    break;
                case 1:
                    rh.setText("Description");
                    break;
                case 2:
                    rh.setText("Status");
                    break;
            }
            rh.setFontSize(14);
            rh.setFontFamily("Arial");
            rh.setBold(true);
            colCt++;
        }
    }
    
    private void setCellWidthAndVAlign(XWPFTableCell cell, STVerticalJc.Enum vAlign) {
    	CTTcPr tcpr = cell.getCTTc().addNewTcPr();
    	tcpr.addNewVAlign().setVal(vAlign);
        if (colCt == 0) {
        	tcpr.addNewTcW().setW(BigInteger.valueOf(2000));
        } else if (colCt == 1) {
        	tcpr.addNewTcW().setW(BigInteger.valueOf(23000));
        } else if (colCt == 2) {
        	tcpr.addNewTcW().setW(BigInteger.valueOf(7000));
        }
    }

    private void createAndPopulateCells(XWPFTableRow row, Map.Entry<GraphType, Map<String, Object>> results) {
    	CTHeight ht = row.getCtRow().addNewTrPr().addNewTrHeight();
        ht.setVal(BigInteger.valueOf(500));
        
        StatusType status = StatusType.OK;
        for (XWPFTableCell cell : row.getTableCells()) {
        	setCellWidthAndVAlign(cell, STVerticalJc.TOP);
        	
        	XWPFRun rh = cell.getParagraphArray(0).createRun();
            switch (colCt) {
                case 0:
                    cell.getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
                    createColumnGraphType(rh, results.getKey());
                    break;
                case 1:
                    cell.getParagraphArray(0).setAlignment(ParagraphAlignment.LEFT);
                	status = createColumnInformation(rh, results);
                    break;
                case 2:
                    cell.getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
                    createColumnResult(cell, status);
                    break;
            }
            colCt++;
        }
    }

    private void createColumnGraphType(XWPFRun rh, GraphType type) {
        rh.setText(type.getType());
        rh.setFontSize(11);
        rh.setFontFamily("Arial");
        rh.setBold(true);
    }

    private StatusType createColumnInformation(XWPFRun rh, Map.Entry<GraphType, Map<String, Object>> results) {
    	StatusType status = StatusType.OK;
    	for (Map.Entry<String, Object> values : results.getValue().entrySet()) {
	    	switch (results.getKey()) {
		    	case CELL_FLOW:
		    		rh.setText("[NAME]:" + values.getKey());
		    		StatusType tempStatus = StatusType.OK;
		    		for (Map.Entry<GraphType, Map<String, Object>> flowResults : ((Map<GraphType, Map<String, Object>>) values.getValue()).entrySet()) {
		    			tempStatus = getFlowStatus(flowResults);
		    			status = StatusType.WARN.equals(tempStatus) ? (StatusType.NOK.equals(status) ? status : tempStatus) : (StatusType.NOK.equals(tempStatus) ? tempStatus : status);
		    		}
		            break;
		    	case CELL_LOGIC:
		    		if (values.getValue() instanceof LogicResultRun) {
		    			LogicResultRun logicResult = (LogicResultRun) values.getValue();
		    			rh.setText("[NAME]:" + values.getKey());
		    			rh.addBreak();
		    			rh.setText("[INPUT]:" + logicResult.getLogic().getInput());
		    			rh.addBreak();
		    			rh.setText("[EXPECTED]:" + logicResult.getLogic().getExpected());
		    			rh.addBreak();
		    			rh.setText("[DENY]:" + logicResult.getLogic().getDenial());
		    			rh.addBreak();
		    			rh.setText("[RESULT]:" + logicResult.getLogicReturn());
		    			rh.addBreak();
		    			status = logicResult.getLogicStatus();
		    		}
		            break;
		    	case CELL_STEP:
		    		if (values.getValue() instanceof List<?>) {
			    		rh.setText("[NAME]:" + values.getKey());
			    		int actionCount = 1;
			    		int msgCount = 0;
			    		int nokCount = 0;
			    		for (ActionResultRun actionResult : (List<ActionResultRun>) values.getValue()) {
			    			rh.addBreak();
			    			rh.setText(actionCount + ". " + buildFinderMessage(actionResult.getFinder(), actionResult.getFinderStatus()));
			    			nokCount += StatusType.NOK.equals(actionResult.getFinderStatus()) ? 1 : 0;
							rh.addBreak();
							rh.setText(actionCount + ". " + buildActionMessage(actionResult.getAction(), actionResult.getActionStatus()));
							nokCount += StatusType.NOK.equals(actionResult.getActionStatus()) ? 1 : 0;
							msgCount++;
							if (ActionType.NOFINDER_DESKTOP_PRINT_SCREEN.equals(actionResult.getAction().getActionType())) {
								rh.addBreak();
								addPicture(rh, actionResult.getActionReturn());
							}
						}
			    		status = nokCount > 0 ? (nokCount == msgCount ? StatusType.NOK : StatusType.WARN) : StatusType.OK;
		    		}
	            default:
	            	break;
	    	}
	    	rh.setFontSize(10);
			rh.setFontFamily("Arial");
    	}

    	return status;
    }

    private StatusType getFlowStatus(Map.Entry<GraphType, Map<String, Object>> results) {
    	StatusType status = StatusType.OK;
    	for (Map.Entry<String, Object> values : results.getValue().entrySet()) {
	    	switch (results.getKey()) {
		    	case CELL_FLOW:
		    		StatusType tempStatus = StatusType.OK;
		    		for (Map.Entry<GraphType, Map<String, Object>> flowResults : ((Map<GraphType, Map<String, Object>>) values.getValue()).entrySet()) {
		    			tempStatus = getFlowStatus(flowResults);
		    			status = StatusType.WARN.equals(tempStatus) ? (StatusType.NOK.equals(status) ? status : tempStatus) : (StatusType.NOK.equals(tempStatus) ? tempStatus : status);
		    		}
		            break;
		    	case CELL_LOGIC:
		    		if (values.getValue() instanceof LogicResultRun) {
		    			status = ((LogicResultRun) values.getValue()).getLogicStatus();
		    		}
		            break;
		    	case CELL_STEP:
		    		if (values.getValue() instanceof List<?>) {
			    		int msgCount = 0;
			    		int nokCount = 0;
			    		for (ActionResultRun actionResult : (List<ActionResultRun>) values.getValue()) {
							nokCount += StatusType.NOK.equals(actionResult.getFinderStatus()) ? 1 : 0;
							nokCount += StatusType.NOK.equals(actionResult.getActionStatus()) ? 1 : 0;
							msgCount++;
						}
			    		status = nokCount > 0 ? (nokCount == msgCount ? StatusType.NOK : StatusType.WARN) : StatusType.OK;
		    		}
	            default:
	            	break;
	    	}
    	}

    	return status;
    }
    
	private String buildFinderMessage(Finder finder, StatusType finderStatus) {
		if (finder == null) {
			return "[FINDER]:No Finder";
		}
		
		return "[FINDER]:" + finder.getNmFinder() + " [RESULT]:" + finderStatus;//On log write finderResult
	}
	
	private String buildActionMessage(Action action, StatusType actionStatus) {
		return "[ACTION]:" + action.getActionType().toString() + " [RESULT]:" + actionStatus;
	}
    
    private void diretoryExists(File diretorio) {
        if (!diretorio.exists()) {
            diretorio.mkdirs();
        }
    }

    private void createColumnResult(XWPFTableCell cell, StatusType status) {
    	XWPFRun rh = cell.getParagraphArray(0).createRun();
    	rh.setText(status.toString());
    	switch (status) {
	    	case OK:
	            cell.setColor("00FF00");
	            break;
	    	case WARN:
	            cell.setColor("FFFF00");
	            break;
	    	case NOK:
	            cell.setColor("FF0000");
	            break;
    	}
        rh.setFontSize(11);
        rh.setFontFamily("Arial");
        rh.setBold(true);
    }
    
    private void addPicture(XWPFRun r, Object picture) {
    	if (picture == null || !(picture instanceof BufferedImage)) {
    		return;
    	}
    	
        try {

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write((BufferedImage) picture, "jpg", os);
			InputStream is = new ByteArrayInputStream(os.toByteArray());

            r.addPicture(is, XWPFDocument.PICTURE_TYPE_JPEG, "evidence.jpg", Units.toEMU(360), Units.toEMU(200));

        } catch (InvalidFormatException | IOException ex) {
            r.setText("Não foi possível inserir a imagem.");
        }
    }
}
