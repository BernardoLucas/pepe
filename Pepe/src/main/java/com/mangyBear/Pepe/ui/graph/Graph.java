package com.mangyBear.Pepe.ui.graph;

import java.util.Map.Entry;

import com.mangyBear.Pepe.domain.entity.EndFlow;
import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.FlowItem;
import com.mangyBear.Pepe.domain.entity.FlowItemConnection;
import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.ui.domain.CellValue;
import com.mxgraph.model.mxCell;

/*
 Copyright (c) 2001-2014, JGraph Ltd
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the JGraph nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL JGRAPH BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
public class Graph extends GraphDefaults {

	private int idCell;
	private int idConn;
	
	private Object addGraphCell(String idCell, CellValue value, double x, double y, double cellWidth, double cellHeight) {
		return getGraph().insertVertex(getGraph().getDefaultParent(), idCell, value, (x < 0 ? 0 : x), (y < 0 ? 0 : y),
				cellWidth, cellHeight, value.getGraphType().getType());
	}
	
	private Object addGraphConnection(FlowItemConnection connection, mxCell source, mxCell target) {
		return getGraph().insertEdge(getGraph().getDefaultParent(), null, connection, source, target);
	}

	public void addNewCell(CellValue value) {
		getGraph().getModel().beginUpdate();

		GraphType type = value.getGraphType();
		if (type == null) {
			return;
		}
		double cellHeight = type.getHeight();
		double cellWidth = type.getWidth();
		double x = getLastClickX() - (cellWidth / 2);
		double y = getLastClickY() - (cellHeight / 2);
		
		if (GraphType.CELL_BEGIN.equals(type)) {
			x = (getGraphComponent().getWidth() / 2) - (cellWidth / 2);
			y = 30.0;
		}
		
		
		Object vertex = addGraphCell(String.valueOf(idCell), value, x, y, cellWidth, cellHeight);
		
		if (vertex != null) {
			getCellMap().put(idCell, (mxCell) vertex);
		}
		idCell++;

		getGraph().getModel().endUpdate();
		
	}
	
	public mxCell addCellBeginToFlow(Flow flow) {
		getGraph().getModel().beginUpdate();
		
		CellValue value = new CellValue(flow.getId(), GraphType.CELL_BEGIN.getLabel(), GraphType.CELL_BEGIN, flow);

		Object vertex = addGraphCell(String.valueOf(idCell), value, flow.getX(), flow.getY(), flow.getWidth(), flow.getHeight());
		
		mxCell cellToReturn = null;
		if (vertex != null) {
			cellToReturn = (mxCell) vertex;
			getCellMap().put(idCell, cellToReturn);
		}

		getGraph().getModel().endUpdate();
		idCell++;
		return cellToReturn;
	}

	public void addFlowItem(FlowItem item) {
		getGraph().getModel().beginUpdate();

		String idVertex = String.valueOf(item.getId());
		CellValue value = flowItemToCellValue(item);
		
		Object vertex = addGraphCell(idVertex, value, item.getX(), item.getY(), item.getWidth(), item.getHeight());
		
		if (vertex != null) {
			getCellMap().put(idCell, (mxCell) vertex);
		}
		idCell++;

		getGraph().getModel().endUpdate();
	}

	public void addEdge(FlowItemConnection connection, mxCell beginCell) {
		mxCell source;
		if (connection.getPreviousItem() == null) {
			source = beginCell;
		} else {
			source = getCellByFlowItem(connection.getPreviousItem());
		}
		
		Object connectionAsObject = addGraphConnection(connection, source, getCellByFlowItem(connection.getCurrentItem()));
		
		if (connectionAsObject != null) {
			getConnMap().put(idConn, (mxCell) connectionAsObject);
		}
		idConn++;
	}
	
	private mxCell getCellByFlowItem(FlowItem flowItem) {
		CellValue flowItemCellValue = flowItemToCellValue(flowItem);
		for (Entry<Integer, mxCell> entryCells : getCellMap().entrySet()) {
			mxCell entryCell = entryCells.getValue();
			CellValue cellValue = (CellValue) entryCell.getValue();
			if (flowItemCellValue.getGraphType().equals(cellValue.getGraphType())
					&& flowItemCellValue.getId() == cellValue.getId()) {
				return entryCell;
			}
		}
		return null;
	}
	
	private CellValue flowItemToCellValue(FlowItem flowItem) {
		Flow flow = flowItem.getFlow();
		if (flow != null) {
			return new CellValue(flow.getId(), flow.getNmFlow(), GraphType.CELL_FLOW, flowItem);
		}
		Step step = flowItem.getStep();
		if (step != null) {
			return new CellValue(step.getId(), step.getNmStep(), GraphType.CELL_STEP, flowItem);
		}
		Logic logic = flowItem.getLogic();
		if (logic != null) {
			return new CellValue(logic.getId(), logic.getNmLogic(), GraphType.CELL_LOGIC, flowItem);
		}
		EndFlow end = flowItem.getEnd();
		return new CellValue(end.getId(), GraphType.CELL_END.getLabel(), GraphType.CELL_END, flowItem);
	}
}
