package com.mangyBear.Pepe.ui.listener.web;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URLDecoder;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

public class HTTPSServer extends Thread {
	
	private int port = 4444;
	private HttpsServer httpsServer;
	private final RecorderManager recorderManager;
	
	public HTTPSServer(RecorderManager recorderManager) {
		this.recorderManager = recorderManager;
	}
	
	public void closeServer() {
		if (httpsServer != null) {
			httpsServer.stop(0);
		}
	}
	
	@Override
	public void start() {
		try {
			//Get any available port
			if (getAvailablePort() < 1) {
				return;
			}
			// setup the socket address
			InetSocketAddress address = new InetSocketAddress(port);
			setPort(address.getPort());
			
			//Initialize the HTTPS server
			httpsServer = HttpsServer.create(address, 0);
			SSLContext sslContext = SSLContext.getInstance("TLS");

			//Initialize the keystore
			char[] password = "password".toCharArray();
			KeyStore ks = KeyStore.getInstance("JKS");
			FileInputStream fis = new FileInputStream("js/keystore.jks");
			ks.load(fis, password);

			// setup the key manager factory
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, password);

			// setup the trust manager factory
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(ks);

			// setup the HTTPS context and parameters
			sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
			httpsServer.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
				public void configure(HttpsParameters params) {
					try {
						//Initialize the SSL context
						SSLContext c = SSLContext.getDefault();
						SSLEngine engine = c.createSSLEngine();
						params.setNeedClientAuth(false);
						params.setCipherSuites(engine.getEnabledCipherSuites());
						params.setProtocols(engine.getEnabledProtocols());

						// get the default parameters
						SSLParameters defaultSSLParameters = c.getDefaultSSLParameters();
						params.setSSLParameters(defaultSSLParameters);

					} catch (Exception ex) {
						System.out.println("Failed to create HTTPS port");
						setPort(0);
					}
				}
			});
			httpsServer.createContext("/", new MyHandler(recorderManager));
			httpsServer.setExecutor(null); // creates a default executor
			httpsServer.start();
			System.out.println("Server is listening on " + address.toString());
			
		} catch (Exception exception) {
			System.out.println("Failed to create HTTPS server on port " + getPort() + " of localhost");
			setPort(0);
		}
	}
	
	private int getAvailablePort() throws IOException {
    	//Get any available port
	    ServerSocket socket = new ServerSocket(0);
    	socket.setReuseAddress(true);
    	setPort(socket.getLocalPort());

		DatagramSocket datagram = new DatagramSocket(getPort());
        datagram.setReuseAddress(true);
        
        if (datagram != null) {
            datagram.close();
        }
        if (socket != null) {
            socket.close();
        }
        
        return getPort();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public static class MyHandler implements HttpHandler {
		
		private final RecorderManager recorderManager;
		
		public MyHandler(RecorderManager recorderManager) {
			this.recorderManager = recorderManager;
		}
		
		@Override
		public void handle(HttpExchange t) throws IOException {
			String response = "This is the response";
			//Allow any origin
			t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
			t.sendResponseHeaders(200, response.length());
			//Get request
			String request = t.getRequestURI().toString();
			//Turn request into object
			int end = request.contains("HTTP") ? request.indexOf("HTTP") : request.length();
			String interaction = request.substring(request.indexOf("/") + 1, end).trim();
			interaction = interaction.replaceAll("\\s+", " ");
			interaction = interaction.replaceAll("TAB", "\t");
			interaction = URLDecoder.decode(interaction, "UTF-8");
			System.out.println("Action from user:" + interaction);
			
			recorderManager.addInTables(interaction);
			//Send response and close
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}
}
