package com.mangyBear.Pepe.ui.component;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.border.Border;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import com.mangyBear.Pepe.service.MyConstants;

public class TimeField extends JFormattedTextField {

	private static final long serialVersionUID = -85200972171574254L;
	private final Border originalBorder;
	private boolean insertError = false;

	public TimeField() {
		this.originalBorder = getBorder();
		setMask();
		filterValidTimes();
	}

	private void setMask() {
		try {
			MaskFormatter dateFormatter = new MaskFormatter("##:##:##");
			dateFormatter.setValidCharacters("0123456789");
			dateFormatter.setValueClass(String.class);   
			setFormatterFactory(new DefaultFormatterFactory(dateFormatter));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	private void filterValidTimes() {
		addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				String currentText = getText();
				String[] timeArray = currentText.split("\\:");
				
				insertError = validateTime(MyConstants.parseInt(timeArray[0].trim()),
						MyConstants.parseInt(timeArray[1].trim()),
						MyConstants.parseInt(timeArray[2].trim()));
				
				setBorder(insertError ? BorderFactory.createLineBorder(Color.RED, 1) : originalBorder);
			}
		});
	}
	
	private boolean validateTime(int hour, int minute, int second) {
		return hour > 23 || minute > 59 || second > 59;
	}
	
	public boolean isInsertError() {
		return insertError;
	}
}
