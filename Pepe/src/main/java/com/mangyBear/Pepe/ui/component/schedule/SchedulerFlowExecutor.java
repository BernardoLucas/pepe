package com.mangyBear.Pepe.ui.component.schedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class SchedulerFlowExecutor extends TimerTask{

	private final Timer timer = new Timer();
	private static int targetHour;
	private static int targetMin;
	private static int targetSec;

	public void schedule(int targetHour, int targetMin, int targetSec) {
		SchedulerFlowExecutor.targetHour = targetHour;
		SchedulerFlowExecutor.targetMin = targetMin;
		SchedulerFlowExecutor.targetSec = targetSec;
		
		try {
			timer.schedule(new SchedulerFlowExecutor(), computeNextExecution());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		(new SchedulerFlow()).schedule();
		schedule(targetHour, targetMin, targetSec);
	}

	private Date computeNextExecution() throws ParseException {
		LocalDateTime localNow = LocalDateTime.now();
		ZoneId currentZone = ZoneId.systemDefault();
		ZonedDateTime zonedNow = ZonedDateTime.of(localNow, currentZone);
		ZonedDateTime zonedNextTarget = zonedNow.withHour(targetHour).withMinute(targetMin).withSecond(targetSec);
		
		if (zonedNow.compareTo(zonedNextTarget) > 0) {
			zonedNextTarget = zonedNextTarget.plusDays(1);
		}
		return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(zonedNextTargetAsString(zonedNextTarget));
	}
	
	private String zonedNextTargetAsString(ZonedDateTime zonedNextTarget) {
		return zonedNextTarget.getYear() + "-" + zonedNextTarget.getMonthValue() + "-" + zonedNextTarget.getDayOfMonth()
				+ " " + zonedNextTarget.getHour() + ":" + zonedNextTarget.getMinute() + ":" + zonedNextTarget.getSecond();
	}
}