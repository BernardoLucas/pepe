package com.mangyBear.Pepe.ui.frame;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.graph.GraphGridStyleType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.component.ExtensionFilter;
import com.mangyBear.Pepe.ui.component.readWriteFile.PropertyWriter;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameSettings extends JDialog {

	private static final long serialVersionUID = -2002164876108226056L;

	private final int VERTICAL_SPACING = 10;
	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
	private final FrameMain frameMain;

	private JButton btnAboutPepe;
	private JLabel labelGridStyle;
	private JComboBox<GraphGridStyleType> comboGridStyle;
	private JLabel labelMouseDelay;
	private JSlider sliderMouseDelay;

	private JLabel labelDocxTmplPath;
	private JTextField txtDocxTmplPath;
	private JButton btnDocxTmplPath;
	private JLabel labelDocxExportPath;
	private JTextField txtDocxExportPath;
	private JButton btnDocxExportPath;
	private JLabel labelLogExportPath;
	private JTextField txtLogExportPath;
	private JButton btnLogExportPath;
	private JLabel labelPrntScrExportPath;
	private JTextField txtPrntScrExportPath;
	private JButton btnPrntScrExportPath;
	private JLabel labelInformation;

	public FrameSettings(FrameMain frameMain) {
		super(frameMain, FramesHelpType.FRAME_SETTINGS.getName(), true);
		this.blockerLoading = new BlockerLoading(this, null, true);
		this.bindingFactory = new BindingFactory();
		this.frameMain = frameMain;
		initGUI(frameMain);
		createComponents();
		populateComponents();
		addComponents();
		addListeners();
	}

	private void initGUI(FrameMain frameMain) {
		setLayout(new RelativeLayout());
		setSize(new Dimension(400, 290));
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(frameMain);
	}

	private void createComponents() {
		btnAboutPepe = new JButton(FramesHelpType.FRAME_SETTINGS.getLabels().get(0));
		labelGridStyle = new JLabel(FramesHelpType.FRAME_SETTINGS.getLabels().get(1));
		createComboGridStyle();
		labelMouseDelay = new JLabel(FramesHelpType.FRAME_SETTINGS.getLabels().get(2));
		createSliderMouseSpeed();
		labelDocxTmplPath = new JLabel(FramesHelpType.FRAME_SETTINGS.getLabels().get(3));
		txtDocxTmplPath = new JTextField();
		btnDocxTmplPath = createFileChooserButton(btnDocxTmplPath);
		labelDocxExportPath = new JLabel(FramesHelpType.FRAME_SETTINGS.getLabels().get(4));
		txtDocxExportPath = new JTextField();
		btnDocxExportPath = createFileChooserButton(btnDocxExportPath);
		labelLogExportPath = new JLabel(FramesHelpType.FRAME_SETTINGS.getLabels().get(5));
		txtLogExportPath = new JTextField();
		btnLogExportPath = createFileChooserButton(btnLogExportPath);
		labelPrntScrExportPath = new JLabel(FramesHelpType.FRAME_SETTINGS.getLabels().get(6));
		txtPrntScrExportPath = new JTextField();
		btnPrntScrExportPath = createFileChooserButton(btnPrntScrExportPath);
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_SETTINGS.getIcons().get(0));
	}
	
	private JButton createFileChooserButton(JButton button) {
		button = new JButton("...");
		button.setPreferredSize(new Dimension(25, 25));
		
		return button;
	}

	private void createComboGridStyle() {
		comboGridStyle = new JComboBox<>();
		for (GraphGridStyleType style : GraphGridStyleType.getEnableGridStyles()) {
			comboGridStyle.addItem(style);
		}
	}

	private void createSliderMouseSpeed() {
		sliderMouseDelay = new JSlider(0, 100);
		sliderMouseDelay.setOrientation(SwingConstants.HORIZONTAL);
		sliderMouseDelay.setFont(new Font("Tahoma", Font.PLAIN, 10));
		sliderMouseDelay.setPaintTicks(true);
		sliderMouseDelay.setMajorTickSpacing(25);
		sliderMouseDelay.setMinorTickSpacing(5);
		sliderMouseDelay.setPaintTrack(true);
		sliderMouseDelay.setPaintLabels(true);
	}
	
	private void populateComponents() {
		comboGridStyle.setSelectedItem(MyConstants.getGridStyle() == null ? GraphGridStyleType.DOT : MyConstants.getGridStyle());
		sliderMouseDelay.setValue(MyConstants.getMouseDelay());
		txtDocxTmplPath.setText(MyConstants.getDocxTmplPath());
		txtDocxExportPath.setText(MyConstants.getDocxExportPath());
		txtLogExportPath.setText(MyConstants.getLogExportPath());
		txtPrntScrExportPath.setText(MyConstants.getPrntScrExportPath());
	}

	private void addComponents() {
		add(blockerLoading.createLoading(18, 5), loadingBinding());

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		//About Pepe
		add(btnAboutPepe, btnAboutPepeBinding());
		//Grid Style
		add(labelGridStyle, labelGridStyleBinding());
		add(comboGridStyle, comboGridStyleBinding());
		//Mouse Delay
		add(labelMouseDelay, labelMouseSpeedBinding());
		add(sliderMouseDelay, sliderMouseDelayBinding());
		//Docx Template
		add(labelDocxTmplPath, labelDocxTmplPathBinding());
		add(txtDocxTmplPath, txtDocxTmplPathBinding());
		add(btnDocxTmplPath, btnDocxTmplPathBinding());
		//Docx Export
		add(labelDocxExportPath, labelDocxExportPathBinding());
		add(txtDocxExportPath, txtDocxExportPathBinding());
		add(btnDocxExportPath, btnDocxExportPathBinding());
		//Log Export
		add(labelLogExportPath, labelLogExportPathBinding());
		add(txtLogExportPath, txtLogExportPathBinding());
		add(btnLogExportPath, btnLogExportPathBinding());
		//Print Export
		add(labelPrntScrExportPath, labelPrntScrExportPathBinding());
		add(txtPrntScrExportPath, txtPrntScrExportPathBinding());
		add(btnPrntScrExportPath, btnPrntScrExportPathBinding());
		
		add(labelInformation, labelInformationBinding());
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private RelativeConstraints btnAboutPepeBinding() {
		Binding[] btnAboutPepeBinding = new Binding[3];
		btnAboutPepeBinding[0] = bindingFactory.topEdge();
		btnAboutPepeBinding[1] = bindingFactory.leftAlignedWith(labelMouseDelay);
		btnAboutPepeBinding[2] = bindingFactory.rightAlignedWith(sliderMouseDelay);
		return new RelativeConstraints(btnAboutPepeBinding);
	}

	private RelativeConstraints labelGridStyleBinding() {
		Binding[] labelGridStyleBinding = new Binding[2];
		labelGridStyleBinding[0] = bindingFactory.leftEdge();
		labelGridStyleBinding[1] = bindingFactory.verticallyCenterAlignedWith(comboGridStyle);
		return new RelativeConstraints(labelGridStyleBinding);
	}

	private RelativeConstraints comboGridStyleBinding() {
		Binding[] comboGridStyleBinding = new Binding[3];
		comboGridStyleBinding[0] = bindingFactory.below(btnAboutPepe);
		comboGridStyleBinding[1] = bindingFactory.leftAlignedWith(sliderMouseDelay);
		comboGridStyleBinding[2] = bindingFactory.rightAlignedWith(sliderMouseDelay);
		return new RelativeConstraints(comboGridStyleBinding);
	}

	private RelativeConstraints labelMouseSpeedBinding() {
		Binding[] labelMouseSpeedBinding = new Binding[2];
		labelMouseSpeedBinding[0] = bindingFactory.leftEdge();
		labelMouseSpeedBinding[1] = new Binding(Edge.VERTICAL_CENTER, 2, Direction.ABOVE, Edge.VERTICAL_CENTER, sliderMouseDelay);
		return new RelativeConstraints(labelMouseSpeedBinding);
	}
	
	private RelativeConstraints sliderMouseDelayBinding() {
		Binding[] sliderMouseDelayBinding = new Binding[3];
		sliderMouseDelayBinding[0] = bindingFactory.below(comboGridStyle);
		sliderMouseDelayBinding[1] = bindingFactory.rightOf(labelMouseDelay);
		sliderMouseDelayBinding[2] = bindingFactory.rightEdge();
		return new RelativeConstraints(sliderMouseDelayBinding);
	}
	
	private RelativeConstraints labelDocxTmplPathBinding() {
		Binding[] labelDocxTmplPathBinding = new Binding[2];
		labelDocxTmplPathBinding[0] = bindingFactory.leftEdge();
		labelDocxTmplPathBinding[1] = bindingFactory.verticallyCenterAlignedWith(txtDocxTmplPath);
		return new RelativeConstraints(labelDocxTmplPathBinding);
	}
	
	private RelativeConstraints txtDocxTmplPathBinding() {
		Binding[] txtDocxTmplPathBinding = new Binding[3];
		txtDocxTmplPathBinding[0] = bindingFactory.leftAlignedWith(txtPrntScrExportPath);
		txtDocxTmplPathBinding[1] = bindingFactory.leftOf(btnDocxTmplPath);
		txtDocxTmplPathBinding[2] = bindingFactory.below(sliderMouseDelay);
		return new RelativeConstraints(txtDocxTmplPathBinding);
	}

	private RelativeConstraints btnDocxTmplPathBinding() {
		Binding[] btnDocxTmplPathBinding = new Binding[3];
		btnDocxTmplPathBinding[0] = bindingFactory.topAlign(txtDocxTmplPath);
		btnDocxTmplPathBinding[1] = bindingFactory.bottomAlignedWith(txtDocxTmplPath);
		btnDocxTmplPathBinding[2] = bindingFactory.rightAlignedWith(sliderMouseDelay);
		return new RelativeConstraints(btnDocxTmplPathBinding);
	}
	
	private RelativeConstraints labelDocxExportPathBinding() {
		Binding[] labelDocxExportPathBinding = new Binding[2];
		labelDocxExportPathBinding[0] = bindingFactory.leftEdge();
		labelDocxExportPathBinding[1] = bindingFactory.verticallyCenterAlignedWith(txtDocxExportPath);
		return new RelativeConstraints(labelDocxExportPathBinding);
	}
	
	private RelativeConstraints txtDocxExportPathBinding() {
		Binding[] txtDocxExportPathBinding = new Binding[3];
		txtDocxExportPathBinding[0] = bindingFactory.leftAlignedWith(txtPrntScrExportPath);
		txtDocxExportPathBinding[1] = bindingFactory.leftOf(btnDocxExportPath);
		txtDocxExportPathBinding[2] = bindingFactory.below(txtDocxTmplPath);
		return new RelativeConstraints(txtDocxExportPathBinding);
	}

	private RelativeConstraints btnDocxExportPathBinding() {
		Binding[] btnDocxExportPathBinding = new Binding[3];
		btnDocxExportPathBinding[0] = bindingFactory.topAlign(txtDocxExportPath);
		btnDocxExportPathBinding[1] = bindingFactory.bottomAlignedWith(txtDocxExportPath);
		btnDocxExportPathBinding[2] = bindingFactory.rightAlignedWith(sliderMouseDelay);
		return new RelativeConstraints(btnDocxExportPathBinding);
	}
	
	private RelativeConstraints labelLogExportPathBinding() {
		Binding[] labelLogExportPathBinding = new Binding[2];
		labelLogExportPathBinding[0] = bindingFactory.leftEdge();
		labelLogExportPathBinding[1] = bindingFactory.verticallyCenterAlignedWith(txtLogExportPath);
		return new RelativeConstraints(labelLogExportPathBinding);
	}
	
	private RelativeConstraints txtLogExportPathBinding() {
		Binding[] txtLogExportPathBinding = new Binding[3];
		txtLogExportPathBinding[0] = bindingFactory.leftAlignedWith(txtPrntScrExportPath);
		txtLogExportPathBinding[1] = bindingFactory.leftOf(btnLogExportPath);
		txtLogExportPathBinding[2] = bindingFactory.below(txtDocxExportPath);
		return new RelativeConstraints(txtLogExportPathBinding);
	}

	private RelativeConstraints btnLogExportPathBinding() {
		Binding[] btnLogExportPathBinding = new Binding[3];
		btnLogExportPathBinding[0] = bindingFactory.topAlign(txtLogExportPath);
		btnLogExportPathBinding[1] = bindingFactory.bottomAlignedWith(txtLogExportPath);
		btnLogExportPathBinding[2] = bindingFactory.rightAlignedWith(sliderMouseDelay);
		return new RelativeConstraints(btnLogExportPathBinding);
	}
	
	private RelativeConstraints labelPrntScrExportPathBinding() {
		Binding[] labelPrntScrPathBinding = new Binding[2];
		labelPrntScrPathBinding[0] = bindingFactory.leftEdge();
		labelPrntScrPathBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnPrntScrExportPath);
		return new RelativeConstraints(labelPrntScrPathBinding);
	}
	
	private RelativeConstraints txtPrntScrExportPathBinding() {
		Binding[] txtPrntScrPathBinding = new Binding[3];
		txtPrntScrPathBinding[0] = bindingFactory.rightOf(labelPrntScrExportPath);
		txtPrntScrPathBinding[1] = bindingFactory.leftOf(btnPrntScrExportPath);
		txtPrntScrPathBinding[2] = bindingFactory.below(txtLogExportPath);
		return new RelativeConstraints(txtPrntScrPathBinding);
	}

	private RelativeConstraints btnPrntScrExportPathBinding() {
		Binding[] btnPrntScrPathBinding = new Binding[3];
		btnPrntScrPathBinding[0] = bindingFactory.topAlign(txtPrntScrExportPath);
		btnPrntScrPathBinding[1] = bindingFactory.bottomAlignedWith(txtPrntScrExportPath);
		btnPrntScrPathBinding[2] = bindingFactory.rightAlignedWith(sliderMouseDelay);
		return new RelativeConstraints(btnPrntScrPathBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private void addListeners() {
		btnAboutPepe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				(new FrameAboutPepe(FrameSettings.this)).setVisible(true);
			}
		});
		sliderMouseDelay.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				MyConstants.setMouseDelay(sliderMouseDelay.getValue());
			}
		});
		comboGridStyle.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				GraphGridStyleType style = (GraphGridStyleType) comboGridStyle.getSelectedItem();
				frameMain.changeStyleOfSelectedGraphComponent(style);
				MyConstants.setGridStyle(style);
			}
		});
		btnDocxTmplPath.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				openFileChooser(txtDocxTmplPath, new ExtensionFilter(new String[] { ".docx"}));
			}
		});
		btnDocxExportPath.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				openFileChooser(txtDocxExportPath, null);
			}
		});
		btnLogExportPath.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				openFileChooser(txtLogExportPath, null);
			}
		});
		btnPrntScrExportPath.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				openFileChooser(txtPrntScrExportPath, null);
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				blockerLoading.start("Saving new settings");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						save();
						blockerLoading.stop();
						return null;
					}
				}.execute();
			}
		});
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameSettings.this, FramesHelpType.FRAME_SETTINGS)).setVisible(true);
			}
		});
	}
	
	private void openFileChooser(JTextField selectedTxtField, FileFilter filter) {
		JFileChooser chooser = new JFileChooser();
		if (filter != null) {
			chooser.setFileFilter(filter);
		} else {
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}
		String filePath = selectedTxtField.getText();
		if (filePath != null && !filePath.isEmpty()) {
			chooser.setCurrentDirectory(new File(filePath));
		}
		int returnVal = chooser.showOpenDialog(FrameSettings.this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			selectedTxtField.setText(chooser.getSelectedFile().getAbsolutePath());
		}
	}

	private void save() {
		//Write on pepe.config
		PropertyWriter propertyWriter = new PropertyWriter();
		propertyWriter.writeProperty(new File(MyConstants.getAppHome() + "/pepe.config"), "[Grid Style]", "style", comboGridStyle.getSelectedItem().toString());
		propertyWriter.writeProperty(new File(MyConstants.getAppHome() + "/pepe.config"), "[Mouse Delay]", "delay", String.valueOf(sliderMouseDelay.getValue()));
		propertyWriter.writeProperty(new File(MyConstants.getAppHome() + "/pepe.config"), "[Docx Tmpl]", "path", txtDocxTmplPath.getText());
		propertyWriter.writeProperty(new File(MyConstants.getAppHome() + "/pepe.config"), "[Docx Export]", "path", txtDocxExportPath.getText());
		propertyWriter.writeProperty(new File(MyConstants.getAppHome() + "/pepe.config"), "[Log Export]", "path", txtLogExportPath.getText());
		propertyWriter.writeProperty(new File(MyConstants.getAppHome() + "/pepe.config"), "[PrntScr Export]", "path", txtPrntScrExportPath.getText());
		//Update MyConstants variables
		MyConstants.setGridStyle((GraphGridStyleType) comboGridStyle.getSelectedItem());
		MyConstants.setMouseDelay(sliderMouseDelay.getValue());
		MyConstants.setDocxTmplPath(txtDocxTmplPath.getText());
		MyConstants.setDocxExportPath(txtDocxExportPath.getText());
		MyConstants.setLogExportPath(txtLogExportPath.getText());
		MyConstants.setPrntScrExportPath(txtPrntScrExportPath.getText());
	}
}
