package com.mangyBear.Pepe.ui.controller;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.FinderParameter;
import com.mangyBear.Pepe.service.FinderParameterService;

public class FinderParameterController {
	
	private FinderParameterService service = new FinderParameterService();

	public List<FinderParameter> findParamLikeValue(String value) throws Exception {
		return service.findParamLikeValue(value);
	}
}
