package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.accessibility.Accessible;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.VarObject;
import com.mangyBear.Pepe.domain.entity.VarObjectParameter;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.entity.VariableParameter;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.domain.types.variable.VariableType;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.component.ComponentsTable;
import com.mangyBear.Pepe.ui.component.ExtensionFilter;
import com.mangyBear.Pepe.ui.component.SuggestCombo;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;
import com.mangyBear.Pepe.ui.controller.FlowController;
import com.mangyBear.Pepe.ui.controller.VarObjectController;
import com.mangyBear.Pepe.ui.controller.VariableController;
import com.mangyBear.Pepe.ui.domain.CellValue;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameFlow extends BaseFrame {

	private static final long serialVersionUID = -8248057444804619341L;
	
	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
	private final FrameMain frameMain;
	private final int VERTICAL_SPACING = 10;
	private final int TABLE_VERTICAL_SPACING = 2;
	private final List<Variable> deleteList;
	
	private Flow flow;
	private boolean wasFileArrows;
	private boolean wasObjectArrows;
	private boolean wasSqlArrows;
	private boolean wasWebDriverArrows;
	//Say if some row is being edited
	private int editingFile = -1;
	private int editingObject = -1;
	private int editingSql = -1;
	private int editingWebDriver = -1;

	//Base Panel Components
	private JLabel labelName;
	private JTextField textName;
	private final JTabbedPane tabbedPane;
	private JLabel labelInformation;
	private JButton btnUpdate;
	private JLabel labelComment;
	//File panel Components
	private JPanel filePanel;
	private JLabel fileNameLabel;
	private SuggestCombo fileNameSuggest;
	private JLabel filePathLabel;
	private SuggestCombo filePathSuggest;
	private JButton filePathChooser;
	private JLabel paramLabel;
	private JToggleButton fileParamBtn;
	private JButton btnAddFile;
	private JButton btnRemoveFile;
	private JTable fileTable;
	private DefaultTableModel fileTableModel;
	private JLabel fileInformationLabel;
	//Object Panel Components
	private JPanel objectPanel;
	private JLabel objectNameLabel;
	private SuggestCombo objectNameSuggest;
	private JLabel valueLabel;
	private SuggestCombo valueSuggest;
	private JLabel objectTypeLabel;
	private JComboBox<VariableType> objectTypeCombo;
	private JButton btnAddObject;
	private JButton btnRemoveObject;
	private JTable objectTable;
	private DefaultTableModel objectTableModel;
	private JLabel objectInformationLabel;
	//SQL panel Components
	private JPanel sqlPanel;
	private JLabel sqlNameLabel;
	private SuggestCombo sqlNameSuggest;
	private JLabel dataBaseLabel;
	private JToggleButton sqlDataBaseBtn;
	private JLabel queryLabel;
	private JToggleButton sqlQueryBtn;
	private JButton btnAddSql;
	private JButton btnRemoveSql;
	private JTable sqlTable;
	private DefaultTableModel sqlTableModel;
	private JLabel sqlInformationLabel;
	//WebDriver panel Components
	private JPanel webDriverPanel;
	private JLabel webDriverNameLabel;
	private SuggestCombo webDriverNameSuggest;
	private JLabel exeLabel;
	private SuggestCombo webDriverExeSuggest;
	private JButton webDriverExeChooser;
	private JLabel webDriverTypeLabel;
	private JComboBox<VariableType> webDriverTypeCombo;
	private JLabel configLabel;
	private JToggleButton webDriverConfigBtn;
	private JButton btnAddWebDriver;
	private JButton btnRemoveWebDriver;
	private JTable webDriverTable;
	private DefaultTableModel webDriverTableModel;
	private JLabel webDriverInformationLabel;
	
	private DefaultTableModel fileParam;
	private VariableType fileParamType;
	private DefaultTableModel sqlDataBase;
	private DefaultTableModel sqlQuery;
	private DefaultTableModel webDriverConfig;
	private VariableType webDriverConfigType;
	
	private JToggleButton pressedBtn;

	public FrameFlow(FrameMain frameMain, Flow flow) {
		super(frameMain, FramesHelpType.FRAME_FLOW.getName(), true);
		this.blockerLoading = new BlockerLoading(this, null, true);
		this.bindingFactory = new BindingFactory();
		this.tabbedPane = new JTabbedPane();
		this.frameMain = frameMain;
		this.flow = flow;
		this.deleteList = new ArrayList<>();
		initGUI();
		addComponents();
		addListeners();
		if (flow != null) {
			setSavedVariables();
		}
	}
	
	private void initGUI() {
		setLayout(new RelativeLayout());
		setSize(new Dimension(750, 550));
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(frameMain);
	}

	private void addComponents() {
		add(blockerLoading.createLoading(60, 5), loadingBinding());
		//Creates File tab
		buildFilePanel();
		//Creates Object tab
		buildObjectPanel();
		//Creates SQL tab
		buildSqlPanel();
		//Creates WebDriver tab
		buildWebDriverPanel();
		//Creates frame
		buildFrame();
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private void buildFrame() {
		createComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		add(labelName, labelNameBinding());
		add(textName, textNameBinding());
		add(labelInformation, labelInformationBinding());
		add(btnUpdate, btnUpdateBinding());
		add(labelComment, labelCommentBinding());
		add(tabbedPane, tabbedPaneBinding());

		addTabs();
	}

	private void createComponents() {
		labelName = new JLabel(FramesHelpType.FRAME_FLOW.getLabels().get(0));
		textName = new JTextField();
		labelInformation = new JLabel(FramesHelpType.FRAME_FLOW.getIcons().get(0));
		btnUpdate = new JButton("Update");
		labelComment = new JLabel(FramesHelpType.FRAME_FLOW.getIcons().get(1));
	}

	private void addTabs() {
		tabbedPane.addTab(FramesHelpType.FRAME_FLOW_FILE.getName(), FramesHelpType.FRAME_FLOW_FILE.getIcons().get(1), filePanel);
		tabbedPane.addTab(FramesHelpType.FRAME_FLOW_OBJECT.getName(), FramesHelpType.FRAME_FLOW_OBJECT.getIcons().get(1), objectPanel);
		tabbedPane.addTab(FramesHelpType.FRAME_FLOW_SQL.getName(), FramesHelpType.FRAME_FLOW_SQL.getIcons().get(1), sqlPanel);
		tabbedPane.addTab(FramesHelpType.FRAME_FLOW_WEBDRIVER.getName(), FramesHelpType.FRAME_FLOW_WEBDRIVER.getIcons().get(1), webDriverPanel);
	}

	private RelativeConstraints labelNameBinding() {
		Binding[] labelNameBinding = new Binding[2];
		labelNameBinding[0] = bindingFactory.leftEdge();
		labelNameBinding[1] = bindingFactory.verticallyCenterAlignedWith(textName);
		return new RelativeConstraints(labelNameBinding);
	}

	private RelativeConstraints textNameBinding() {
		Binding[] textNameBinding = new Binding[3];
		textNameBinding[0] = bindingFactory.topEdge();
		textNameBinding[1] = bindingFactory.rightOf(labelName);
		textNameBinding[2] = bindingFactory.rightAlignedWith(tabbedPane);
		return new RelativeConstraints(textNameBinding);
	}
	
	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private RelativeConstraints btnUpdateBinding() {
		Binding[] btnUpdateBinding = new Binding[2];
		btnUpdateBinding[0] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		btnUpdateBinding[1] = new Binding(Edge.HORIZONTAL_CENTER, 0, Direction.LEFT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
		return new RelativeConstraints(btnUpdateBinding);
	}

	private RelativeConstraints labelCommentBinding() {
		Binding[] labelCommentBinding = new Binding[2];
		labelCommentBinding[0] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		labelCommentBinding[1] = new Binding(Edge.RIGHT, 3, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		return new RelativeConstraints(labelCommentBinding);
	}

	private RelativeConstraints tabbedPaneBinding() {
		Binding[] tabbedPaneBinding = new Binding[4];
		tabbedPaneBinding[0] = bindingFactory.below(textName);
		tabbedPaneBinding[1] = bindingFactory.leftAlignedWith(labelInformation);
		tabbedPaneBinding[2] = bindingFactory.rightAlignedWith(labelComment);
		tabbedPaneBinding[3] = bindingFactory.above(btnUpdate);
		return new RelativeConstraints(tabbedPaneBinding);
	}

	private RelativeConstraints tableBinding(JComponent below, JComponent above) {
		Binding[] tableBinding = new Binding[4];
		tableBinding[0] = bindingFactory.below(below);
		tableBinding[1] = bindingFactory.leftEdge();
		tableBinding[2] = bindingFactory.rightEdge();
		tableBinding[3] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.TOP, above);
		return new RelativeConstraints(tableBinding);
	}

	private void buildFilePanel() {
		filePanel = new JPanel(new RelativeLayout());
		createFilePanelComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		filePanel.add(fileNameLabel, fileNameLabelBinding());
		filePanel.add(fileNameSuggest, fileNameSuggestBinding());
		filePanel.add(filePathLabel, filePathLabelBinding());
		filePanel.add(filePathSuggest, filePathSuggestBinding());
		filePanel.add(filePathChooser, filePathChooserBinding());
		filePanel.add(paramLabel, paramLabelBinding());
		filePanel.add(fileParamBtn, paramBtnBinding());
		filePanel.add(btnAddFile, btnAddFileBinding());
		filePanel.add(btnRemoveFile, btnRemoveFileBinding());
		filePanel.add(fileInformationLabel, labelInformationBinding());

		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);

		Integer[] checkBoxColumns = new Integer[] { 1 };
		fileTable = new ComponentsTable(new String[] { "Entity", "Delete", "Name",
				"Path", "Type", "Parameters" }, checkBoxColumns,
				new Integer[] { 0, 2, 3, 4, 5 }, true, true, false);
		setInvisibleColumn(fileTable);
		centerTableLabels(checkBoxColumns, fileTable);
		setFileTableModel((DefaultTableModel) fileTable.getModel());
		filePanel.add(new JScrollPane(fileTable), tableBinding(btnRemoveFile, fileInformationLabel));
	}

	private void createFilePanelComponents() {
		fileNameLabel = new JLabel(FramesHelpType.FRAME_FLOW_FILE.getLabels().get(0));
		fileNameSuggest = new SuggestCombo("", true, Color.BLACK, 0, Color.BLACK);
		filePathLabel = new JLabel(FramesHelpType.FRAME_FLOW_FILE.getLabels().get(1));
		filePathSuggest = new SuggestCombo("", true, Color.BLACK, 0, Color.BLACK);
		filePathChooser = new JButton("...");
		filePathChooser.setPreferredSize(new Dimension(25, 25));
		filePathChooser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new ExtensionFilter(new String[]{".csv", ".prop", ".xml"}));
				Object filePath = filePathSuggest.getSelectedItem();
				if (filePath != null && filePath.toString().length() > 0) {
					chooser.setCurrentDirectory(new File(filePath.toString()));
				}
				int returnVal = chooser.showOpenDialog(FrameFlow.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					filePathSuggest.setSelectedItem(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		paramLabel = new JLabel(FramesHelpType.FRAME_FLOW_FILE.getLabels().get(2));
		fileParamBtn = new JToggleButton();
		fileParamBtn.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.setColor(fileParam != null ? Color.decode("#32ff32") : Color.decode("#ff3232"));
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
		        fileParamBtn.setText(fileParam != null ? "Setted" : "Unsetted");
				super.paint(g, c);
			}
		});
		//Table components
		btnAddFile = new JButton(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(2));
		btnRemoveFile = new JButton(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(3));
		//Information icon
		fileInformationLabel = new JLabel(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(0));
	}

	private RelativeConstraints fileNameLabelBinding() {
		Binding[] fileNameLabelBinding = new Binding[2];
		fileNameLabelBinding[0] = bindingFactory.leftEdge();
		fileNameLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(fileNameSuggest);
		return new RelativeConstraints(fileNameLabelBinding);
	}
	
	private RelativeConstraints fileNameSuggestBinding() {
		Binding[] fileNameSuggestBinding = new Binding[3];
		fileNameSuggestBinding[0] = bindingFactory.rightOf(fileNameLabel);
		fileNameSuggestBinding[1] = bindingFactory.rightEdge();
		fileNameSuggestBinding[2] = bindingFactory.topEdge();
		return new RelativeConstraints(fileNameSuggestBinding);
	}

	private RelativeConstraints filePathLabelBinding() {
		Binding[] filePathLabelBinding = new Binding[2];
		filePathLabelBinding[0] = bindingFactory.leftEdge();
		filePathLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(filePathSuggest);
		return new RelativeConstraints(filePathLabelBinding);
	}
	
	private RelativeConstraints filePathSuggestBinding() {
		Binding[] filePathSuggestBinding = new Binding[3];
		filePathSuggestBinding[0] = bindingFactory.leftAlignedWith(fileNameSuggest);
		filePathSuggestBinding[1] = bindingFactory.leftOf(filePathChooser);
		filePathSuggestBinding[2] = bindingFactory.below(fileNameSuggest);
		return new RelativeConstraints(filePathSuggestBinding);
	}
	
	private RelativeConstraints filePathChooserBinding() {
		Binding[] filePathChooserBinding = new Binding[3];
		filePathChooserBinding[0] = bindingFactory.rightEdge();
		filePathChooserBinding[1] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, filePathSuggest);
		filePathChooserBinding[2] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, filePathSuggest);
		return new RelativeConstraints(filePathChooserBinding);
	}
	
	private RelativeConstraints paramLabelBinding() {
		Binding[] paramLabelBinding = new Binding[2];
		paramLabelBinding[0] = bindingFactory.leftEdge();
		paramLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(fileParamBtn);
		return new RelativeConstraints(paramLabelBinding);
	}

	private RelativeConstraints paramBtnBinding() {
		Binding[] paramBtnBinding = new Binding[2];
		paramBtnBinding[0] = bindingFactory.rightOf(paramLabel);
		paramBtnBinding[1] = new Binding(Edge.TOP, 1, Direction.BELOW, Edge.TOP, btnRemoveFile);
		return new RelativeConstraints(paramBtnBinding);
	}

	private RelativeConstraints btnAddFileBinding() {
		Binding[] btnAddFileBinding = new Binding[2];
		btnAddFileBinding[0] = bindingFactory.leftOf(btnRemoveFile);
		btnAddFileBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveFile);
		return new RelativeConstraints(btnAddFileBinding);
	}

	private RelativeConstraints btnRemoveFileBinding() {
		Binding[] btnRemoveFileBinding = new Binding[2];
		btnRemoveFileBinding[0] = bindingFactory.below(filePathSuggest);
		btnRemoveFileBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveFileBinding);
	}

	private void buildObjectPanel() {
		objectPanel = new JPanel(new RelativeLayout());
		createVariablesPanelComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		objectPanel.add(objectNameLabel, objectNameLabelBinding());
		objectPanel.add(objectNameSuggest, objectNameSuggestBinding());
		objectPanel.add(objectTypeLabel, objectTypeLabelBinding());
		objectPanel.add(objectTypeCombo, objectTypeComboBinding());
		objectPanel.add(valueLabel, valueLabelBinding());
		objectPanel.add(valueSuggest, valueSuggestBinding());
		objectPanel.add(btnAddObject, btnAddObjectBinding());
		objectPanel.add(btnRemoveObject, btnRemoveObjectBinding());
		objectPanel.add(objectInformationLabel, labelInformationBinding());

		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);
		Integer[] checkBoxColumns = new Integer[] { 1 };
		objectTable = new ComponentsTable(new String[] { "Entity", "Delete",
				"Name", "Type", "Value" }, checkBoxColumns,
				new Integer[] { 0, 2, 3, 4 }, false, false, true);
		setInvisibleColumn(objectTable);
		centerTableLabels(checkBoxColumns, objectTable);
		setVariableTableModel((DefaultTableModel) objectTable.getModel());
		objectPanel.add(new JScrollPane(objectTable), tableBinding(btnRemoveObject, objectInformationLabel));
	}

	private void createVariablesPanelComponents() {
		objectNameLabel = new JLabel(FramesHelpType.FRAME_FLOW_OBJECT.getLabels().get(0));
		objectNameSuggest = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		objectTypeLabel = new JLabel(FramesHelpType.FRAME_FLOW_OBJECT.getLabels().get(1));
		createObjectTypeCombo();
		valueLabel = new JLabel(FramesHelpType.FRAME_FLOW_OBJECT.getLabels().get(2));
		valueSuggest = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		btnAddObject = new JButton(FramesHelpType.FRAME_FLOW_OBJECT.getIcons().get(2));
		btnRemoveObject = new JButton(FramesHelpType.FRAME_FLOW_OBJECT.getIcons().get(3));
		objectInformationLabel = new JLabel(FramesHelpType.FRAME_FLOW_OBJECT.getIcons().get(0));
	}

	private void createObjectTypeCombo() {
		objectTypeCombo = new JComboBox<>();
		objectTypeCombo.setPreferredSize(new Dimension(100, 22));
		List<VariableType> variableTypeList = VariableType.getVariablesByGroup(VariableGroupType.VARIABLE_OBJECT_GROUP, true);
		for (VariableType variableType : variableTypeList) {
			objectTypeCombo.addItem(variableType);
		}
	}
	
	private RelativeConstraints objectNameLabelBinding() {
		Binding[] objectNameLabelBinding = new Binding[2];
		objectNameLabelBinding[0] = bindingFactory.leftEdge();
		objectNameLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(objectNameSuggest);
		return new RelativeConstraints(objectNameLabelBinding);
	}

	private RelativeConstraints objectNameSuggestBinding() {
		Binding[] objectNameSuggestBinding = new Binding[3];
		objectNameSuggestBinding[0] = bindingFactory.topEdge();
		objectNameSuggestBinding[1] = bindingFactory.rightOf(objectNameLabel);
		objectNameSuggestBinding[2] = bindingFactory.rightEdge();
		return new RelativeConstraints(objectNameSuggestBinding);
	}

	private RelativeConstraints objectTypeLabelBinding() {
		Binding[] objectTypeLabelBinding = new Binding[2];
		objectTypeLabelBinding[0] = bindingFactory.leftEdge();
		objectTypeLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(objectTypeCombo);
		return new RelativeConstraints(objectTypeLabelBinding);
	}

	private RelativeConstraints objectTypeComboBinding() {
		Binding[] objectTypeComboBinding = new Binding[2];
		objectTypeComboBinding[0] = bindingFactory.leftAlignedWith(objectNameSuggest);
		objectTypeComboBinding[1] = new Binding(Edge.TOP, 1, Direction.BELOW, Edge.TOP, btnRemoveObject);
		return new RelativeConstraints(objectTypeComboBinding);
	}
	
	private RelativeConstraints valueLabelBinding() {
		Binding[] valueLabelBinding = new Binding[2];
		valueLabelBinding[0] = bindingFactory.rightOf(objectTypeCombo);
		valueLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(objectTypeCombo);
		return new RelativeConstraints(valueLabelBinding);
	}

	private RelativeConstraints valueSuggestBinding() {
		Binding[] valueSuggestBinding = new Binding[3];
		valueSuggestBinding[0] = bindingFactory.rightOf(valueLabel);
		valueSuggestBinding[1] = bindingFactory.leftOf(btnAddObject);
		valueSuggestBinding[2] = bindingFactory.verticallyCenterAlignedWith(objectTypeCombo);
		return new RelativeConstraints(valueSuggestBinding);
	}

	private RelativeConstraints btnAddObjectBinding() {
		Binding[] btnAddObjectBinding = new Binding[2];
		btnAddObjectBinding[0] = bindingFactory.leftOf(btnRemoveObject);
		btnAddObjectBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveObject);
		return new RelativeConstraints(btnAddObjectBinding);
	}

	private RelativeConstraints btnRemoveObjectBinding() {
		Binding[] btnRemoveObjectBinding = new Binding[2];
		btnRemoveObjectBinding[0] = bindingFactory.below(objectNameSuggest);
		btnRemoveObjectBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveObjectBinding);
	}
	
	private void buildSqlPanel() {
		sqlPanel = new JPanel(new RelativeLayout());
		createSqlPanelComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		sqlPanel.add(sqlNameLabel, sqlNameLabelBinding());
		sqlPanel.add(sqlNameSuggest, sqlNameSuggestBinding());
		sqlPanel.add(dataBaseLabel, dataBaseLabelBinding());
		sqlPanel.add(sqlDataBaseBtn, dataBaseBtnBinding());
		sqlPanel.add(queryLabel, queryLabelBinding());
		sqlPanel.add(sqlQueryBtn, queryBtnBinding());
		sqlPanel.add(btnAddSql, btnAddSqlBinding());
		sqlPanel.add(btnRemoveSql, btnRemoveSqlBinding());
		sqlPanel.add(sqlInformationLabel, labelInformationBinding());

		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);

		Integer[] checkBoxColumns = new Integer[] { 1 };
		sqlTable = new ComponentsTable(new String[] { "Entity", "Delete", "Name",
				"DataBase", "Query" }, checkBoxColumns,
				new Integer[] { 0, 2, 3, 4 }, true, true, false);
		setInvisibleColumn(sqlTable);
		centerTableLabels(checkBoxColumns, sqlTable);
		setSqlTableModel((DefaultTableModel) sqlTable.getModel());
		sqlPanel.add(new JScrollPane(sqlTable), tableBinding(btnRemoveSql, sqlInformationLabel));
	}
	
	private void createSqlPanelComponents() {
		sqlNameLabel = new JLabel(FramesHelpType.FRAME_FLOW_SQL.getLabels().get(0));
		sqlNameSuggest = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		dataBaseLabel = new JLabel(FramesHelpType.FRAME_FLOW_SQL.getLabels().get(1));
		sqlDataBaseBtn = new JToggleButton();
		sqlDataBaseBtn.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.setColor(sqlDataBase != null ? Color.decode("#32ff32") : Color.decode("#ff3232"));
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
		        sqlDataBaseBtn.setText(sqlDataBase != null ? "Setted" : "Unsetted");
				super.paint(g, c);
			}
		});
		queryLabel = new JLabel(FramesHelpType.FRAME_FLOW_SQL.getLabels().get(2));
		sqlQueryBtn = new JToggleButton();
		sqlQueryBtn.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.setColor(sqlQuery != null ? Color.decode("#32ff32") : Color.decode("#ff3232"));
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
		        sqlQueryBtn.setText(sqlQuery != null ? "Setted" : "Unsetted");
				super.paint(g, c);
			}
		});
		btnAddSql = new JButton(FramesHelpType.FRAME_FLOW_SQL.getIcons().get(2));
		btnRemoveSql = new JButton(FramesHelpType.FRAME_FLOW_SQL.getIcons().get(3));
		sqlInformationLabel = new JLabel(FramesHelpType.FRAME_FLOW_SQL.getIcons().get(0));
	}
	
	private RelativeConstraints sqlNameLabelBinding() {
		Binding[] sqlNameLabelBinding = new Binding[2];
		sqlNameLabelBinding[0] = bindingFactory.leftEdge();
		sqlNameLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(sqlNameSuggest);
		return new RelativeConstraints(sqlNameLabelBinding);
	}
	
	private RelativeConstraints sqlNameSuggestBinding() {
		Binding[] sqlNameSuggestBinding = new Binding[3];
		sqlNameSuggestBinding[0] = bindingFactory.rightOf(sqlNameLabel);
		sqlNameSuggestBinding[1] = bindingFactory.rightEdge();
		sqlNameSuggestBinding[2] = bindingFactory.topEdge();
		return new RelativeConstraints(sqlNameSuggestBinding);
	}

	private RelativeConstraints dataBaseLabelBinding() {
		Binding[] dataBaseLabelBinding = new Binding[2];
		dataBaseLabelBinding[0] = bindingFactory.leftEdge();
		dataBaseLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(sqlQueryBtn);
		return new RelativeConstraints(dataBaseLabelBinding);
	}
	
	private RelativeConstraints dataBaseBtnBinding() {
		Binding[] dataBaseBtnBinding = new Binding[2];
		dataBaseBtnBinding[0] = bindingFactory.rightOf(dataBaseLabel);
		dataBaseBtnBinding[1] = bindingFactory.verticallyCenterAlignedWith(sqlQueryBtn);
		return new RelativeConstraints(dataBaseBtnBinding);
	}
	
	private RelativeConstraints queryLabelBinding() {
		Binding[] queryLabelBinding = new Binding[2];
		queryLabelBinding[0] = new Binding(Edge.HORIZONTAL_CENTER, 213, Direction.LEFT, Edge.HORIZONTAL_CENTER, sqlPanel);
		queryLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(sqlQueryBtn);
		return new RelativeConstraints(queryLabelBinding);
	}

	private RelativeConstraints queryBtnBinding() {
		Binding[] queryBtnBinding = new Binding[2];
		queryBtnBinding[0] = bindingFactory.rightOf(queryLabel);
		queryBtnBinding[1] = new Binding(Edge.TOP, 1, Direction.BELOW, Edge.TOP, btnRemoveSql);
		return new RelativeConstraints(queryBtnBinding);
	}

	private RelativeConstraints btnAddSqlBinding() {
		Binding[] btnAddSqlBinding = new Binding[2];
		btnAddSqlBinding[0] = bindingFactory.leftOf(btnRemoveSql);
		btnAddSqlBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveSql);
		return new RelativeConstraints(btnAddSqlBinding);
	}

	private RelativeConstraints btnRemoveSqlBinding() {
		Binding[] btnRemoveSqlBinding = new Binding[2];
		btnRemoveSqlBinding[0] = bindingFactory.below(sqlNameSuggest);
		btnRemoveSqlBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveSqlBinding);
	}

	private void buildWebDriverPanel() {
		webDriverPanel = new JPanel(new RelativeLayout());
		createWebDriverPanelComponents();
		
		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		webDriverPanel.add(webDriverNameLabel, webDriverNameLabelBinding());
		webDriverPanel.add(webDriverNameSuggest, webDriverNameSuggestBinding());
		webDriverPanel.add(exeLabel, exeLabelBinding());
		webDriverPanel.add(webDriverExeSuggest, webDriverExeSuggestBinding());
		webDriverPanel.add(webDriverExeChooser, webDriverExeChooserBinding());
		webDriverPanel.add(webDriverTypeLabel, webDriverTypeLabelBinding());
		webDriverPanel.add(webDriverTypeCombo, webDriverTypeComboBinding());
		webDriverPanel.add(configLabel, configLabelBinding());
		webDriverPanel.add(webDriverConfigBtn, webDriverConfigBtnBinding());
		webDriverPanel.add(btnAddWebDriver, btnAddWebDriverBinding());
		webDriverPanel.add(btnRemoveWebDriver, btnRemoveWebDriverBinding());
		webDriverPanel.add(webDriverInformationLabel, labelInformationBinding());
		
		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);
		Integer[] checkBoxColumns = new Integer[] { 1 };
		webDriverTable = new ComponentsTable(new String[] { "Entity", "Delete", "Name",
				"Path", "Type", "Configurations" }, checkBoxColumns,
				new Integer[] { 0, 2, 3, 4, 5 }, false, false, true);
		setInvisibleColumn(webDriverTable);
		centerTableLabels(checkBoxColumns, webDriverTable);
		setWebDriverTableModel((DefaultTableModel) webDriverTable.getModel());
		webDriverPanel.add(new JScrollPane(webDriverTable), tableBinding(btnRemoveWebDriver, webDriverInformationLabel));
	}
	
	private void createWebDriverPanelComponents() {
		webDriverNameLabel = new JLabel(FramesHelpType.FRAME_FLOW_WEBDRIVER.getLabels().get(0));
		webDriverNameSuggest = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		exeLabel = new JLabel(FramesHelpType.FRAME_FLOW_WEBDRIVER.getLabels().get(1));
		webDriverExeSuggest = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		webDriverExeChooser = new JButton("...");
		webDriverExeChooser.setPreferredSize(new Dimension(25, 25));
		webDriverExeChooser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new ExtensionFilter(new String[]{".exe"}));
				Object filePath = webDriverExeSuggest.getSelectedItem();
				if (filePath != null && filePath.toString().length() > 0) {
					chooser.setCurrentDirectory(new File(filePath.toString()));
				}
				int returnVal = chooser.showOpenDialog(FrameFlow.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					webDriverExeSuggest.setSelectedItem(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		webDriverTypeLabel = new JLabel(FramesHelpType.FRAME_FLOW_WEBDRIVER.getLabels().get(2));
		createWebDriverTypeCombo();
		configLabel = new JLabel(FramesHelpType.FRAME_FLOW_WEBDRIVER.getLabels().get(3));
		webDriverConfigBtn = new JToggleButton();
		webDriverConfigBtn.setUI(new BasicButtonUI() {
			
			@Override
			public void paint(Graphics g, JComponent c) {
		        g.setColor(webDriverConfig != null ? Color.decode("#32ff32") : Color.decode("#ff3232"));
		        g.fillRect(0, 0, getWidth(), getHeight());
		        g.setColor(Color.DARK_GRAY);
		        webDriverConfigBtn.setText(webDriverConfig != null ? "Setted" : "Unsetted");
				super.paint(g, c);
			}
		});
		btnAddWebDriver = new JButton(FramesHelpType.FRAME_FLOW_WEBDRIVER.getIcons().get(2));
		btnRemoveWebDriver = new JButton(FramesHelpType.FRAME_FLOW_WEBDRIVER.getIcons().get(3));
		webDriverInformationLabel = new JLabel(FramesHelpType.FRAME_FLOW_WEBDRIVER.getIcons().get(0));
	}

	private void createWebDriverTypeCombo() {
		webDriverTypeCombo = new JComboBox<>();
		webDriverTypeCombo.setPreferredSize(new Dimension(120, 22));
		List<VariableType> variableTypeList = VariableType.getVariablesByGroup(VariableGroupType.VARIABLE_WEBDRIVER_GROUP, true);
		for (VariableType variableType : variableTypeList) {
			webDriverTypeCombo.addItem(variableType);
		}
	}
	
	private RelativeConstraints webDriverNameLabelBinding() {
		Binding[] webDriverNameLabelBinding = new Binding[2];
		webDriverNameLabelBinding[0] = bindingFactory.leftEdge();
		webDriverNameLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(webDriverNameSuggest);
		return new RelativeConstraints(webDriverNameLabelBinding);
	}
	
	private RelativeConstraints webDriverNameSuggestBinding() {
		Binding[] webDriverNameSuggestBinding = new Binding[3];
		webDriverNameSuggestBinding[0] = bindingFactory.rightOf(webDriverNameLabel);
		webDriverNameSuggestBinding[1] = bindingFactory.rightEdge();
		webDriverNameSuggestBinding[2] = bindingFactory.topEdge();
		return new RelativeConstraints(webDriverNameSuggestBinding);
	}

	private RelativeConstraints exeLabelBinding() {
		Binding[] exeLabelBinding = new Binding[2];
		exeLabelBinding[0] = bindingFactory.leftEdge();
		exeLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(webDriverExeSuggest);
		return new RelativeConstraints(exeLabelBinding);
	}
	
	private RelativeConstraints webDriverExeSuggestBinding() {
		Binding[] webDriverExeSuggestBinding = new Binding[3];
		webDriverExeSuggestBinding[0] = bindingFactory.leftAlignedWith(webDriverNameSuggest);
		webDriverExeSuggestBinding[1] = bindingFactory.leftOf(webDriverExeChooser);
		webDriverExeSuggestBinding[2] = bindingFactory.below(webDriverNameSuggest);
		return new RelativeConstraints(webDriverExeSuggestBinding);
	}
	
	private RelativeConstraints webDriverExeChooserBinding() {
		Binding[] webDriverExeChooserBinding = new Binding[3];
		webDriverExeChooserBinding[0] = bindingFactory.rightEdge();
		webDriverExeChooserBinding[1] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, webDriverExeSuggest);
		webDriverExeChooserBinding[2] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, webDriverExeSuggest);
		return new RelativeConstraints(webDriverExeChooserBinding);
	}
	
	private RelativeConstraints webDriverTypeLabelBinding() {
		Binding[] webDriverTypeLabelBinding = new Binding[2];
		webDriverTypeLabelBinding[0] = bindingFactory.leftEdge();
		webDriverTypeLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(webDriverConfigBtn);
		return new RelativeConstraints(webDriverTypeLabelBinding);
	}

	private RelativeConstraints webDriverTypeComboBinding() {
		Binding[] webDriverTypeComboBinding = new Binding[2];
		webDriverTypeComboBinding[0] = bindingFactory.leftAlignedWith(webDriverNameSuggest);
		webDriverTypeComboBinding[1] = bindingFactory.verticallyCenterAlignedWith(webDriverConfigBtn);
		return new RelativeConstraints(webDriverTypeComboBinding);
	}
	
	private RelativeConstraints configLabelBinding() {
		Binding[] configLabelBinding = new Binding[2];
		configLabelBinding[0] = bindingFactory.rightOf(webDriverTypeCombo);
		configLabelBinding[1] = bindingFactory.verticallyCenterAlignedWith(webDriverConfigBtn);
		return new RelativeConstraints(configLabelBinding);
	}

	private RelativeConstraints webDriverConfigBtnBinding() {
		Binding[] webDriverConfigBtnBinding = new Binding[2];
		webDriverConfigBtnBinding[0] = bindingFactory.rightOf(configLabel);
		webDriverConfigBtnBinding[1] = new Binding(Edge.TOP, 1, Direction.BELOW, Edge.TOP, btnRemoveWebDriver);
		return new RelativeConstraints(webDriverConfigBtnBinding);
	}

	private RelativeConstraints btnAddWebDriverBinding() {
		Binding[] btnAddWebDriverBinding = new Binding[2];
		btnAddWebDriverBinding[0] = bindingFactory.leftOf(btnRemoveWebDriver);
		btnAddWebDriverBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveWebDriver);
		return new RelativeConstraints(btnAddWebDriverBinding);
	}

	private RelativeConstraints btnRemoveWebDriverBinding() {
		Binding[] btnRemoveWebDriverBinding = new Binding[2];
		btnRemoveWebDriverBinding[0] = bindingFactory.below(webDriverExeSuggest);
		btnRemoveWebDriverBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveWebDriverBinding);
	}

	private void centerTableLabels(Integer[] checkBoxColumns, JTable table) {
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < table.getColumnCount(); i++) {
			if (!isCheckBoxCol(checkBoxColumns, i)) {
				table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
			}
		}
	}
	
	private boolean isCheckBoxCol(Integer[] checkBoxColumns, int i) {
		for (Integer column : checkBoxColumns) {
			if (i == column) {
				return true;
			}
		}
		return false;
	}
	
	private void addListeners() {
		//Add all File tab listeners
		addFileTabListeners();
		//Add all Object tab listeners
		addObjectTabListeners();
		//Add all SQL tab listeners
		addSQLTabListeners();
		//Add all WebDriver tab listeners
		addWebDriverTabListeners();
		//Add base frame listeners
		labelInformation.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameFlow.this, FramesHelpType.FRAME_FLOW)).setVisible(true);
			}
		});
		labelComment.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
//				(new FrameComment(FrameFlow.this, comment)).setVisible(true);
			}
		});
		btnUpdate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveFlow(textName.getText());
			}
		});
	}
	
	private void addFileTabListeners() {
		fileParamBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				setPressedBtn(fileParamBtn);
				VariableType textFieldType = getTextFieldType();
				if (fileParam != null && textFieldType.equals(fileParamType)) {
					callFrameAdd((new VariableController()).buildVariable(null, null, null, null, textFieldType, fileParam, null));
				} else if (textFieldType != null) {
					callFrameAdd(textFieldType);
				}
			}
		});
		fileTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				editRow(evt, fileTable);//Populate fields
			}
		});
		activateFileNameSuggest();
		btnAddFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object selectedItem = fileNameSuggest.getSelectedItem();
				String variableName = selectedItem != null ? selectedItem.toString() : null;
				if (variableName == null || variableName.length() < 3) {
					JOptionPane.showMessageDialog(FrameFlow.this, "The file name entered is less than three characters or null", "File Alert", JOptionPane.WARNING_MESSAGE);
					return;
				}
				addVariableInTable(VariableGroupType.VARIABLE_FILE_GROUP, null, VariableGroupType.VARIABLE_FILE_GROUP.getFrameAddType().getLabels().get(0), filePathSuggest.getSelectedItem(),
						variableName, fileParamType, fileParam, null);
			}
		});
		btnRemoveFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				blockerLoading.start("Removing File");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						//Check if the variable is being edited
						int editingRow = getEditingRowByGroup(VariableGroupType.VARIABLE_FILE_GROUP);
						if (editingRow >= 0) {
							setDefaultFieldsByGroup(VariableGroupType.VARIABLE_FILE_GROUP, editingRow);
						} else {
							removeSelectedVariables(getFileTableModel());
						}
						blockerLoading.stop();
						return null;
					}
				}.execute();
			}
		});
		fileInformationLabel.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameFlow.this, FramesHelpType.FRAME_FLOW_FILE)).setVisible(true);
			}
		});
	}
	
	private VariableType getTextFieldType() {
		Object item = filePathSuggest.getSelectedItem();
		if (item == null) {
			JOptionPane.showMessageDialog(FrameFlow.this, "The path field is blank.", "File Alert", JOptionPane.WARNING_MESSAGE);
			return null;
		}
		
		String itemAsString = item.toString();
		if (!itemAsString.contains(".")) {
			JOptionPane.showMessageDialog(FrameFlow.this, "The path field does not contains a valid file path.", "File Alert", JOptionPane.WARNING_MESSAGE);
			return null;
		}
		
		String extension = itemAsString.substring(itemAsString.lastIndexOf("."));
		if (extension == null || extension.isEmpty()) {
			JOptionPane.showMessageDialog(FrameFlow.this, "The path field does not contains a valid extension.", "File Alert", JOptionPane.WARNING_MESSAGE);
			return null;
		}
		
		switch(extension) {
			case ".csv":
				return VariableType.VARIABLE_FILE_CSV;
			case ".prop":
				return VariableType.VARIABLE_FILE_PROPERTY;
			case ".xml":
				return VariableType.VARIABLE_FILE_XML;
			default:
				JOptionPane.showMessageDialog(FrameFlow.this, "The path field does not contains an acceptable extension.", "File Alert", JOptionPane.WARNING_MESSAGE);
				return null;
		}
	}
	
	private void addObjectTabListeners() {
		objectTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				editRow(evt, objectTable);//Populate fields
			}
		});
		activateObjectNameSuggest();
		btnAddObject.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object selectedItem = objectNameSuggest.getSelectedItem();
				String variableName = selectedItem != null ? selectedItem.toString() : null;
				if (variableName == null || variableName.length() <= 3) {
					JOptionPane.showMessageDialog(FrameFlow.this, "The object variable name entered is less than three characters or null", "Object Alert", JOptionPane.WARNING_MESSAGE);
					return;
				}
				addVariableInTable(VariableGroupType.VARIABLE_OBJECT_GROUP, null, null, null,
						variableName, (VariableType) objectTypeCombo.getSelectedItem(), null, valueSuggest.getSelectedItem());
			}
		});
		btnRemoveObject.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				blockerLoading.start("Removing Object");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						//Check if the variable is being edited
						int editingRow = getEditingRowByGroup(VariableGroupType.VARIABLE_OBJECT_GROUP);
						if (editingRow >= 0) {
							setDefaultFieldsByGroup(VariableGroupType.VARIABLE_OBJECT_GROUP, editingRow);
						} else {
							removeSelectedVariables(getObjectTableModel());
						}
						blockerLoading.stop();
						return null;
					}
				}.execute();
			}
		});
		objectInformationLabel.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameFlow.this, FramesHelpType.FRAME_FLOW_OBJECT)).setVisible(true);
			}
		});
	}
	
	//Listeners of SQL tab
	private void addSQLTabListeners() {
		sqlDataBaseBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setPressedBtn(sqlDataBaseBtn);
				if (sqlDataBase != null) {
					callFrameAdd((new VarObjectController()).buildVarObject(null, null, VariableGroupType.VARIABLE_SQL_GROUP, sqlDataBase, null, null));
				} else {
					callFrameAdd(VariableGroupType.VARIABLE_SQL_GROUP);
				}
			}
		});
		sqlQueryBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setPressedBtn(sqlQueryBtn);
				if (sqlQuery != null) {
					callFrameAdd((new VariableController()).buildVariable(null, null, null, null, VariableType.VARIABLE_SQL_QUERY, sqlQuery, null));
				} else {
					callFrameAdd(VariableType.VARIABLE_SQL_QUERY);
				}
			}
		});
		sqlTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				editRow(evt, sqlTable);//Populate fields
			}
		});
		activateSqlNameSuggest();
		btnAddSql.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object selectedItem = sqlNameSuggest.getSelectedItem();
				String variableName = selectedItem != null ? selectedItem.toString() : null;
				if (variableName == null || variableName.length() < 3) {
					JOptionPane.showMessageDialog(FrameFlow.this, "The sql name entered is less than three characters or null", "SQL Alert", JOptionPane.WARNING_MESSAGE);
					return;
				}
				addVariableInTable(VariableGroupType.VARIABLE_SQL_GROUP, sqlDataBase, null, null,
						variableName, VariableType.VARIABLE_SQL_QUERY, sqlQuery, null);
			}
		});
		btnRemoveSql.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				blockerLoading.start("Removing SQL");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						//Check if the variable is being edited
						int editingRow = getEditingRowByGroup(VariableGroupType.VARIABLE_SQL_GROUP);
						if (editingRow >= 0) {
							setDefaultFieldsByGroup(VariableGroupType.VARIABLE_SQL_GROUP, editingRow);
						} else {
							removeSelectedVariables(getSqlTableModel());
						}
						blockerLoading.stop();
						return null;
					}
				}.execute();
			}
		});
		sqlInformationLabel.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameFlow.this, FramesHelpType.FRAME_FLOW_SQL)).setVisible(true);
			}
		});
	}

	//Listeners of WebDriver tab
	private void addWebDriverTabListeners() {
		webDriverConfigBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setPressedBtn(webDriverConfigBtn);
				VariableType comboType = (VariableType) webDriverTypeCombo.getSelectedItem();
				if (webDriverConfig != null && comboType.equals(webDriverConfigType)) {
					callFrameAdd((new VariableController()).buildVariable(null, null, null, null, comboType, webDriverConfig, null));
				} else {
					callFrameAdd(webDriverTypeCombo.getSelectedItem());
				}
			}
		});
		webDriverTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				editRow(evt, webDriverTable);//Populate fields
			}
		});
		activateWebDriverNameSuggest();
		btnAddWebDriver.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object selectedItem = webDriverNameSuggest.getSelectedItem();
				String variableName = selectedItem != null ? selectedItem.toString() : null;
				if (variableName == null || variableName.length() < 3) {
					JOptionPane.showMessageDialog(FrameFlow.this, "The web driver name entered is less than three characters or null", "WebDriver Alert", JOptionPane.WARNING_MESSAGE);
					return;
				}
				addVariableInTable(VariableGroupType.VARIABLE_WEBDRIVER_GROUP, null, VariableGroupType.VARIABLE_WEBDRIVER_GROUP.getFrameAddType().getLabels().get(0), webDriverExeSuggest.getSelectedItem(),
						variableName, webDriverConfigType, webDriverConfig, null);
			}
		});
		btnRemoveWebDriver.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				blockerLoading.start("Removing WebDriver");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						//Check if the variable is being edited
						int editingRow = getEditingRowByGroup(VariableGroupType.VARIABLE_WEBDRIVER_GROUP);
						if (editingRow >= 0) {
							setDefaultFieldsByGroup(VariableGroupType.VARIABLE_WEBDRIVER_GROUP, editingRow);
						} else {
							removeSelectedVariables(getWebDriverTableModel());
						}
						blockerLoading.stop();
						return null;
					}
				}.execute();
			}
		});
		webDriverInformationLabel.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameFlow.this, FramesHelpType.FRAME_FLOW_WEBDRIVER)).setVisible(true);
			}
		});
	}

	private boolean addVariableInTable(VariableGroupType groupType, DefaultTableModel groupModel, String groupColName, Object groupValue,
			String variableName, VariableType variableType, DefaultTableModel variableModel, Object variableValue) {
		try {
			Long groupId = null;
			Long varId = null;
			//Check if the variable is being edited
			int editingRow = getEditingRowByGroup(groupType);
			//Get tableModel being edited
			DefaultTableModel model = getTableModelByGroup(groupType);
			//Checks if the name have been changed
			String validName = null;
			if (editingRow >= 0) {
				Variable var = (Variable) model.getValueAt(editingRow, 0);
				groupId = var.getVarObject().getId();
				varId = var.getId();
				validName = variableName.equals(var.getName()) ? variableName : null;
			}

			//Build variable group
			VarObject group = getVariableGroup(groupId, groupType, groupModel, groupColName, groupValue);
			
			//Verifies if is a valid variable name
			VariableController variableController = new VariableController();
			if (validName == null) {
				validName = getValidVariableName(variableController, variableName);
				if (validName == null) {
					return false;
				}
			}
			
			//Build variable
			Variable variable = variableController.buildVariable(varId, validName, flow, group, variableType, variableModel, variableValue);
			//Add file variable to file table
			addNewTableRow(model, group, variable, editingRow);
			//Clean file fields to wait another entry
			setDefaultFieldsByGroup(groupType, editingRow);
		} catch (Exception e) {
			showError(e);
		}
		
		return true;
	}
	
	private int getEditingRowByGroup(VariableGroupType groupType) {
		switch (groupType) {
		case VARIABLE_FILE_GROUP:
			return getEditingFile();
		case VARIABLE_OBJECT_GROUP:
			return getEditingObject();
		case VARIABLE_SQL_GROUP:
			return getEditingSql();
		case VARIABLE_WEBDRIVER_GROUP:
			return getEditingWebDriver();
		}
		
		return -1;
	}
	
	private DefaultTableModel getTableModelByGroup(VariableGroupType groupType) {
		switch (groupType) {
		case VARIABLE_FILE_GROUP:
			return getFileTableModel();
		case VARIABLE_OBJECT_GROUP:
			return getObjectTableModel();
		case VARIABLE_SQL_GROUP:
			return getSqlTableModel();
		case VARIABLE_WEBDRIVER_GROUP:
			return getWebDriverTableModel();
		}
		
		return null;
	}
	
	private VarObject getVariableGroup(Long groupId, VariableGroupType type, DefaultTableModel model, String colName, Object value) throws Exception {
		if (value == null) {
			value = "";
		}
		return (new VarObjectController()).buildVarObject(groupId, flow, type, model, colName, value.toString());
	}

	private String getValidVariableName(VariableController variableController, Object name) throws Exception {
		if (name instanceof SuggestDoubleColumn) {
			name = ((SuggestDoubleColumn) name).getLeftText().getName();
		}
		
		String validName = treatVariableName(name.toString());
		
		Variable result = variableController.findByName(validName);
		if (result != null) {
			Object newName = JOptionPane.showInputDialog(FrameFlow.this, "A variable with this name already exists, please insert a new one with more than three characters.", "Variable Alert", JOptionPane.WARNING_MESSAGE, null, null, name);
			if (newName == null || newName.toString().isEmpty() || newName.toString().length() < 3) {
				return null;
			}
			return getValidVariableName(variableController, newName);
		}
		
		return validName;
	}
	
	private String treatVariableName(String name) {
		name = name.replaceAll("\\s+","");
    	
    	if (!name.startsWith("[")) {
    		name = "[" + name;
    	}
    	if (!name.endsWith("]")) {
    		name = name + "]";
    	}
    	return name;
	}
	
	private void setDefaultFieldsByGroup(VariableGroupType groupType, int editingRow) {
		switch (groupType) {
		case VARIABLE_FILE_GROUP:
			fileNameSuggest.setSelectedItem("");
			filePathSuggest.setSelectedItem("");
			fileParamBtn.setSelected(false);
			fileParam = null;
			if (editingRow >= 0) {
				btnAddFile.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(2));
				btnRemoveFile.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(3));
				setEditingFile(-1);
			}
			break;
		case VARIABLE_OBJECT_GROUP:
			objectNameSuggest.setSelectedItem("");
			valueSuggest.setSelectedItem("");
			objectTypeCombo.setSelectedIndex(0);
			if (editingRow >= 0) {
				btnAddObject.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(2));
				btnRemoveObject.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(3));
				setEditingObject(-1);
			}
			break;
		case VARIABLE_SQL_GROUP:
			sqlNameSuggest.setSelectedItem("");
			sqlDataBaseBtn.setSelected(false);
			sqlDataBase = null;
			sqlQueryBtn.setSelected(false);
			sqlQuery = null;
			if (editingRow >= 0) {
				btnAddSql.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(2));
				btnRemoveSql.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(3));
				setEditingSql(-1);
			}
			break;
		case VARIABLE_WEBDRIVER_GROUP:
			webDriverNameSuggest.setSelectedItem("");
			webDriverExeSuggest.setSelectedItem("");
			webDriverTypeCombo.setSelectedIndex(0);
			webDriverConfigBtn.setSelected(false);
			webDriverConfig = null;
			if (editingRow >= 0) {
				btnAddWebDriver.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(2));
				btnRemoveWebDriver.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(3));
				setEditingWebDriver(-1);
			}
			break;
		}
	}
	
	private void addNewTableRow(DefaultTableModel tableModel, VarObject group, Variable variable, int editingRow) {
		if (tableModel == null) {
			return;
		}
		boolean isObj = VariableGroupType.VARIABLE_OBJECT_GROUP.equals(group.getType());
		boolean isSql = VariableGroupType.VARIABLE_SQL_GROUP.equals(group.getType());
		//Add file variable to file table
		if (editingRow < 0) {
			editingRow = tableModel.getRowCount();
			tableModel.setRowCount(editingRow + 1);
		}
		tableModel.setValueAt(variable, editingRow, 0);// Entity
		tableModel.setValueAt(false, editingRow, 1);//Delete
		tableModel.setValueAt(variable.getName(), editingRow, 2);//Variable name
		//Validate if is VARIABLE_OBJECT_GROUP because it has less columns
		tableModel.setValueAt(isObj ? variable.getVariableType() : groupListToString(group.getParameters()), editingRow, 3);//VariableGroup params (If isObj = Variable VariableType)
		//Validate if is VARIABLE_OBJECT_GROUP because it has less columns
		tableModel.setValueAt(isObj || isSql ? varListToString(variable.getParameters()) : variable.getVariableType(), editingRow, 4);//VariableType (If isObj or isSql = Variable params)
		//Validate if is VARIABLE_OBJECT_GROUP because it has less columns
		if (!isObj && !isSql) {
			tableModel.setValueAt(varListToString(variable.getParameters()), editingRow, 5);//Variable params
		}
	}
	
	private void editRow(MouseEvent evt, JTable table) {
		if (evt.getClickCount() == 2) {
			int selectedRow = table.getSelectedRow();
			Object varAsObj = table.getValueAt(selectedRow, 0);
			if (varAsObj == null || !(varAsObj instanceof Variable)) {
				return;
			}
			
			populateFieldsByGroup((Variable) varAsObj, selectedRow);
		}
	}
	
	private void populateFieldsByGroup(Variable variable, int selectedRow) {
		if (variable == null) {
			return;
		}
		
		DefaultTableModel variableModel = variableListAsModel(variable.getParameters());
		if (variableModel == null) {
			return;
		}
		
		switch (variable.getVarObject().getType()) {
		case VARIABLE_FILE_GROUP:
			fileNameSuggest.setSelectedItem(variable.getName());
			filePathSuggest.setSelectedItem(variable.getVarObject().getParameters().get(0).getValue());
			fileParamBtn.setSelected(true);
			fileParam = variableModel;
			fileParamType = variable.getVariableType();
			btnAddFile.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(4));
			btnRemoveFile.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(5));
			setEditingFile(selectedRow);
			break;
		case VARIABLE_OBJECT_GROUP:
			objectNameSuggest.setSelectedItem(variable.getName());
			valueSuggest.setSelectedItem(variable.getParameters().get(0).getValue());
			objectTypeCombo.setSelectedItem(variable.getVariableType());
			btnAddObject.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(4));
			btnRemoveObject.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(5));
			setEditingObject(selectedRow);
			break;
		case VARIABLE_SQL_GROUP:
			sqlNameSuggest.setSelectedItem(variable.getName());
			sqlDataBaseBtn.setSelected(true);
			sqlDataBase = groupListAsModel(variable.getVarObject().getParameters());
			sqlQueryBtn.setSelected(true);
			sqlQuery = variableModel;
			btnAddSql.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(4));
			btnRemoveSql.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(5));
			setEditingSql(selectedRow);
			break;
		case VARIABLE_WEBDRIVER_GROUP:
			webDriverNameSuggest.setSelectedItem(variable.getName());
			webDriverExeSuggest.setSelectedItem(variable.getVarObject().getParameters().get(0).getValue());
			webDriverTypeCombo.setSelectedItem(variable.getVariableType());
			webDriverConfigBtn.setSelected(true);
			webDriverConfig = variableModel;
			webDriverConfigType = variable.getVariableType();
			btnAddWebDriver.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(4));
			btnRemoveWebDriver.setIcon(FramesHelpType.FRAME_FLOW_FILE.getIcons().get(5));
			setEditingWebDriver(selectedRow);
			break;
		}
	}
	
	private DefaultTableModel variableListAsModel(List<VariableParameter> variableListParam) {
		if (variableListParam == null || variableListParam.isEmpty()) {
			return null;
		}
		DefaultTableModel modelToReturn = new DefaultTableModel(new Object[]{"Parameter", "Value"}, variableListParam.size());
		for (VariableParameter param : variableListParam) {
			modelToReturn.addRow(new Object[]{param.getName(), param.getValue()});
		}
		
		return modelToReturn;
	}
	
	private DefaultTableModel groupListAsModel(List<VarObjectParameter> groupListParam) {
		if (groupListParam == null || groupListParam.isEmpty()) {
			return null;
		}
		DefaultTableModel modelToReturn = new DefaultTableModel(new Object[]{"Parameter", "Value"}, groupListParam.size());
		for (VarObjectParameter param : groupListParam) {
			modelToReturn.addRow(new Object[]{param.getName(), param.getValue()});
		}
		
		return modelToReturn;
	}
	
	private void saveFlow(final String name) {
		blockerLoading.start("Updating Flow");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				if (name == null || name.length() < 3) {
					JOptionPane.showMessageDialog(FrameFlow.this, "The name entered is less than three characters or null", "Flow Alert", JOptionPane.WARNING_MESSAGE);
					blockerLoading.stop();
					return null;
				}
				
				boolean nameChanged = !name.equals(flow.getNmFlow());
				FlowController flowController = new FlowController();
				
				try {
					if (nameChanged) {
						String validName = name.equalsIgnoreCase(flow.getNmFlow()) ? name : getValidFlowName(flowController, name);
						if (validName == null) {
							blockerLoading.stop();
							return null;
						}
						flow.setNmFlow(validName);
					}
				
					deleteVariables();
					flowController.update(flow, getFileTableModel(), getObjectTableModel(), getSqlTableModel(), getWebDriverTableModel());
				
					if (nameChanged) {
						frameMain.updateTabTitle(flow.getNmFlow());
					}
				} catch (Exception e) {
					showError(e);
				}
				blockerLoading.stop();
				dispose();
				return null;
			}
		}.execute();
	}
	
	private String getValidFlowName(FlowController flowController, String name) throws Exception {
		Flow result = flowController.findByName(name);
		if (result != null) {
			Object newName = JOptionPane.showInputDialog(FrameFlow.this, "This name already exist, please insert a new one with more than three characters.", "Flow Alert", JOptionPane.WARNING_MESSAGE, null, null, name);
			if (newName == null || newName.toString().isEmpty() || newName.toString().length() < 3) {
				return null;
			}
			return getValidFlowName(flowController, newName.toString());
		}
		
		return name;
	}
	
	private void deleteVariables() throws Exception {
		VariableController variableController = new VariableController();
		for (Variable toDelete : deleteList) {
			variableController.remove((Variable) toDelete);
		}
	}
	
	private void callFrameAdd(Object selected) {
		if (selected instanceof VariableGroupType|| selected instanceof VarObject 
				|| selected instanceof VariableType || selected instanceof Variable || selected instanceof List<?>) {
			(new FrameAdd(FrameFlow.this, selected)).setVisible(true);
		} else {
			JOptionPane.showMessageDialog(FrameFlow.this, "Select a valid option.", "Flow Alert", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public boolean frameAddCallBack(DefaultTableModel model) {
		if (fileParamBtn.equals(getPressedBtn())) {
			fileParam = model;
			fileParamType = getTextFieldType();
			return true;
		} else if (sqlDataBaseBtn.equals(getPressedBtn())) {
			sqlDataBase = model;
			return true;
		} else if (sqlQueryBtn.equals(getPressedBtn())) {
			sqlQuery = model;
			return true;
		} else if (webDriverConfigBtn.equals(getPressedBtn())) {
			webDriverConfig = model;
			webDriverConfigType = (VariableType) webDriverTypeCombo.getSelectedItem();
			return true;
		}
		
		return false;
	}
	
	private boolean removeSelectedVariables(DefaultTableModel model) {
		List<Integer> variablesToDelete = new ArrayList<>();
		for (int i = 0; i < model.getRowCount(); i++) {
			if ((boolean) model.getValueAt(i, 1)) {
				variablesToDelete.add(i);
			}
		}
		
		if (variablesToDelete.isEmpty()) {
			JOptionPane.showMessageDialog(FrameFlow.this, "Select at least one row.", "Flow Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		deleteVariables(variablesToDelete, model);
		return true;
	}
	
	private void deleteVariables(List<Integer> variablesToDelete, DefaultTableModel model) {
		for (Integer toDelete : variablesToDelete) {
			Variable variableToDelete = (Variable) model.getValueAt(toDelete, 0);
			if (variableToDelete.getId() != null) {
				deleteList.add(variableToDelete);
			}
			model.removeRow(toDelete);
		}
	}

	private String groupListToString(List<VarObjectParameter> objParamList) {
		String treatedParams = "";
		for (VarObjectParameter objParam : objParamList) {
			treatedParams += ";" + objParam.getName() + "=" + objParam.getValue();
		}
		return treatedParams.isEmpty() ? treatedParams : treatedParams.substring(1);
	}

	private String varListToString(List<VariableParameter> varParamList) {
		String treatedParams = "";
		for (VariableParameter varParam : varParamList) {
			treatedParams += ";" + varParam.getName() + "=" + varParam.getValue();
		}
		return treatedParams.isEmpty() ? treatedParams : treatedParams.substring(1);
	}

	private void setSavedVariables() {
		textName.setText(flow.getNmFlow());
		for (Variable variable : flow.getFlowVariables()) {
			VarObject group = variable.getVarObject();
			addNewTableRow(getTableModelByGroup(group.getType()), group, variable, -1);
		}
	}

    private void setInvisibleColumn(JTable table) {
		table.getColumnModel().getColumn(0).setWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
    }
    
    private void activateFileNameSuggest() {
    	fileNameSuggest.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = fileNameSuggest.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasFileArrows && key == KeyEvent.VK_ENTER) {
								fileNameSuggest.setSelectedItem(item);
								fileNameSuggest.hidePopup();
								wasFileArrows = false;
								setParams(item);
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(fileNameSuggest, item.toString(), VariableGroupType.VARIABLE_FILE_GROUP);
							} else {
								wasFileArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		JList<?> suggestList = getSuggestList(fileNameSuggest);
		if (suggestList != null) {
			suggestList.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					JList<?> list = (JList<?>) e.getComponent();
					Point pt = e.getPoint();
					int index = list.locationToIndex(pt);
					if (index >= 0) {
						Object item = fileNameSuggest.getItemAt(index);
						fileNameSuggest.setSelectedItem(item);
						fileNameSuggest.hidePopup();
						setParams(item);
					}
				}
			});
		}
	}
    
    private void activateObjectNameSuggest() {
    	objectNameSuggest.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = objectNameSuggest.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasObjectArrows && key == KeyEvent.VK_ENTER) {
								objectNameSuggest.setSelectedItem(item);
								objectNameSuggest.hidePopup();
								wasObjectArrows = false;
								setParams(item);
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(objectNameSuggest, item.toString(), VariableGroupType.VARIABLE_OBJECT_GROUP);
							} else {
								wasObjectArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		JList<?> suggestList = getSuggestList(objectNameSuggest);
		if (suggestList != null) {
			suggestList.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					JList<?> list = (JList<?>) e.getComponent();
					Point pt = e.getPoint();
					int index = list.locationToIndex(pt);
					if (index >= 0) {
						Object item = objectNameSuggest.getItemAt(index);
						objectNameSuggest.setSelectedItem(item);
						objectNameSuggest.hidePopup();
						setParams(item);
					}
				}
			});
		}
	}
    
    private void activateSqlNameSuggest() {
    	sqlNameSuggest.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = sqlNameSuggest.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasSqlArrows && key == KeyEvent.VK_ENTER) {
								sqlNameSuggest.setSelectedItem(item);
								sqlNameSuggest.hidePopup();
								wasSqlArrows = false;
								setParams(item);
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(sqlNameSuggest, item.toString(), VariableGroupType.VARIABLE_SQL_GROUP);
							} else {
								wasSqlArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		JList<?> suggestList = getSuggestList(sqlNameSuggest);
		if (suggestList != null) {
			suggestList.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					JList<?> list = (JList<?>) e.getComponent();
					Point pt = e.getPoint();
					int index = list.locationToIndex(pt);
					if (index >= 0) {
						Object item = sqlNameSuggest.getItemAt(index);
						sqlNameSuggest.setSelectedItem(item);
						sqlNameSuggest.hidePopup();
						setParams(item);
					}
				}
			});
		}
	}
    
    private void activateWebDriverNameSuggest() {
    	webDriverNameSuggest.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = webDriverNameSuggest.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasWebDriverArrows && key == KeyEvent.VK_ENTER) {
								webDriverNameSuggest.setSelectedItem(item);
								webDriverNameSuggest.hidePopup();
								wasWebDriverArrows = false;
								setParams(item);
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(webDriverNameSuggest, item.toString(), VariableGroupType.VARIABLE_WEBDRIVER_GROUP);
							} else {
								wasWebDriverArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		JList<?> suggestList = getSuggestList(webDriverNameSuggest);
		if (suggestList != null) {
			suggestList.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					JList<?> list = (JList<?>) e.getComponent();
					Point pt = e.getPoint();
					int index = list.locationToIndex(pt);
					if (index >= 0) {
						Object item = webDriverNameSuggest.getItemAt(index);
						webDriverNameSuggest.setSelectedItem(item);
						webDriverNameSuggest.hidePopup();
						setParams(item);
					}
				}
			});
		}
	}

	public void comboFilter(final SuggestCombo suggest, final String searchFor, final VariableGroupType type) {
		blockerLoading.start("Searching equivalences");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				List<SuggestDoubleColumn> items = getItems(searchFor, type);
				if (items.size() > 0) {
					suggest.setModel(new DefaultComboBoxModel(items.toArray()));
					suggest.setSelectedItem(searchFor.isEmpty() ? "" : searchFor);
					JTextField editor = (JTextField) suggest.getEditor().getEditorComponent();
					editor.setCaretPosition(editor.getDocument().getLength());
					suggest.showPopup();
				} else {
					suggest.hidePopup();
				}
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}

	private List<SuggestDoubleColumn> getItems(String searchFor, VariableGroupType type) {
		List<SuggestDoubleColumn> returnList = new ArrayList<>();
		try {
			for (Variable var : (new VariableController()).findLikeNameAndGroupType(searchFor, type)) {
				returnList.add(new SuggestDoubleColumn(new CellValue(var.getId(), var.getName(), null, var), searchFor));
			}
		} catch (Exception e) {
			showError(e);
		}
		return returnList;
	}

	private JList<?> getSuggestList(SuggestCombo suggest) {
		Accessible a = suggest.getAccessibleContext().getAccessibleChild(0);
		if (a instanceof BasicComboPopup) {
			return ((BasicComboPopup) a).getList();
		} else {
			return null;
		}
	}
	
	private void setParams(Object item) {
		if (!(item instanceof SuggestDoubleColumn)) {
			return;
		}
		callFrameAdd(((CellValue) ((SuggestDoubleColumn) item).getLeftText()).getFullItem());
	}
	
	private DefaultTableModel getFileTableModel() {
		return fileTableModel;
	}
	
	private void setFileTableModel(DefaultTableModel fileTableModel) {
		this.fileTableModel = fileTableModel;
	}
	
	private DefaultTableModel getSqlTableModel() {
		return sqlTableModel;
	}
	
	private void setSqlTableModel(DefaultTableModel sqlTableModel) {
		this.sqlTableModel = sqlTableModel;
	}

	private DefaultTableModel getWebDriverTableModel() {
		return webDriverTableModel;
	}

	private void setWebDriverTableModel(DefaultTableModel webDriverTableModel) {
		this.webDriverTableModel = webDriverTableModel;
	}

	private DefaultTableModel getObjectTableModel() {
		return objectTableModel;
	}

	private void setVariableTableModel(DefaultTableModel variablesTableModel) {
		this.objectTableModel = variablesTableModel;
	}

	public JToggleButton getPressedBtn() {
		return pressedBtn;
	}

	public void setPressedBtn(JToggleButton pressedBtn) {
		this.pressedBtn = pressedBtn;
	}

	public int getEditingFile() {
		return editingFile;
	}

	public void setEditingFile(int editingFile) {
		this.editingFile = editingFile;
	}

	public int getEditingObject() {
		return editingObject;
	}

	public void setEditingObject(int editingObject) {
		this.editingObject = editingObject;
	}

	public int getEditingSql() {
		return editingSql;
	}

	public void setEditingSql(int editingSql) {
		this.editingSql = editingSql;
	}

	public int getEditingWebDriver() {
		return editingWebDriver;
	}

	public void setEditingWebDriver(int editingWebDriver) {
		this.editingWebDriver = editingWebDriver;
	}
}
