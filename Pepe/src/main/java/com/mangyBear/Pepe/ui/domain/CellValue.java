package com.mangyBear.Pepe.ui.domain;

import java.io.Serializable;

import com.mangyBear.Pepe.domain.types.graph.GraphType;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class CellValue implements Serializable{
    
	private static final long serialVersionUID = 1L;
	
	private Long id;
    private String name;
    private GraphType graphType;
    private Object fullItem;
    
    public CellValue(Long id, String name, GraphType graphType, Object fullItem) {
    	this.id = id;
    	this.name = name;
    	this.graphType = graphType;
    	this.fullItem = fullItem;
    }

	@Override
    public String toString() {
        return this.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long idValue) {
        this.id = idValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String nameValue) {
        this.name = nameValue;
    }

    public GraphType getGraphType() {
        return graphType;
    }

    public void setGraphType(GraphType graphType) {
        this.graphType = graphType;
    }
    
    public Object getFullItem() {
		return fullItem;
	}

	public void setFullItem(Object fullItem) {
		this.fullItem = fullItem;
	}
}
