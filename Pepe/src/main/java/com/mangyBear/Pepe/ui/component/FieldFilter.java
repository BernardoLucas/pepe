package com.mangyBear.Pepe.ui.component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.mangyBear.Pepe.domain.types.frame.FrameFieldType;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FieldFilter extends PlainDocument {

	private static final long serialVersionUID = -2843839446882209885L;
	
	private final int maxLength;
	private final FrameFieldType frameFieldType;

	public FieldFilter(FrameFieldType frameField) {
		this.maxLength = 1000;
		this.frameFieldType = frameField;
	}

	@Override
	public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {

		if (str == null) {
			return;
		}

		Pattern p = null;
		Matcher m = null;
		try {
			switch (frameFieldType) {
				case STRING:// anything
					break;
				case POSITION:// only numbers
				case INTEGER: // only numbers
					p = Pattern.compile("^[0-9]+$");
					m = p.matcher(str);
					break;
				case DOUBLE:// numbers and period
					p = Pattern.compile("^[0-9\\.]+$");
					m = p.matcher(str);
					break;
				default:
			}
		} catch (NumberFormatException e) {
			return;
		}
		
		if (m != null && !m.matches()) {
			return;
		}

		String oldString = getText(0, getLength());
		String newString = oldString.substring(0, offs) + str + oldString.substring(offs);

		int newStringLength = newString.length();
		if (newStringLength > frameFieldType.getLength() || newStringLength > maxLength) {
			super.insertString(offs, "", a);
		} else {
			super.insertString(offs, str, a);
		}
	}
}
