package com.mangyBear.Pepe.ui.frame;

import static com.mangyBear.Pepe.domain.types.frame.FrameAddType.SEND_COMMAND;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BoxView;
import javax.swing.text.ComponentView;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.ParagraphView;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

import com.mangyBear.Pepe.domain.types.action.ActionType;
import com.mangyBear.Pepe.domain.types.action.FinderType;
import com.mangyBear.Pepe.domain.types.frame.FrameAddHelpType;
import com.mangyBear.Pepe.domain.types.frame.FrameAddType;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.domain.types.variable.VariableType;
import com.mangyBear.Pepe.service.MyConstants;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameInformation extends JDialog {

	private static final long serialVersionUID = -1766540586363990481L;
	private static final String frameName = "Information";
	
    private JPanel basePanel;
    private final double separator;
    private final double cols;
    private final double margin;
    private double fontHeight;
    private double rows;
    private double frameWidth;

    private double descriptionHeight;
    private double columnZeroWidth;
    private double columnOneWidth;
    private double rowsHeight;
    private double screenTotalHeight;
    private double panelHeight;
    private double frameMaxHeight;

    private JTextPane descriptionText;
    private JTextPane columnZeroText;
    private JTextPane columnOneText;

    private String description;
    private List<String> params;
    private List<String> tips;
    private boolean hasLink;
    private boolean isTableHigher;
    
    public FrameInformation(JFrame frame, Object withinParams) {
        super(frame, frameName, true);
        this.separator = 1;
        this.cols = 2;
        this.margin = 5;
        this.frameWidth = 500;

        buildFrame(withinParams);
        setLocationRelativeTo(frame);
    }

    public FrameInformation(JDialog dialog, Object withinParams) {
        super(dialog, frameName, true);
        this.separator = 1;
        this.cols = 2;
        this.margin = 5;
        this.frameWidth = 500;

        buildFrame(withinParams);
        setLocationRelativeTo(dialog);
    }
    
    private void buildFrame(Object withinParams) {
        setLayout(new BorderLayout());
        setResizable(false);
    	screenTotalHeight = (double) MyConstants.getScreenSize().getHeight();
    	getParams(withinParams);
    	
        calcComponentsBounds();
        createComponents();
		calcGeneralBounds();
        
        while(isTableHigher && frameWidth < 800) {
    		frameWidth += 100;
    		calcComponentsBounds();
    		createComponents();
    		calcGeneralBounds();
        }

        addBasePanel();
    }
    
    private void calcGeneralBounds() {
    	double tableHeight = (rows * rowsHeight) + descriptionHeight;
        panelHeight = tableHeight + (hasLink ? 35.0 : 0.0);
        frameMaxHeight = screenTotalHeight - (screenTotalHeight / 4);
        isTableHigher = panelHeight > frameMaxHeight;
    }

    private void addBasePanel() {
    	basePanel.setPreferredSize(new Dimension((int) frameWidth, (int) panelHeight));
    	if (isTableHigher) {
    		basePanel.setPreferredSize(new Dimension((int) frameWidth, ((int) panelHeight + (int) rowsHeight)));
    		JScrollPane scroll = new JScrollPane(basePanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    		scroll.setPreferredSize(new Dimension((int) frameWidth, (int) frameMaxHeight));
    		add(scroll, BorderLayout.CENTER);
    	} else {
	    	add(basePanel, BorderLayout.CENTER);
    	}
    	pack();
    }

    private void getParams(Object withinParams) {
        if (withinParams instanceof FinderType) {
        	withinParams = ((FinderType) withinParams).getFrameAddType();
        } else if (withinParams instanceof ActionType) {
        	withinParams = ((ActionType) withinParams).getFrameAddType();
        } else if (withinParams instanceof VariableGroupType) {
        	withinParams = ((VariableGroupType) withinParams).getFrameAddType();
        } else if (withinParams instanceof VariableType) {
        	withinParams = ((VariableType) withinParams).getFrameAddType();
        }
        
        if (withinParams instanceof FrameAddType) {
        	FrameAddHelpType param = ((FrameAddType) withinParams).getHelpType();
        	description = param.getHelpDescription();
        	params = param.getLabels();
        	tips = param.getInfos();
        }
        if (withinParams instanceof FramesHelpType) {
        	FramesHelpType param = (FramesHelpType) withinParams;
            description = param.getDescription();
            params = param.getLabels();
            tips = param.getInfos();
        }
        if (params != null && !params.get(0).isEmpty()) {
            rows = params.size();
        }
    }

    private void calcComponentsBounds() {
        columnZeroWidth = calcColumnZeroWidth();
        rowsHeight = calcColumnOneWidthAndHeight();
        descriptionHeight = calcDescriptionHeight();
    }

    private double calcColumnZeroWidth() {
        double width = getBiggerLength(params);
        double halfFrame = (frameWidth / 2);
        return width > halfFrame ? halfFrame : width;
    }

    private double calcColumnOneWidthAndHeight() {
        columnOneWidth = frameWidth - columnZeroWidth;
        double numOfRows = getNumOfRows(tips);
        return (numOfRows * fontHeight) + margin;
    }

    private double calcDescriptionHeight() {
        double numOfRows;
        if (description.contains("\n")) {
            numOfRows = calcNumOfRowsWithWrap(description, frameWidth);
        } else {
            numOfRows = calcNumOfRows(description, frameWidth);
        }
        return (numOfRows * fontHeight) + margin;
    }

    private double getBiggerLength(List<String> toCount) {
        double biggerLength = 2;
        JTextPane textPane = new JTextPane();
        FontMetrics fm = textPane.getFontMetrics(textPane.getFont());
        for (String parameter : toCount) {
        	double paramLength = fm.stringWidth(parameter);
            biggerLength = (biggerLength < paramLength) ? paramLength : biggerLength;
        }
        return biggerLength + 15;
    }

    private double getNumOfRows(List<String> toCount) {
        double biggerRowCount = 0;
        double rowsNum;
        for (String parameter : toCount) {
            if (parameter.contains("\n")) {
                rowsNum = calcNumOfRowsWithWrap(parameter, columnOneWidth);
            } else {
                rowsNum = calcNumOfRows(parameter, columnOneWidth);
            }
            biggerRowCount = (biggerRowCount < rowsNum) ? rowsNum : biggerRowCount;
        }
        return biggerRowCount;
    }

    private double calcNumOfRowsWithWrap(String parameter, double areaWidth) {
        double rowsNum = 0;
        String[] tempList = parameter.split("\n");
        for (String temp : tempList) {
        	rowsNum += calcNumOfRows(temp, areaWidth);
        }
        return rowsNum;
    }

    private double calcNumOfRows(String temp, double areaWidth) {
        JTextPane textPane = new JTextPane();
        FontMetrics fm = textPane.getFontMetrics(textPane.getFont());
        fontHeight = fm.getHeight();
    	double tempWidth = fm.stringWidth(hasLink(temp));
        return (tempWidth / areaWidth) + 1;
    }

    private String hasLink(String param) {
        if (param.contains("Click \"More\"")) {
            hasLink = true;
        }
        return param;
    }
    
    private void createComponents() {
    	int contParams = 0;
    	int contTips = 0;
    	basePanel = new JPanel(new RelativeLayout());
        descriptionText = createTextPane(frameWidth, descriptionHeight, description);
        basePanel.add(descriptionText, descriptionBinding());
        for (int i = 0; i < (cols * rows); i++) {
            if (i % 2 == 0) {
                columnZeroText = createTextPane(columnZeroWidth, rowsHeight, params.get(contParams));
                basePanel.add(columnZeroText, zeroBinding(i));
                contParams++;
            } else {
                columnOneText = createTextPane(columnOneWidth, rowsHeight, tips.get(contTips));
                basePanel.add(columnOneText, oneBinding());
                contTips++;
            }
        }
        if (hasLink) {
            basePanel.add(createBtnMore(), btnMoreBinding());
        }
    }

    private JTextPane createTextPane(double width, double height, String text) {
        JTextPane textPane = new JTextPane();
        textPane.setPreferredSize(new Dimension((int) width, (int) height));
        setCenteredText(textPane);
        textPane.setEditable(false);
        textPane.setText(text);
        return textPane;
    }

    private void setCenteredText(JTextPane textPane) {
        textPane.setEditorKit(new MyEditorKit());
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        StyledDocument doc = textPane.getStyledDocument();
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }

    private RelativeConstraints descriptionBinding() {
        Binding[] descriptionBinding = new Binding[3];
        descriptionBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.TOP, Binding.PARENT);
        descriptionBinding[1] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        descriptionBinding[2] = new Binding(Edge.RIGHT, 0, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        return new RelativeConstraints(descriptionBinding);
    }

    private RelativeConstraints zeroBinding(int i) {
        Binding[] zeroBinding = new Binding[3];
        zeroBinding[0] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        zeroBinding[1] = new Binding(Edge.RIGHT, (int) columnZeroWidth, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        zeroBinding[2] = new Binding(Edge.TOP, (int) separator, Direction.BELOW, Edge.BOTTOM, (i == 0) ? descriptionText : columnOneText);
        return new RelativeConstraints(zeroBinding);
    }

    private RelativeConstraints oneBinding() {
        Binding[] oneBinding = new Binding[4];
        oneBinding[0] = new Binding(Edge.LEFT, (int) (columnZeroWidth + 1), Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        oneBinding[1] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.TOP, columnZeroText);
        oneBinding[2] = new Binding(Edge.BOTTOM, 0, Direction.ABOVE, Edge.BOTTOM, columnZeroText);
        oneBinding[3] = new Binding(Edge.RIGHT, 0, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        return new RelativeConstraints(oneBinding);
    }

    private JButton createBtnMore() {
        JButton btn = new JButton("More");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                (new FrameInformation(FrameInformation.this, SEND_COMMAND)).setVisible(true);
            }
        });
        btn.setVisible(hasLink);
        return btn;
    }

    private RelativeConstraints btnMoreBinding() {
        Binding[] btnMoreBinding = new Binding[2];
        btnMoreBinding[0] = new Binding(Edge.HORIZONTAL_CENTER, 0, Direction.RIGHT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
        btnMoreBinding[1] = new Binding(Edge.TOP, 5, Direction.BELOW, Edge.BOTTOM, columnOneText);
        return new RelativeConstraints(btnMoreBinding);
    }
}

class MyEditorKit extends StyledEditorKit {

	private static final long serialVersionUID = -6930201989439496853L;

	@Override
    public ViewFactory getViewFactory() {
        return new StyledViewFactory();
    }

    static class StyledViewFactory implements ViewFactory {

        @Override
        public View create(Element elem) {
            String kind = elem.getName();
            if (kind != null) {
                switch (kind) {
                    case AbstractDocument.ContentElementName:
                        return new LabelView(elem);
                    case AbstractDocument.ParagraphElementName:
                        return new ParagraphView(elem);
                    case AbstractDocument.SectionElementName:
                        return new CenteredBoxView(elem, View.Y_AXIS);
                    case StyleConstants.ComponentElementName:
                        return new ComponentView(elem);
                    case StyleConstants.IconElementName:
                        return new IconView(elem);
                }
            }
            return new LabelView(elem);
        }
    }
}

class CenteredBoxView extends BoxView {

    public CenteredBoxView(Element elem, int axis) {
        super(elem, axis);
    }

    @Override
    protected void layoutMajorAxis(int targetSpan, int axis, int[] offsets, int[] spans) {

        super.layoutMajorAxis(targetSpan, axis, offsets, spans);
        int spansLength = spans.length;
        int textBlockHeight = spansLength > 0 ? spans[0] : 0;
        int offset;

        if (spansLength > 1) {
            textBlockHeight = textBlockHeight * spansLength;
        }

        offset = (targetSpan - textBlockHeight) / 2;
        for (int i = 0; i < offsets.length; i++) {
            offsets[i] += offset;
        }
    }
}

