package com.mangyBear.Pepe.ui.frame;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.service.ExecuteFlowService;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameAskPause extends JDialog {

	private static final long serialVersionUID = 6824925852023704982L;

	private final BindingFactory bindingFactory;

	private JLabel message;
	private JLabel labelInformation;
	private JButton btnPlay;
	private JButton btnRestart;
	private JButton btnStop;

	public FrameAskPause() {
		this.bindingFactory = new BindingFactory();
		setModal(true);
		initGUI();
		createComponents();
		addComponents();
		addListeners();
	}

	private void initGUI() {
		setTitle(FramesHelpType.FRAME_ASK_PAUSE.getName());
		setLayout(new RelativeLayout());
		setSize(new Dimension(300, 135));
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
	}

	private void createComponents() {
		message = new JLabel("Please, choose an option.");
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_ASK_PAUSE.getIcons().get(0));
		btnPlay = new JButton(FramesHelpType.FRAME_ASK_PAUSE.getLabels().get(0));
		btnPlay.setIcon(FramesHelpType.FRAME_ASK_PAUSE.getIcons().get(1));
		btnRestart = new JButton(FramesHelpType.FRAME_ASK_PAUSE.getLabels().get(1));
		btnRestart.setIcon(FramesHelpType.FRAME_ASK_PAUSE.getIcons().get(2));
		btnStop = new JButton(FramesHelpType.FRAME_ASK_PAUSE.getLabels().get(2));
		btnStop.setIcon(FramesHelpType.FRAME_ASK_PAUSE.getIcons().get(3));
	}

	private void addComponents() {
		add(message, messageBinding());
		add(labelInformation, labelInformationBinding());
		add(btnPlay, btnPlayBinding());
		add(btnRestart, btnRestartBinding());
		add(btnStop, btnStopBinding());
	}

	private RelativeConstraints messageBinding() {
		Binding[] messageBinding = new Binding[2];
		messageBinding[0] = new Binding(Edge.HORIZONTAL_CENTER, 0, Direction.RIGHT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
		messageBinding[1] = new Binding(Edge.TOP, 15, Direction.BELOW, Edge.TOP, Binding.PARENT);
		return new RelativeConstraints(messageBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private RelativeConstraints btnPlayBinding() {
		Binding[] btnPlayBinding = new Binding[2];
		btnPlayBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRestart);
		btnPlayBinding[1] = new Binding(Edge.RIGHT, 5, Direction.LEFT, Edge.LEFT, btnRestart);
		return new RelativeConstraints(btnPlayBinding);
	}

	private RelativeConstraints btnRestartBinding() {
		Binding[] btnRestartBinding = new Binding[2];
		btnRestartBinding[0] = new Binding(Edge.TOP, 10, Direction.BELOW, Edge.BOTTOM, message);
		btnRestartBinding[1] = new Binding(Edge.HORIZONTAL_CENTER, 0, Direction.RIGHT, Edge.HORIZONTAL_CENTER, message);
		return new RelativeConstraints(btnRestartBinding);
	}

	private RelativeConstraints btnStopBinding() {
		Binding[] btnStopBinding = new Binding[2];
		btnStopBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRestart);
		btnStopBinding[1] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.RIGHT, btnRestart);
		return new RelativeConstraints(btnStopBinding);
	}

	private void addListeners() {
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameAskPause.this, FramesHelpType.FRAME_ASK_PAUSE)).setVisible(true);
			}
		});
		btnPlay.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ExecuteFlowService.setExecStatus(0);
				dispose();
			}
		});
		btnRestart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ExecuteFlowService.setExecStatus(2);
				dispose();
			}
		});
		btnStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ExecuteFlowService.setExecStatus(3);
				dispose();
			}
		});
	}
}
