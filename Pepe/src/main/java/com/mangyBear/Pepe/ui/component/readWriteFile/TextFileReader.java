package com.mangyBear.Pepe.ui.component.readWriteFile;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class TextFileReader {
	
	protected Charset defaultCharset;

	protected List<String> readFile(File pathFile) {
        try {
            if (!pathFile.exists()) {
            	return null;
            }
            
            String[] charsetsToBeTested = {"UTF-8", "windows-1252", "ISO-8859-7", "windows-1251"};
            setDefaultCharset((new CharsetDetector()).detectCharset(pathFile, charsetsToBeTested));
            
            BufferedReader buffer;
            if (getDefaultCharset() == null) {
            	buffer = new BufferedReader(new InputStreamReader(new FileInputStream(pathFile)));
            } else {
            	buffer = new BufferedReader(new InputStreamReader(new FileInputStream(pathFile), getDefaultCharset()));
            }
            
            List<String> rows = new ArrayList<>();
            String row = buffer.readLine();
            while (row != null) {
                rows.add(row);
                row = buffer.readLine();
            }
            buffer.close();
            return rows.isEmpty() ? null : rows;
        } catch (IOException ex) {
        	return null;
        }
    }

    protected int validateInt(int intToValidate) {
        return intToValidate < 0 ? 0 : intToValidate;
    }

	public Charset getDefaultCharset() {
		return defaultCharset;
	}

	public void setDefaultCharset(Charset defaultCharset) {
		this.defaultCharset = defaultCharset;
	}
}

class CharsetDetector {

    public Charset detectCharset(File file, String[] charsets) {

        for (String charsetName : charsets) {
        	Charset charset = detectCharset(file, Charset.forName(charsetName));
            if (charset != null) {
                return charset;
            }
        }

        return null;
    }

    private Charset detectCharset(File file, Charset charset) {
        try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(file))) {

            CharsetDecoder decoder = charset.newDecoder();
            decoder.reset();

            byte[] buffer = new byte[512];
            boolean identified = true;
            while ((input.read(buffer) != -1) && identified) {
                identified = identify(buffer, decoder);
            }

            input.close();

            if (identified) {
                return charset;
            }

        } catch (Exception e) {
        }
        return null;
    }

    private boolean identify(byte[] bytes, CharsetDecoder decoder) {
        try {
            decoder.decode(ByteBuffer.wrap(bytes));
        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }
}
