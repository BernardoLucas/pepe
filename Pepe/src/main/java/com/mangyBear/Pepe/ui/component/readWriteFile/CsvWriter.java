package com.mangyBear.Pepe.ui.component.readWriteFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 *
 * @author LucasBS
 */
public class CsvWriter extends CsvReader {

	public void updateCell(File filePath, int row, int column, String text) {
		int validRow = row < 0 ? 0 : row;
		int validColumn = column < 0 ? 0 : column;

		List<String[]> matrixCsv = readAll(filePath);

		validateMatrix(matrixCsv, row, column);

		(matrixCsv.get(validRow))[validColumn] = text;

		writeCsv(filePath, matrixCsv);
	}

	private void validateMatrix(List<String[]> matrix, int row, int column) {
		String[] columns = new String[column];
		if (matrix.isEmpty()) {
			for (int i = 0; i < row; i++) {
				String[] newColumns = cleanNull(columns);
				matrix.add(newColumns);
			}
		}

		if (matrix.size() < row) {// If matrix has less rows, add the difference
			int variation = row - matrix.size();
			for (int i = 0; i < variation; i++) {
				String[] newColumns = cleanNull(columns);
				matrix.add(newColumns);
			}
		}

		if (matrix.get(row).length < column) {// If matrix has less columns, add the difference
			String[] newColumns = cleanNull(columns);
			String[] matrixRow = matrix.get(row);
			for (int i = 0; i < matrixRow.length; i++) {
				String cell = matrixRow[i];
				newColumns[i] = cell;
			}
			matrix.add(row, newColumns);
		}
	}

	private String[] cleanNull(String[] columns) {
		String[] resultColumns = new String[columns.length];
		for (int i = 0; i < columns.length; i++) {
			String cell = columns[i];
			if (cell == null) {
				cell = "";
			}
			resultColumns[i] = cell;
		}
		return resultColumns;
	}

	private void writeCsv(File filepath, List<String[]> matrix) {
		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filepath), getDefaultCharset()))) {
			for (String[] row : matrix) {
				for (String cell : row) {
					writer.append(cell);
					writer.append(isComma() ? "," : ";");
				}
				writer.newLine();
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
