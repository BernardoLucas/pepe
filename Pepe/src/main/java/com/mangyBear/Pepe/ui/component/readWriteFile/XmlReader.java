package com.mangyBear.Pepe.ui.component.readWriteFile;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class XmlReader extends TextFileReader {

    private final String OPEN_TAG = "<";
    private final String CLOSE_TAG = ">";
    private final String CLOSE_SECTION = "</";
    private final String EQUALS = "=";
    private final String SPACE = " ";
    private final String LINE_SEPARATOR = System.getProperty("line.separator");

    public String getAttribute(File filePath, String section, String tag, String attribute, String defaultVal) {
    	List<String> fileRows = readFile(filePath);
        if (fileRows == null || fileRows.isEmpty()) {
        	return defaultVal;
        }

        List<String> sectionRows = getSection(fileRows, section);

        List<String> tagRows = getSection((sectionRows == null || sectionRows.isEmpty()) ? fileRows : sectionRows, tag);
        if (tagRows == null || tagRows.isEmpty()) {
        	return defaultVal;
        }

        Map<String, String> attributes = getAttributeMap(tagRows.get(0).trim());
        if (attributes == null || attributes.isEmpty()) {
        	return defaultVal;
        }
        
        return attributes.get(attribute);
    }

    private Map<String, String> getAttributeMap(String tag) {
        Map<String, String> attributeList = new LinkedHashMap<>();
        if (!tag.contains(EQUALS)) {
        	return null;
        }
        
        String[] temps = tag.split(SPACE);
        for (String temp : temps) {
			if (temp.contains(EQUALS)) {
				String[] attVal = temp.split(EQUALS, 2);
				String value = attVal[1];
				if (value.endsWith(CLOSE_TAG)) {
					value = value.substring(0, value.length() - 1);
				}
				attributeList.put(attVal[0].trim(), value.trim());
			}
		}
        
        return attributeList;
    }

    public String getValue(File filePath, String section, String tag, String defaultVal) {
    	List<String> fileRows = readFile(filePath);
        if (fileRows == null || fileRows.isEmpty()) {
        	return defaultVal;
        }

        List<String> sectionRows = getSection(fileRows, section);

        List<String> tagRows = getSection((sectionRows == null || sectionRows.isEmpty()) ? fileRows : sectionRows, tag);
        if (tagRows == null || tagRows.isEmpty()) {
            return defaultVal;
        }
        
        String value = "";
        for (int i = 1; i < tagRows.size(); i++) {
        	value += tagRows.get(i).trim() + LINE_SEPARATOR;
		}
        
        return value.substring(0, value.lastIndexOf(LINE_SEPARATOR));
    }

    private List<String> getSection(List<String> rows, String tag) {
        if (tag == null || tag.isEmpty()) {
            return null;
        }
        
        tag = format(tag);
        String endSection = tag.contains(SPACE) ? tag.substring(0, tag.indexOf(SPACE)) : tag;
        boolean found = false;
        List<String> sectionRows = new ArrayList<>();
        for (String string : rows) {
			if (found && string.contains(CLOSE_SECTION + endSection + CLOSE_TAG)) {
				break;
			}
			if (found || string.contains(OPEN_TAG + tag + CLOSE_TAG) || string.contains(OPEN_TAG + tag + SPACE)) {
				found = true;
				sectionRows.add(string);
			}
		}
        
        return sectionRows;
    }

    private String format(String string) {
    	if (string.startsWith(CLOSE_SECTION)) {
    		string = string.substring(2);
    	}
    	if (string.startsWith(OPEN_TAG)) {
    		string = string.substring(1);
    	}
    	if (string.endsWith(CLOSE_TAG)) {
    		string = string.substring(0, string.length() - 1);
    	}
    	
        return string;
    }
}
