package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;
import com.mangyBear.Pepe.ui.component.SuggestCombo;
import com.mangyBear.Pepe.ui.controller.LogicController;
import com.mangyBear.Pepe.ui.controller.VariableController;
import com.mangyBear.Pepe.ui.domain.CellValue;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

public class FrameLogic extends BaseFrame {
	
	private static final long serialVersionUID = 7826800961721672836L;
	
	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
    private final FrameMain frameMain;
    private final int VERTICAL_SPACING = 8;
    private Logic logic;
    private boolean isUpdating;
    private boolean isSuggest;

    private JLabel labelName;
	private JTextField textName;
    private JLabel labelInput;
	private SuggestCombo suggestInput;
	private JLabel labelDeny;
	private JCheckBox checkDenial;
    private JLabel labelExpected;
	private SuggestCombo suggestExpected;
	private JLabel labelInformation;
	private JButton btnClone;
	private JButton btnImport;
	private JButton btnAdd;

	private boolean wasArrows;
	private Color expectedBorderColor;

    public FrameLogic(FrameMain frameMain, Logic logic, boolean isSuggest) {
        super(frameMain, FramesHelpType.FRAME_LOGIC.getName(), true);
		this.blockerLoading = new BlockerLoading(this, null, true);
        this.bindingFactory = new BindingFactory();
        this.frameMain = frameMain;
        this.logic = logic;
        this.isUpdating = logic != null;
        this.isSuggest = isSuggest;
        initGUI();
        createComponents();
        addComponents();
        getExpectedBorderColor();
        addListeners();
        if (isUpdating) {
        	setLogic();
        }
    }

    private void initGUI() {
        setLayout(new RelativeLayout());
        setSize(new Dimension(500, 148));
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(frameMain);
    }

    private void createComponents() {
    	labelName = new JLabel(FramesHelpType.FRAME_LOGIC.getLabels().get(0));
		textName = new JTextField();
    	labelInput = new JLabel(FramesHelpType.FRAME_LOGIC.getLabels().get(1));
    	suggestInput = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
    	labelDeny = new JLabel(FramesHelpType.FRAME_LOGIC.getLabels().get(2));
    	checkDenial = new JCheckBox();
    	labelExpected = new JLabel(FramesHelpType.FRAME_LOGIC.getLabels().get(3));
    	suggestExpected = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_LOGIC.getIcons().get(0));
		if (isSuggest) {
			btnClone = new JButton("Clone");
			btnImport = new JButton("Import");
		}
		btnAdd = new JButton(isUpdating ? "Update" : "Add");
    }

	private void addComponents() {
		add(blockerLoading.createLoading(16, 5), loadingBinding());
		
		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		add(labelName, labelNameBinding());
		add(textName, textNameBinding());
		add(labelInput, labelInputBinding());
		add(suggestInput, suggestInputBinding());
		add(labelDeny, labelDenyBinding());
		add(checkDenial, checkDenialBinding());
		add(labelExpected, labelExpectedBinding());
		add(suggestExpected, suggestExpectedBinding());
		add(labelInformation, labelInformationBinding());
		if (isSuggest) {
			add(btnClone, btnCloneBinding());
			add(btnImport, btnImportBinding());
		}
		add(btnAdd, btnAddBinding());
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private RelativeConstraints labelNameBinding() {
		Binding[] labelNameBinding = new Binding[2];
		labelNameBinding[0] = bindingFactory.leftEdge();
		labelNameBinding[1] = bindingFactory.verticallyCenterAlignedWith(textName);
		return new RelativeConstraints(labelNameBinding);
	}
	
	private RelativeConstraints textNameBinding() {
		Binding[] textNameBinding = new Binding[3];
		textNameBinding[0] = bindingFactory.topEdge();
		textNameBinding[1] = new Binding(Edge.LEFT, 5, Direction.RIGHT, Edge.LEFT, checkDenial);
		textNameBinding[2] = bindingFactory.rightEdge();
		return new RelativeConstraints(textNameBinding);
	}

	private RelativeConstraints labelInputBinding() {
		Binding[] labelInputBinding = new Binding[2];
		labelInputBinding[0] = bindingFactory.leftAlignedWith(labelName);
		labelInputBinding[1] = bindingFactory.verticallyCenterAlignedWith(suggestInput);
		return new RelativeConstraints(labelInputBinding);
	}

	private RelativeConstraints suggestInputBinding() {
		Binding[] suggestInputBinding = new Binding[3];
		suggestInputBinding[0] = bindingFactory.below(textName);
		suggestInputBinding[1] = bindingFactory.leftAlignedWith(textName);
		suggestInputBinding[2] = bindingFactory.rightEdge();
		return new RelativeConstraints(suggestInputBinding);
	}
	
	private RelativeConstraints labelDenyBinding() {
		Binding[] labelDenyBinding = new Binding[2];
		labelDenyBinding[0] = bindingFactory.leftAlignedWith(labelName);
		labelDenyBinding[1] = bindingFactory.verticallyCenterAlignedWith(suggestExpected);
		return new RelativeConstraints(labelDenyBinding);
	}
	
	private RelativeConstraints checkDenialBinding() {
		Binding[] checkDenialBinding = new Binding[2];
		checkDenialBinding[0] = bindingFactory.rightOf(labelDeny);
		checkDenialBinding[1] = bindingFactory.verticallyCenterAlignedWith(suggestExpected);
		return new RelativeConstraints(checkDenialBinding);
	}

	private RelativeConstraints labelExpectedBinding() {
		Binding[] labelExpectedBinding = new Binding[2];
		labelExpectedBinding[0] = bindingFactory.rightOf(checkDenial);
		labelExpectedBinding[1] = bindingFactory.verticallyCenterAlignedWith(suggestExpected);
		return new RelativeConstraints(labelExpectedBinding);
	}

	private RelativeConstraints suggestExpectedBinding() {
		Binding[] suggestExpectedBinding = new Binding[3];
		suggestExpectedBinding[0] = bindingFactory.below(suggestInput);
		suggestExpectedBinding[1] = bindingFactory.rightOf(labelExpected);
		suggestExpectedBinding[2] = bindingFactory.rightEdge();
		return new RelativeConstraints(suggestExpectedBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}
	
	private RelativeConstraints btnCloneBinding() {
		Binding[] btnCloneBinding = new Binding[2];
		btnCloneBinding[0] = bindingFactory.leftOf(btnImport);
		btnCloneBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnAdd);
		return new RelativeConstraints(btnCloneBinding);
	}
	
	private RelativeConstraints btnImportBinding() {
		Binding[] btnImportBinding = new Binding[2];
		btnImportBinding[0] = bindingFactory.leftOf(btnAdd);
		btnImportBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnAdd);
		return new RelativeConstraints(btnImportBinding);
	}

	private RelativeConstraints btnAddBinding() {
		Binding[] btnAddBinding = new Binding[2];
		btnAddBinding[0] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		btnAddBinding[1] = new Binding(Edge.RIGHT, 9, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		return new RelativeConstraints(btnAddBinding);
	}
	
	private void getExpectedBorderColor() {
		expectedBorderColor = ((LineBorder)suggestExpected.getBorder()).getLineColor();
	}
	
	private void addListeners() {
		suggestInput.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = suggestInput.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasArrows && key == KeyEvent.VK_ENTER) {
								suggestInput.hidePopup();
								wasArrows = false;
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(suggestInput, item.toString());
							} else {
								wasArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		checkDenial.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				suggestExpected.setBorder(BorderFactory.createLineBorder(checkDenial.isSelected() ? Color.RED : expectedBorderColor));
			}
		});
		suggestExpected.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = suggestExpected.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasArrows && key == KeyEvent.VK_ENTER) {
								suggestExpected.hidePopup();
								wasArrows = false;
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(suggestExpected, item.toString());
							} else {
								wasArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				(new FrameInformation(FrameLogic.this, FramesHelpType.FRAME_LOGIC)).setVisible(true);
			}
		});
		if (isSuggest) {
			btnClone.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if (frameMain.getSelectedGraphComponent() == null) {
						JOptionPane.showMessageDialog(FrameLogic.this, "There's no open flow to clone the logic to.", "Clone Alert", JOptionPane.WARNING_MESSAGE);
						return;
					}
					
					if (validateFields()) {
						isUpdating = false;
						saveLogic(true, false);
						MyConstants.easterEggDarthVader(frameMain);
					}
				}
			});
			btnImport.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if (frameMain.getSelectedGraphComponent() == null) {
						JOptionPane.showMessageDialog(FrameLogic.this, "There's no open flow to import the logic to.", "Import Alert", JOptionPane.WARNING_MESSAGE);
						return;
					}
					
					if (validateFields()) {
						saveLogic(true, false);
					}
				}
			});
		}
		btnAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (validateFields()) {
					saveLogic(!isUpdating, isUpdating && !isSuggest);
				}
			}
		});
	}

	private void comboFilter(final SuggestCombo suggest, final String searchFor) {
		blockerLoading.start("Searching equivalences");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				List<SuggestDoubleColumn> items = getItems(searchFor);
				if (items.size() > 0) {
					suggest.setModel(new DefaultComboBoxModel(items.toArray()));
					suggest.setSelectedItem(searchFor.isEmpty() ? "" : searchFor);
					JTextField editor = (JTextField) suggest.getEditor().getEditorComponent();
					editor.setCaretPosition(editor.getDocument().getLength());
					suggest.showPopup();
				} else {
					suggest.hidePopup();
				}
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}

	private List<SuggestDoubleColumn> getItems(String searchFor) {
		List<SuggestDoubleColumn> returnList = new ArrayList<>();
		try {
			for (Variable variable : (new VariableController()).findLikeName(searchFor)) {
				returnList.add(new SuggestDoubleColumn(new CellValue(variable.getId(), variable.getName(), null, variable), searchFor));
			}
		} catch (Exception e) {
			showError(e);
		}
		return returnList;
	}

	private boolean validateFields() {
		if (textName.getText() == null || textName.getText().length() < 3) {
			JOptionPane.showMessageDialog(FrameLogic.this, "The logic name entered is less than three characters or null.", "Logic Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (suggestInput.getSelectedItem() == null || suggestInput.getSelectedItem().toString().isEmpty()) {
			JOptionPane.showMessageDialog(FrameLogic.this, "Please, enter an input value.", "Logic Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		if (suggestExpected.getSelectedItem() == null || suggestExpected.getSelectedItem().toString().isEmpty()) {
			JOptionPane.showMessageDialog(FrameLogic.this, "Please, enter an expected value.", "Logic Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	private void saveLogic(final boolean haveCreateCell, final boolean haveChangeName) {
		blockerLoading.start((isUpdating ? "Updating" : "Saving") + " Logic");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				try {
					String name = textName.getText();
					if (!isUpdating) {
						logic = new Logic();
					}
					
					String validLogicName = name;
					LogicController logicController = new LogicController();
				
					if ((!isUpdating && !isSuggest) || !name.equals(logic.getNmLogic())) {
						validLogicName = name.equalsIgnoreCase(logic.getNmLogic()) ? name : getValidLogicName(logicController, name);
						if (validLogicName == null) {
							isUpdating = true;
							blockerLoading.stop();
							return null;
						}
						logic.setNmLogic(validLogicName);
					}
					
					logic.setInput(suggestInput.getSelectedItem().toString());
					logic.setDenial(checkDenial.isSelected());
					logic.setExpected(suggestExpected.getSelectedItem().toString());
					
					logic = logicController.save(logic);
				
				
					if (haveCreateCell) {
						createCell();
					}
					if (haveChangeName) {
						frameMain.changeCellValueName(validLogicName);
					}
				} catch (Exception e) {
					showError(e);
				}
				
				blockerLoading.stop();
				dispose();
				return null;
			}
		}.execute();
	}
	
	private String getValidLogicName(LogicController logicController, String name) throws Exception {
		Logic result = logicController.findByName(name);
		if (result != null) {
			Object newName = JOptionPane.showInputDialog(FrameLogic.this, "This name already exist, please insert a new one with more than three characters.", "Logic Alert", JOptionPane.WARNING_MESSAGE, null, null, name);
			if (newName == null || newName.toString().isEmpty() || newName.toString().length() < 3) {
				return null;
			}
			return getValidLogicName(logicController, newName.toString());
		}
		
		return name;
	}
	
	private void createCell() {
		frameMain.addDefaultCellToSelectedGraphComponent(new CellValue(logic.getId(), logic.getNmLogic(), GraphType.CELL_LOGIC, logic), GraphType.CELL_LOGIC);
	}
	
	private void setLogic() {
		textName.setText(logic.getNmLogic());
		suggestInput.setSelectedItem(logic.getInput());
		checkDenial.setSelected(logic.getDenial());
		suggestExpected.setSelectedItem(logic.getExpected());
	}
}
