package com.mangyBear.Pepe.ui.controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.domain.entity.ActionParameter;
import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.entity.Variable;
import com.mangyBear.Pepe.domain.types.action.ActionType;
import com.mangyBear.Pepe.domain.types.variable.VariableType;
import com.mangyBear.Pepe.service.ActionService;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.VariableService;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;

public class ActionController {

    private ActionService service = new ActionService();
	
	public Action store(Action action) throws Exception {
		service.store(action);
		return action;
	}

    public Action update(Action action) throws Exception {
        service.update(action);
        return action;
    }
	
	public void remove(Action action) throws Exception {
		service.remove(action);
	}
	
	public Action updateParams(Action action, DefaultTableModel model) throws Exception {
        boolean hasReturn = action.getActionType().getReflectionType().getReflectionReturn() != null;
        int lastRow = model.getRowCount() - 1;
		setActionParams(action, model, hasReturn, lastRow);
        return action;
	}
	
	private void setActionParams(Action action, DefaultTableModel model, boolean hasReturn, int lastRow) throws Exception {
		List<ActionParameter> actionParamList = action.getParameters();
		for (int i = 0; i < model.getRowCount(); i++) {
            ActionParameter parameter = actionParamList.get(i);
            parameter.setName(MyConstants.setAsString(model.getValueAt(i, 0)));
            parameter.setValue(validateName(hasReturn, i == lastRow, model.getValueAt(i, 1)));
            parameter.setAction(action);
        }
	}
    
    public Action buildAction(Object finder, ActionType type, DefaultTableModel model, int order) throws Exception {
        Action action = new Action();

        if (finder instanceof Finder) {
        	action.setFinder((Finder) finder);
        }
        action.setActionType(type);
        action.setOrder(order);
        
        if (model != null) {
        	createModelParams(action, type, model);
        }
        
        return action;
    }
    
    private void createModelParams(Action action, ActionType type, DefaultTableModel model) throws Exception {
        boolean hasReturn = type.getReflectionType().getReflectionReturn() != null;
        int lastRow = model.getRowCount() - 1;

        action.setParameters(new ArrayList<ActionParameter>());
        for (int i = 0; i < model.getRowCount(); i++) {
            ActionParameter parameter = new ActionParameter();
            parameter.setName(MyConstants.setAsString(model.getValueAt(i, 0)));
            parameter.setValue(validateName(hasReturn, i == lastRow, model.getValueAt(i, 1)));
            parameter.setAction(action);
            action.getParameters().add(parameter);
        }
    }
	
    private String validateName(boolean hasReturn, boolean isLast, Object value) throws Exception {
    	String name = MyConstants.setAsString(value);
    	if (hasReturn && isLast && !(value instanceof SuggestDoubleColumn) && !name.isEmpty()) {
    		name = getValidName(name);
    		name = putBracket(name);
    		if ((new VariableService()).findByName(name) == null) {
    			saveVariable(name);
    		}
    	}
    	return name;
    }
    
    private String getValidName(String name) {
    	int start = name.indexOf("[") > -1 ? name.indexOf("[") : 0;
    	int end = name.indexOf("]") > -1 ? (name.indexOf("]") + 1) : name.length();
    	return name.substring(start < end ? start : end, end > start ? end : start);
    }
    
    private String putBracket(String name) {
    	if (!name.startsWith("[")) {
    		name = "[" + name;
    	}
    	if (!name.endsWith("]")) {
    		name = name + "]";
    	}
    	return name;
    }
    
    private void saveVariable(String name) throws Exception {
    	Variable variable = new Variable();
    	variable.setName(name);
    	variable.setVariableType(VariableType.VARIABLE_OBJECT_CHANGEABLE);
    	(new VariableService()).store(variable);
    }
}
