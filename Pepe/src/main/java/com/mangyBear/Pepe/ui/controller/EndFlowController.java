package com.mangyBear.Pepe.ui.controller;

import com.mangyBear.Pepe.domain.entity.EndFlow;
import com.mangyBear.Pepe.service.EndFlowService;

public class EndFlowController {

	private final EndFlowService service = new EndFlowService();
	
	public EndFlow save(EndFlow endFlow) throws Exception {
		if (endFlow.getId() != null) {
			service.update(endFlow);
		} else {
			service.store(endFlow);
		}
		return endFlow;
	}
}
