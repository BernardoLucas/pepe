package com.mangyBear.Pepe.ui.listener;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;

public class GetInfoComponent {
	
	private final Map<String, Integer> map = new HashMap<>();
	private String className;
	private Rectangle compRec;
	
	public InfoComponent getWindowComponent(final JNAWindow window, final Point point) {
		
		if (window.isNull() || point == null) {
			return null;
		}
		
		for (JNAWindow child : window.getChildren()) {
			Rectangle rect = child.getRectangle();
			
			String compClass = child.getClassName();
			
			map.put(compClass, getCompInstance(compClass));
			
			if (rect != null && isClickOnFrame(point, rect)) {
				className = compClass + map.get(compClass);
				compRec = rect;
			}
		}
		
		return new InfoComponent(window.hWnd, className, compRec);
	}
	
	private int getCompInstance(String className) {
		Integer count = map.get(className);
		return count == null ? 1 : count + 1;
	}

	private boolean isClickOnFrame(Point point, Rectangle rect) {
		return (point.x > rect.x && point.x < (rect.x + rect.width))
				&& (point.y > rect.y && point.y < (rect.y + rect.height));
	}
}
