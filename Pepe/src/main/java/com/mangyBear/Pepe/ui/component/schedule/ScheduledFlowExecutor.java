package com.mangyBear.Pepe.ui.component.schedule;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.ui.component.AutoCloseJOption;
import com.mangyBear.Pepe.ui.controller.FlowController;
import com.mangyBear.Pepe.ui.domain.CellValue;

public class ScheduledFlowExecutor extends TimerTask{

	private final Timer timer;
	private final Flow flow;

	public ScheduledFlowExecutor(Timer timer, Flow flow) {
		this.timer = timer;
        this.flow = flow;
    }
	
	public void run() {
		int option = (new AutoCloseJOption()).showConfirmDialog(null, "A scheduled flow is about to run. Do you want to abort?",
				"Scheduled Flow Run", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, 5000);
		if (option != JOptionPane.YES_OPTION) {
			try {
				FlowController.execute(new CellValue(flow.getId(), flow.getNmFlow(), GraphType.CELL_FLOW, flow));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			timer.cancel();
		}
	}
	
	public Timer getTimer() {
		return timer;
	}
}
