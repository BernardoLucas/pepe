package com.mangyBear.Pepe.ui.frame;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class BaseFrame extends JDialog {

	private static final long serialVersionUID = 1267890057974733550L;

	public BaseFrame(JFrame frame, String name, boolean b) {
		super(frame, name, b);
	}
	
	public BaseFrame(JDialog frame, String name, boolean b) {
		super(frame, name, b);
	}

	protected void showError(Exception e) {
		JOptionPane.showMessageDialog(this,
				e.getMessage(),
				"Error",
				JOptionPane.ERROR_MESSAGE);
	}
}
