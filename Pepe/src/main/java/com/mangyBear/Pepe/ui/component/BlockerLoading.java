package com.mangyBear.Pepe.ui.component;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLayer;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.plaf.LayerUI;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class BlockerLoading extends JPanel implements MouseListener,
		MouseMotionListener, FocusListener {

	private static final long serialVersionUID = 1074151987356311874L;

	private Loading loading;

	private final JMenuBar menuBar;
	private final Container contentPane;

	private boolean inDrag = false;
	private boolean needToRedispatch = false;
	
	public BlockerLoading(JDialog dialog, JFrame frame, boolean block) {
		setOpaque(false);
		if (dialog != null) {
			this.menuBar = dialog.getJMenuBar();
			this.contentPane = dialog.getContentPane();
		} else {
			this.menuBar = frame.getJMenuBar();
			this.contentPane = frame.getContentPane();
		}
		
		addListeners(block);
		if (dialog != null) {
			dialog.setGlassPane(this);
		} else {
			frame.setGlassPane(this);
		}
	}

	public JLayer<JPanel> createLoading(int sizeDivider, int clarity) {
		JPanel panelLoading = new JPanel();
		panelLoading.setOpaque(false);
		loading = new Loading(sizeDivider, clarity);
		return new JLayer<>(panelLoading, loading);
	}
	
	private void addListeners(boolean block) {
		if (block) {
			addMouseListener(this);
			addMouseMotionListener(this);
			addFocusListener(this);
		}
	}
	
	public void start(String message) {
		loading.start();
		loading.setMessage(message != null ? message : "");
		setVisible(true);
	}

	public void stop() {
		loading.stop();
		loading.setMessage("");
		setVisible(false);
	}
	
	public void setMessage(String message) {
		loading.setMessage(message);
	}

	@Override
	public void setVisible(boolean v) {
		if (v) {
			requestFocus();
		}
		super.setVisible(v);
	}

	@Override
	public void focusLost(FocusEvent fe) {
		if (isVisible()) {
			requestFocus();
		}
	}

	@Override
	public void focusGained(FocusEvent fe) {
	}

	public void setNeedToRedispatch(boolean need) {
		needToRedispatch = need;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (needToRedispatch) {
			redispatchMouseEvent(e);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (needToRedispatch) {
			redispatchMouseEvent(e);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (needToRedispatch) {
			redispatchMouseEvent(e);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (needToRedispatch) {
			redispatchMouseEvent(e);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (needToRedispatch) {
			redispatchMouseEvent(e);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (needToRedispatch) {
			redispatchMouseEvent(e);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (needToRedispatch) {
			redispatchMouseEvent(e);
			inDrag = false;
		}
	}

	private void redispatchMouseEvent(MouseEvent e) {
		boolean inButton;
		boolean inMenuBar = false;
		Point glassPanePoint = e.getPoint();
		Container container = contentPane;
		Point containerPoint = SwingUtilities.convertPoint(this, glassPanePoint, contentPane);
		int eventID = e.getID();

		if (containerPoint.y < 0) {
			inMenuBar = true;
			container = menuBar;
			containerPoint = SwingUtilities.convertPoint(this, glassPanePoint, menuBar);
			testForDrag(eventID);
		}
		Component component = SwingUtilities.getDeepestComponentAt(container, containerPoint.x, containerPoint.y);

		if (component == null) {
			return;
		} else {
			inButton = true;
			testForDrag(eventID);
		}

		if (inMenuBar || inButton || inDrag) {
			Point componentPoint = SwingUtilities.convertPoint(this, glassPanePoint, component);
			component.dispatchEvent(new MouseEvent(component, eventID, e.getWhen(), e.getModifiers(),
					componentPoint.x, componentPoint.y, e.getClickCount(), e.isPopupTrigger()));
		}
	}

	private void testForDrag(int eventID) {
		if (eventID == MouseEvent.MOUSE_PRESSED) {
			inDrag = true;
		}
	}
}

class Loading extends LayerUI<JPanel> implements ActionListener {

	private static final long serialVersionUID = 280895181804558638L;
	
	private boolean mIsRunning;
	private boolean mIsFadingOut;
	private Timer mTimer;

	private final int mFadeLimit;
	private int mAngle;
	private int mAngleInc;
	private int mFadeCount;
	private int sizeDivider;
	private float clarity;
	private String message;

	public Loading(int sizeDivider, int clarity) {
		this.mFadeLimit = 15;
		this.sizeDivider = sizeDivider;
		getAngleInc(sizeDivider);
		setClarity(clarity);
	}

	private void setClarity(int clarity) {
		this.clarity = Float.valueOf("." + clarity + "f");
	}
	
	private void getAngleInc(int sizeDivider) {
		mAngleInc = 3;
		for (int i = 0; i < (sizeDivider / 10); i++) {
			mAngleInc++;
		}
	}

	@Override
	public void paint(Graphics g, JComponent c) {
		int compW = c.getWidth();
		int compH = c.getHeight();

		// Paint the view.
		super.paint(g, c);

		if (!mIsRunning) {
			return;
		}

		Graphics2D g2 = (Graphics2D) g.create();

		float fade = (float) mFadeCount / (float) mFadeLimit;
		fade = (float) (fade > 1.0 ? 1.0 : fade);
		fade = (float) (fade < 0.0 ? 0.0 : fade);
		
		// Gray it out.
		Composite urComposite = g2.getComposite();
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, clarity * fade));
		g2.fillRect(0, 0, compW, compH);
		g2.setComposite(urComposite);

		// Paint the loading indicator.
		int loadingSize = Math.min(compW, compH) / sizeDivider;
		int loadingX = compW / 2;
		int loadingY = compH / 2;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setStroke(new BasicStroke(loadingSize / 4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g2.setPaint(Color.WHITE);
		drawCenteredString(g2, c, loadingSize);
		g2.rotate(Math.PI * mAngle / 180, loadingX, loadingY);
		for (int i = 0; i < 12; i++) {
			float scale = (11.0f - (float) i) / 11.0f;
			g2.drawLine(loadingX + loadingSize, loadingY, loadingX + loadingSize * 2, loadingY);
			g2.rotate(-Math.PI / 6, loadingX, loadingY);
			g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, scale * fade));
		}

		g2.dispose();
	}
	
	private void drawCenteredString(Graphics g, JComponent component, int loadingSize) {
		if (g == null || component == null) {
			return;
		}
		
		Rectangle rect = component.getBounds();
	    // Get the FontMetrics
	    FontMetrics metrics = g.getFontMetrics(component.getFont());
	    // Determine the X coordinate for the text
	    int x = rect.x + (rect.width - metrics.stringWidth(message == null ? "" : message)) / 2;
	    // Determine the Y coordinate for the text (note we add the ascent, as in java 2d 0 is top of the screen)
	    int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent() + (loadingSize * 3);
	    // Set the font
	    g.setFont(component.getFont());
	    // Draw the String
	    g.drawString(message == null ? "" : message, x, y);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (mIsRunning) {
			firePropertyChange("tick", 0, 1);
			mAngle += mAngleInc;
			if (mAngle >= 360) {
				mAngle = 0;
			}
			if (mIsFadingOut) {
				if (--mFadeCount <= 0) {
					mIsRunning = false;
					mIsFadingOut = false;
					mTimer.stop();
				}
			} else if (mFadeCount < mFadeLimit) {
				mFadeCount++;
			}
		}
	}

	public void start() {
		if (mIsRunning) {
			return;
		}

		// Run a thread for animation.
		mIsRunning = true;
		mIsFadingOut = false;
		mFadeCount = 0;
	    int fps = 24;
	    int tick = 1000 / fps;
		mTimer = new Timer(tick, this);
		mTimer.start();
	}

	public void stop() {
		mIsFadingOut = true;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public void applyPropertyChange(PropertyChangeEvent pce, JLayer l) {
		if ("tick".equals(pce.getPropertyName())) {
			l.repaint();
		}
	}
}
