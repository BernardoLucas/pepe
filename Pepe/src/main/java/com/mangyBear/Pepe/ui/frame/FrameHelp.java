package com.mangyBear.Pepe.ui.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.mangyBear.Pepe.domain.types.action.ActionType;
import com.mangyBear.Pepe.domain.types.action.FinderType;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameHelp extends JDialog {

	private static final long serialVersionUID = 627520838256416485L;
	
	private JToolBar toolBar;
    private JTextField textSearch;
    private JList<LeftRightItem> resultList;
    private boolean wasArrows;

    public FrameHelp(FrameMain frameMain) {
        super(frameMain, "Help", true);
        initGUI(frameMain);
        addComponents();
        addListeners();
    }

    private void initGUI(FrameMain frameMain) {
        setLayout(new RelativeLayout());
        setSize(new Dimension(500, 400));
        setResizable(false);
        setLocationRelativeTo(frameMain);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    private void addComponents() {
        createToolBar();
        add(toolBar, toolBarBinding());
        createResultList();
        add(new JScrollPane(resultList), resultListBinding());
    }

    private void createToolBar() {
        toolBar = new JToolBar();
        toolBar.setLayout(new RelativeLayout());
        toolBar.setPreferredSize(new Dimension(getWidth(), 45));
        toolBar.setFloatable(false);

        createToolBarComponents();
        toolBar.add(textSearch, textSearchBinding());
    }

    private void createToolBarComponents() {
        textSearch = new JTextField();
        textSearch.setPreferredSize(new Dimension(getWidth() - 50, 22));
    }

    private RelativeConstraints textSearchBinding() {
        Binding[] textSearchBinding = new Binding[2];
        textSearchBinding[0] = new Binding(Edge.VERTICAL_CENTER, 3, Direction.ABOVE, Edge.VERTICAL_CENTER, Binding.PARENT);
        textSearchBinding[1] = new Binding(Edge.HORIZONTAL_CENTER, 4, Direction.LEFT, Edge.HORIZONTAL_CENTER, Binding.PARENT);
        return new RelativeConstraints(textSearchBinding);
    }

    private RelativeConstraints toolBarBinding() {
        Binding[] toolBarBinding = new Binding[3];
        toolBarBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.TOP, Binding.PARENT);
        toolBarBinding[1] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        toolBarBinding[2] = new Binding(Edge.RIGHT, 0, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        return new RelativeConstraints(toolBarBinding);
    }

    private void createResultList() {
    	List<LeftRightItem> params = startList();
        resultList = new JList<>(params.toArray(new LeftRightItem[]{}));
        resultList.setCellRenderer(new ListMultiColumn(90, Color.DARK_GRAY));
        resultList.setBackground(Color.WHITE);
        resultList.setDragEnabled(false);
        resultList.getAccessibleContext().getAccessibleChild(0);
    }

    private RelativeConstraints resultListBinding() {
        Binding[] resultListBinding = new Binding[4];
        resultListBinding[0] = new Binding(Edge.TOP, 0, Direction.BELOW, Edge.BOTTOM, toolBar);
        resultListBinding[1] = new Binding(Edge.LEFT, 0, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
        resultListBinding[2] = new Binding(Edge.RIGHT, 0, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
        resultListBinding[3] = new Binding(Edge.BOTTOM, 0, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
        return new RelativeConstraints(resultListBinding);
    }

    private void addListeners() {
        textSearch.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        filter(textSearch.getText());
                    }
                });
            }
        });
        resultList.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent ke) {
                int key = ke.getKeyCode();
                if (wasArrows && key == KeyEvent.VK_ENTER) {
                    callFrameInformation();
                } else {
                    wasArrows = (key == KeyEvent.VK_KP_DOWN) || (key == KeyEvent.VK_KP_UP)
                            || (key == KeyEvent.VK_DOWN) || (key == KeyEvent.VK_UP);
                }
            }
        });
        resultList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent ke) {
                callFrameInformation();
            }
        });
    }

    public void filter(String searchFor) {
        DefaultListModel<LeftRightItem> newModel = new DefaultListModel<>();
        List<LeftRightItem> items = joinLists(searchFor);
        for (LeftRightItem item : items) {
        	newModel.addElement(item);
        }
        resultList.removeAll();
        resultList.setModel(newModel);
    }
    
    private void callFrameInformation() {
        (new FrameInformation(FrameHelp.this, resultList.getSelectedValue().getLeftText())).setVisible(true);
    }
    
    private List<LeftRightItem> joinLists(String searchFor) {
    	List<LeftRightItem> params = new ArrayList<>();
    	for (ActionType actionType : ActionType.getActionTypesLike(searchFor, true)) {
    		params.add(new LeftRightItem(actionType.getFrameAddType(), actionType.getActionGroupType(), searchFor));
    	}
    	for (FinderType finderType : FinderType.getFinderTypesLike(searchFor, true)) {
    		params.add(new LeftRightItem(finderType.getFrameAddType(), "Finder", searchFor));
    	}
    	for (FramesHelpType frameHelp : FramesHelpType.getFrameHelpTypesLike(searchFor, true)) {
    		params.add(new LeftRightItem(frameHelp, "Frame", searchFor));
		}
    	return params;
    }
    
    private List<LeftRightItem> startList() {
    	List<LeftRightItem> params = new ArrayList<>();
    	for (ActionType actionType : ActionType.getAllActionTypes()) {
    		params.add(new LeftRightItem(actionType.getFrameAddType(), actionType.getActionGroupType(), ""));
    	}
    	for (FinderType finderType : FinderType.getVisibleFinders(true)) {
    		params.add(new LeftRightItem(finderType.getFrameAddType(), "Finder", ""));
    	}
    	for (FramesHelpType frameHelp : FramesHelpType.getAllFrameHelpTypes()) {
    		params.add(new LeftRightItem(frameHelp, "Frame", ""));
		}
    	return params;
    }
}

class ListMultiColumn extends JPanel implements ListCellRenderer<LeftRightItem> {

	private static final long serialVersionUID = 8087482217463528023L;
	
	private final JLabel leftLabel;
    private final JLabel rightLabel;

    public ListMultiColumn(int rightWidth, Color secondColumnColor) {
        super(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));

        leftLabel = new JLabel();
        leftLabel.setOpaque(false);
        leftLabel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
        leftLabel.setHorizontalAlignment(SwingConstants.CENTER);

        final Dimension dim = new Dimension(rightWidth, 0);
        rightLabel = new JLabel() {
        	
			private static final long serialVersionUID = 7817541139173692169L;

			@Override
            public Dimension getPreferredSize() {
                return dim;
            }
        };
        rightLabel.setOpaque(false);
        rightLabel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
        rightLabel.setForeground(secondColumnColor);
        rightLabel.setHorizontalAlignment(SwingConstants.CENTER);

        this.add(leftLabel);
        this.add(rightLabel, BorderLayout.EAST);
    }

    @Override
    public Component getListCellRendererComponent(JList list, LeftRightItem value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value != null) {
            String html = changeColor(value.getLeftText().toString(), value.getSearchFor());
            leftLabel.setText("<html><head></head><body style=\"color:#0077be;\">" + html + "</body></head>");
            rightLabel.setText(value.getRightText().toString());
        }

        leftLabel.setFont(list.getFont());
        rightLabel.setFont(list.getFont());

        if (index < 0) {
            leftLabel.setForeground(list.getForeground());
            this.setOpaque(false);
        } else {
            leftLabel.setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
            this.setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
            this.setOpaque(true);
        }
        return this;
    }

    private String changeColor(String string, String searchFor) {
        List<String> notMatching = new ArrayList<>();
        int searchForLength = 0;
        if (searchFor != null && !searchFor.isEmpty()) {
            searchForLength = searchFor.length();
            String upperString = string.toUpperCase();
            String upperSearchFor = searchFor.toUpperCase();
            int fs;
            int lastFs = 0;
            while ((fs = upperString.indexOf(upperSearchFor, (lastFs == 0) ? -1 : lastFs)) > -1) {
                notMatching.add(string.substring(lastFs, fs));
                lastFs = fs + searchForLength;
            }
            notMatching.add(string.substring(lastFs));
        }
        String html = string;
        if (notMatching.size() > 1) {
            html = notMatching.get(0);
            int start = html.length();
            for (int i = 1; i < notMatching.size(); i++) {
                String t = notMatching.get(i);
                html += "<b style=\"color:#064273;\">" + string.substring(start, start + searchForLength) + "</b>" + t;
                start += searchForLength + t.length();
            }
        }
        return html;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        return new Dimension(0, d.height + 2);
    }

    @Override
    public void updateUI() {
        super.updateUI();
        this.setName("List.cellRenderer");
    }
}

class LeftRightItem {

    private final Object leftText;
    private final Object rightText;
    private final String searchFor;

    public LeftRightItem(Object leftText, Object rightText, String searchFor) {
        this.leftText = leftText;
        this.rightText = rightText;
        this.searchFor = searchFor;
    }

    @Override
    public String toString() {
        return getLeftText().toString();
    }
    
    public Object getLeftText() {
        return leftText;
    }

    public Object getRightText() {
        return rightText;
    }

    public String getSearchFor() {
        return searchFor;
    }
}

