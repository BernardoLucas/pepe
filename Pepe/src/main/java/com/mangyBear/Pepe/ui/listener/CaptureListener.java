package com.mangyBear.Pepe.ui.listener;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;
import org.jnativehook.mouse.NativeMouseWheelEvent;
import org.jnativehook.mouse.NativeMouseWheelListener;

import com.mangyBear.Pepe.domain.types.action.FinderType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.BorderFoundComponent;
import com.mangyBear.Pepe.ui.frame.FrameAdd;

/**
 * JNativeHook: Global keyboard and mouse hooking for Java.
 * Copyright (C) 2006-2015 Alexander Barker.  All Rights Received.
 * https://github.com/kwhat/jnativehook/
 * JNativeHook is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JNativeHook is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CaptureListener implements NativeMouseInputListener, NativeMouseWheelListener, NativeKeyListener {
	
	private final FrameAdd frameAdd;
	private final FinderType finderType;
	private final BorderFoundComponent borderFoundComponent;
	
	private Point rectangleBegin;
	
	public CaptureListener(FrameAdd frameAdd, FinderType finderType) {
		this.frameAdd = frameAdd;
		this.finderType = finderType;
		this.borderFoundComponent = new BorderFoundComponent(finderType.getCatchBorderColor(), finderType.getCatchBorderWidth());
		regNativeHook();
		addNativeListeners();
	}
	
	//NativeMouseInputListener 
    public void nativeMouseClicked(NativeMouseEvent e) {
    	setHeardValues(new Point(e.getX(), e.getY()), CaptureTypes.MOUSE_CLICKED);
    }

    public void nativeMousePressed(NativeMouseEvent e) {
    	setHeardValues(new Point(e.getX(), e.getY()), CaptureTypes.MOUSE_PRESSED);
    }

    public void nativeMouseReleased(NativeMouseEvent e) {
    	setHeardValues(new Point(e.getX(), e.getY()), CaptureTypes.MOUSE_RELEASED);
    }

    public void nativeMouseMoved(NativeMouseEvent e) {
    	setHeardValues(new Point(e.getX(), e.getY()), CaptureTypes.MOUSE_MOVED);
    }

    public void nativeMouseDragged(NativeMouseEvent e) {
    	setHeardValues(new Point(e.getX(), e.getY()), CaptureTypes.MOUSE_DRAGGED);
    }
    
    //NativeMouseWheelListener
    public void nativeMouseWheelMoved(NativeMouseWheelEvent e) {
    	setHeardValues(new Point(e.getX(), e.getY()), CaptureTypes.MOUSE_WHEEL_MOVED);
    }
    
    //NativeKeyListener
    public void nativeKeyTyped(NativeKeyEvent e) {
    }
    
    public void nativeKeyPressed(NativeKeyEvent e) {
    }

    public void nativeKeyReleased(NativeKeyEvent e) {
    	int keyCode = e.getKeyCode();
    	if (keyCode == NativeKeyEvent.VC_CONTROL_L || keyCode == NativeKeyEvent.VC_CONTROL_R) {
    		clear();
    	}
    }
    
    public void clear() {
    	borderFoundComponent.frameDispose();
		removeNativeListeners();
		unregNativeHook();
    }
    
    //Methods	
	private void regNativeHook() {
		if (!GlobalScreen.isNativeHookRegistered()) {
            try {
                GlobalScreen.registerNativeHook();
        		// Get the logger for "org.jnativehook" and set the level to warning.
        		Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        		logger.setLevel(Level.WARNING);
            } catch (NativeHookException ex) {
                System.err.println(ex.getMessage());
            }
        }
	}
	
	private void unregNativeHook() {
		if (GlobalScreen.isNativeHookRegistered()) {
            try {
                GlobalScreen.unregisterNativeHook();
            } catch (NativeHookException ex) {
                System.err.println(ex.getMessage());
            }
        }
	}
	
	private void addNativeListeners() {
		GlobalScreen.addNativeMouseListener(this);
		GlobalScreen.addNativeMouseMotionListener(this);
		GlobalScreen.addNativeMouseWheelListener(this);
		GlobalScreen.addNativeKeyListener(this);
	}
	
	private void removeNativeListeners() {
		GlobalScreen.removeNativeMouseListener(this);
        GlobalScreen.removeNativeMouseMotionListener(this);
		GlobalScreen.removeNativeMouseWheelListener(this);
		GlobalScreen.removeNativeKeyListener(this);
	}
	
	private void setHeardValues(Point mousePosition, CaptureTypes type) {
		List<String> labels = finderType.getFrameAddType().getLabels();
		switch (finderType) {
			case CONTROL:
				JNAWindow window = (new GetInfoWindow()).getByPoint(mousePosition);
				InfoComponent compInfo = (new GetInfoComponent()).getWindowComponent(window, mousePosition);
				if (compInfo != null && !compInfo.isEmpity()) {
					borderFoundComponent.resize(compInfo.getRectangle());
					setValuesOnModel(labels, new Object[]{window.getTitle(), "", compInfo});
				}
				break;
			case POSITION:
				if (mousePosition != null) {
					setValuesOnModel(labels, new Object[]{mousePosition.x, mousePosition.y});
				}
				break;
			case SCREEN_REGION:
				if (mousePosition != null && CaptureTypes.MOUSE_PRESSED.equals(type)) {
					rectangleBegin = mousePosition;
					setValuesOnModel(labels, new Object[]{mousePosition.x, mousePosition.y});
				} else if (mousePosition != null && CaptureTypes.MOUSE_DRAGGED.equals(type)) {
					borderFoundComponent.resize(getScreenRegion(mousePosition));
					setLastTwoValuesOnModel(labels, new Object[]{mousePosition.x, mousePosition.y});
				} else if (CaptureTypes.MOUSE_RELEASED.equals(type)) {
					clear();
				}
				break;
			case WINDOW:
				JNAWindow jnaWindow = (new GetInfoWindow()).getByPoint(mousePosition);
				if (!jnaWindow.isNull()) {
					borderFoundComponent.resize(jnaWindow.getRectangle());
					setValuesOnModel(labels, new Object[]{jnaWindow});
				}
				break;
			case WEB_ELEMENT:
//				Rectangle rec = (new ReadWebPage()).getWebElementRectangle(driver, mousePosition);
//				if (rec != null || !rec.isEmpty()) {
//					borderFoundComponent.resize(rec);
//					setValuesOnModel(labels, new Object[]{jnaWindow});
//				}
			default:
		}
	}
	
	private void setValuesOnModel(List<String> labels, Object[] heardValue) {
		for (int i = 0; i < labels.size(); i++) {
			if (heardValue.length > i) {
				frameAdd.model.setValueAt(heardValue[i], i, 1);
			}
		}
	}
	
	private void setLastTwoValuesOnModel(List<String> labels, Object[] heardValue) {
		if (labels.size() <= 2) {
			return;
		}
		for (int i = 2; i < labels.size(); i++) {
			frameAdd.model.setValueAt(heardValue[i - 2], i, 1);
		}
	}
	
	private Rectangle getScreenRegion(Point rectangleEnd) {
		if (rectangleBegin == null || rectangleEnd == null) {
			return new Rectangle(new Point(0, 0), MyConstants.getScreenSize());
		}
		
		Rectangle rec = new Rectangle(new Point(rectangleBegin.x, rectangleBegin.y));
		rec.add(new Point(rectangleEnd.x, rectangleEnd.y));
		return rec;
	}
}

enum CaptureTypes {
	MOUSE_CLICKED, MOUSE_PRESSED, MOUSE_RELEASED, MOUSE_MOVED, MOUSE_DRAGGED, MOUSE_WHEEL_MOVED;
}
