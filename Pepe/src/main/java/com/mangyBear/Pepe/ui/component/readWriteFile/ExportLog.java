package com.mangyBear.Pepe.ui.component.readWriteFile;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.mangyBear.Pepe.domain.entityRun.ActionResultRun;
import com.mangyBear.Pepe.domain.entityRun.LogicResultRun;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.MyConstants;

public class ExportLog {
	
	private BufferedWriter writer;
	private final List<String> exportList;
	
	public ExportLog() {
		this.exportList = new ArrayList<>();
	}
	
	public void write(GraphType type, Object object) {
		try {
			writer = new BufferedWriter(new FileWriter(MyConstants.getAppHome() + "/pepe.log", true));
			List<String> msgList = getMessage(type, object);
			for (String msg : msgList) {
				writer.write(msg);
				writer.newLine();
				exportList.add(msg);
			}
		} catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
            	e.printStackTrace();
            }
        }
	}
	
	private List<String> getMessage(GraphType type, Object object) throws IOException {
		if (object == null) {
			return Arrays.asList();
		}
		switch (type) {
			case CELL_LOGIC:
				LogicResultRun lResult = (LogicResultRun) object;
				return Arrays.asList(lResult.getDate() + " [LOGIC]:" + lResult.getLogic().getNmLogic(), lResult.getLogicStatus() + ": Return:" + lResult.getLogicReturn());
			case CELL_STEP:
				ActionResultRun aResult = (ActionResultRun) object;
				if (aResult.getAction() == null && aResult.getFinder() != null) {
					return Arrays.asList(aResult.getFinderDate() + " [FINDER]:" + aResult.getFinder().getNmFinder(), aResult.getFinderStatus() + ": Return:" + aResult.getFinderReturn());
				} else if (aResult.getAction() != null) {
					return Arrays.asList(aResult.getActionDate() + " [ACTION]:" + aResult.toString(), aResult.getActionStatus() + ": Return:" + aResult.getActionReturn());
				}
			default:
				return Arrays.asList();
		}
	}
	
	public void export(String flowName) {
		try {
			String exportPath = MyConstants.getLogExportPath();
			if (exportPath == null || exportPath.isEmpty()) {
				exportPath = MyConstants.USER_HOME + "/Desktop";
			}
				writer = new BufferedWriter(new FileWriter(exportPath + "/" + flowName + " - " + new SimpleDateFormat("ddMMyy_hhmmss").format(new Date()) + ".log"));
				for (String msg : exportList) {
					writer.write(msg);
					writer.newLine();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
	            try {
	                writer.close();
	            } catch (Exception e) {
	            	e.printStackTrace();
	            }
	        }
	}
}
