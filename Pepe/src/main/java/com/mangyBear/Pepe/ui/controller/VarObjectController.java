package com.mangyBear.Pepe.ui.controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.mangyBear.Pepe.domain.entity.Flow;
import com.mangyBear.Pepe.domain.entity.VarObject;
import com.mangyBear.Pepe.domain.entity.VarObjectParameter;
import com.mangyBear.Pepe.domain.types.variable.VariableGroupType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.VarObjectService;

public class VarObjectController {

	private VarObjectService service = new VarObjectService();
	
	public void store(VarObject varObject) throws Exception {
		service.store(varObject);
	}
	
	public void update(VarObject varObject) throws Exception {
		service.update(varObject);
	}
	
	public void remove(VarObject varObject) throws Exception {
		service.remove(varObject);
	}
	
	public VarObject updateParams(VarObject varObject, DefaultTableModel model) {
		setVarObjectParams(varObject, model);
		return varObject;
	}
	
	private void setVarObjectParams(VarObject varObject, DefaultTableModel model) {
		List<VarObjectParameter> varObjectParamList = varObject.getParameters();
		for (int i = 0; i < model.getRowCount(); i++) {
			VarObjectParameter varObjectParameter = varObjectParamList.get(i);
			varObjectParameter.setName(MyConstants.setAsString(model.getValueAt(i, 0)));
			varObjectParameter.setValue(MyConstants.setAsString(model.getValueAt(i, 1)));
			varObjectParameter.setObject(varObject);
		}
	}

	public VarObject buildVarObject(Long groupId, Flow flowDad, VariableGroupType type, DefaultTableModel model, String colName, String value) {
		VarObject varObject = new VarObject();

		varObject.setId(groupId);
		varObject.setFlowDad(flowDad);
		varObject.setType(type);
//		varObject.setNameObject(name);

		varObject.setParameters(new ArrayList<VarObjectParameter>());
		if (model != null) {
			setParametersByModel(varObject, model);
		}
		if (colName != null && value != null) {
			setParametersByNameAndValue(varObject, colName, value);
		}

		return varObject;
	}
	
	private void setParametersByNameAndValue(VarObject varObject, String name, String value) {
		VarObjectParameter varObjectParameter = new VarObjectParameter();
		varObjectParameter.setName(name);
		varObjectParameter.setValue(value);
		varObjectParameter.setObject(varObject);
		varObject.getParameters().add(varObjectParameter);
	}
	
	private void setParametersByModel(VarObject varObject, DefaultTableModel model) {
		for (int i = 0; i < model.getRowCount(); i++) {
			VarObjectParameter varObjectParameter = new VarObjectParameter();
			varObjectParameter.setName(MyConstants.setAsString(model.getValueAt(i, 0)));
			varObjectParameter.setValue(MyConstants.setAsString(model.getValueAt(i, 1)));
			varObjectParameter.setObject(varObject);
			varObject.getParameters().add(varObjectParameter);
		}
	}
	
	private void setParametersByList(VarObject varObject, List<VarObjectParameter> parameters) {
		for (VarObjectParameter parameter : parameters) {
			parameter.setObject(varObject);
			varObject.getParameters().add(parameter);
		}
	}

	public VarObject findByName(String name) throws Exception {
		return service.findByName(name);
	}

	public List<VarObject> findLikeName(String name) throws Exception {
		return service.findLikeName(name);
	}
}
