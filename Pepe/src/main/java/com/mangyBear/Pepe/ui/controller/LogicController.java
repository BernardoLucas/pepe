package com.mangyBear.Pepe.ui.controller;

import java.util.List;

import com.mangyBear.Pepe.domain.entity.Logic;
import com.mangyBear.Pepe.service.LogicService;

public class LogicController {

	private final LogicService service = new LogicService();
	
	public Logic clone(Long id) throws Exception {
		Logic clone = service.findById(id).clone();
		
		service.store(clone);
		return clone;
	}
	
	public Logic save(Logic logic) throws Exception {
		if (logic.getId() != null) {
			service.update(logic);
		} else {
			service.store(logic);
		}
		return logic;
	}
	
	public Logic findByName(String name) throws Exception {
		return service.findByName(name);
	}

	public List<Logic> findLikeName(String name) throws Exception {
		return service.findLikeName(name);
	}

	public List<Logic> findLikeNameLimited(String name, int limit) throws Exception{
		return service.findLikeNameLimited(name, limit);
	}
}
