package com.mangyBear.Pepe.ui.component;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

public class HintTextField extends JTextField implements FocusListener {

	private static final long serialVersionUID = -5156835972714566091L;

	private final String hint;
	private final Color hintColor;
	private boolean showingHint;

	public HintTextField(final String hint, Color hintColor) {
		super(hint);
		this.hint = hint;
		this.hintColor = hintColor;
		this.showingHint = true;
		setForeground(hintColor);
		super.addFocusListener(this);
	}

	@Override
	public void focusGained(FocusEvent e) {
		if (this.getText().isEmpty()) {
			super.setText("");
			setForeground(Color.BLACK);
			showingHint = false;
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		if (this.getText().isEmpty()) {
			super.setText(hint);
			setForeground(hintColor);
			showingHint = true;
		}
	}

	@Override
	public String getText() {
		return showingHint ? "" : super.getText();
	}
}
