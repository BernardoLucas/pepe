package com.mangyBear.Pepe.ui.component;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class PositionCompansation {

	private final Dimension currentScreenDimension;

	public PositionCompansation() {
		this.currentScreenDimension = Toolkit.getDefaultToolkit().getScreenSize();
	}

	public List<String> getValidPosition(Double width, Double height, Double x, Double y) {
		if ((x == null || x <= 0) || (y == null || y <= 0)) {
			return Arrays.asList(String.valueOf(0), String.valueOf(0));
		}
		
		double currentWidth = currentScreenDimension.getWidth();
		double currentHeight = currentScreenDimension.getHeight();
		double defaultWidth = (width != null && width >= 0) ? width : currentWidth;
		double defaultHeight = (height != null && height >= 0) ? height : currentHeight;
		double xComp = 0;
		double yComp = 0;
		
		if (defaultWidth != currentWidth) {
			xComp = getCompansation(defaultWidth, currentWidth, x);
		}
		if (defaultHeight != currentHeight) {
			yComp = getCompansation(defaultHeight, currentHeight, y);
		}

		return Arrays.asList(String.valueOf((int) compensate(defaultWidth, currentWidth, x, xComp)),
				String.valueOf((int) compensate(defaultHeight, currentHeight, y, yComp)));
	}

	private double getCompansation(double old, double current, double point) {
		int divider = 20;
		double bigger = old > current ? old : current;
		double slice = bigger / divider;

		double compansation = 0;
		for (int i = 0; i < (divider - 1); i++) {
			if (point < slice) {
				return compansation;
			}
			compansation += slice;
		}

		return 100;
	}

	private double compensate(double defaultWidth, double currentWidth, double point, double compansation) {
		if (defaultWidth > currentWidth) {
			return point - ((point * compansation) / 100);
		}
		return point + ((point * compansation) / 100);
	}
}
