package com.mangyBear.Pepe.ui.frame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.accessibility.Accessible;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang3.StringUtils;

import com.mangyBear.Pepe.domain.entity.Action;
import com.mangyBear.Pepe.domain.entity.ActionParameter;
import com.mangyBear.Pepe.domain.entity.Finder;
import com.mangyBear.Pepe.domain.entity.FinderParameter;
import com.mangyBear.Pepe.domain.entity.Step;
import com.mangyBear.Pepe.domain.types.action.ActionGroupType;
import com.mangyBear.Pepe.domain.types.action.ActionType;
import com.mangyBear.Pepe.domain.types.action.FinderType;
import com.mangyBear.Pepe.domain.types.desktopParams.DesktopType;
import com.mangyBear.Pepe.domain.types.frame.ComboBoxType;
import com.mangyBear.Pepe.domain.types.frame.FramesHelpType;
import com.mangyBear.Pepe.domain.types.graph.GraphType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.BlockerLoading;
import com.mangyBear.Pepe.ui.component.ComponentsTable;
import com.mangyBear.Pepe.ui.component.SuggestDoubleColumn;
import com.mangyBear.Pepe.ui.component.SuggestCombo;
import com.mangyBear.Pepe.ui.component.readWriteFile.ExportLog;
import com.mangyBear.Pepe.ui.controller.ActionController;
import com.mangyBear.Pepe.ui.controller.FinderController;
import com.mangyBear.Pepe.ui.controller.StepController;
import com.mangyBear.Pepe.ui.domain.CellValue;

import edu.cmu.relativelayout.Binding;
import edu.cmu.relativelayout.BindingFactory;
import edu.cmu.relativelayout.Direction;
import edu.cmu.relativelayout.Edge;
import edu.cmu.relativelayout.RelativeConstraints;
import edu.cmu.relativelayout.RelativeLayout;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class FrameStep extends BaseFrame {

	private static final long serialVersionUID = 8079016351539689387L;
	
	private final BlockerLoading blockerLoading;
	private final BindingFactory bindingFactory;
	private final FrameMain frameMain;
	private final int VERTICAL_SPACING = 10;
	private final int TABLE_VERTICAL_SPACING = 2;
	private final List<Object> deleteList;
	
	private Step step;
	private boolean wasArrows;
	private boolean cancelFinderTableChanged;
	private boolean isUpdating;
	private boolean fromSuggest;

	// BasePanel Components
	private JLabel labelName;
	private JTextField textName;
	private final JTabbedPane tabbedPane;
	private JLabel labelInformation;
	private JButton btnExecute;
	private JButton btnImport;
	private JButton btnClone;
	private JButton btnAdd;
	// FindersPanel Components
	private JPanel findersPanel;
	private JLabel labelFinderName;
	private SuggestCombo suggestFinderName;
	private JLabel labelFinder;
	private JComboBox<FinderType> comboFinder;
	private JButton btnAddFinder;
	private JButton btnRemoveFinder;
	private JTable findersTable;
	private DefaultTableModel findersTableModel;
	private JLabel labelFinderInformation;
	// ActionsPanel Components
	private JPanel actionsPanel;
	private JLabel labelActionFinder;
	private JComboBox<Object> comboActionFinder;
	private JLabel labelGroup;
	private JComboBox<ActionGroupType> comboGroup;
	private JLabel labelAction;
	private JComboBox<ActionType> comboAction;
	private JButton btnAddAction;
	private JButton btnRemoveAction;
	private JTable actionsTable;
	private DefaultTableModel actionsTableModel;
	private JLabel labelActionInformation;

	public FrameStep(FrameMain frameMain, Step step, boolean fromSuggest) {
		super(frameMain, FramesHelpType.FRAME_STEP.getName(), true);
		this.blockerLoading = new BlockerLoading(this, null, true);
		this.bindingFactory = new BindingFactory();
		this.tabbedPane = new JTabbedPane();
		this.frameMain = frameMain;
		this.step = step;
		this.isUpdating = step != null;
		this.fromSuggest = fromSuggest;
		this.deleteList = new ArrayList<>();
		initGUI();
		addComponents();
		addListeners();
		if (isUpdating) {
			cancelFinderTableChanged = true;
			setSavedStep();
		}
	}

	private void initGUI() {
		setLayout(new RelativeLayout());
		setSize(new Dimension(750, 550));
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(frameMain);
	}

	private void addComponents() {
		add(blockerLoading.createLoading(60, 5), loadingBinding());

		buildFindersPanel();
		buildActionsPanel();
		buildFrameConstants();
	}

	private RelativeConstraints loadingBinding() {
		Binding[] loadingBinding = new Binding[4];
		loadingBinding[0] = new Binding(Edge.TOP, 1, Direction.ABOVE, Edge.TOP, Binding.PARENT);
		loadingBinding[1] = new Binding(Edge.LEFT, 1, Direction.LEFT, Edge.LEFT, Binding.PARENT);
		loadingBinding[2] = new Binding(Edge.RIGHT, 1, Direction.RIGHT, Edge.RIGHT, Binding.PARENT);
		loadingBinding[3] = new Binding(Edge.BOTTOM, 1, Direction.BELOW, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(loadingBinding);
	}

	private void buildFrameConstants() {
		createComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		add(labelName, labelNameBinding());
		add(textName, textNameBinding());
		add(labelInformation, labelInformationBinding());
		add(btnExecute, btnExecuteBinding());
		if (fromSuggest) {
			add(btnClone, btnCloneBinding());
			add(btnImport, btnImportBinding());
		}
		add(btnAdd, btnAddBinding());
		add(tabbedPane, tabbedPaneBinding());

		addTabs();
	}

	private void createComponents() {
		labelName = new JLabel(FramesHelpType.FRAME_STEP.getLabels().get(0));
		textName = new JTextField();
		labelInformation = new JLabel();
		labelInformation.setIcon(FramesHelpType.FRAME_STEP.getIcons().get(0));
		btnExecute = new JButton("Execute");
		btnExecute.setEnabled(false);
		btnClone = new JButton("Clone");
		btnImport = new JButton("Import");
		btnAdd = new JButton(isUpdating ? "Update" : "Add");
	}

	private void addTabs() {
		tabbedPane.addTab(FramesHelpType.FRAME_STEP_FINDER.getName(), FramesHelpType.FRAME_STEP_FINDER.getIcons().get(1), findersPanel);
		tabbedPane.addTab(FramesHelpType.FRAME_STEP_ACTION.getName(), FramesHelpType.FRAME_STEP_ACTION.getIcons().get(1), actionsPanel);
	}

	private RelativeConstraints labelNameBinding() {
		Binding[] labelNameBinding = new Binding[2];
		labelNameBinding[0] = bindingFactory.leftEdge();
		labelNameBinding[1] = bindingFactory.verticallyCenterAlignedWith(textName);
		return new RelativeConstraints(labelNameBinding);
	}

	private RelativeConstraints textNameBinding() {
		Binding[] textNameBinding = new Binding[3];
		textNameBinding[0] = bindingFactory.topEdge();
		textNameBinding[1] = bindingFactory.rightOf(labelName);
		textNameBinding[2] = bindingFactory.rightAlignedWith(tabbedPane);
		return new RelativeConstraints(textNameBinding);
	}

	private RelativeConstraints labelInformationBinding() {
		Binding[] labelInformationBinding = new Binding[2];
		labelInformationBinding[0] = new Binding(Edge.LEFT, 3, Direction.RIGHT, Edge.LEFT, Binding.PARENT);
		labelInformationBinding[1] = new Binding(Edge.BOTTOM, 3, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		return new RelativeConstraints(labelInformationBinding);
	}

	private RelativeConstraints btnExecuteBinding() {
		Binding[] btnExecuteBinding = new Binding[2];
		btnExecuteBinding[0] = bindingFactory.horizontallyCenterAlignedWith(Binding.PARENT);
		btnExecuteBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnAdd);
		return new RelativeConstraints(btnExecuteBinding);
	}
	
	private RelativeConstraints btnCloneBinding() {
		Binding[] btnCloneBinding = new Binding[2];
		btnCloneBinding[0] = bindingFactory.leftOf(btnImport);
		btnCloneBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnAdd);
		return new RelativeConstraints(btnCloneBinding);
	}
	
	private RelativeConstraints btnImportBinding() {
		Binding[] btnImportBinding = new Binding[2];
		btnImportBinding[0] = bindingFactory.leftOf(btnAdd);
		btnImportBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnAdd);
		return new RelativeConstraints(btnImportBinding);
	}

	private RelativeConstraints btnAddBinding() {
		Binding[] btnAddBinding = new Binding[2];
		btnAddBinding[0] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.BOTTOM, Binding.PARENT);
		btnAddBinding[1] = new Binding(Edge.RIGHT, 9, Direction.LEFT, Edge.RIGHT, Binding.PARENT);
		return new RelativeConstraints(btnAddBinding);
	}

	private RelativeConstraints tabbedPaneBinding() {
		Binding[] tabbedPaneBinding = new Binding[4];
		tabbedPaneBinding[0] = bindingFactory.below(textName);
		tabbedPaneBinding[1] = bindingFactory.leftEdge();
		tabbedPaneBinding[2] = bindingFactory.rightAlignedWith(btnAdd);
		tabbedPaneBinding[3] = bindingFactory.above(btnAdd);
		return new RelativeConstraints(tabbedPaneBinding);
	}

	private void buildFindersPanel() {
		findersPanel = new JPanel(new RelativeLayout());
		createFindersPanelComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		findersPanel.add(labelFinderName, labelFinderNameBinding());
		findersPanel.add(suggestFinderName, textFinderNameBinding());
		findersPanel.add(labelFinder, labelFinderBinding());
		findersPanel.add(comboFinder, comboFinderBinding());
		findersPanel.add(btnAddFinder, btnAddFinderBinding());
		findersPanel.add(btnRemoveFinder, btnRemoveFinderBinding());
		findersPanel.add(labelFinderInformation, labelInformationBinding());

		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);

		Integer[] checkBoxColumns = new Integer[] { 1 };
		findersTable = new ComponentsTable(new String[] { "Entity", "Delete", "Name",
				"Finder", "Parameters" }, checkBoxColumns,
				new Integer[] { 0, 3, 4 }, true, true, false);
		setInvisibleColumn(findersTable);
		centerTableLabels(checkBoxColumns, findersTable);
		setFindersModel((DefaultTableModel) findersTable.getModel());
		findersPanel.add(new JScrollPane(findersTable), findersTableBinding());
	}

	private void createFindersPanelComponents() {
		labelFinderName = new JLabel(FramesHelpType.FRAME_STEP_FINDER.getLabels().get(0));
		suggestFinderName = new SuggestCombo(null, true, Color.BLACK, 0, Color.BLACK);
		labelFinder = new JLabel(FramesHelpType.FRAME_STEP_FINDER.getLabels().get(1));
		createComboFinder();
		btnAddFinder = new JButton(FramesHelpType.FRAME_STEP_FINDER.getIcons().get(2));
		btnRemoveFinder = new JButton(FramesHelpType.FRAME_STEP_FINDER.getIcons().get(3));
		labelFinderInformation = new JLabel();
		labelFinderInformation.setIcon(FramesHelpType.FRAME_STEP_FINDER.getIcons().get(0));
	}

	private void createComboFinder() {
		comboFinder = new JComboBox<>();
		comboFinder.setPreferredSize(new Dimension(150, 22));
		List<FinderType> finderList = FinderType.getVisibleFinders(true);
		for (FinderType finder : finderList) {
			comboFinder.addItem(finder);
		}
	}
	
	private RelativeConstraints labelFinderNameBinding() {
		Binding[] labelFinderNameBinding = new Binding[2];
		labelFinderNameBinding[0] = bindingFactory.leftEdge();
		labelFinderNameBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveFinder);
		return new RelativeConstraints(labelFinderNameBinding);
	}
	
	private RelativeConstraints textFinderNameBinding() {
		Binding[] textFinderNameBinding = new Binding[3];
		textFinderNameBinding[0] = bindingFactory.rightOf(labelFinderName);
		textFinderNameBinding[1] = bindingFactory.leftOf(labelFinder);
		textFinderNameBinding[2] = bindingFactory.verticallyCenterAlignedWith(btnRemoveFinder);
		return new RelativeConstraints(textFinderNameBinding);
	}

	private RelativeConstraints labelFinderBinding() {
		Binding[] labelFinderBinding = new Binding[2];
		labelFinderBinding[0] = bindingFactory.leftOf(comboFinder);
		labelFinderBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveFinder);
		return new RelativeConstraints(labelFinderBinding);
	}

	private RelativeConstraints comboFinderBinding() {
		Binding[] comboFinderBinding = new Binding[2];
		comboFinderBinding[0] = bindingFactory.leftOf(btnAddFinder);
		comboFinderBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveFinder);
		return new RelativeConstraints(comboFinderBinding);
	}

	private RelativeConstraints btnAddFinderBinding() {
		Binding[] btnAddFinderBinding = new Binding[2];
		btnAddFinderBinding[0] = bindingFactory.leftOf(btnRemoveFinder);
		btnAddFinderBinding[1] = bindingFactory.verticallyCenterAlignedWith(btnRemoveFinder);
		return new RelativeConstraints(btnAddFinderBinding);
	}

	private RelativeConstraints btnRemoveFinderBinding() {
		Binding[] btnRemoveFinderBinding = new Binding[2];
		btnRemoveFinderBinding[0] = bindingFactory.topEdge();
		btnRemoveFinderBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveFinderBinding);
	}

	private RelativeConstraints findersTableBinding() {
		Binding[] findersTableBinding = new Binding[4];
		findersTableBinding[0] = bindingFactory.below(btnRemoveFinder);
		findersTableBinding[1] = bindingFactory.leftEdge();
		findersTableBinding[2] = bindingFactory.rightEdge();
		findersTableBinding[3] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.TOP, labelFinderInformation);
		return new RelativeConstraints(findersTableBinding);
	}

	private void buildActionsPanel() {
		actionsPanel = new JPanel(new RelativeLayout());
		createActionsPanelComponents();

		bindingFactory.setVerticalSpacing(VERTICAL_SPACING);
		actionsPanel.add(labelActionFinder, labelActionFinderBinding());
		actionsPanel.add(comboActionFinder, comboActionFinderBinding());
		actionsPanel.add(labelGroup, labelGroupBinding());
		actionsPanel.add(comboGroup, comboGroupBinding());
		actionsPanel.add(labelAction, labelActionBinding());
		actionsPanel.add(comboAction, comboActionBinding());
		actionsPanel.add(btnAddAction, btnAddActionBinding());
		actionsPanel.add(btnRemoveAction, btnRemoveActionBinding());
		actionsPanel.add(labelActionInformation, labelInformationBinding());

		bindingFactory.setVerticalSpacing(TABLE_VERTICAL_SPACING);
		Integer[] checkBoxColumns = new Integer[] { 1 };
		actionsTable = new ComponentsTable(new String[] { "Entity", "Delete", "Finder",
				"Group", "Type", "Parameters" }, checkBoxColumns,
				new Integer[] { 0, 2, 3, 4, 5 }, false, false, true);
		setInvisibleColumn(actionsTable);
		centerTableLabels(checkBoxColumns, actionsTable);
		setActionsModel((DefaultTableModel) actionsTable.getModel());
		actionsPanel.add(new JScrollPane(actionsTable), actionsTableBinding());
	}

	private void createActionsPanelComponents() {
		labelActionFinder = new JLabel(FramesHelpType.FRAME_STEP_ACTION.getLabels().get(0));
		createComboActionFinder();
		labelGroup = new JLabel(FramesHelpType.FRAME_STEP_ACTION.getLabels().get(1));
		createComboGroup();
		labelAction = new JLabel(FramesHelpType.FRAME_STEP_ACTION.getLabels().get(2));
		createComboType();
		btnAddAction = new JButton(FramesHelpType.FRAME_STEP_ACTION.getIcons().get(2));
		btnRemoveAction = new JButton(FramesHelpType.FRAME_STEP_ACTION.getIcons().get(3));
		labelActionInformation = new JLabel();
		labelActionInformation.setIcon(FramesHelpType.FRAME_STEP_ACTION.getIcons().get(0));
	}
	
	private void createComboActionFinder() {
		comboActionFinder = new JComboBox<>();
		refreshComboActionFinder();
	}

	private void createComboGroup() {
		comboGroup = new JComboBox<>();
		comboGroup.setPreferredSize(new Dimension(150, 22));
		refreshComboGroup();
	}

	private void createComboType() {
		comboAction = new JComboBox<>();
		comboAction.setPreferredSize(new Dimension(150, 22));
		refreshComboAction();
	}
	
	private RelativeConstraints labelActionFinderBinding() {
		Binding[] labelActionFinderBinding = new Binding[2];
		labelActionFinderBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRemoveAction);
		labelActionFinderBinding[1] = bindingFactory.leftEdge();
		return new RelativeConstraints(labelActionFinderBinding);
	}

	private RelativeConstraints comboActionFinderBinding() {
		Binding[] comboActionFinderBinding = new Binding[3];
		comboActionFinderBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRemoveAction);
		comboActionFinderBinding[1] = bindingFactory.rightOf(labelActionFinder);
		comboActionFinderBinding[2] = bindingFactory.leftOf(labelGroup);
		return new RelativeConstraints(comboActionFinderBinding);
	}

	private RelativeConstraints labelGroupBinding() {
		Binding[] labelGroupBinding = new Binding[2];
		labelGroupBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRemoveAction);
		labelGroupBinding[1] = bindingFactory.leftOf(comboGroup);
		return new RelativeConstraints(labelGroupBinding);
	}

	private RelativeConstraints comboGroupBinding() {
		Binding[] comboGroupBinding = new Binding[2];
		comboGroupBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRemoveAction);
		comboGroupBinding[1] = bindingFactory.leftOf(labelAction);
		return new RelativeConstraints(comboGroupBinding);
	}

	private RelativeConstraints labelActionBinding() {
		Binding[] labelActionBinding = new Binding[2];
		labelActionBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRemoveAction);
		labelActionBinding[1] = bindingFactory.leftOf(comboAction);
		return new RelativeConstraints(labelActionBinding);
	}

	private RelativeConstraints comboActionBinding() {
		Binding[] comboActionBinding = new Binding[2];
		comboActionBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRemoveAction);
		comboActionBinding[1] = bindingFactory.leftOf(btnAddAction);
		return new RelativeConstraints(comboActionBinding);
	}

	private RelativeConstraints btnAddActionBinding() {
		Binding[] btnAddActionBinding = new Binding[2];
		btnAddActionBinding[0] = bindingFactory.verticallyCenterAlignedWith(btnRemoveAction);
		btnAddActionBinding[1] = bindingFactory.leftOf(btnRemoveAction);
		return new RelativeConstraints(btnAddActionBinding);
	}

	private RelativeConstraints btnRemoveActionBinding() {
		Binding[] btnRemoveActionBinding = new Binding[2];
		btnRemoveActionBinding[0] = bindingFactory.topEdge();
		btnRemoveActionBinding[1] = bindingFactory.rightEdge();
		return new RelativeConstraints(btnRemoveActionBinding);
	}

	private RelativeConstraints actionsTableBinding() {
		Binding[] actionsTableBinding = new Binding[4];
		actionsTableBinding[0] = bindingFactory.below(btnRemoveAction);
		actionsTableBinding[1] = bindingFactory.leftEdge();
		actionsTableBinding[2] = bindingFactory.rightEdge();
		actionsTableBinding[3] = new Binding(Edge.BOTTOM, 5, Direction.ABOVE, Edge.TOP, labelActionInformation);
		return new RelativeConstraints(actionsTableBinding);
	}

	private void centerTableLabels(Integer[] checkBoxColumns, JTable table) {
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		for (int i = 0; i < table.getColumnCount(); i++) {
			if (!isCheckBoxCol(checkBoxColumns, i)) {
				table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
			}
		}
	}
	
	private boolean isCheckBoxCol(Integer[] checkBoxColumns, int i) {
		for (Integer column : checkBoxColumns) {
			if (i == column) {
				return true;
			}
		}
		return false;
	}

	private void addListeners() {
		findersTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				editRow(evt, findersTable);
			}
		});
		findersTable.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(final TableModelEvent e) {
            	int col = e.getColumn();
				if (cancelFinderTableChanged) {
					cancelFinderTableChanged = col == (findersTable.getColumnCount() - 1) ? false : true;
					return;
				}
				
                if (e.getType() == TableModelEvent.UPDATE && e.getColumn() == 2) {
                	
                	blockerLoading.start("Updating Finder");

    				new SwingWorker<Void, Void>() {
    					@Override
    					protected Void doInBackground() {
		                	TableModel model = findersTable.getModel();
		                    int row = e.getFirstRow();
    						String finderName = model.getValueAt(row, e.getColumn()).toString();
    						if (finderName == null || finderName.length() < 3) {
    							JOptionPane.showMessageDialog(FrameStep.this, "The finder name entered is less than three characters or null", "Finder Alert", JOptionPane.WARNING_MESSAGE);
    							blockerLoading.stop();
    							return null;
    						}

		                    Finder finder = (Finder) model.getValueAt(row, 0);
    						FinderController finderController = new FinderController();
    						boolean sameName = finderName.equalsIgnoreCase(finder.getNmFinder());
    						try {
	    						if (!finderName.equals(finder.getNmFinder())) {
		    						String validFinderName = sameName ? finderName : getValidFinderName(finderController, finderName);
		    						if (validFinderName != null) {
					                    finder.setNmFinder(validFinderName);
					                    finderController.update(finder);
		    						}
	    						}
    						} catch (Exception e) {
    							showError(e);
    						}
							blockerLoading.stop();
							return null;
						}
					}.execute();
                }
            }
        });
		activateSuggest();
		btnAddFinder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object selectedItem = suggestFinderName.getSelectedItem();
				String finderName = selectedItem != null ? selectedItem.toString() : null;
				if (finderName == null || finderName.length() < 3) {
					JOptionPane.showMessageDialog(FrameStep.this, "The finder name entered is less than three characters or is null", "Finder Alert", JOptionPane.WARNING_MESSAGE);
					return;
				}
				callFrameAdd(comboFinder.getSelectedItem());
			}
		});
		btnRemoveFinder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				blockerLoading.start("Removing Finder");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						if (removeSelectedFinders()) {
							refreshComboActionFinder();
						}
						blockerLoading.stop();
						return null;
					}
				}.execute();
			}
		});
		labelFinderInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameStep.this, FramesHelpType.FRAME_STEP_FINDER)).setVisible(true);
			}
		});
		comboActionFinder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshComboGroup();		
			}
		});
		comboGroup.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshComboAction();
			}
		});
		actionsTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				editRow(evt, actionsTable);
			}
		});
		btnAddAction.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				ActionType actionType = (ActionType) comboAction.getSelectedItem();
				if (actionType == null) {
					return;
				}
				
				List<String> labelsList = actionType.getFrameAddType().getLabels();
				if (labelsList.isEmpty() || labelsList.get(0).isEmpty() || labelsList.get(0).equalsIgnoreCase("NONE")) {
					try {
						Action action = (new ActionController()).buildAction(comboActionFinder.getSelectedItem(), actionType, null, getActionsModel().getRowCount());
						addInTable(getActionsModel(), null, action);
					} catch (Exception e) {
						showError(e);
					}
				} else {
					callFrameAdd(actionType);
				}
			}
		});
		btnRemoveAction.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				blockerLoading.start("Removing Action");

				new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() {
						removeSelectedActions();
						blockerLoading.stop();
						return null;
					}
				}.execute();
			}
		});
		labelActionInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameStep.this, FramesHelpType.FRAME_STEP_ACTION)).setVisible(true);
			}
		});
		labelInformation.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				(new FrameInformation(FrameStep.this, FramesHelpType.FRAME_STEP)).setVisible(true);
			}
		});
		btnExecute.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					saveStep(textName.getText(), false, false, false);
					
					blockerLoading.start("Executing Step");
					
					new SwingWorker<Void, Void>() {
						@Override
						protected Void doInBackground() {
							frameMain.minimizeFrameMain();
							(new StepController()).execute(new ExportLog(), step);
							blockerLoading.stop();
							return null;
						}
					}.execute();
				}
		});
		if (fromSuggest) {
			btnClone.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (frameMain.getSelectedGraphComponent() == null) {
						JOptionPane.showMessageDialog(FrameStep.this, "There's no open flow to clone the step to.", "Clone Alert", JOptionPane.WARNING_MESSAGE);
						return;
					}
					
					isUpdating = false;
					saveStep(textName.getText(), true, false, true);
					MyConstants.easterEggDarthVader(frameMain);
				}
			});
			btnImport.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (frameMain.getSelectedGraphComponent() == null) {
						JOptionPane.showMessageDialog(FrameStep.this, "There's no open flow to import the step to.", "Import Alert", JOptionPane.WARNING_MESSAGE);
						return;
					}
					
					saveStep(textName.getText(), true, false, true);
				}
			});
		}
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveStep(textName.getText(), !isUpdating, isUpdating && !fromSuggest, true);
			}
		});
	}
	
	private void saveStep(final String name, final boolean haveCreateCell, final boolean haveChangeName, final boolean haveDispose) {
		blockerLoading.start((isUpdating ? "Updating" : "Saving") + " Step");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				if (name == null || name.length() < 3) {
					JOptionPane.showMessageDialog(FrameStep.this, "The name entered is less than three characters or null", "Finder Alert", JOptionPane.WARNING_MESSAGE);
					blockerLoading.stop();
					return null;
				}
				
				try {
					if (!isUpdating) {
						step = new Step();
					}
					
					String validStepName = name;
					StepController stepController = new StepController();
					if (!isUpdating || !name.equals(step.getNmStep())) {
						validStepName = name.equalsIgnoreCase(step.getNmStep()) ? name : getValidStepName(stepController, name);
						if (validStepName == null) {
							isUpdating = true;
							blockerLoading.stop();
							return null;
						}
						step.setNmStep(validStepName);
					}
					
					deleteFindersAndActions();
					step = stepController.save(step,
							getFindersModel(),
							getActionsModel());
				
					if (haveCreateCell) {
						createCell();
					} 
					if (haveChangeName) {
						frameMain.changeCellValueName(validStepName);
					}
				} catch (Exception e) {
					showError(e);
				}
				
				blockerLoading.stop();
				if (haveDispose) {
					dispose();
				}
				return null;
			}
		}.execute();
	}
	
	private String getValidStepName(StepController stepController, String name) throws Exception {
		Step result = stepController.findByName(name);
		if (result != null) {
			Object newName = JOptionPane.showInputDialog(FrameStep.this, "This name already exist, please insert a new one with more than three characters.", "Step Alert", JOptionPane.WARNING_MESSAGE, null, null, name);
			if (newName == null || newName.toString().isEmpty() || newName.toString().length() < 3) {
				return null;
			}
			return getValidStepName(stepController, newName.toString());
		}
		
		return name;
	}
	
	private void deleteFindersAndActions() throws Exception {
		FinderController finderController = new FinderController();
		ActionController actionController = new ActionController();
		for (Object toDelete : deleteList) {
			if (toDelete instanceof Finder) {
				finderController.remove((Finder) toDelete);
			}
			actionController.remove((Action) toDelete);
		}
	}
	
	private void createCell() {
		frameMain.addDefaultCellToSelectedGraphComponent(new CellValue(step.getId(), step.getNmStep(), GraphType.CELL_STEP, step), GraphType.CELL_STEP);
	}

	private void editRow(MouseEvent evt, JTable table) {
		if (evt.getClickCount() == 2) {
			List<Object> rowValueList = new ArrayList<>();
			int selectedRow = table.getSelectedRow();
			for (int i = 0; i < table.getColumnCount(); i++) {
				rowValueList.add(table.getValueAt(selectedRow, i));
			}
			
			Object value = rowValueList.get(rowValueList.size() - 1);
			if (value == null || value.toString().isEmpty()) {
				return;
			}
			
			callFrameAdd(rowValueList);
		}
	}

	private void callFrameAdd(Object selected) {
		if (selected instanceof FinderType || selected instanceof ActionType
				|| selected instanceof Finder || selected instanceof List<?>) {
			(new FrameAdd(FrameStep.this, selected)).setVisible(true);
		} else {
			JOptionPane.showMessageDialog(FrameStep.this, "Select a valid option.", "Step Alert", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	private boolean removeSelectedFinders() {
		DefaultTableModel findersModel = getFindersModel();
		List<Integer> rowsToDelete = new ArrayList<>();
		List<Finder> findersToCheck = new ArrayList<>();
		for (int i = 0; i < findersModel.getRowCount(); i++) {
			if ((boolean) findersModel.getValueAt(i, 1)) {
				rowsToDelete.add(i);
				findersToCheck.add((Finder) findersModel.getValueAt(i, 0));
			}
		}
		
		if (rowsToDelete.isEmpty()) {
			JOptionPane.showMessageDialog(FrameStep.this, "Select at least one row.", "Step Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		DefaultTableModel actionsModel = getActionsModel();
		List<Integer> actionsToDelete = checkActionFinder(findersToCheck, actionsModel);
		if (!actionsToDelete.isEmpty()) {
			int result = JOptionPane.showConfirmDialog(FrameStep.this, "There are Finders in use. If you continue the Actions with these Finders will be deleted.\nContinue?", "Step Alert", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
			if (JOptionPane.NO_OPTION == result) {
				return false;
			}
			addActionsToDeleteList(actionsToDelete, actionsModel);
		}
		
		addFindersToDeleteList(rowsToDelete, findersModel);
		return true;
	}
	
	private List<Integer> checkActionFinder(List<Finder> findersToCheck, DefaultTableModel actionsModel) {
		List<Integer> toDeleteList = new ArrayList<>();
		for (Finder finder : findersToCheck) {
			for (int i = 0; i < actionsModel.getRowCount(); i++) {
				Object finderAsObj = actionsModel.getValueAt(i, 2);
				if (finderAsObj != null && !finderAsObj.toString().isEmpty() && finder.equals((Finder) finderAsObj)) {
					toDeleteList.add(i);
				}
			}
		}
		return toDeleteList;
	}
	
	private void addFindersToDeleteList(List<Integer> rowsToDelete, DefaultTableModel findersModel) {
		for (Integer toDelete : rowsToDelete) {
			Finder finderToDelete = (Finder) findersModel.getValueAt(toDelete, 0);
			if (finderToDelete.getId() != null) {
				deleteList.add(finderToDelete);
			}
			findersModel.removeRow(toDelete);
		}
	}
	
	private void addActionsToDeleteList(List<Integer> actionsToDelete, DefaultTableModel actionsModel) {
		for (Integer toDelete : actionsToDelete) {
			Action actionToDelete = (Action) actionsModel.getValueAt(toDelete, 0);
			if (actionToDelete.getId() != null) {
				deleteList.add(actionToDelete);
			}
			actionsModel.removeRow(toDelete);
		}
	}

	private void removeSelectedActions() {
		DefaultTableModel actionsModel = getActionsModel();
		List<Integer> actionsToDelete = new ArrayList<>();
		for (int i = 0; i < actionsModel.getRowCount(); i++) {
			if ((boolean) actionsModel.getValueAt(i, 1)) {
				actionsToDelete.add(i);
			}
		}
		
		if (actionsToDelete.isEmpty()) {
			JOptionPane.showMessageDialog(FrameStep.this, "Select at least one row.", "Step Alert", JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		addActionsToDeleteList(actionsToDelete, actionsModel);
		
		if (actionsModel.getRowCount() == 0) {
			btnExecute.setEnabled(false);
		}
	}

	public boolean addCallBack(Object addType, DefaultTableModel model) {
		try {
			DefaultTableModel tableModel = null;
			Finder finder = null;
			Action action = null;
			if (addType instanceof FinderType) {
				tableModel = getFindersModel();
				FinderController finderController = new FinderController();
				String validName = getValidFinderName(finderController, suggestFinderName.getSelectedItem());
				if (validName == null) {
					return false;
				}
				finder = finderController.buildFinder(validName, (FinderType) addType, model);
			} else {
				tableModel = getActionsModel();
				action = (new ActionController()).buildAction(comboActionFinder.getSelectedItem(), (ActionType) addType, model, tableModel.getRowCount());
			}

			addInTable(tableModel, finder, action);
		} catch (Exception e) {
			showError(e);
		}
		
		return true;
	}
	
	private String getValidFinderName(FinderController finderController, Object name) throws Exception {
		if (name instanceof SuggestDoubleColumn) {
			name = ((SuggestDoubleColumn) name).getLeftText().getName();
		}
		
		Finder result = finderController.findByName(name.toString());
		if (result != null) {
			Object newName = JOptionPane.showInputDialog(FrameStep.this, "This name already exist, please insert a new one with more than three characters.", "Finder Alert", JOptionPane.WARNING_MESSAGE, null, null, name);
			if (newName == null || newName.toString().isEmpty() || newName.toString().length() < 3) {
				return null;
			}
			return getValidFinderName(finderController, newName);
		}
		
		return name.toString();
	}

	private void addInTable(DefaultTableModel tableModel, Finder finder, Action action) {
		if (tableModel == null) {
			return;
		}
		boolean isFinder = finder != null;
		Object finderName = "";
		String params = "";
		
		if (isFinder) {
			cancelFinderTableChanged = true;
			finderName = finder.getNmFinder();
			params = finderToParamsString(finder);
		} else {
			if (action.getFinder() != null) {
				finderName = action.getFinder();
			}
			params = replaceFlagDesktopType(action);
		}

		int rowCount = tableModel.getRowCount();
		tableModel.setRowCount(rowCount + 1);
		tableModel.setValueAt(isFinder ? finder : action, rowCount, 0);// Entity
		tableModel.setValueAt(false, rowCount, 1);//Delete
		tableModel.setValueAt(finderName, rowCount, 2);//Finder name
		tableModel.setValueAt(isFinder ? finder.getFinderType() : action.getActionType().getActionGroupType(), rowCount, 3);//FinderType or ActionGroupType
		tableModel.setValueAt(isFinder ? params : action.getActionType(), rowCount, 4);//Finder Parameters or ActionType
		if (!isFinder) {
			tableModel.setValueAt(params, rowCount,	5);//Parameters
			btnExecute.setEnabled(true);
		}

		if (isFinder) {
			suggestFinderName.setSelectedItem("");
			refreshComboActionFinder();
		}
	}
	
	private String replaceFlagDesktopType(Action action) {
		String params = actionToParamsString(action);
		ComboBoxType comboType = action.getActionType().getFrameAddType().getComboBoxType();
		if (comboType == null || !(comboType.getComboItems().get(0) instanceof DesktopType)) {
			return params;
		}
		
		int flagValue = getFlagValue(params);
		if (flagValue == -1) {
			return params;
		}
		
		for (Object item : comboType.getComboItems()) {
			DesktopType deskItem = (DesktopType) item;
			if (deskItem.getValue() == flagValue) {
				return params.replace("Flag=" + flagValue, "Flag=" + deskItem);
			}
		}
		return params;
	}
	
	private int getFlagValue(String params) {
		String paramsFlag = params.substring(params.indexOf("Flag"));
		if (paramsFlag == null || paramsFlag.isEmpty()) {
			return -1;
		}
		String value;
		if (paramsFlag.contains(";")) {
			value = paramsFlag.substring(paramsFlag.indexOf("=") + 1, paramsFlag.indexOf(";"));
		} else {
			value = paramsFlag.substring(paramsFlag.indexOf("=") + 1);
		}
		return value == null || value.isEmpty() || StringUtils.isAlphaSpace(value) ? -1 : Integer.parseInt(value);
	}

	public void changeCallBack(Object addType, DefaultTableModel model) {
		try {
			if (addType instanceof FinderType) {
				DefaultTableModel findersModel = getFindersModel();
				int selectedRow = findersTable.getSelectedRow();
				Finder updateFinder = (Finder) findersModel.getValueAt(selectedRow, 0);
				(new FinderController()).updateModelParams(updateFinder, model);
				
				findersModel.setValueAt(finderToParamsString(updateFinder), selectedRow, (findersModel.getColumnCount() - 1));
				refreshComboActionFinder();
			} else {
				DefaultTableModel actionsModel = getActionsModel();
				int selectedRow = actionsTable.getSelectedRow();
				Action updateAction = (Action) actionsModel.getValueAt(selectedRow, 0);
				(new ActionController()).updateParams(updateAction, model);
				
				String params = replaceFlagDesktopType(updateAction);
				actionsModel.setValueAt(params, selectedRow, (actionsModel.getColumnCount() - 1));
			}
		} catch (Exception e) {
			showError(e);
		}
	}

	private void refreshComboActionFinder() {
		DefaultTableModel tableModel = getFindersModel();
		List<Finder> finderList = new ArrayList<>();
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			finderList.add((Finder) tableModel.getValueAt(i, 0));
		}

		sortFinderByName(finderList);
		if (comboActionFinder.getItemCount() > 0) {
			comboActionFinder.removeAllItems();
		}
		
		comboActionFinder.setEnabled(!finderList.isEmpty());
		comboActionFinder.addItem("None");
		for (Finder finder : finderList) {
			comboActionFinder.addItem(finder);
		}
	}

    private List<Finder> sortFinderByName(List<Finder> listToSort) {
    	Collections.sort(listToSort, new Comparator<Finder>() {
            @Override
            public int compare(Finder o1, Finder o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        return listToSort;
    }
    
    private void refreshComboGroup() {
    	if (comboGroup.getItemCount() > 0) {
    		comboGroup.removeAllItems();
    	}
    	
    	Object selected = comboActionFinder.getSelectedItem();
    	List<ActionGroupType> groupList = new ArrayList<>();
    	if (selected instanceof Finder) {
    		groupList = ActionGroupType.getGroupsByFinder(((Finder) selected).getFinderType(), true);
    	} else {
    		groupList = ActionGroupType.getGroupsWithNoFinder(true);
    	}
    	
    	for (ActionGroupType group : groupList) {
    		comboGroup.addItem(group);
    	}
    }

	private void refreshComboAction() {
		if (comboAction.getItemCount() > 0) {
			comboAction.removeAllItems();
		}
		
		Object selected = comboGroup.getSelectedItem();
		if (selected instanceof ActionGroupType) {
			comboAction.setEnabled(true);
			List<ActionType> typeGroupList = ActionType.getActionTypesByGroup((ActionGroupType) selected, true);
			for (ActionType typeGroup : typeGroupList) {
				comboAction.addItem(typeGroup);
			}
		} else {
			comboAction.setEnabled(false);
		}
	}
	
	private void setSavedStep() {
		textName.setText(step.getNmStep());
		DefaultTableModel findersTableModel = getFindersModel();
		for (Finder finder : step.getFinders()) {
			addInTable(findersTableModel, finder, null);
		}
		DefaultTableModel actionsTableModel = getActionsModel();
		for (Action action : step.getActionsByOrder()) {
			addInTable(actionsTableModel, null, action);
		}
	}

	private String finderToParamsString(Finder finder) {
		String treatedParams = "";
		for (String label : finder.getFinderType().getFrameAddType().getLabels()) {
			for (FinderParameter finderParam : finder.getParameters()) {
				String name = finderParam.getName();
				if (label.equalsIgnoreCase(name)) {
					treatedParams += ";" + name + "=" + finderParam.getValue();
				}
			}
		}
		return treatedParams.isEmpty() ? treatedParams : treatedParams.substring(1);
	}

	private String actionToParamsString(Action action) {
		String treatedParams = "";
		for (String label : action.getActionType().getFrameAddType().getLabels()) {
			for (ActionParameter actionParam : action.getParameters()) {
				String name = actionParam.getName();
				if (label.equalsIgnoreCase(name)) {
					treatedParams += ";" + name + "=" + actionParam.getValue();
				}
			}
		}
		return treatedParams.isEmpty() ? treatedParams : treatedParams.substring(1);
	}
    
    private void setInvisibleColumn(JTable table) {
		table.getColumnModel().getColumn(0).setWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
    }
    
    private void activateSuggest() {
    	suggestFinderName.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent ke) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int key = ke.getKeyCode();
						Object item = suggestFinderName.getSelectedItem();
						if (item != null && item.toString().length() > 2) {
							if (wasArrows && key == KeyEvent.VK_ENTER) {
								suggestFinderName.hidePopup();
								wasArrows = false;
								setFinderParams(item);
							} else if (key == KeyEvent.VK_ENTER) {
								comboFilter(item.toString());
							} else {
								wasArrows = (key == KeyEvent.VK_KP_DOWN)
										|| (key == KeyEvent.VK_KP_UP)
										|| (key == KeyEvent.VK_DOWN)
										|| (key == KeyEvent.VK_UP);
							}
						}
					}
				});
			}
		});
		JList<?> suggestList = getSuggestList(suggestFinderName);
		if (suggestList != null) {
			suggestList.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					JList<?> list = (JList<?>) e.getComponent();
					Point pt = e.getPoint();
					int index = list.locationToIndex(pt);
					if (index >= 0) {
						setFinderParams(suggestFinderName.getItemAt(index));
					}
				}
			});
		}
	}

	public void comboFilter(final String searchFor) {
		blockerLoading.start("Searching equivalences");

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				List<SuggestDoubleColumn> items = getItems(suggestFinderName.getName(), searchFor);
				if (items.size() > 0) {
					suggestFinderName.setModel(new DefaultComboBoxModel(items.toArray()));
					suggestFinderName.setSelectedItem(searchFor.isEmpty() ? "" : searchFor);
					JTextField editor = (JTextField) suggestFinderName.getEditor().getEditorComponent();
					editor.setCaretPosition(editor.getDocument().getLength());
					suggestFinderName.showPopup();
				} else {
					suggestFinderName.hidePopup();
				}
				blockerLoading.stop();
				return null;
			}
		}.execute();
	}

	private List<SuggestDoubleColumn> getItems(String suggestName, String searchFor) {
		List<SuggestDoubleColumn> returnList = new ArrayList<>();
		try {
			for (Finder finder : (new FinderController()).findLikeName(searchFor)) {
				returnList.add(new SuggestDoubleColumn(new CellValue(finder.getId(), finder.getNmFinder(), null, finder), searchFor));
			}
		} catch (Exception e) {
			showError(e);
		}
		return returnList;
	}

	private JList<?> getSuggestList(SuggestCombo suggest) {
		Accessible a = suggest.getAccessibleContext().getAccessibleChild(0);
		if (a instanceof BasicComboPopup) {
			return ((BasicComboPopup) a).getList();
		} else {
			return null;
		}
	}
	
	private void setFinderParams(Object item) {
		SuggestDoubleColumn lrItem = (SuggestDoubleColumn) item;
		callFrameAdd((Finder)((CellValue) lrItem.getLeftText()).getFullItem());
	}

	private DefaultTableModel getFindersModel() {
		return findersTableModel;
	}

	private void setFindersModel(DefaultTableModel findersModel) {
		this.findersTableModel = findersModel;
	}

	private DefaultTableModel getActionsModel() {
		return actionsTableModel;
	}

	private void setActionsModel(DefaultTableModel actionsModel) {
		this.actionsTableModel = actionsModel;
	}
}
