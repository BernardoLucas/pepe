package com.mangyBear.Pepe.action.variableManagement;

import java.util.List;

import com.mangyBear.Pepe.service.MyConstants;

public class ListGroup {
	
	public boolean add(List<Object> list, Object obj) {
		return list.add(obj);
	}
	
	public boolean addAll(List<Object> list, List<Object> listTwo) {
		return list.addAll(listTwo);
	}
	
	public boolean addAllIndex(List<Object> list, List<Object> listTwo, String index) {
		return list.addAll(getValidIndex(index, list.size()), listTwo);
	}
	
	public void addIndex(List<Object> list, String index, Object obj) {
		list.add(getValidIndex(index, list.size()), obj);
	}
	
	public void clear(List<Object> list) {
		list.clear();
	}
	
	public boolean contains(List<Object> list, Object obj) {
		return list.contains(obj);
	}
	
	public Object get(List<Object> list, String index) {
		return list.get(getValidIndex(index, list.size()));
	}
	
	public int indexOf(List<Object> list, Object obj) {
		return list.indexOf(obj);
	}
	
	public boolean isEmpty(List<Object> list) {
		return list.isEmpty();
	}

	public int lastIndexOf(List<Object> list, Object obj) {
		return list.lastIndexOf(obj);
	}
	
	public boolean removeElement(List<Object> list, Object obj) {
		return list.remove(obj);
	}
	
	public Object removeIndex(List<Object> list, String index) {
		return list.remove(getValidIndex(index, list.size()));
	}
	
	public Object set(List<Object> list, String index, Object obj) {
		return list.set(getValidIndex(index, list.size()), obj);
	}
	
	public int size(List<Object> list) {
		return list.size();
	}
	
	private int getValidIndex(String index, int listSize) {
		int indexI = MyConstants.parseInt(index);
		return (indexI < 0) ? 0 : (indexI >= listSize) ? listSize - 1 : indexI;
	}
}
