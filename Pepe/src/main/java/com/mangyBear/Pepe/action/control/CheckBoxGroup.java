package com.mangyBear.Pepe.action.control;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Control;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class CheckBoxGroup {

    private final AutoItX autoItX;

    public CheckBoxGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public void check(Control control) {
        autoItX.controlCommandCheck(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public boolean isChecked(Control control) {
        return autoItX.controlCommandIsChecked(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public void uncheck(Control control) {
        autoItX.controlCommandUncheck(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }
}
