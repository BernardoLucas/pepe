package com.mangyBear.Pepe.action.web;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class JavascriptExecutorGroup {

    public void execute(WebDriver driver, String script) {
        ((JavascriptExecutor) driver).executeScript(script);
        /*
        scrollDown  - "scroll(0, " + scroll + ");"
        scrollLeft  - "scroll(-" + scroll + ", 0);"
        scrollRight - "scroll(" + scroll + ", 0);"
        scrollUp    - "scroll(0, -" + scroll + ");"
        */
        //
    }
}
