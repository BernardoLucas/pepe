package com.mangyBear.Pepe.action.web;

import java.util.List;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ManageImeGroup {

    public void activateEngine(WebDriver driver, String string) {
        driver.manage().ime().activateEngine(string);
    }

    public void deactivate(WebDriver driver) {
        driver.manage().ime().deactivate();
    }

    public String getActiveEngine(WebDriver driver) {
        return driver.manage().ime().getActiveEngine();
    }

    public List<String> getAvailableEngines(WebDriver driver) {
        return driver.manage().ime().getAvailableEngines();
    }

    public boolean isActivated(WebDriver driver) {
        return driver.manage().ime().isActivated();
    }
}
