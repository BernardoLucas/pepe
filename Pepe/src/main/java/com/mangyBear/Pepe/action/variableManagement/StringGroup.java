package com.mangyBear.Pepe.action.variableManagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mangyBear.Pepe.service.MyConstants;

public class StringGroup {
	
	public Character charAt(String string, String index) {
		int indexI = MyConstants.parseInt(index);
		indexI = (indexI < 0 || indexI > string.length()) ? 0 : indexI;
		return string.charAt(indexI);
	}
	
	public String concat(String string, String anotherString) {
		return string.concat(anotherString);
	}
	
	public Boolean contains(String string, String anotherString) {
		return string.contains(anotherString);
	}
	
	public Boolean endsWith(String string, String suffix) {
		return string.endsWith(suffix);
	}
	
	public Boolean equals(String string, String anotherString) {
		return string.equals(anotherString);
	}
	
	public Boolean equalsIgnoreCase(String string, String anotherString) {
		return string.equalsIgnoreCase(anotherString);
	}
	
	public String insertInIndex(String string, String anotherString, String index) {
		int indexI = MyConstants.parseInt(index);
		indexI = (indexI < 0 || indexI > string.length()) ? 0 : indexI;
		return string.substring(0, indexI) + anotherString + string.substring(indexI);
	}
	
	public Boolean isEmpty(String string) {
		return string.isEmpty();
	}
	
	public String length(String string) {
		return String.valueOf(string.length());
	}
	
	public String replace(String string, String target, String replacement) {
		return string.replace(target, replacement);
	}
	
	public String replaceAll(String string, String regex, String replacement) {
		return string.replaceAll(regex, replacement);
	}
	
	public String replaceBetween(String string, String anotherString, String beginIndex, String endIndex) {
		int beginIndexI = MyConstants.parseInt(beginIndex);
		int endIndexI = MyConstants.parseInt(endIndex);
		beginIndexI = (beginIndexI < 0 || beginIndexI > string.length()) ? 0 : beginIndexI;
		endIndexI = (endIndexI < 0 || endIndexI > string.length()) ? 1 : endIndexI;
		return string.substring(0, beginIndexI) + anotherString + string.substring(endIndexI);
	}
	
	public String replaceFirst(String string, String regex, String replacement) {
		return string.replaceFirst(regex, replacement);
	}

	public List<String> split(String string, String regex, String limit) {
		int limitI = MyConstants.parseInt(limit);
		if (limitI == 0) {
			return new ArrayList<String>(Arrays.asList(string.split(regex)));
		}
		return new ArrayList<String>(Arrays.asList(string.split(regex, limitI)));
	}
	
	public Boolean startsWith(String string, String prefix, String toffset) {
		return string.startsWith(prefix, MyConstants.parseInt(toffset));
	}
	
	public String substring(String string, String beginIndex, String endIndex) {
		int beginIndexI = MyConstants.parseInt(beginIndex);
		int endIndexI = MyConstants.parseInt(endIndex);
		beginIndexI = (beginIndexI < 0) ? 0 : beginIndexI;
		if (beginIndexI > endIndexI || endIndexI > string.length()) {
			return string.substring(beginIndexI);
		}
		return string.substring(beginIndexI, endIndexI);
	}
	
	public List<String> toCharArray(String string) {
		List<String> result = new ArrayList<>();
		for (char c : string.toCharArray()) {
			result.add(String.valueOf(c));
		}
		return result;
	}
	
	public String toLowerCase(String string) {
		return string.toLowerCase();
	}
	
	public String toUpperCase(String string) {
		return string.toUpperCase();
	}
	
	public String trim(String string) {
		return string.trim();
	}
}
