package com.mangyBear.Pepe.action.variableManagement;

import com.mangyBear.Pepe.service.MyConstants;

public class MathGroup {
	
	public String division(String number, String anotherNumber) {
		return String.valueOf(MyConstants.parseDouble(number) / MyConstants.parseDouble(anotherNumber));
	}
	
	public String multiplication(String number, String anotherNumber) {
		return String.valueOf(MyConstants.parseDouble(number) * MyConstants.parseDouble(anotherNumber));
	}
	
	public String getRest(String number, String anotherNumber) {
		return String.valueOf(MyConstants.parseDouble(number) % MyConstants.parseDouble(anotherNumber));
	}
	
	public String subtraction(String number, String anotherNumber) {
		return String.valueOf(MyConstants.parseDouble(number) - MyConstants.parseDouble(anotherNumber));
	}
	
	public String summation(String number, String anotherNumber) {
		return String.valueOf(MyConstants.parseDouble(number) + MyConstants.parseDouble(anotherNumber));
	}
}
