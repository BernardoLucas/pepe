package com.mangyBear.Pepe.action.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;

import com.mangyBear.Pepe.service.MyConstants;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ManageWindowGroup {

    public List<String> getPosition(WebDriver driver) {
        Point point = driver.manage().window().getPosition();
        return new ArrayList<String>(Arrays.asList(String.valueOf(point.x), String.valueOf(point.y)));
    }

    public List<String> getSize(WebDriver driver) {
    	Dimension dimension = driver.manage().window().getSize();
        return new ArrayList<String>(Arrays.asList(String.valueOf(dimension.width), String.valueOf(dimension.height)));
    }

    public void maximize(WebDriver driver) {
        driver.manage().window().maximize();
    }

    public void setPosition(WebDriver driver, List<String> listPoint) {
    	java.awt.Point point = MyConstants.parsePoint(listPoint);
        driver.manage().window().setPosition(new Point(point.x, point.y));
    }

    public void setSize(WebDriver driver, List<String> listDimension) {
    	java.awt.Dimension dimension = MyConstants.parseDimension(listDimension);
        driver.manage().window().setSize(new Dimension(dimension.width, dimension.height));
    }
}
