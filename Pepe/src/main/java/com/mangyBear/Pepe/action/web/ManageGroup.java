package com.mangyBear.Pepe.action.web;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ManageGroup {

    public void addCookie(WebDriver driver, Cookie cookie) {
        driver.manage().addCookie(cookie);
    }

    public void deleteAllCookies(WebDriver driver) {
        driver.manage().deleteAllCookies();
    }

    public void deleteCookie(WebDriver driver, Cookie cookie) {
        driver.manage().deleteCookie(cookie);
    }

    public void deleteCookieNamed(WebDriver driver, String cookie) {
        driver.manage().deleteCookieNamed(cookie);
    }

    public List<String> getCookies(WebDriver driver) {
    	List<String> result = new ArrayList<>();
    	for (Cookie cookie : driver.manage().getCookies()) {
    		result.add(cookie.toString());
		}
        return result;
    }

    public Cookie getCookieNamed(WebDriver driver, String cookie) {
        return driver.manage().getCookieNamed(cookie);
    }
}
