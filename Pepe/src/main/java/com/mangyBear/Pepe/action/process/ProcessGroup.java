package com.mangyBear.Pepe.action.process;

import com.mangyBear.Pepe.domain.types.desktopParams.DesktopType;
import com.mangyBear.Pepe.service.MyConstants;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ProcessGroup {

    private final AutoItX autoIt;

    public ProcessGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoIt = new AutoItX();
    }

    public void close(String process) {
        autoIt.processClose(process);
    }

    public boolean exists(String process) {
        return autoIt.processExists(process) == 1;
    }

    public boolean setPriority(String process, String priority) {
        return autoIt.processSetPriority(process, DesktopType.getDesktopTypeValueByName(priority));
    }

    public boolean wait(String process, String timeout) {
        return autoIt.processWait(process, MyConstants.parseInt(timeout));
    }

    public boolean waitClose(String process, String timeout) {
        return autoIt.processWaitClose(process, MyConstants.parseInt(timeout));
    }
}
