package com.mangyBear.Pepe.action.web;

import java.net.URL;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ManageNavigateGroup {

    public void back(WebDriver driver) {
        driver.navigate().back();
    }

    public void forward(WebDriver driver) {
        driver.navigate().forward();
    }

    public void refresh(WebDriver driver) {
        driver.navigate().refresh();
    }

    public void to(WebDriver driver, Object navigateTo) {
        if (navigateTo instanceof String) {
            driver.navigate().to((String) navigateTo);
        }
        if (navigateTo instanceof URL) {
            driver.navigate().to((URL) navigateTo);
        }
    }
}
