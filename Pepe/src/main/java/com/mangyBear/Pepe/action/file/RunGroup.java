package com.mangyBear.Pepe.action.file;

import java.io.File;

import com.mangyBear.Pepe.domain.types.desktopParams.DesktopType;
import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Window;
import com.mangyBear.Pepe.ui.listener.GetInfoWindow;
import com.mangyBear.Pepe.ui.listener.JNAWindow;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class RunGroup {

    private final AutoItX autoItX;

    public RunGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public Window run(File file, String workingDirectory, String flag) {
        return getWindowByPid(autoItX.run(file.getAbsolutePath(), workingDirectory, DesktopType.getDesktopTypeValueByName(flag)));
    }

    public boolean runAsSet(String userName, String domain, String password, String option) {
        return autoItX.runAsSet(userName, domain, password, DesktopType.getDesktopTypeValueByName(option)) != 0;
    }

    public Window runWait(File file, String workingDirectory, String flag) {
        return getWindowByPid(autoItX.runWait(file.getAbsolutePath(), workingDirectory, DesktopType.getDesktopTypeValueByName(flag)));
    }
    
    private Window getWindowByPid(int pid) {
    	JNAWindow jnaWindow = (new GetInfoWindow()).getByPid(pid);
    	if (jnaWindow != null) {
    		return new Window(jnaWindow.getTitle(), "", jnaWindow.getHexStringHwnd(), pid);
    	}
    	return new Window(null, null, null,pid);
    }
}
