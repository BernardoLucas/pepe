package com.mangyBear.Pepe.action.file;

import java.io.File;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.readWriteFile.CsvWriter;
import com.mangyBear.Pepe.ui.component.readWriteFile.PropertyWriter;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class WriteGroup {

    public void csvWrite(File filePath, String row, String column, String text) {
    	(new CsvWriter()).updateCell(filePath, MyConstants.parseInt(row), MyConstants.parseInt(column), text);
    }

    public void propertyWrite(File filePath, String section, String key, String value) {
        (new PropertyWriter()).writeProperty(filePath, section, key, value);
    }

    public void propertyDelete(File filePath, String section, String key) {
        (new PropertyWriter()).deleteProperty(filePath, section, key);
    }
}
