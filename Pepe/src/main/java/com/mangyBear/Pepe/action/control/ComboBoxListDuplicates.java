package com.mangyBear.Pepe.action.control;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Control;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ComboBoxListDuplicates {

    protected final AutoItX autoItX;

    public ComboBoxListDuplicates() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    protected void addString(Control control, String string) {
    	autoItX.controlCommandAddString(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), string);
    }

    protected void deleteString(Control control, String string) {
    	autoItX.controlCommandDeleteString(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), string);
    }

    protected boolean findString(Control control, String string) {
        return autoItX.controlCommandFindString(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), string) == 1;
    }

    protected String getCurrentSelection(Control control, String charLength) {
        return autoItX.controlCommandGetCurrentSelection(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), MyConstants.parseInt(charLength));
    }

    protected void selectString(Control control, String string) {
    	autoItX.controlCommandSelectString(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), string);
    }

    protected void setCurrentSelection(Control control, String occurance) {
    	autoItX.controlCommandSetCurrentSelection(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), occurance);
    }
}

