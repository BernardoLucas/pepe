package com.mangyBear.Pepe.action.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ElementGroup {

	public void clear(WebElement webElement) {
		webElement.clear();
	}

	public void click(WebElement webElement) {
		webElement.click();
	}

	public String getAttribute(WebElement webElement, String name) {
		return webElement.getAttribute(name);
	}

	public String getCssValue(WebElement webElement, String propertyName) {
		return webElement.getCssValue(propertyName);
	}

	public List<String> getLocation(WebElement webElement) {
		Point point = webElement.getLocation();
		if (point == null) {
			new ArrayList<String>(Arrays.asList("0", "0"));
		}

		return new ArrayList<String>(Arrays.asList(String.valueOf(point.x), String.valueOf(point.y)));
	}

	public List<String> getSize(WebElement webElement) {
		Dimension dimension = webElement.getSize();
		if (dimension == null) {
			new ArrayList<String>(Arrays.asList("0", "0"));
		}

		return new ArrayList<String>(Arrays.asList(String.valueOf(dimension.height), String.valueOf(dimension.width)));
	}

	public String getTagName(WebElement webElement) {
		return webElement.getTagName();
	}

	public String getText(WebElement webElement) {
		return webElement.getText();
	}

	public boolean isDisplayed(WebElement webElement) {
		return webElement.isDisplayed();
	}

	public boolean isEnabled(WebElement webElement) {
		return webElement.isEnabled();
	}

	public boolean isSelected(WebElement webElement) {
		return webElement.isSelected();
	}

	public void sendKeys(WebElement webElement, String keysToSend) {
		webElement.sendKeys(keysToSend);
	}

	public void submit(WebElement webElement) {
		webElement.submit();
	}
}
