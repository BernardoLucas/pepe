package com.mangyBear.Pepe.action.control;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Control;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ComboBoxGroup extends ComboBoxListDuplicates{

    public void hideDropdown(Control control) {
        autoItX.controlCommandHideDropDown(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public void showDropdown(Control control) {
    	autoItX.controlCommandShowDropdown(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }
}