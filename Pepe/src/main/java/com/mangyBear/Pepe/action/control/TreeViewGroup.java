package com.mangyBear.Pepe.action.control;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Control;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class TreeViewGroup {

    private final AutoItX autoItX;

    public TreeViewGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public boolean aBoolean(Control control, String command, String option, String option2) {
        return autoItX.controlTreeViewBoolean(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), command, option, option2);
    }

    public void check(Control control, String item) {
        autoItX.controlTreeViewCheck(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }

    public void collapse(Control control, String item) {
        autoItX.controlTreeViewCollapse(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }

    public boolean exists(Control control, String item) {
        return autoItX.controlTreeViewExists(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }

    public void expand(Control control, String item) {
        autoItX.controlTreeViewExpand(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }

    public int getItemCount(Control control, String item) {
        return autoItX.controlTreeViewGetItemCount(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }

    public int getSelectedItemIndex(Control control) {
        return autoItX.controlTreeViewGetSelectedItemIndex(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public String getSelectedItemText(Control control) {
        return autoItX.controlTreeViewGetSelectedItemText(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public String getText(Control control, String item) {
        return autoItX.controlTreeViewGetText(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }

    public boolean isChecked(Control control) {
        return autoItX.controlTreeViewIsChecked(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle()) == 1;
    }

    public void select(Control control, String item) {
        autoItX.controlTreeViewSelect(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }

    public void uncheck(Control control, String item) {
        autoItX.controlTreeViewUncheck(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }
}
