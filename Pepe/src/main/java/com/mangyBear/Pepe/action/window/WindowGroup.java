package com.mangyBear.Pepe.action.window;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Window;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class WindowGroup {

    private final AutoItX autoItX;

    public WindowGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }
    
    public void activate(Window window) {
        autoItX.winActivate(MyConstants.getValidTitle(window), window.getText());
    }

    public void active(Window window) {
        autoItX.winActive(MyConstants.getValidTitle(window), window.getText());
    }

    public void close(Window window) {
        autoItX.winClose(MyConstants.getValidTitle(window), window.getText());
    }

    public boolean exists(Window window) {
        return autoItX.winExists(MyConstants.getValidTitle(window), window.getText());
    }

    public List<String> getCaretPosition() {
        return new ArrayList<String>(Arrays.asList(String.valueOf(autoItX.winGetCaretPosX()), String.valueOf(autoItX.winGetCaretPosY())));
    }

    public String getClassList(Window window) {
        return autoItX.winGetClassList(MyConstants.getValidTitle(window), window.getText());
    }

    public String getHandle(Window window) {
        return autoItX.winGetHandle(MyConstants.getValidTitle(window), window.getText());
    }

    public List<String> getPosition(Window window) {
    	String validTitle = MyConstants.getValidTitle(window);
        return new ArrayList<String>(Arrays.asList(String.valueOf(autoItX.winGetPosX(validTitle, window.getText())),
        		String.valueOf(autoItX.winGetPosY(validTitle, window.getText()))));
    }

    public String getProcess(Window window) {
        return autoItX.winGetProcess(MyConstants.getValidTitle(window), window.getText());
    }

    public List<String> getSize(Window window) {
    	String validTitle = MyConstants.getValidTitle(window);
//        autoItX.winGetClientSizeHeight(validTitle, window.getText());
//        autoItX.winGetClientSizeWidth(validTitle, window.getText());
        return new ArrayList<String>(Arrays.asList(String.valueOf(autoItX.winGetPosWidth(validTitle, window.getText())),
        		String.valueOf(autoItX.winGetPosHeight(validTitle, window.getText()))));
    }

    public int getState(Window window) {
        return autoItX.winGetState(MyConstants.getValidTitle(window), window.getText());
    }

    public String getText(Window window) {
        return autoItX.winGetText(MyConstants.getValidTitle(window), window.getText());
    }

    public String getTitle(Window window) {
        return autoItX.winGetTitle(MyConstants.getValidTitle(window), window.getText());
    }

    public void kill(Window window) {
        autoItX.winKill(MyConstants.getValidTitle(window), window.getText());
    }

    public List<String> listHwnd(Window window) {
        return getWinList(window, false);
    }
    
    public List<String> listTitle(Window window) {
    	return getWinList(window, true);
    }
    
    private List<String> getWinList(Window window, boolean getTitle) {
    	List<String> titleList = new ArrayList<>();
    	List<String> hwndList = new ArrayList<>();
    	String[][] map = autoItX.winList(MyConstants.getValidTitle(window), window.getText());
    	for (int i = 1; i < map.length; i++) {
			String[] win = map[i];
			if (win.length == 1) {
				titleList.add(win[0]);
			}
			if (win.length >= 2) {
				titleList.add(win[0]);
				hwndList.add(win[1]);
			}
		}
    	return getTitle ? titleList : hwndList;
    }

    public boolean menuSelectItem(Window window, String item) {
        return autoItX.winMenuSelectItem(MyConstants.getValidTitle(window), window.getText(), item);
    }

    public void minimizeAll() {
        autoItX.winMinimizeAll();
    }

    public void minimizeAllUndo() {
        autoItX.winMinimizeAllUndo();
    }

    public void move(Window window, String x, String y) {
        autoItX.winMove(MyConstants.getValidTitle(window), window.getText(), MyConstants.parseInt(x), MyConstants.parseInt(y));
    }

    public void resize(Window window, String width, String height) {
        List<String> point = getPosition(window);
        autoItX.winMove(MyConstants.getValidTitle(window), window.getText(), MyConstants.parseInt(point.get(0)), MyConstants.parseInt(point.get(1)), MyConstants.parseInt(width), MyConstants.parseInt(height));
    }

    public void setOnTop(Window window, String isTopMost) {
        autoItX.winSetOnTop(MyConstants.getValidTitle(window), window.getText(), Boolean.getBoolean(isTopMost));
    }

    public void setState(Window window, String state) {
        autoItX.winSetState(MyConstants.getValidTitle(window), window.getText(), MyConstants.parseInt(state));
    }

    public void setTitle(Window window, String newtitle) {
        autoItX.winSetTitle(MyConstants.getValidTitle(window), window.getText(), newtitle);
    }

    public boolean setTrans(Window window, String transparency) {
    	int trans = MyConstants.parseInt(transparency);
    	trans = trans < 0 ? 0 : trans > 255 ? 255 : trans;
        return autoItX.winSetTrans(MyConstants.getValidTitle(window), window.getText(), trans);
    }

    public boolean wait(Window window, String timeout) {
        return autoItX.winWait(MyConstants.getValidTitle(window), window.getText(), MyConstants.parseInt(timeout));
    }

    public boolean waitActive(Window window, String timeout) {
        return autoItX.winWaitActive(MyConstants.getValidTitle(window), window.getText(), MyConstants.parseInt(timeout));
    }

    public boolean waitClose(Window window, String timeout) {
        return autoItX.winWaitClose(MyConstants.getValidTitle(window), window.getText(), MyConstants.parseInt(timeout));
    }

    public boolean waitNoActive(Window window, String timeout) {
        return autoItX.winWaitNoActive(MyConstants.getValidTitle(window), window.getText(), MyConstants.parseInt(timeout));
    }
}
