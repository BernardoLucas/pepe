package com.mangyBear.Pepe.action.file;

import java.io.File;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.ui.component.readWriteFile.CsvReader;
import com.mangyBear.Pepe.ui.component.readWriteFile.PropertyReader;
import com.mangyBear.Pepe.ui.component.readWriteFile.XmlReader;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ReadGroup {

    public String csvReadCell(File filePath, String row, String column, String defaultVal) {
        return (new CsvReader()).readCell(filePath, MyConstants.parseInt(row), MyConstants.parseInt(column), defaultVal);
    }

    public String propertyRead(File filePath, String section, String key, String defaultVal) {
        return (new PropertyReader()).getKeyValue(filePath, section, key, defaultVal);
    }

    public String xmlGetAttribute(File filePath, String section, String tag, String attribute, String defaultVal) {
        return (new XmlReader()).getAttribute(filePath, section, tag, attribute, defaultVal);
    }

    public String xmlGetValue(File filePath, String section, String tag, String defaultVal) {
    	return (new XmlReader()).getValue(filePath, section, tag, defaultVal);
    }
}
