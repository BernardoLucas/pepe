package com.mangyBear.Pepe.action.web;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class BasicWebGroup {

	public WebDriver chromeDriver(String driverPath, String stringCapabilities, String options) {
		new ChromeDriverService.Builder().usingDriverExecutable(new File(driverPath)).usingAnyFreePort().build();
		System.setProperty("webdriver.chrome.driver", driverPath);
		
		ChromeOptions chromeOptions = new ChromeOptions();
		setChromeOptions(chromeOptions, options);
		
		DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
		setCapabilities(desiredCapabilities, stringCapabilities);
		desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		
		return new ChromeDriver(desiredCapabilities);
	}
	
	private void setChromeOptions(ChromeOptions chromeOptions, String options) {
		for (String option : options.split(";")) {
			chromeOptions.addArguments(option);
		}
	}

	public WebDriver firefoxDriver(String driverPath, String stringCapabilities, String stringProfiles) {
		FirefoxProfile profile = setFirefoxProfile(stringProfiles);
		
		DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
		setCapabilities(desiredCapabilities, stringCapabilities);
		desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile);
		
		return new FirefoxDriver(desiredCapabilities);
	}
	
	private FirefoxProfile setFirefoxProfile(String stringProfiles) {
		FirefoxProfile ffProfile;
		
		String[] profiles = stringProfiles.split(";");
		if (profiles.length == 1 && !profiles[0].contains("=")) {
			ffProfile = new ProfilesIni().getProfile(profiles[0]);
		} else {
			ffProfile = new FirefoxProfile();
			for (String profile : profiles) {
				if (profile.contains("=")) {
					ffProfile.setPreference(profile.substring(0, profile.lastIndexOf("=")), profile.substring(profile.lastIndexOf("=") + 1));
				}
			}
		}
		return ffProfile;
	}

	public WebDriver ieDriver(String driverPath, String stringCapabilities) {
		System.setProperty("webdriver.ie.driver", driverPath);
		
		DesiredCapabilities desiredCapabilities = DesiredCapabilities.internetExplorer();
		setCapabilities(desiredCapabilities, stringCapabilities);
		
		return new InternetExplorerDriver(desiredCapabilities);
	}
	
	private void setCapabilities(DesiredCapabilities desiredCapabilities, String capabilities) {
		for (String capability : capabilities.split(";")) {
			if (capability.contains("=")) {
				desiredCapabilities.setCapability(capability.substring(0, capability.lastIndexOf("=")), capability.substring(capability.lastIndexOf("=") + 1));
			}
		}
	}

	public void close(WebDriver driver) {
		driver.close();
	}

	public void get(WebDriver driver, String string) {
		driver.get(string);
	}

	public String getCurrentUrl(WebDriver driver) {
		return driver.getCurrentUrl();
	}

	public String getPageSource(WebDriver driver) {
		return driver.getPageSource();
	}

	public String getTitle(WebDriver driver) {
		return driver.getTitle();
	}

	public String getWindowHandle(WebDriver driver) {
		return driver.getWindowHandle();
	}

	public List<String> getWindowHandles(WebDriver driver) {
		return new ArrayList<>(driver.getWindowHandles());
	}

	public void quit(WebDriver driver) {
		driver.quit();
	}
}
