package com.mangyBear.Pepe.action.web;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ManageLogsGroup {

    public LogEntries get(WebDriver driver, String string) {
        return driver.manage().logs().get(string);
    }

    public List<String> getAvailableLogTypes(WebDriver driver) {
        return new ArrayList<String>(driver.manage().logs().getAvailableLogTypes());
    }
}
