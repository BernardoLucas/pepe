package com.mangyBear.Pepe.action.registry;

import com.mangyBear.Pepe.service.MyConstants;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class RegistryGroup {
    
    private final AutoItX autoItX;

    public RegistryGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }
    
    public boolean deleteKey(String keyname) {
        return autoItX.regDeleteKey(keyname) == 1;
    }
    
    public boolean deleteVal(String keyname) {
        return autoItX.regDeleteVal(keyname) == 1;
    }
    
    public String enumKey(String keyname, String instance) {
        return autoItX.regEnumKey(keyname, MyConstants.parseInt(instance));
    }
    
    public String enumVal(String keyname, String instance) {
        return autoItX.regEnumVal(keyname, MyConstants.parseInt(instance));
    }
    
    public String read(String keyname, String valueName) {
        return autoItX.regRead(keyname, valueName);
    }
    
    public boolean write(String keyname, String valueName, String type, String value) {
        return autoItX.regWrite(keyname, valueName, type, value);
    }
}
