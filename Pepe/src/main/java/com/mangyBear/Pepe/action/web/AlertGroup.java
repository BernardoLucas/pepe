package com.mangyBear.Pepe.action.web;

import org.openqa.selenium.Alert;
import org.openqa.selenium.security.Credentials;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class AlertGroup {

    public void accept(Alert alert) {
        alert.accept();
    }

    public void authenticateUsing(Alert alert, Credentials credentials) {
        alert.authenticateUsing(credentials);
    }

    public void dismiss(Alert alert) {
        alert.dismiss();
    }

    public String getText(Alert alert) {
        return alert.getText();
    }

    public void sendKeys(Alert alert, String string) {
        alert.sendKeys(string);
    }
}
