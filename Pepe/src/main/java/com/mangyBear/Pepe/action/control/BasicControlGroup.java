package com.mangyBear.Pepe.action.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Control;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class BasicControlGroup {

    private final AutoItX autoItX;

    public BasicControlGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public boolean click(Control control, String button, String clicks) {
        return autoItX.controlClick(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl(), button, MyConstants.parseInt(clicks));
    }

    public boolean disable(Control control) {
        return autoItX.controlDisable(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl());
    }

    public boolean enable(Control control) {
        return autoItX.controlEnable(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl());
    }

    public boolean focus(Control control) {
        return autoItX.controlFocus(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl());
    }

    public String getFocus(Control control) {
        return autoItX.controlGetFocus(MyConstants.getValidTitle(control.getWindow()));
    }

    public String getHandle(Control control) {
        return autoItX.controlGetHandle(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl());//usando como finder
    }

    public String getText(Control control) {
        return autoItX.controlGetText(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl());
    }

    public List<String> getPosition(Control control) {
    	String validTitle = MyConstants.getValidTitle(control.getWindow());
    	return new ArrayList<String>(Arrays.asList(String.valueOf(autoItX.controlGetPosX(validTitle, control.getWindow().getText(), control.getControl())),
    			String.valueOf(autoItX.controlGetPosY(validTitle, control.getWindow().getText(), control.getControl()))));
    }

    public List<String> getSize(Control control) {
    	String validTitle = MyConstants.getValidTitle(control.getWindow());
        return new ArrayList<String>(Arrays.asList(String.valueOf(autoItX.controlGetPosWidth(validTitle, control.getWindow().getText(), control.getControl())),
        		String.valueOf(autoItX.controlGetPosHeight(validTitle, control.getWindow().getText(), control.getControl()))));
    }

    public boolean hide(Control control) {
        return autoItX.controlHide(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl());
    }

    public boolean move(Control control, String x, String y) {
        return autoItX.controlMove(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl(), MyConstants.parseInt(x), MyConstants.parseInt(y));
    }

    public boolean resize(Control control, String width, String height) {
    	List<String> point = getPosition(control);
        return autoItX.controlMove(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl(), MyConstants.parseInt(point.get(0)), MyConstants.parseInt(point.get(1)), MyConstants.parseInt(width), MyConstants.parseInt(height));
    }

    public boolean send(Control control, String string, String isRaw) {
        return autoItX.controlSend(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl(), string, MyConstants.getBoolean(isRaw));
    }

    public boolean setText(Control control, String string) {
        return autoItX.ControlSetText(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl(), string);
    }

    public boolean show(Control control) {
        return autoItX.controlShow(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getControl());
    }
}
