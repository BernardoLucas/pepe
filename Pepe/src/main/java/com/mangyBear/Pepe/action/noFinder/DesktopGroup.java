package com.mangyBear.Pepe.action.noFinder;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.imageio.ImageIO;

import com.mangyBear.Pepe.domain.types.desktopParams.DesktopType;
import com.mangyBear.Pepe.service.MyConstants;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class DesktopGroup {

	private final AutoItX autoItX;

	public DesktopGroup() {
		(new MyConstants()).setJACOB_DLL_PATH();
		this.autoItX = new AutoItX();
	}

	public void blockInput(String disableInput) {
		autoItX.blockInput(MyConstants.getBoolean(disableInput));
	}

	public boolean cdTray(String drive, String status) {
		return autoItX.cdTray(drive, status);
	}

	public String clipGet() {
		return autoItX.clipGet();
	}

	public void clipPut(String value) {
		autoItX.clipPut(value);
	}

	public boolean driveMapAdd(String device, String remote, String flags, String username, String password) {
		return autoItX.driveMapAdd(device, remote, DesktopType.getDesktopTypeValueByName(flags), username, password);
	}

	public boolean driveMapDelete(String device) {
		return autoItX.driveMapDelete(device);
	}

	public String driveMapGet(String device) {
		return autoItX.driveMapGet(device);
	}

	public int getError() {
		return autoItX.getError();
	}

	public String getVersion() {
		return autoItX.getVersion();
	}

	public String getDate(String format) {
		return new SimpleDateFormat(format).format(new Date());
	}

	public boolean isAdmin() {
		return autoItX.isAdmin();
	}

	public BufferedImage printScreen(String x, String y, String width, String height) throws AWTException {
		Rectangle screenRegion = MyConstants.parseRectangle(Arrays.asList(x, y, width, height));
		if (screenRegion == null || screenRegion.isEmpty()) {
			screenRegion = new Rectangle(MyConstants.getScreenSize());
		}
		
		BufferedImage screenImage = (new Robot()).createScreenCapture(screenRegion);
		if (MyConstants.getPrntScrExportPath() != null && !MyConstants.getPrntScrExportPath().isEmpty()) {
			try {
				File screenPath = new File(MyConstants.getPrntScrExportPath(), MyConstants.getExecutingFlow().getNmFlow() + " - " + new SimpleDateFormat("ddMMyy_hhmmss").format(new Date()) + ".jpg");
				ImageIO.write(screenImage, "JPG", screenPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return screenImage;
	}

	public void send(String keys, String isRaw) {
		autoItX.send(keys, MyConstants.getBoolean(isRaw));
	}

	public void setOption(String option, String param) {
		autoItX.autoItSetOption(option, param);
		autoItX.setOption(option, param);
	}

	public boolean shutDown(String code) {
		return autoItX.shutdown(DesktopType.getDesktopTypeValueByName(code));
	}

	public void sleep(String delay) {
		autoItX.sleep(MyConstants.parseInt(delay));
	}

	public String statusbarGetText(String title, String text, String part) {
		// autoItX.StatusbarGetText(title, text);
		return autoItX.statusbarGetText(title, text, MyConstants.parseInt(part));
	}

	public void toolTip(String text, String x, String y) {
		int xI = MyConstants.parseInt(x);
		int yI = MyConstants.parseInt(y);
		if (xI == -1 || yI == -1) {
			autoItX.toolTip(text);
		} else {
			autoItX.toolTip(text, xI, yI);
		}
	}
}
