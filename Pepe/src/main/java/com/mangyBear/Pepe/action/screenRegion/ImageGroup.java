package com.mangyBear.Pepe.action.screenRegion;

import java.awt.Point;

import com.mangyBear.Pepe.service.MyConstants;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ImageGroup {

    private final AutoItX autoItX;

    public ImageGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public void click(Point point, String button, String clicks) {
        autoItX.mouseClick(button, point.x, point.y, MyConstants.parseInt(clicks), MyConstants.getMouseDelay());
    }

    public void clickDrag(Point pointOne, Point pointTwo, String button) {
        autoItX.mouseClickDrag(button, pointOne.x, pointOne.y, pointTwo.x, pointTwo.y, MyConstants.getMouseDelay());
    }

    public String getText(Point point) {
        click(point, "primary", "2");
        autoItX.send("^C", false);
        return autoItX.clipGet();
    }
    
    public boolean move(Point point) {
        return autoItX.mouseMove(point.x, point.y, MyConstants.getMouseDelay());
    }

    public void send(Point point, String text, String isRaw) {
        click(point, "primary", "1");
        autoItX.send(text, MyConstants.getBoolean(isRaw));
    }
}
