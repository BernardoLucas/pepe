package com.mangyBear.Pepe.action.noFinder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mangyBear.Pepe.service.MyConstants;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class MouseGroup {

    private final AutoItX autoItX;

    public MouseGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public void down(String button) {
        autoItX.mouseDown(button);
    }

    public int getCursor() {
        return autoItX.mouseGetCursor();
    }

    public List<String> getPosition() {
        return new ArrayList<String>(Arrays.asList(String.valueOf(autoItX.mouseGetPosX()), String.valueOf(autoItX.mouseGetPosY())));
    }

    public void up(String button) {
        autoItX.mouseUp(button);
    }

    public void wheel(String direction, String times) {
        autoItX.mouseWheel(direction, MyConstants.parseInt(times));
    }
}