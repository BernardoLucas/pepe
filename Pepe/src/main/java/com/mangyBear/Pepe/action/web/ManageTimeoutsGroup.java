package com.mangyBear.Pepe.action.web;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;

import com.mangyBear.Pepe.service.MyConstants;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ManageTimeoutsGroup {

    public Timeouts implicitlyWait(WebDriver driver, String time) {
        return driver.manage().timeouts().implicitlyWait(MyConstants.parseLong(time), TimeUnit.SECONDS);
    }
    
    public Timeouts pageLoadTimeout(WebDriver driver, String time) {
        return driver.manage().timeouts().pageLoadTimeout(MyConstants.parseLong(time), TimeUnit.SECONDS);
    }
    
    public Timeouts setScriptTimeout(WebDriver driver, String time) {
        return driver.manage().timeouts().setScriptTimeout(MyConstants.parseLong(time), TimeUnit.SECONDS);
    }
}
