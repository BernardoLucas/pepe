package com.mangyBear.Pepe.action.control;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Control;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class CommandGroup {

    private final AutoItX autoItX;

    public CommandGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public void currentTab(Control control) {
        autoItX.controlCommandCurrentTab(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public void editPaste(Control control, String string) {
        autoItX.controlCommandEditPaste(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), string); // insere texto onde estiver o marcador |
    }

    public int getCurrentCol(Control control) {
        return autoItX.controlCommandGetCurrentCol(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public int getCurrentLine(Control control) {
        return autoItX.controlCommandGetCurrentLine(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public int getLineCount(Control control) {
        return autoItX.controlCommandGetLineCount(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public String getSelected(Control control, String charLength) {
        return autoItX.controlCommandGetSelected(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), MyConstants.parseInt(charLength));
    }

    public boolean isEnabled(Control control) {
        return autoItX.controlCommandIsEnabled(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public boolean isVisible(Control control) {
        return autoItX.controlCommandIsVisible(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public void tabLeft(Control control) {
        autoItX.controlCommandTabLeft(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public void tabRight(Control control) {
        autoItX.controlCommandTabRight(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }
}
