package com.mangyBear.Pepe.action.web;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class ManageSwitchToGroup {

    public WebElement activeElement(WebDriver driver) {
        return driver.switchTo().activeElement();
    }

    public Alert alert(WebDriver driver) {
        return driver.switchTo().alert();
    }

    public WebDriver defaultContent(WebDriver driver) {
        return driver.switchTo().defaultContent();
    }

    public WebDriver frame(WebDriver driver, Object frame) {
        WebDriver webDriverToReturn = null;
        if (frame instanceof String) {
            webDriverToReturn = driver.switchTo().frame((String) frame);
        }
        if (frame instanceof WebElement) {
            webDriverToReturn = driver.switchTo().frame((WebElement) frame);
        }
        if (frame instanceof Integer) {
            webDriverToReturn = driver.switchTo().frame((Integer) frame);
        }
        return webDriverToReturn;
    }

    public WebDriver parentFrame(WebDriver driver) {
        return driver.switchTo().parentFrame();
    }

    public WebDriver window(WebDriver driver, String string) {
        return driver.switchTo().window(string);
    }
}
