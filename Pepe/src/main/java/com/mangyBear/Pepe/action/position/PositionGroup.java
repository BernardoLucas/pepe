package com.mangyBear.Pepe.action.position;

import static com.mangyBear.Pepe.service.MyConstants.stopWaitingColor;

import java.awt.Point;
import java.util.List;

import javax.swing.SwingUtilities;

import com.mangyBear.Pepe.service.MyConstants;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class PositionGroup {

    private final AutoItX autoItX;

    public PositionGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public void click(List<String> listPoint, String button, String clicks) {
    	Point point = MyConstants.parsePoint(listPoint);
        autoItX.mouseClick(button, point.x, point.y, MyConstants.parseInt(clicks), MyConstants.getMouseDelay());
    }

    public void clickDrag(List<String> listPointOne, List<String> listPointTwo, String button) {
    	Point pointOne = MyConstants.parsePoint(listPointOne);
    	Point pointTwo = MyConstants.parsePoint(listPointTwo);
        autoItX.mouseClickDrag(button, pointOne.x, pointOne.y, pointTwo.x, pointTwo.y, MyConstants.getMouseDelay());
    }
    
    public float getColor(List<String> listPoint) {
    	Point point = MyConstants.parsePoint(listPoint);
        return autoItX.pixelGetColor(point.x, point.y);
    }
    
    public boolean move(List<String> listPoint) {
    	Point point = MyConstants.parsePoint(listPoint);
        return autoItX.mouseMove(point.x, point.y, MyConstants.getMouseDelay());
    }

    public boolean waitColor(List<String> listPoint, String color, String seconds) {
    	Point point = MyConstants.parsePoint(listPoint);
        mouseEffect(point);
        int secondsI = MyConstants.parseInt(seconds);

        long waitingTime = System.currentTimeMillis() + (secondsI * 1000);
        while (secondsI == 0 || System.currentTimeMillis() < waitingTime) {
            if (color.equals(Float.toHexString(autoItX.pixelGetColor(point.x, point.y)))) {
            	stopWaitingColor = true;
                return true;
            }
            if (stopWaitingColor) {
                stopWaitingColor = false;
                return false;
            }
        }
        return false;
    }

    private void mouseEffect(final Point point) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                while (!stopWaitingColor) {
                    autoItX.mouseMove(point.x, point.y);
                    autoItX.mouseMove(point.x + 5, point.y);
                    autoItX.mouseMove(point.x + 5, point.y + 5);
                    autoItX.mouseMove(point.x, point.y + 5);
                }
            }
        });
    }
}
