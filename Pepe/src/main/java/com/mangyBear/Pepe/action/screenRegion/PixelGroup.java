package com.mangyBear.Pepe.action.screenRegion;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import com.mangyBear.Pepe.service.MyConstants;

import autoitx4java.AutoItX;

/**
 *
 * @author Lucas Silva Bernardo
 */
public class PixelGroup {

    private final AutoItX autoItX;

    public PixelGroup() {
    	(new MyConstants()).setJACOB_DLL_PATH();
        this.autoItX = new AutoItX();
    }

    public double checksum(List<String> listRectangle, String step) {
    	Rectangle screenRegion = MyConstants.parseRectangle(listRectangle);
        return autoItX.pixelChecksum(screenRegion.x, screenRegion.y, screenRegion.width, screenRegion.height, MyConstants.parseInt(step));
    }

    public List<String> search(List<String> listRectangle, String color, String shadeVariation, String step) {
    	List<String> stringList = new ArrayList<>();
    	Rectangle screenRegion = MyConstants.parseRectangle(listRectangle);
    	long[] longList = autoItX.pixelSearch(screenRegion.x, screenRegion.y, screenRegion.width, screenRegion.height, MyConstants.parseInt(color), MyConstants.parseInt(shadeVariation), MyConstants.parseInt(step));
        for (long l : longList) {
        	stringList.add(String.valueOf(l));
		}
    	return stringList;
    }
}