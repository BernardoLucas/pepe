package com.mangyBear.Pepe.action.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mangyBear.Pepe.service.MyConstants;
import com.mangyBear.Pepe.service.Finder.Control;

/**
 *
 * @author Lucas Silva Bernado
 */
public class ListViewGroup extends ComboBoxListDuplicates {

    public int findItem(Control control, String string, String subItem) {
        return autoItX.controlListViewFindItem(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), string, subItem);
    }
    
    public int getItemCount(Control control) {
        return autoItX.controlListViewGetItemCount(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }
    
    public String getSelected(Control control) {
        return autoItX.controlListViewGetSelected(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public List<String> getSelectedArray(Control control) {
        return new ArrayList<String>(Arrays.asList(autoItX.controlListViewGetSelectedArray(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle())));
    }

    public int getSelectedCount(Control control) {
        return autoItX.controlListViewGetSelectedCount(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public int getSubItemCount(Control control) {
        return autoItX.controlListViewGetSubItemCount(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public String getText(Control control, String item, String subItem) {
        return autoItX.controlListViewGetText(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item, subItem);
    }

    public boolean isSelected(Control control, String item) {
        return autoItX.controlListViewIsSelected(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), item);
    }

    public void select(Control control, String from, String to) {
        autoItX.controlListViewSelect(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), from, to);
    }

    public void selectAll(Control control, String from) {
        autoItX.controlListViewSelectAll(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), from);
    }

    public void selectClear(Control control) {
        autoItX.controlListViewSelectClear(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public void selectInvert(Control control) {
        autoItX.controlListViewSelectInvert(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle());
    }

    public void selectViewChange(Control control, String view) {
        autoItX.controlListViewSelectViewChange(MyConstants.getValidTitle(control.getWindow()), control.getWindow().getText(), control.getHandle(), view);
    }
}
